class ClsApiResponse {
  String StatusCode;
  String Message;

  ClsApiResponse({this.StatusCode, this.Message});

  factory ClsApiResponse.fromJson(Map<String, dynamic> obj) {
    return ClsApiResponse(
      StatusCode: obj["ID"].toString(),
      Message: obj["Message"].toString()
    );
  }
}