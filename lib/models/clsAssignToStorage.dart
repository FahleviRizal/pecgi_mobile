class ClsAssignToStorage {
  String fromLocation;
  String warehouseCode;
  String locationCarryCode;
  String locationCarryDescription;
  String barcode;
  String itemCode;
  String itemName;
  String lotNo;
  String qty;
  String id;
  String description;

  ClsAssignToStorage({
    this.fromLocation,
    this.warehouseCode,
    this.locationCarryCode,
    this.locationCarryDescription,
    this.barcode,
    this.itemCode,
    this.itemName,
    this.lotNo,
    this.qty,
    this.id,
    this.description,
  });

  // Get Location / Carry
  factory ClsAssignToStorage.ddlLocationCarry(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsAssignToStorage(
          locationCarryCode: object['LocationCarryCode'] as String,
          locationCarryDescription:
              object['LocationCarryDescription'] as String);
    }
  }

  factory ClsAssignToStorage.getLocationCarryDesc(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsAssignToStorage(
          locationCarryCode: object['LocationCarryCode'] as String,
          locationCarryDescription:
              object['LocationCarryDescription'] as String);
    }
  }

  // factory ClsAssignToStorage.getData(Map<String, dynamic> object) {
  //   if (object == null) {
  //     return null;
  //   } else {
  //     return ClsAssignToStorage(
  //         Barcode: object['Barcode'].toString(),
  //         Item: object['Item'].toString(),
  //         Lot: object['Lot'].toString(),
  //         Qty: object['Qty'].toString());
  //   }
  // }

  factory ClsAssignToStorage.scanBarcode(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsAssignToStorage(
          fromLocation: object['FromLocationCarry'] as String,
          barcode: object['Barcode'] as String,
          itemCode: object['ItemCode'] as String,
          itemName: object['ItemName'] as String,
          lotNo: object['Lot'] as String,
          qty: object['Qty'] as String,
          id: object['ID'] as String,
          description: object['Message'] as String);
    }
  }

  factory ClsAssignToStorage.submit(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsAssignToStorage(
          id: object['ID'].toString(),
          description: object['Message'].toString());
    }
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'FromLocationCarry': fromLocation.trim(),
      'LocationCarryCode': locationCarryCode.trim(),
      'Barcode': barcode.trim(),
      'ItemCode': itemCode.trim(),
      'Lot': lotNo.trim(),
      'Qty': qty,
      'WarehouseCode': warehouseCode.trim()
    };

    return map;
  }
}
