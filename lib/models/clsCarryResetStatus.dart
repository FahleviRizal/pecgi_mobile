import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:panasonic/utilities/constantsApi.dart';
import 'dart:async';

class ClsCarryResetStatus {
  String carryNo;
  String barcodeNo;
  String type;
  String sublotNo;
  String lotNo;
  String qty;
  String unitCode;
  String unitName;
  String expDate;
  String registerUser;
  String lastUpdate;

  String id;
  String description;

  ClsCarryResetStatus({
    this.carryNo,
    this.barcodeNo,
    this.type,
    this.sublotNo,
    this.lotNo,
    this.qty,
    this.unitCode,
    this.unitName,
    this.expDate,
    this.registerUser,
    this.lastUpdate,
    this.id,
    this.description,
  });

  // Get Location / Carry
  factory ClsCarryResetStatus.getDataHeader(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsCarryResetStatus(
        carryNo: object['CarryNo'] as String,
        barcodeNo: object['BarcodeNo'] as String,
        type: object['Type'] as String,
        sublotNo: object['SublotNo'] as String,
        lotNo: object['LotNo'] as String,
        qty: object['Qty'] as String,
        unitCode: object['UnitCode'] as String,
        unitName: object['UnitName'] as String,
        expDate: object['ExpDate'] as String,
        registerUser: object['RegisterUser'] as String,
        lastUpdate: object['LastUpdate'] as String,
      );
    }
  }

  factory ClsCarryResetStatus.submitData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsCarryResetStatus(
          id: object['ID'] as String,
          description: object['Description'] as String);
    }
  }

  static Future getData(String carryNo) async {
    String apiURL = URLAPI + 'CarryResetStatus_getdata?carryno=' + carryNo;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsCarryResetStatus> clsCarryResetStatusData =
        jsonObject.map((e) => ClsCarryResetStatus.getDataHeader(e)).toList();
    return clsCarryResetStatusData;
  }

  static Future submit(String carryNo, String userid) async {
    String apiURL = URLAPI +
        'CarryResetStatus_submit?carryno=' +
        carryNo +
        '&userid=' +
        userid;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsCarryResetStatus> clsCarryResetStatusData =
        jsonObject.map((e) => ClsCarryResetStatus.submitData(e)).toList();
    return clsCarryResetStatusData;
  }
}
