import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:panasonic/utilities/constantsApi.dart';
import 'dart:async';

class ClsDeliveryScan {
  String invoiceNo;
  String customerCode;
  String customerName;
  String poNo;
  String no;
  String type;
  String typeName;
  String planQty;
  String scanQty;
  String palletNo;
  String barcodeNo;
  String lotNo;

  String id;
  String description;

  ClsDeliveryScan({
    this.invoiceNo,
    this.customerCode,
    this.customerName,
    this.poNo,
    this.no,
    this.type,
    this.typeName,
    this.planQty,
    this.scanQty,
    this.palletNo,
    this.barcodeNo,
    this.lotNo,
    this.id,
    this.description,
  });

  // Get Location / Carry
  factory ClsDeliveryScan.getInvoiceNoDdl(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsDeliveryScan(
          invoiceNo: object['InvoiceNo'] as String,
          customerCode: object['CustomerCode'] as String,
          customerName: object['CustomerName'] as String,
          poNo: object['PONo'] as String);
    }
  }

  factory ClsDeliveryScan.getDataHeader(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      if (object['Type'] != null) {
        return ClsDeliveryScan(
          no: object['No'] as String,
          type: object['Type'] as String,
          typeName: object['TypeName'] as String,
          planQty: object['PlanQty'] as String,
          scanQty: object['ScanQty'] as String,
        );
      }
    }
  }

  factory ClsDeliveryScan.getDetail(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsDeliveryScan(
        barcodeNo: object['BarcodeNo'] as String,
        palletNo: object['PalletNo'] as String,
        type: object['Type'] as String,
        typeName: object['TypeName'] as String,
        lotNo: object['LotNo'] as String,
        scanQty: object['ScanQty'] as String,
        id: object['ID'] as String,
        description: object['Description'] as String,
      );
    }
  }

  factory ClsDeliveryScan.getList(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsDeliveryScan(
        palletNo: object['PalletNo'] as String,
        barcodeNo: object['BarcodeNo'] as String,
        lotNo: object['LotNo'] as String,
        scanQty: object['ScanQty'] as String,
      );
    }
  }

  factory ClsDeliveryScan.submitData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsDeliveryScan(
          id: object['ID'] as String,
          description: object['Description'] as String);
    }
  }

  static Future getInvoiceNo() async {
    String apiURL = URLAPI + 'deliveryscan_getinvoiceno/';

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsDeliveryScan> clsDeliveryScanData =
        jsonObject.map((e) => ClsDeliveryScan.getInvoiceNoDdl(e)).toList();
    return clsDeliveryScanData;
  }

  static Future getData(String invoiceNo) async {
    String apiURL = URLAPI + 'deliveryscan_getdata?invoiceno=' + invoiceNo;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsDeliveryScan> clsDeliveryScanData =
        jsonObject.map((e) => ClsDeliveryScan.getDataHeader(e)).toList();

    if (clsDeliveryScanData.length > 0) {
      for (var i = 0; i < clsDeliveryScanData.length; i++) {
        if (clsDeliveryScanData[i] != null) {
          clsDeliveryScanData[i].no = (i + 1).toString();
        }
      }
    }

    return clsDeliveryScanData;
  }

  static Future getDataDetail(String barcodeNo, String invoiceNo) async {
    String apiURL = URLAPI +
        'deliveryscan_getdatadetail?barcodeno=' +
        barcodeNo +
        '&invoiceno=' +
        invoiceNo;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsDeliveryScan> clsDeliveryScanData =
        jsonObject.map((e) => ClsDeliveryScan.getDetail(e)).toList();
    return clsDeliveryScanData;
  }

  static Future getDataList(String invoiceNo, String type) async {
    String apiURL = URLAPI +
        'deliveryscan_getdatalist?invoiceno=' +
        invoiceNo +
        '&type=' +
        type;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsDeliveryScan> clsDeliveryScanData =
        jsonObject.map((e) => ClsDeliveryScan.getList(e)).toList();
    return clsDeliveryScanData;
  }

  static Future submit(
      String invoiceNo, String barcodeNo, String userID) async {
    String apiURL = URLAPI +
        'deliveryscan_submit?invoiceno=' +
        invoiceNo +
        '&barcodeno=' +
        barcodeNo +
        '&userid=' +
        userID;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsDeliveryScan> clsDeliveryScanData =
        jsonObject.map((e) => ClsDeliveryScan.submitData(e)).toList();
    return clsDeliveryScanData;
  }
}
