class ClsMaterialConsumption {
  String linecode;
  String linename;
  String instNo;
  String typedesc;
  String labelBarcode;
  String materialCode;
  String materialDesc;
  String lotNo;
  String qty;
  String unitCode;
  String unitName;
  String expDate;
  String supplierCode;
  String supplierName;

  String id;
  String description;

  ClsMaterialConsumption({
    this.linecode,
    this.linename,
    this.instNo,
    this.typedesc,
    this.labelBarcode,
    this.materialCode,
    this.materialDesc,
    this.lotNo,
    this.qty,
    this.unitCode,
    this.unitName,
    this.expDate,
    this.supplierCode,
    this.supplierName,
    this.id,
    this.description,
  });

  // Get Location / Carry
  factory ClsMaterialConsumption.combotInstructionNo(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialConsumption(
        instNo: object['InstNo'] as String,
        typedesc: object['TypeDesc'] as String,
      );
    }
  }

  factory ClsMaterialConsumption.scanData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialConsumption(
          instNo: object['InstNo'] as String,
          labelBarcode: object['LabelBarcode'] as String,
          materialCode: object['MaterialCode'] as String,
          materialDesc: object['MaterialDesc'] as String,
          lotNo: object['LotNo'] as String,
          qty: object['Qty'] as String,
          unitCode: object['UnitCode'] as String,
          unitName: object['UnitName'] as String,
          expDate: object['ExpDate'] as String,
          supplierCode: object['SupplierCode'] as String,
          supplierName: object['SupplierName'] as String,
          id: object['ID'] as String,
          description: object['Message'] as String);
    }
  }

  factory ClsMaterialConsumption.getDataList(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialConsumption(
          instNo: object['InstNo'] as String,
          labelBarcode: object['LabelBarcode'] as String,
          materialCode: object['MaterialCode'] as String,
          materialDesc: object['MaterialDesc'] as String,
          lotNo: object['LotNo'] as String,
          qty: object['Qty'] as String,
          unitCode: object['UnitCode'] as String,
          unitName: object['UnitName'] as String,
          expDate: object['ExpDate'] as String,
          supplierCode: object['SupplierCode'] as String,
          supplierName: object['SupplierName'] as String);
    }
  }

  factory ClsMaterialConsumption.submitData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialConsumption(
          id: object['ID'] as String, description: object['Message'] as String);
    }
  }

  factory ClsMaterialConsumption.comboLine(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialConsumption(
        linecode: object['LineCode'] as String,
        linename: object['LineName'] as String,
      );
    }
  }
}
