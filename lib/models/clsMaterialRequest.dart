import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:panasonic/utilities/constantsApi.dart';
import 'dart:async';

class ClsMaterialRequest {
  ClsMaterialRequest({
    this.instNo,
    this.labelBarcode,
    this.materialCode,
    this.materialDesc,
    this.lotNo,
    this.qty,
    this.resultQty,
    this.unitCode,
    this.unitName,
    this.expDate,
    this.supplierCode,
    this.supplierName,
    this.factoryCode,
    this.factoryName,
    this.processCode,
    this.processName,
    this.lineCode,
    this.lineName,
    this.productionDate,
    this.shiftCode,
    this.shiftName,
    this.type,
    this.productionPlanQty,
    this.productionResult,
    this.id,
    this.description,
  });

  factory ClsMaterialRequest.getData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialRequest(
          instNo: object['InstNo'].toString(),
          labelBarcode: object['LabelBarcode'].toString(),
          materialCode: object['MaterialCode'].toString(),
          materialDesc: object['MaterialDesc'].toString(),
          lotNo: object['LotNo'].toString(),
          qty: object['Qty'].toString(),
          resultQty: object['ResultQty'].toString(),
          unitCode: object['UnitCode'].toString(),
          unitName: object['UnitName'].toString(),
          expDate: object['ExpDate'].toString(),
          supplierCode: object['SupplierCode'].toString(),
          supplierName: object['SupplierName'].toString());
    }
  }

  factory ClsMaterialRequest.getDataDetail(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialRequest(
          instNo: object['InstNo'].toString(),
          labelBarcode: object['LabelBarcode'].toString(),
          materialCode: object['MaterialCode'].toString(),
          materialDesc: object['MaterialDesc'].toString(),
          lotNo: object['LotNo'].toString(),
          qty: object['Qty'].toString(),
          unitCode: object['UnitCode'].toString(),
          unitName: object['UnitName'].toString(),
          expDate: object['ExpDate'].toString(),
          supplierCode: object['SupplierCode'].toString(),
          supplierName: object['SupplierName'].toString());
    }
  }

  factory ClsMaterialRequest.getDataList(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialRequest(
        instNo: object['InstNo'].toString(),
        productionDate: object['ProductionDate'].toString(),
        type: object['Type'].toString(),
        materialCode: object['MaterialCode'].toString(),
        materialDesc: object['MaterialDesc'].toString(),
        lotNo: object['LotNo'].toString(),
        qty: object['Qty'].toString(),
      );
    }
  }

  factory ClsMaterialRequest.getDataProductionInformation(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialRequest(
          instNo: object['InstNo'].toString(),
          factoryCode: object['FactoryCode'].toString(),
          factoryName: object['FactoryName'].toString(),
          processCode: object['ProcessCode'].toString(),
          processName: object['ProcessName'].toString(),
          lineCode: object['LineCode'].toString(),
          lineName: object['LineName'].toString(),
          productionDate: object['ProductionDate'].toString(),
          shiftCode: object['ShiftCode'].toString(),
          shiftName: object['ShiftName'].toString(),
          type: object['Type'].toString(),
          productionPlanQty: object['ProductionPlanQty'].toString(),
          productionResult: object['ProductionResult'].toString());
    }
  }

  // Get Location / Carry
  factory ClsMaterialRequest.getInstNo(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialRequest(
        instNo: object['InstNo'].toString(),
      );
    }
  }

  factory ClsMaterialRequest.submit(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialRequest(
          id: object['ID'].toString(),
          description: object['Description'].toString());
    }
  }

  String description;
  String expDate;
  String factoryCode;
  String factoryName;
  String id;
  String instNo;
  String labelBarcode;
  String lineCode;
  String lineName;
  String lotNo;
  String materialCode;
  String materialDesc;
  String processCode;
  String processName;
  String productionDate;
  String productionPlanQty;
  String productionResult;
  String qty;
  String resultQty;
  String shiftCode;
  String shiftName;
  String supplierCode;
  String supplierName;
  String type;
  String unitCode;
  String unitName;

  static Future getInstructionNo() async {
    String apiURL = URLAPI + 'materialrequest_getinstructionno/';

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialRequest> clsMaterialRequestData =
        jsonObject.map((e) => ClsMaterialRequest.getInstNo(e)).toList();
    return clsMaterialRequestData;
  }

  static Future getDataHeader(String instructionNo) async {
    String apiURL =
        URLAPI + 'materialrequest_getdata?instructionno=' + instructionNo;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialRequest> clsMaterialRequestData =
        jsonObject.map((e) => ClsMaterialRequest.getData(e)).toList();
    return clsMaterialRequestData;
  }

  static Future getDetail(String instructionNo, String labelBarcode) async {
    String apiURL = URLAPI +
        'materialrequest_getdatadetail?instructionno=' +
        instructionNo +
        '&labelbarcode=' +
        labelBarcode;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialRequest> clsMaterialRequestData =
        jsonObject.map((e) => ClsMaterialRequest.getDataDetail(e)).toList();
    return clsMaterialRequestData;
  }

  static Future submitData(
      String instructionNo, String labelBarcode, String qty) async {
    String apiURL = URLAPI +
        'materialrequest_submit?instructionno=' +
        instructionNo +
        '&labelbarcode=' +
        labelBarcode +
        '&qty=' +
        qty;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialRequest> clsMaterialRequestData =
        jsonObject.map((e) => ClsMaterialRequest.submit(e)).toList();
    return clsMaterialRequestData;
  }

  static Future getDataProductionInfo(String instructionNo) async {
    String apiURL = URLAPI +
        'materialrequest_getdataproductioninformation?instructionno=' +
        instructionNo;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialRequest> clsMaterialRequestData = jsonObject
        .map((e) => ClsMaterialRequest.getDataProductionInformation(e))
        .toList();
    return clsMaterialRequestData;
  }

  static Future getListData(String instructionNo, String materialCode) async {
    String apiURL = URLAPI +
        'materialrequest_getdatalist?instructionno=' +
        instructionNo +
        '&materialcode=' +
        materialCode;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialRequest> clsMaterialRequestData =
        jsonObject.map((e) => ClsMaterialRequest.getDataList(e)).toList();
    return clsMaterialRequestData;
  }
}
