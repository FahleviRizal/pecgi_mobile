class ClsSupplyRequest {
  String supplyNo;
  String instNo;
  String typeDesc;
  String lineCode;
  String lineName;
  String materialCode;
  String materialDesc;
  String unitCode;
  String unitName;
  String barcode;
  String lot;
  String qty;
  String planQty;
  String resultQty;
  String warehouse;
  String location;
  String expDate;
  String supplierCode;
  String supplierName;
  String labelBarcode;
  String id;
  String description;
  String warehouselogin;

  ClsSupplyRequest(
      {this.supplyNo,
      this.instNo,
      this.typeDesc,
      this.lineCode,
      this.lineName,
      this.materialCode,
      this.materialDesc,
      this.unitCode,
      this.unitName,
      this.barcode,
      this.lot,
      this.qty,
      this.planQty,
      this.resultQty,
      this.warehouse,
      this.location,
      this.expDate,
      this.supplierCode,
      this.supplierName,
      this.labelBarcode,
      this.id,
      this.description,
      this.warehouselogin});

  factory ClsSupplyRequest.ddlLine(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSupplyRequest(
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
      );
    }
  }

  // Get Supply Request No
  factory ClsSupplyRequest.ddlSupplyNo(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSupplyRequest(
        supplyNo: object['SupplyNo'] as String,
        instNo: object['InstNo'] as String,
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
        typeDesc: object['TypeDesc'] as String,
      );
    }
  }

  factory ClsSupplyRequest.getListDataH(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSupplyRequest(
          materialCode: object['MaterialCode'] as String,
          materialDesc: object['MaterialDesc'] as String,
          planQty: object['PlanQty'] as String,
          resultQty: object['ResultQty'] as String,
          location: object['Location'] as String);
    }
  }

  factory ClsSupplyRequest.getScan(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSupplyRequest(
          supplyNo: object['SupplyNo'] as String,
          materialCode: object['MaterialCode'] as String,
          materialDesc: object['MaterialDesc'] as String,
          unitCode: object['UnitCode'] as String,
          unitName: object['UnitName'] as String,
          lot: object['Lot'] as String,
          planQty: object['PlanQty'] as String,
          expDate: object['ExpDate'] as String,
          supplierCode: object['SupplierCode'] as String,
          supplierName: object['SupplierName'] as String,
          location: object['Location'] as String,
          id: object['ID'] as String,
          description: object['Description'] as String);
    }
  }

  factory ClsSupplyRequest.getDetail(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSupplyRequest(
          supplyNo: object['SupplyNo'] as String,
          instNo: object['InstNo'] as String,
          materialCode: object['MaterialCode'] as String,
          materialDesc: object['MaterialDesc'] as String,
          unitCode: object['UnitCode'] as String,
          unitName: object['UnitName'] as String,
          barcode: object['Barcode'] as String,
          lot: object['Lot'] as String,
          qty: object['Qty'] as String,
          location: object['Location'] as String);
    }
  }

  factory ClsSupplyRequest.getlistStock(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSupplyRequest(
          materialCode: object['MaterialCode'] as String,
          materialDesc: object['MaterialDesc'] as String,
          warehouse: object['Warehouse'] as String,
          location: object['Location'] as String,
          lot: object['Lot'] as String,
          qty: object['Qty'] as String);
    }
  }

  factory ClsSupplyRequest.submitData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSupplyRequest(
          id: object['ID'] as String,
          description: object['Description'] as String);
    }
  }
}
