import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:panasonic/utilities/constantsApi.dart';
import 'dart:async';

class ClsSyncLamp {
  String lampID;
  String carryNo;

  String id;
  String description;
  String lastUpdate;
  String timerMiliseconds;

  ClsSyncLamp({
    this.lampID,
    this.carryNo,
    this.id,
    this.description,
    this.lastUpdate,
    this.timerMiliseconds,
  });

  // Get Carry
  factory ClsSyncLamp.getDataCarryH(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSyncLamp(
          lampID: object['LampID'] as String,
          carryNo: object['CarryNo'] as String);
    }
  }

  // Ins Towerlamp History
  factory ClsSyncLamp.insTowerlampHistoryData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSyncLamp(
          id: object['ID'] as String,
          description: object['Description'] as String);
    }
  }

  // Update MS Carry
  factory ClsSyncLamp.updMSCarryData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSyncLamp(
          id: object['ID'] as String,
          description: object['Description'] as String,
          lastUpdate: object['LastUpdate'] as String);
    }
  }

  // Get Timer
  factory ClsSyncLamp.timerData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSyncLamp(
          timerMiliseconds: object['TimerMiliseconds'] as String);
    }
  }

  static Future getDataCarry(String ipAddress) async {
    String apiURL = URLAPI + 'synclamp_getdatacarry?ipaddress=' + ipAddress;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSyncLamp> clsSyncLampData =
        jsonObject.map((e) => ClsSyncLamp.getDataCarryH(e)).toList();
    return clsSyncLampData;
  }

  static Future insTowerlampHistory(
      String carryNo,
      String lampID,
      String ipAddress,
      String status,
      String remarks,
      String datetime,
      String user) async {
    String apiURL = URLAPI +
        'synclamp_inserttowerlamphistory?carryno=' +
        carryNo +
        '&lampid=' +
        lampID +
        '&ipaddress=' +
        ipAddress +
        '&status=' +
        status +
        '&remarks=' +
        remarks +
        '&datetime=' +
        datetime +
        '&user=' +
        user;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSyncLamp> clsSyncLampData =
        jsonObject.map((e) => ClsSyncLamp.insTowerlampHistoryData(e)).toList();
    return clsSyncLampData;
  }

  static Future updMSCarry(
      String func,
      String carryNo,
      String status,
      String batteryPercent,
      String batteryVolt,
      String mode,
      String activeDate,
      String user) async {
    String apiURL = URLAPI +
        'synclamp_updatestatusmscarry?func=' +
        func +
        '&carryno=' +
        carryNo +
        '&status=' +
        status +
        '&batterypercent=' +
        batteryPercent +
        '&batteryvolt=' +
        batteryVolt +
        '&mode=' +
        mode +
        '&activedate=' +
        activeDate +
        '&user=' +
        user;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSyncLamp> clsSyncLampData =
        jsonObject.map((e) => ClsSyncLamp.updMSCarryData(e)).toList();
    return clsSyncLampData;
  }

  static Future timer() async {
    String apiURL = URLAPI + 'synclamp_timer';

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSyncLamp> clsSyncLampData =
        jsonObject.map((e) => ClsSyncLamp.timerData(e)).toList();
    return clsSyncLampData;
  }
}
