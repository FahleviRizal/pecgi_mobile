import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:panasonic/utilities/constantsApi.dart';
import 'dart:async';

class ClsTowerLampInformation {
  String carryNo;
  String carryName;
  String factoryCode;
  String factoryName;
  String warehouseCode;
  String warehouseName;
  String locationCode;
  String locationName;
  String towerLampCode;
  String towerLampDesc;
  String channelNo;
  String battery;
  String mode;
  String lastUpdate;
  String statusLamp;
  String id;
  String description;

  ClsTowerLampInformation({
    this.carryNo,
    this.carryName,
    this.factoryCode,
    this.factoryName,
    this.warehouseCode,
    this.warehouseName,
    this.locationCode,
    this.locationName,
    this.towerLampCode,
    this.towerLampDesc,
    this.channelNo,
    this.battery,
    this.mode,
    this.lastUpdate,
    this.statusLamp,
    this.id,
    this.description,
  });

  factory ClsTowerLampInformation.getDataLamp(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsTowerLampInformation(
        carryNo: object['CarryNo'] as String,
        carryName: object['CarryName'] as String,
        factoryCode: object['FactoryCode'] as String,
        factoryName: object['FactoryName'] as String,
        warehouseCode: object['WarehouseCode'] as String,
        warehouseName: object['WarehouseName'] as String,
        locationCode: object['LocationCode'] as String,
        locationName: object['LocationName'] as String,
        towerLampCode: object['TowerLampCode'] as String,
        towerLampDesc: object['TowerLampDesc'] as String,
        channelNo: object['ChannelNo'] as String,
        battery: object['Battery'] as String,
        mode: object['Mode'] as String,
        statusLamp: object['StatusLamp'] as String,
        lastUpdate: object['LastUpdate'] as String,
      );
    }
  }

  static Future getData(String carryNo) async {
    String apiURL =
        URLAPI + 'towerlampinformation_getdatalist?carryno=' + carryNo;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsTowerLampInformation> clsTowerLampInformationData =
        jsonObject.map((e) => ClsTowerLampInformation.getDataLamp(e)).toList();
    return clsTowerLampInformationData;
  }
}
