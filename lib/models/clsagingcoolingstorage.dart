class ClsAgingCoolingStorage {
  String carryNo;
  String locationAddress1;
  String locationDestination;
  String locationAddress2;

  String lampid;
  String deepsleeptimehour;
  String statuslamp;

  String itemCode;
  String itemName;
  String lotNo;
  String qty;
  String unit;
  String sublotNo;
  String id;
  String description;

  ClsAgingCoolingStorage(
      {this.carryNo,
      this.locationAddress1,
      this.locationDestination,
      this.locationAddress2,
      this.deepsleeptimehour,
      this.statuslamp,
      this.lampid,
      this.itemCode,
      this.itemName,
      this.lotNo,
      this.sublotNo,
      this.qty,
      this.unit,
      this.id,
      this.description});

  factory ClsAgingCoolingStorage.getLocationAddress1(
      Map<String, dynamic> object) {
    return ClsAgingCoolingStorage(
        locationAddress1: object['LocationAddress'] as String,
        lampid: object['LampID'] as String,
        statuslamp: object['StatusLamp'] as String);
  }

  factory ClsAgingCoolingStorage.getLocationAddress2(
      Map<String, dynamic> object) {
    return ClsAgingCoolingStorage(
        locationAddress2: object['LocationAddress'] as String);
  }

  factory ClsAgingCoolingStorage.listHeader(Map<String, dynamic> object) {
    return ClsAgingCoolingStorage(
        itemCode: object['ItemCode'] as String,
        itemName: object['ItemName'] as String,
        lotNo: object['LotNo'] as String,
        qty: object['Qty'] as String,
        unit: object['UnitName'] as String);
  }

  factory ClsAgingCoolingStorage.listDetail(Map<String, dynamic> object) {
    return ClsAgingCoolingStorage(
        sublotNo: object['SublotNo'] as String,
        qty: object['Qty'] as String,
        unit: object['UnitName'] as String);
  }

  factory ClsAgingCoolingStorage.listSubmit(Map<String, dynamic> object) {
    return ClsAgingCoolingStorage(
        id: object['ID'] as String,
        description: object['Description'] as String);
  }

  factory ClsAgingCoolingStorage.listDeepSleep(Map<String, dynamic> object) {
    return ClsAgingCoolingStorage(
        deepsleeptimehour: object['DeepSleepTimeHour'] as String);
  }
}
