class ClsAssignToCarryByLot {
  String warehouseCode;
  String locationCode;
  String carryNo;
  String carryName;
  String barcode;
  String itemCode;
  String itemName;
  String lotNo;
  String qty;
  String expDate;
  String supplier;

  String id;
  String description;

  ClsAssignToCarryByLot({
    this.warehouseCode,
    this.locationCode,
    this.carryNo,
    this.carryName,
    this.barcode,
    this.itemCode,
    this.itemName,
    this.lotNo,
    this.qty,
    this.expDate,
    this.supplier,
    this.id,
    this.description,
  });

  factory ClsAssignToCarryByLot.getCarryDesc(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsAssignToCarryByLot(
          carryNo: object['CarryNo'] as String,
          carryName: object['CarryName'] as String);
    }
  }

  factory ClsAssignToCarryByLot.getDetailData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsAssignToCarryByLot(
        barcode: object['BarcodeNo'] as String,
        itemCode: object['ItemCode'] as String,
        itemName: object['ItemName'] as String,
        lotNo: object['Lot'] as String,
        qty: object['Qty'] as String,
        expDate: object['ExpiredDate'] as String,
        supplier: object['Supplier'] as String,
      );
    }
  }

  factory ClsAssignToCarryByLot.scanBarcode(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsAssignToCarryByLot(
          barcode: object['BarcodeNo'] as String,
          itemCode: object['ItemCode'] as String,
          itemName: object['ItemName'] as String,
          lotNo: object['Lot'] as String,
          qty: object['Qty'] as String,
          id: object['ID'] as String,
          description: object['Message'] as String);
    }
  }

  factory ClsAssignToCarryByLot.submit(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsAssignToCarryByLot(
          id: object['ID'].toString(),
          description: object['Message'].toString());
    }
  }
}
