class ClsBarcodeTracebility {
  String BarcodeNo;
  String ItemName;
  String ConsumptDate;
  String Qty;
  String QtyUsed;
  String QtyRemaining;

  ClsBarcodeTracebility(
      {this.BarcodeNo,
      this.ConsumptDate,
      this.Qty,
      this.QtyRemaining,
      this.QtyUsed});

  factory ClsBarcodeTracebility.fromJson(Map<String, dynamic> obj) {
    return ClsBarcodeTracebility(
      BarcodeNo: obj["BarcodeNo"].toString(),
      ConsumptDate: obj["ConsumptDate"].toString(),
      Qty: obj["Qty"].toString(),
      QtyUsed: obj["QtyUsed"].toString(),
      QtyRemaining: obj["QtyRemaining"].toString(),
    );
  } 
}
