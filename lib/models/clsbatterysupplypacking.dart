class ClsBatterySupplyPacking {
  String supplyNo;
  String instNo;
  String typeDesc;
  String itemCode;
  String itemName;
  String unitCode;
  String unitName;
  String barcode;
  String lot;
  String qty;
  String labelBarcode;
  String id;
  String description;
  String fromlocation;
  String warehouselogin;
  String carryNo;

  ClsBatterySupplyPacking(
      {this.supplyNo,
      this.instNo,
      this.typeDesc,
      this.itemCode,
      this.itemName,
      this.unitCode,
      this.unitName,
      this.barcode,
      this.lot,
      this.qty,
      this.labelBarcode,
      this.id,
      this.description,
      this.warehouselogin,
      this.fromlocation,
      this.carryNo});

  // Get Supply Request No
  factory ClsBatterySupplyPacking.ddlSupplyNo(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsBatterySupplyPacking(
        supplyNo: object['SupplyNo'] as String,
        typeDesc: object['ItemType'] as String,
      );
    }
  }

  factory ClsBatterySupplyPacking.getListDataH(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsBatterySupplyPacking(
        barcode: object['Barcode'] as String,
        itemCode: object['ItemCode'] as String,
        itemName: object['ItemName'] as String,
        lot: object['Lot'] as String,
        qty: object['Qty'] as String,
        fromlocation: object['FromWarehouse'] as String,
        id: object['ID'] as String,
        description: object['Description'] as String,
      );
    }
  }

  factory ClsBatterySupplyPacking.submitData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsBatterySupplyPacking(
          id: object['ID'] as String,
          description: object['Description'] as String);
    }
  }
}
