class ClsCarryMovingLocation {
  String carryNo;
  String locationAddress1;
  String locationDestination;
  String locationAddress2;

  String itemCode;
  String itemName;
  String lotNo;
  String qty;
  String unit;
  String sublotNo;
  String id;
  String description;

  ClsCarryMovingLocation(
      {this.carryNo,
      this.locationAddress1,
      this.locationDestination,
      this.locationAddress2,
      this.itemCode,
      this.itemName,
      this.lotNo,
      this.sublotNo,
      this.qty,
      this.unit,
      this.id,
      this.description});

  factory ClsCarryMovingLocation.getLocationAddress1(
      Map<String, dynamic> object) {
    return ClsCarryMovingLocation(
        locationAddress1: object['LocationAddress'] as String);
  }

  factory ClsCarryMovingLocation.getLocationAddress2(
      Map<String, dynamic> object) {
    return ClsCarryMovingLocation(
        locationAddress2: object['LocationAddress'] as String);
  }

  factory ClsCarryMovingLocation.listHeader(Map<String, dynamic> object) {
    return ClsCarryMovingLocation(
        itemCode: object['ItemCode'] as String,
        itemName: object['ItemName'] as String,
        lotNo: object['LotNo'] as String,
        qty: object['Qty'] as String,
        unit: object['UnitName'] as String);
  }

  factory ClsCarryMovingLocation.listDetail(Map<String, dynamic> object) {
    return ClsCarryMovingLocation(
        sublotNo: object['SublotNo'] as String,
        qty: object['Qty'] as String,
        unit: object['UnitName'] as String);
  }

  factory ClsCarryMovingLocation.listSubmit(Map<String, dynamic> object) {
    return ClsCarryMovingLocation(
        id: object['ID'] as String,
        description: object['Description'] as String);
  }
}
