class ClsConsumpUnschedule {
  String typedesc;
  String labelBarcode;
  String materialCode;
  String materialDesc;
  String lotNo;
  String qtyBefore;
  String qtyActual;
  String unitName;
  String expDate;
  String supplierName;
  String lastUser;
  String lastUpdate;

  String id;
  String description;

  ClsConsumpUnschedule({
    this.typedesc,
    this.labelBarcode,
    this.materialCode,
    this.materialDesc,
    this.lotNo,
    this.qtyBefore,
    this.qtyActual,
    this.unitName,
    this.expDate,
    this.supplierName,
    this.lastUser,
    this.lastUpdate,
    this.id,
    this.description,
  });

  factory ClsConsumpUnschedule.scanData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsConsumpUnschedule(
          labelBarcode: object['BarcodeNo'] as String,
          materialCode: object['MaterialCode'] as String,
          materialDesc: object['MaterialDesc'] as String,
          lotNo: object['LotNo'] as String,
          qtyBefore: object['QtyBefore'] as String,
          qtyActual: object['QtyActual'] as String,
          unitName: object['UnitName'] as String,
          expDate: object['ExpDate'] as String,
          supplierName: object['SupplierName'] as String,
          lastUser: object['LastUser'] as String,
          lastUpdate: object['LastUpdate'] as String,
          id: object['ID'] as String,
          description: object['Message'] as String);
    }
  }

  factory ClsConsumpUnschedule.submitData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsConsumpUnschedule(
          id: object['ID'] as String, description: object['Message'] as String);
    }
  }
}
