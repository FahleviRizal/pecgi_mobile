class ClsManpowerPreparation {
  String instructionNo;
  String lineCode;
  String lineName;
  String prodDate;
  String typeDesc;
  String empID;
  String skillCode;
  String skillName;
  String totalManpower;
  String remainingscan;

  String id;
  String description;

  ClsManpowerPreparation(
      {this.instructionNo,
      this.lineCode,
      this.lineName,
      this.prodDate,
      this.typeDesc,
      this.empID,
      this.skillCode,
      this.skillName,
      this.totalManpower,
      this.remainingscan,
      this.id,
      this.description});

  factory ClsManpowerPreparation.comboLine(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparation(
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
      );
    }
  }
  factory ClsManpowerPreparation.ddlInstructionNo(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparation(
        instructionNo: object['InstructionNo'] as String,
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
        prodDate: object['ProdDate'] as String,
        typeDesc: object['TypeDesc'] as String,
      );
    }
  }

  factory ClsManpowerPreparation.fromJson(Map<String, dynamic> object) {
    return ClsManpowerPreparation(
        instructionNo: object['InstructionNo'] as String,
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
        prodDate: object['ProductionDate'] as String,
        //empID: object['EmployeeID'] as String,
        skillCode: object['SkillCode'] as String,
        skillName: object['SkillName'] as String,
        totalManpower: object['TotalManpower'] as String,
        remainingscan: object['RemainingScan'] as String);
  }

  factory ClsManpowerPreparation.scanSubmitData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparation(
          id: object['ID'] as String, description: object['Message'] as String);
    }
  }
}

class ClsManpowerPreparationDetil {
  String empID;
  String empName;
  String status;

  ClsManpowerPreparationDetil({this.empID, this.empName, this.status});

  factory ClsManpowerPreparationDetil.fromJson(Map<String, dynamic> object) {
    return ClsManpowerPreparationDetil(
        empID: object['EmployeeID'] as String,
        empName: object['EmployeeName'] as String,
        status: object['StatusScan'] as String);
  }
}
