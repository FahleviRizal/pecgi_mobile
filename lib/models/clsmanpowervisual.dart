class ClsManpowerPreparationVisual {
  String instructionNo;
  String lineCode;
  String lineName;
  String prodDate;
  String typeDesc;
  String lotCard;
  String empID;
  String skillCode;
  String skillName;
  String totalManpower;
  String remainingscan;

  String id;
  String description;

  ClsManpowerPreparationVisual(
      {this.instructionNo,
      this.lineCode,
      this.lineName,
      this.prodDate,
      this.typeDesc,
      this.lotCard,
      this.empID,
      this.skillCode,
      this.skillName,
      this.totalManpower,
      this.remainingscan,
      this.id,
      this.description});

  factory ClsManpowerPreparationVisual.comboLine(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisual(
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
      );
    }
  }
  factory ClsManpowerPreparationVisual.ddlInstructionNo(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisual(
        instructionNo: object['InstructionNo'] as String,
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
        prodDate: object['ProdDate'] as String,
        typeDesc: object['TypeDesc'] as String,
      );
    }
  }

  factory ClsManpowerPreparationVisual.fromJson(Map<String, dynamic> object) {
    return ClsManpowerPreparationVisual(
        instructionNo: object['InstructionNo'] as String,
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
        prodDate: object['ProductionDate'] as String,
        //empID: object['EmployeeID'] as String,
        skillCode: object['SkillCode'] as String,
        skillName: object['SkillName'] as String,
        totalManpower: object['TotalManpower'] as String,
        remainingscan: object['RemainingScan'] as String);
  }

  factory ClsManpowerPreparationVisual.scanLotCard(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisual(
          id: object['ID'] as String, description: object['Message'] as String);
    }
  }

  factory ClsManpowerPreparationVisual.scanSubmitData(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisual(
          id: object['ID'] as String, description: object['Message'] as String);
    }
  }
}

class ClsManpowerPreparationVisualDetil {
  String empID;
  String empName;
  String status;

  ClsManpowerPreparationVisualDetil({this.empID, this.empName, this.status});

  factory ClsManpowerPreparationVisualDetil.fromJson(
      Map<String, dynamic> object) {
    return ClsManpowerPreparationVisualDetil(
        empID: object['EmployeeID'] as String,
        empName: object['EmployeeName'] as String,
        status: object['StatusScan'] as String);
  }
}
