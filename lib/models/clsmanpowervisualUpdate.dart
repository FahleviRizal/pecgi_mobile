class ClsManpowerPreparationVisualUpdate {
  String instructionNo;
  String lineCode;
  String Line_Code;
  String lineName;
  String prodDate;
  String typeDesc;
  String lotCard;
  String empID;
  String skillCode;
  String skillName;
  String totalManpower;
  String remainingscan;
  String MachineNo;

  String LotCard;
  String InstructionNo;

  String id;
  String description;

  ClsManpowerPreparationVisualUpdate(
      {this.instructionNo,
      this.lineCode,
      this.Line_Code,
      this.MachineNo,
      this.lineName,
      this.prodDate,
      this.typeDesc,
      this.lotCard,
      this.empID,
      this.skillCode,
      this.skillName,
      this.totalManpower,
      this.remainingscan,
      this.id,
      this.description,
      this.LotCard,
      this.InstructionNo});

  factory ClsManpowerPreparationVisualUpdate.comboLine(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisualUpdate(
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
      );
    }
  }

  factory ClsManpowerPreparationVisualUpdate.comboLotCard(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisualUpdate(
        LotCard: object['LotCard'] as String,
        InstructionNo: object['InstructionNo'] as String,
        prodDate: object['ProductionDate'] as String,
      );
    }
  }

  factory ClsManpowerPreparationVisualUpdate.comboEmpID(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisualUpdate(
          skillCode: object['SkillCode'] as String,
          empID: object['EmployeeID'] as String);
    }
  }

  factory ClsManpowerPreparationVisualUpdate.comboMachineID(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisualUpdate(
          Line_Code: object['Line_Code'] as String,
          MachineNo: object['MachineNo'] as String);
    }
  }

  factory ClsManpowerPreparationVisualUpdate.ddlInstructionNo(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisualUpdate(
        instructionNo: object['InstructionNo'] as String,
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
        prodDate: object['ProdDate'] as String,
        typeDesc: object['TypeDesc'] as String,
      );
    }
  }

  factory ClsManpowerPreparationVisualUpdate.fromJson(
      Map<String, dynamic> object) {
    return ClsManpowerPreparationVisualUpdate(
        instructionNo: object['InstructionNo'] as String,
        lineCode: object['LineCode'] as String,
        lineName: object['LineName'] as String,
        prodDate: object['ProductionDate'] as String,
        //empID: object['EmployeeID'] as String,
        skillCode: object['SkillCode'] as String,
        skillName: object['SkillName'] as String,
        totalManpower: object['TotalManpower'] as String,
        remainingscan: object['RemainingScan'] as String);
  }

  factory ClsManpowerPreparationVisualUpdate.scanLotCard(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisualUpdate(
          id: object['ID'] as String, description: object['Message'] as String);
    }
  }

  factory ClsManpowerPreparationVisualUpdate.scanSubmitData(
      Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsManpowerPreparationVisualUpdate(
          id: object['ID'] as String, description: object['Message'] as String);
    }
  }
}

class ClsManpowerPreparationVisualUpdateDetil {
  String empID;
  String empName;
  String status;

  ClsManpowerPreparationVisualUpdateDetil(
      {this.empID, this.empName, this.status});

  factory ClsManpowerPreparationVisualUpdateDetil.fromJson(
      Map<String, dynamic> object) {
    return ClsManpowerPreparationVisualUpdateDetil(
        empID: object['EmployeeID'] as String,
        empName: object['EmployeeName'] as String,
        status: object['StatusScan'] as String);
  }
}
