class ClsMaterialpreparation {
  String materialcode;
  String linecode;
  String linename;
  String proddate;
  String instNo;
  String typedesc;
  String labelbarcode;
  String materialname;
  String scanstatus;
  String barcodeno;
  String registerby;
  String registerdate;
  String itemtype;
  //message
  String id;
  String description;

  ClsMaterialpreparation({
    this.instNo,
    this.linecode,
    this.linename,
    this.typedesc,
    this.proddate,
    this.materialcode,
    this.materialname,
    this.scanstatus,
    this.barcodeno,
    this.registerby,
    this.registerdate,
    this.itemtype,
    this.id,
    this.description,
  });

  factory ClsMaterialpreparation.getData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialpreparation(
          instNo: object['InstructionNo'] as String,
          linecode: object['LineCode'] as String,
          linename: object['LineName'] as String,
          proddate: object['ProductionDate'] as String,
          materialcode: object['MaterialCode'] as String,
          materialname: object['MaterialName'] as String,
          scanstatus: object['ScanStatus'] as String);
    }
  }

  factory ClsMaterialpreparation.getDataDetail(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialpreparation(
          linecode: object['LineCode'] as String,
          linename: object['LineName'] as String,
          proddate: object['ProductionDate'] as String,
          barcodeno: object['BarcodeNo'] as String,
          itemtype: object['ItemType'] as String,
          materialcode: object['MaterialCode'] as String,
          materialname: object['MaterialName'] as String,
          registerby: object['RegisterBy'] as String,
          registerdate: object['RegisterDate'] as String);
    }
  }

  // Get Location / Carry
  factory ClsMaterialpreparation.getInstructionNo(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialpreparation(
        instNo: object['InstNo'] as String,
        proddate: object['ProductionDate'] as String,
        typedesc: object['TypeDesc'] as String,
      );
    }
  }

  factory ClsMaterialpreparation.getLine(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialpreparation(
        linecode: object['LineCode'] as String,
        linename: object['LineName'] as String,
      );
    }
  }

  factory ClsMaterialpreparation.submitdata(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMaterialpreparation(
          id: object['ID'] as String,
          description: object['Description'] as String);
    }
  }
}
