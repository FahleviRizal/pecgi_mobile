class ClsMenu {
  String menuid;
  String menuName;
  String menuDesc;
  int menuIndex;
  String imageName;

  ClsMenu(
      {this.menuid,
      this.menuName,
      this.menuDesc,
      this.menuIndex,
      this.imageName});

  factory ClsMenu.getMenu(Map<String, dynamic> obj) {
    return ClsMenu(
        menuid: obj['MenuID'] as String,
        menuName: (obj['MenuName'] as String).replaceAll("\n", "\n"),
        menuDesc: obj['MenuDescription'] as String,
        menuIndex: obj['MenuIndex'] as int,
        imageName: obj['ImageName'] as String);
  }
}
