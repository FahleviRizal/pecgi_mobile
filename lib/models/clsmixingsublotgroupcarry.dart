class ClsMixingSublotGroupCarry {
  String carryNo;
  String carryName;
  String sublotNo;
  String barcodeNo;
  String type;
  String lotNo;
  String qty;
  String unit;
  String id;
  String message;

  ClsMixingSublotGroupCarry(
      {this.carryNo,
      this.carryName,
      this.sublotNo,
      this.barcodeNo,
      this.type,
      this.lotNo,
      this.qty,
      this.unit,
      this.id,
      this.message});

  factory ClsMixingSublotGroupCarry.fromJson(Map<String, dynamic> object) {
    return ClsMixingSublotGroupCarry(
        carryNo: object['CarryNo'] as String,
        carryName: object['CarryName'] as String,
        sublotNo: object['SublotNo'] as String,
        barcodeNo: object['BarcodeNo'] as String,
        type: object['Type'] as String,
        lotNo: object['LotNo'] as String,
        qty: object['Qty'] as String,
        unit: object['Unit'] as String);
  }

  factory ClsMixingSublotGroupCarry.submit(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsMixingSublotGroupCarry(
          id: object['ID'].toString(), message: object['Message'].toString());
    }
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'SublotNo': sublotNo.trim(),
      'BarcodeNo': barcodeNo.trim(),
      'Type': type.trim(),
      'LotNo': lotNo.trim(),
      'Qty': qty,
    };

    return map;
  }
}
