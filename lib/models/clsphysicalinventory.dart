class ClsPhysicalInventoryByBarcode {
  String period;
  String barcodeNo;
  String itemCode;
  String itemName;
  String lotNo;
  String qty;
  String actualQty;
  String warehouse;
  String location;
  String actualLocation;
  String actualLocName;

  String id;
  String message;

  ClsPhysicalInventoryByBarcode(
      {this.period,
      this.barcodeNo,
      this.itemCode,
      this.itemName,
      this.lotNo,
      this.qty,
      this.actualQty,
      this.warehouse,
      this.location,
      this.actualLocation,
      this.actualLocName,
      this.id,
      this.message});

  factory ClsPhysicalInventoryByBarcode.getPeriod(Map<String, dynamic> object) {
    return ClsPhysicalInventoryByBarcode(period: object['Period'] as String);
  }

  factory ClsPhysicalInventoryByBarcode.getData(Map<String, dynamic> obj) {
    return ClsPhysicalInventoryByBarcode(
      itemCode: obj['ItemCode'] as String,
      itemName: obj['ItemName'] as String,
      lotNo: obj['LotNo'] as String,
      qty: obj['Qty'] as String,
      warehouse: obj['WarehouseName'] as String,
      location: obj['LocationName'] as String,
    );
  }

  factory ClsPhysicalInventoryByBarcode.getLocationNameActual(
      Map<String, dynamic> obj) {
    return ClsPhysicalInventoryByBarcode(
        actualLocName: obj['ActualLocationName'] as String);
  }

  factory ClsPhysicalInventoryByBarcode.submit(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsPhysicalInventoryByBarcode(
          id: object['ID'].toString(), message: object['Message'].toString());
    }
  }
}

class ClsPhysicalInventoryByCarry {
  String carryNo;
  String carryName;
  String type;
  String typedesc;
  String lotNo;
  String currentQty;
  String inventory;
  String difference;
  String barcodeNo;
  String sublotNo;
  String prodDate;

  String id;
  String message;

  ClsPhysicalInventoryByCarry(
      {this.carryNo,
      this.carryName,
      this.type,
      this.typedesc,
      this.lotNo,
      this.currentQty,
      this.inventory,
      this.difference,
      this.prodDate,
      this.barcodeNo,
      this.sublotNo,
      this.id,
      this.message});

  factory ClsPhysicalInventoryByCarry.ddlCarryNo(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsPhysicalInventoryByCarry(
          carryNo: object['CarryNo'] as String,
          carryName: object['CarryName'] as String);
    }
  }

  factory ClsPhysicalInventoryByCarry.listHeader(Map<String, dynamic> obj) {
    return ClsPhysicalInventoryByCarry(
        type: obj['ItemCode'] as String,
        typedesc: obj['ItemName'] as String,
        lotNo: obj['LotNo'] as String,
        prodDate: obj['ProductionDate'] as String,
        currentQty: obj['CurrentQty'] as String,
        inventory: obj['InventoryQty'] as String,
        difference: obj['Difference'] as String);
  }

  factory ClsPhysicalInventoryByCarry.listDetail(Map<String, dynamic> obj) {
    return ClsPhysicalInventoryByCarry(
        carryNo: obj['CarryNo'] as String,
        type: obj['ItemCode'] as String,
        lotNo: obj['LotNo'] as String,
        prodDate: obj['ProductionDate'] as String,
        barcodeNo: obj['BarcodeNo'] as String,
        sublotNo: obj['SublotNo'] as String,
        currentQty: obj['CurrentQty'] as String,
        inventory: obj['InventoryQty'] as String,
        difference: obj['Difference'] as String);
  }

  factory ClsPhysicalInventoryByCarry.submit(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsPhysicalInventoryByCarry(
          id: object['ID'].toString(), message: object['Message'].toString());
    }
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'CarryNo': carryNo.trim(),
      'ItemCode': type.trim(),
      'LotNo': lotNo.trim(),
      'ProductionDate': prodDate,
      'BarcodeNo': barcodeNo.trim(),
      'SublotNo': sublotNo.trim(),
      'CurrentQty': currentQty,
      'InventoryQty': inventory,
      'Difference': difference
    };

    return map;
  }
}
