class ClsPredischargeLogging {
  String instructionNo;
  String type;
  String itemcode;
  String itemname;
  String lotno;
  String barcodeno;
  String sublotno;
  String qty;
  //message
  String id;
  String description;

  ClsPredischargeLogging(
      {this.instructionNo,
      this.type,
      this.itemcode,
      this.itemname,
      this.lotno,
      this.barcodeno,
      this.sublotno,
      this.qty,
      this.id,
      this.description});

  factory ClsPredischargeLogging.submitData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsPredischargeLogging(
          id: object['ID'] as String, description: object['Message'] as String);
    }
  }

  factory ClsPredischargeLogging.getList(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsPredischargeLogging(
          itemcode: object['ItemCode'] as String,
          itemname: object['ItemName'] as String,
          lotno: object['LotNo'] as String,
          barcodeno: object['BarcodeNo'] as String,
          sublotno: object['SublotNo'] as String,
          qty: object['Qty'] as String);
    }
  }

  factory ClsPredischargeLogging.ddlInstructionNo(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsPredischargeLogging(
        instructionNo: object['InstructionNo'] as String,
        type: object['Type'] as String,
      );
    }
  }
}
