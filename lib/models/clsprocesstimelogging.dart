class ClsProcessTimeLogging {
  String locCode;
  String locName;
  String statusScan;
  String type;
  String typeDesc;
  String scanIn;
  String leadtimehour;
  String scanOut;

  String carryNo;
  String carryName;
  String materialCode;
  String materialName;
  String lotNo;
  String barcode;
  String sublot;
  String unit;
  String qty;

  String id;
  String description;

  ClsProcessTimeLogging(
      {this.locCode,
      this.locName,
      this.type,
      this.typeDesc,
      this.statusScan,
      this.scanIn,
      this.leadtimehour,
      this.scanOut,
      this.carryNo,
      this.carryName,
      this.materialCode,
      this.materialName,
      this.lotNo,
      this.barcode,
      this.sublot,
      this.unit,
      this.qty,
      this.id,
      this.description});

  factory ClsProcessTimeLogging.ddlLocation(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsProcessTimeLogging(
          locCode: object['LocationCode'] as String,
          locName: object['LocationName'] as String);
    }
  }

  factory ClsProcessTimeLogging.autoComplete(Map<String, dynamic> object) {
    return ClsProcessTimeLogging(
        type: object['Type'] as String,
        typeDesc: object['TypeDesc'] as String,
        statusScan: object['StatusScan'] as String);
  }

  factory ClsProcessTimeLogging.submitData(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsProcessTimeLogging(
          id: object['ID'] as String, description: object['Message'] as String);
    }
  }

  factory ClsProcessTimeLogging.complete(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsProcessTimeLogging(
          id: object['ID'].toString(),
          description: object['Message'].toString());
    }
  }

  factory ClsProcessTimeLogging.fromJson(Map<String, dynamic> object) {
    return ClsProcessTimeLogging(
        scanIn: object['TimeScanIn'] as String,
        leadtimehour: object['LeadTimeHour'] as String,
        scanOut: object['TimeScanOut'] as String);
  }

  factory ClsProcessTimeLogging.getlistProd(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsProcessTimeLogging(
          carryNo: object['CarryNo'] as String,
          carryName: object['CarryName'] as String,
          materialCode: object['Type'] as String,
          materialName: object['TypeDesc'] as String,
          lotNo: object['LotNo'] as String,
          barcode: object['BarcodeNo'] as String,
          sublot: object['SublotNo'] as String,
          unit: object['Unit'] as String,
          qty: object['Qty'] as String);
    }
  }

  factory ClsProcessTimeLogging.checkLocation(Map<String, dynamic> json) {
    return ClsProcessTimeLogging(
        id: json["ID"] as String, description: json["Message"] as String);
  }
}
