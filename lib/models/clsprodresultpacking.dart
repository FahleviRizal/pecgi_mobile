class ClsProdResultPacking {
  String barcodeNo;
  String itemCode;
  String itemName;
  String lotNo;
  String sublotNo;
  String qty;
  String unit;
  String warehouselogin;
  String id;
  String description;
  String isflag;

  ClsProdResultPacking(
      {this.barcodeNo,
      this.itemCode,
      this.itemName,
      this.lotNo,
      this.sublotNo,
      this.qty,
      this.unit,
      this.warehouselogin,
      this.id,
      this.description,
      this.isflag});

  factory ClsProdResultPacking.scanBarcode(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsProdResultPacking(
          barcodeNo: object['BarcodeNo'] as String,
          itemCode: object['ItemCode'] as String,
          itemName: object['ItemName'] as String,
          lotNo: object['LotNo'] as String,
          sublotNo: object['SublotNo'] as String,
          qty: object['Qty'] as String,
          unit: object['UnitName'] as String,
          id: object['ID'] as String,
          description: object['Message'] as String);
    }
  }

  factory ClsProdResultPacking.submit(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsProdResultPacking(
          id: object['ID'].toString(),
          description: object['Message'].toString());
    }
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'BarcodeNo': barcodeNo.trim(),
      'ItemCode': itemCode.trim(),
      'ItemName': itemName.trim(),
      'LotNo': lotNo.trim(),
      'SublotNo': sublotNo,
      'Qty': qty,
      'Unit': unit.trim(),
      'WarehouseCode': warehouselogin.trim()
    };

    return map;
  }
}
