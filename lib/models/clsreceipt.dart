//recipt schedule & receipt unschedule
class ReceiptScheduleHeader {
  //filter
  String requestNo;
  String requestDate;
  String barcodeNo;

  //listview
  String materialCode;
  String materialName;
  String qty;
  String status;

  ReceiptScheduleHeader({
    this.requestNo,
    this.requestDate,
    this.barcodeNo,
    this.materialCode,
    this.materialName,
    this.qty,
    this.status,
  });

  // factory ReceiptScheduleHeader.fromJson(json) {
  //   ReceiptScheduleHeader list;
  //   for (int i = 0; i < json.length; i++) {
  //     list.materialName = json[i]['MaterialName'];
  //     list.qty = json[i]['Qty'];
  //     list.status = json[i]['Status'];
  //   }

  //   return list;
  //   // return ReceiptScheduleHeader(
  //   //   materialName: json['MaterialName'],
  //   //   qty: json['Qty'],
  //   //   status: json['Status'],
  //   // );
  // }

  factory ReceiptScheduleHeader.ddlRequestNo(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ReceiptScheduleHeader(
          requestNo: object['RequestNo'] as String,
          requestDate: object['RequestDate']);
    }
  }

  factory ReceiptScheduleHeader.listH(Map<String, dynamic> json) {
    return ReceiptScheduleHeader(
        requestNo: json["RequestNo"] as String,
        requestDate: json["RequestDate"] as String,
        barcodeNo: json["BarcodeNo"] as String,
        materialCode: json["MaterialCode"] as String,
        materialName: json["MaterialName"] as String,
        qty: json["Qty"] as String,
        status: json["Status"] as String);
  }
}

//receipt detail & list schedule and unschedule
class ReceiptScheduleDetail {
  String reqNo;
  String barcodeNo;
  String materialCode;
  String materialName;
  String lotNo;
  String qty;
  String expiredDate;
  String supplierName;
  String userid;
  String id;
  String message;

  ReceiptScheduleDetail(
      {this.reqNo,
      this.barcodeNo,
      this.materialCode,
      this.materialName,
      this.lotNo,
      this.qty,
      this.expiredDate,
      this.supplierName,
      this.userid,
      this.id,
      this.message});

  factory ReceiptScheduleDetail.listData(Map<String, dynamic> json) {
    return ReceiptScheduleDetail(
        reqNo: json["RequestNo"] as String,
        barcodeNo: json["BarcodeNo"] as String,
        materialCode: json["MaterialCode"] as String,
        materialName: json['MaterialName'] as String,
        lotNo: json["LotNo"] as String,
        qty: json["Qty"] as String,
        expiredDate: json["ExpiredDate"] as String,
        supplierName: json["SupplierName"] as String);
  }

  factory ReceiptScheduleDetail.validation(Map<String, dynamic> json) {
    return ReceiptScheduleDetail(
        id: json["ID"] as String, message: json["Message"] as String);
  }

  factory ReceiptScheduleDetail.submit(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ReceiptScheduleDetail(
          id: object['ID'].toString(), message: object['Message'].toString());
    }
  }
}
