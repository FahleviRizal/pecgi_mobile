class ClsStuffingCheck {
  //filter
  String invoiceNo;
  String totalQty;
  String type;
  String typeDesc;
  String itemCode;
  String itemName;
  String lotNo;
  String barcodeNo;
  String palletNo;
  String qty;
  String isflag;
  //message
  String id;
  String description;

  ClsStuffingCheck(
      {this.invoiceNo,
      this.totalQty,
      this.type,
      this.typeDesc,
      this.itemCode,
      this.itemName,
      this.barcodeNo,
      this.palletNo,
      this.lotNo,
      this.qty,
      this.isflag,
      this.id,
      this.description});

  factory ClsStuffingCheck.ddlInvoiceNo(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsStuffingCheck(
        invoiceNo: object['InvoiceNo'] as String,
        totalQty: object['TotalQty'] as String,
      );
    }
  }

  factory ClsStuffingCheck.listHeader(Map<String, dynamic> object) {
    return ClsStuffingCheck(
      invoiceNo: object['InvoiceNo'] as String,
      type: object['ItemType'] as String,
      typeDesc: object['ItemTypeName'] as String,
      itemCode: object['ItemCode'] as String,
      itemName: object['ItemName'] as String,
      barcodeNo: object['BarcodeNo'] as String,
      palletNo: object['PalletNo'] as String,
      lotNo: object['LotNo'] as String,
      qty: object['Qty'] as String,
      isflag: object['Scanned'] as String,
    );
  }

  factory ClsStuffingCheck.listDetail(Map<String, dynamic> object) {
    return ClsStuffingCheck(
      type: object['ItemCode'] as String,
      typeDesc: object['ItemName'] as String,
      palletNo: object['PalletNo'] as String,
      barcodeNo: object['BarcodeNo'] as String,
      lotNo: object['LotNo'] as String,
      qty: object['Qty'] as String,
    );
  }

  factory ClsStuffingCheck.submit(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsStuffingCheck(
          id: object['ID'].toString(),
          description: object['Description'].toString());
    }
  }
}
