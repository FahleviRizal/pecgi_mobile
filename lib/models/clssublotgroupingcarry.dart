class ClsSublotGroupingCarry {
  String carryNo;
  String carryName;
  String sublotNo;
  String barcodeNo;
  String type;
  String lotNo;
  String qty;
  String unit;
  String id;
  String message;

  ClsSublotGroupingCarry(
      {this.carryNo,
      this.carryName,
      this.sublotNo,
      this.barcodeNo,
      this.type,
      this.lotNo,
      this.qty,
      this.unit,
      this.id,
      this.message});

  factory ClsSublotGroupingCarry.fromJson(Map<String, dynamic> object) {
    return ClsSublotGroupingCarry(
        carryNo: object['CarryNo'] as String,
        carryName: object['CarryName'] as String,
        sublotNo: object['SublotNo'] as String,
        barcodeNo: object['BarcodeNo'] as String,
        type: object['Type'] as String,
        lotNo: object['LotNo'] as String,
        qty: object['Qty'] as String,
        unit: object['Unit'] as String);
  }

  factory ClsSublotGroupingCarry.complete(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSublotGroupingCarry(
          id: object['ID'].toString(), message: object['Message'].toString());
    }
  }
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'SublotNo': sublotNo.trim(),
      'BarcodeNo': barcodeNo.trim(),
      'Type': type.trim(),
      'LotNo': lotNo.trim(),
      'Qty': qty,
    };

    return map;
  }
}

class ClsSublotCarryDetail {
  String barcodeNo;
  String sublotNo;
  String type;
  String lotNo;
  String qty;
  String unit;
  String expiredDate;
  String lastUser;
  String lastUpdate;
  String id;
  String message;

  ClsSublotCarryDetail(
      {this.sublotNo,
      this.barcodeNo,
      this.type,
      this.lotNo,
      this.qty,
      this.unit,
      this.expiredDate,
      this.lastUser,
      this.lastUpdate,
      this.id,
      this.message});

  factory ClsSublotCarryDetail.fromJson(Map<String, dynamic> obj) {
    return ClsSublotCarryDetail(
        sublotNo: obj['SublotNo'] as String,
        barcodeNo: obj['BarcodeNo'] as String,
        type: obj['Type'] as String,
        lotNo: obj['LotNo'] as String,
        qty: obj['Qty'] as String,
        unit: obj['Unit'] as String,
        expiredDate: obj['ExpiredDate'] as String,
        lastUser: obj['LastUser'] as String,
        lastUpdate: obj['LastUpdate'] as String,
        id: obj['ID'] as String,
        message: obj['Message'] as String);
  }

  factory ClsSublotCarryDetail.checkData(Map<String, dynamic> json) {
    return ClsSublotCarryDetail(
        id: json["ID"] as String, message: json["Message"] as String);
  }

  factory ClsSublotCarryDetail.submit(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsSublotCarryDetail(
          id: object['ID'].toString(), message: object['Message'].toString());
    }
  }
}
