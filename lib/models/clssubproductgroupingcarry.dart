class ClsSubProductGroupingCarry {
  String warehouseCode;
  String itemCode;
  String itemName;
  String unitCls;
  String unitDesc;
  String barcodeNo;
  String lotNo;
  String subLotNo;
  String qty;
  String carryNo;
  String scanDate; 
  String userID;

  ClsSubProductGroupingCarry(
      {this.warehouseCode,
      this.itemCode,
      this.itemName,
      this.unitCls,
      this.unitDesc,
      this.barcodeNo,
      this.lotNo,
      this.subLotNo,
      this.scanDate,
      this.qty,
      this.carryNo,
      this.userID
      });

  factory ClsSubProductGroupingCarry.fromJson(Map<String, dynamic> obj) {
    return ClsSubProductGroupingCarry(
        warehouseCode: obj["WarehouseCode"] != null ? obj["WarehouseCode"].toString() : null,
        itemCode: obj["ItemCode"] != null ? obj["ItemCode"].toString() : null,
        itemName: obj["ItemName"] != null ? obj["ItemName"].toString() : null,
        unitCls: obj["UnitCls"] != null ? obj["UnitCls"].toString() : null,
        unitDesc: obj["Unit"] != null ? obj["Unit"].toString() : null,
        barcodeNo: obj["BarcodeNo"] != null ? obj["BarcodeNo"].toString() : null,
        lotNo: obj["LotNo"] != null ? obj["LotNo"].toString() : null,
        qty: obj["Qty"] != null ? obj["Qty"].toString() : null,
        subLotNo: obj["SubLotNo"] != null ? obj["SubLotNo"].toString() : null,
        scanDate: obj["ScanDate"] != null ? obj["ScanDate"].toString() : null
      );
  }

  Map<String, dynamic> toJson(){
    Map<String, dynamic> map = {
      'WarehouseCode' : warehouseCode,
      'ItemCode' : itemCode,
      'UnitCls' : unitCls,
      'CarryNo' : carryNo,
      'BarcodeNo' : barcodeNo,
      'LotNo' : lotNo,
      'Qty' : qty,
      'SubLotNo' : subLotNo,
      'ScanDate' : scanDate,
      'UserId' : userID
    };

    return map;
  }
} 