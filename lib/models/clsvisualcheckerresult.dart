class ClsVisualCheckerResult {
  String carryNo;
  String carryName;
  String assessmentAging;
  String assessmentAgingDays;
  String assessmentAgingHours;

  String assessmentCooling;
  String assessmentCoolingDays;
  String assessmentCoolingHours;

  String assessmentDate;
  String remarks;
  String scanSublot;

  //detail
  String sublotNo;
  String resultQty;
  String ngQty;

  ClsVisualCheckerResult(
      {this.carryNo,
      this.carryName,
      this.assessmentAging,
      this.assessmentAgingDays,
      this.assessmentAgingHours,
      this.assessmentCooling,
      this.assessmentCoolingDays,
      this.assessmentCoolingHours,
      this.assessmentDate,
      this.remarks,
      this.scanSublot,
      this.sublotNo,
      this.resultQty,
      this.ngQty});

  factory ClsVisualCheckerResult.ddlCarryNo(Map<String, dynamic> object) {
    if (object == null) {
      return null;
    } else {
      return ClsVisualCheckerResult(
          carryNo: object['CarryNo'] as String,
          carryName: object['CarryName'] as String);
    }
  }

  factory ClsVisualCheckerResult.getData(Map<String, dynamic> object) {
    return ClsVisualCheckerResult(
      assessmentAging: object['AssessmentAging'] as String,
      assessmentAgingDays: object['AssessmentAgingDays'] as String,
      assessmentAgingHours: object['AssessmentAgingHours'] as String,
      assessmentCooling: object['AssessmentCooling'] as String,
      assessmentCoolingDays: object['AssessmentCoolingDays'] as String,
      assessmentCoolingHours: object['AssessmentCoolingHours'] as String,
      assessmentDate: object['AssessmentDate'] as String,
      remarks: object['Remarks'] as String,
    );
  }

  factory ClsVisualCheckerResult.listHeader(Map<String, dynamic> object) {
    return ClsVisualCheckerResult(
        carryNo: object['CarryNo'] as String,
        sublotNo: object['SublotNo'] as String,
        resultQty: object['ResultQty'] as String,
        ngQty: object['NGQty'] as String);
  }
}
