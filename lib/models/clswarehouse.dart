import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/utilities/constantsApi.dart';

class ClsWarehouse {
  String warehouseCode;
  String warehouseName;

  ClsWarehouse({this.warehouseCode, this.warehouseName});

  factory ClsWarehouse.ddlWarehouse(Map<String, dynamic> object) {
    try {
      if (object == null) {
        return null;
      } else {
        return ClsWarehouse(
            warehouseCode: object['WarehouseCode'] as String,
            warehouseName: object['WarehouseName'] as String);
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  static Future<List<ClsWarehouse>> getComboWarehouse(String userid) async {
    // String urlCommon = 'http://192.168.200.12:8090/api/common/';
    //String urlCommon = 'http://192.168.137.1:8090/api/common/';
    try {
      final response = await http
          .get(Uri.parse(URLAPI + '/cbouserwarehouse?userid=' + userid));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ClsWarehouse> listData = body
            .map((dynamic item) => ClsWarehouse.ddlWarehouse(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }
}
