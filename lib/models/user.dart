// class User {
//   final String userID;
//   final String userName;
//   final String password;

//   User(this.userID, this.userName, this.password);

//   User.fromJson(Map<String, dynamic> json)
//       : userID = json['userid'],
//         userName = json['username'],
//         password = json['password'];

//   Map<String, dynamic> toJson() =>
//       {'userid': userID, 'username': userName, 'password': password};
// }

class UserResponseModel {
  final String status;

  UserResponseModel({this.status});

  factory UserResponseModel.fromJson(Map<String, dynamic> json) {
    return UserResponseModel(
      status: json["StatusLogin"] != null ? json["StatusLogin"] : "",
    );
  }
}

class User {
  String userid;
  String password;

  User({
    this.userid,
    this.password,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'userid': userid.trim(),
      'password': password.trim(),
    };

    return map;
  }
}
