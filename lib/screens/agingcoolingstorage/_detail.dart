import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsagingcoolingstorage.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/services/agingcoolingstorageservice.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Detail',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class AgingStorageDetail extends StatefulWidget {
  const AgingStorageDetail(
      {Key key,
      this.userid,
      this.carryNo,
      this.itemCode,
      this.itemName,
      this.lotNo})
      : super(key: key);

  final String userid;
  final String carryNo;
  final String itemCode;
  final String itemName;
  final String lotNo;

  @override
  _Detail createState() => _Detail();
}

class _Detail extends State<AgingStorageDetail> {
  AgingCoolingStorageService api = AgingCoolingStorageService();
  TextEditingController iItemCode = TextEditingController();
  TextEditingController iItemName = TextEditingController();
  TextEditingController iLotNo = TextEditingController();
  TextEditingController iSearch = TextEditingController();

  List<ClsAgingCoolingStorage> listH = [
    // ClsAgingCoolingStorage(sublotNo: "Sublot 1", qty: "1000", unit: "PCS"),
    // ClsAgingCoolingStorage(sublotNo: "Sublot 2", qty: "1000", unit: "PCS"),
    // ClsAgingCoolingStorage(sublotNo: "Sublot 3", qty: "1000", unit: "PCS"),
    // ClsAgingCoolingStorage(sublotNo: "Sublot 3", qty: "1000", unit: "PCS"),
  ];
  List<ClsAgingCoolingStorage> searchdata = [
    // ClsAgingCoolingStorage(sublotNo: "Sublot 1", qty: "1000", unit: "PCS"),
    // ClsAgingCoolingStorage(sublotNo: "Sublot 2", qty: "1000", unit: "PCS"),
    // ClsAgingCoolingStorage(sublotNo: "Sublot 3", qty: "1000", unit: "PCS"),
    // ClsAgingCoolingStorage(sublotNo: "Sublot 3", qty: "1000", unit: "PCS"),
  ];

  String _searchResult = '';
  final value = new NumberFormat("#,##0.00", "en_US");

  @override
  void initState() {
    super.initState();

    iItemCode.text = widget.itemCode;
    iItemName.text = widget.itemName;
    iLotNo.text = widget.lotNo;

    asynclistGetData(widget.carryNo, widget.itemCode, widget.lotNo);
  }

  void asynclistGetData(String carryNo, String itemCode, String lotNo) async {
    listH = await api.getDetail(carryNo, itemCode, lotNo);
    searchdata = listH;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text("Aging Storage Detail"),
        ),
        body: FutureBuilder(
            future:
                api.getDetail(widget.carryNo, widget.itemCode, widget.lotNo),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              }

              return Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: <Widget>[
                  SizedBox(height: 10.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Item Code",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iItemCode,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Item Name",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iItemName,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Lot No",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iLotNo,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                    height: 20,
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iSearch,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        labelText: 'Search',
                        prefixIcon: IconButton(
                          onPressed: () {},
                          icon: Icon(_searchResult == ''
                              ? Icons.search
                              : Icons.search),
                          //Icons.search,
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              iSearch.clear();
                              _searchResult = '';
                              searchdata = listH;
                            });
                          },
                          icon: Icon(_searchResult == ''
                              ? Icons.cancel
                              : Icons.cancel_outlined),
                          color: _searchResult == ''
                              ? Colors.white12.withOpacity(1)
                              : Colors.grey[600],
                        ),
                      ),
                      onChanged: (value) {
                        setState(() {
                          if (value != null) {
                            _searchResult = value;
                            searchdata = listH
                                .where((rec) =>
                                    rec.sublotNo.toUpperCase().contains(
                                        _searchResult.toUpperCase()) ||
                                    rec.qty.toUpperCase().contains(
                                        _searchResult.toUpperCase()) ||
                                    rec.unit
                                        .toUpperCase()
                                        .contains(_searchResult.toUpperCase()))
                                .toList();
                          } else {
                            searchdata = listH;
                          }
                        });
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Expanded(child: _dataTableWidget()),
                ],
              );
            }),
      ),
    );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 0),
      _getTitleItemWidget('Sublot No', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Unit', MediaQuery.of(context).size.width / 3),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      width: 1,
      height: 45,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].sublotNo),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(value.format(double.parse(searchdata[index].qty))),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(searchdata[index].unit),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.center,
        ),
      ],
    );
  }

  /*
  Container(
                      padding: const EdgeInsets.only(top: 10),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: DataTable(
                            decoration: BoxDecoration(
                                border: Border.all(
                              width: 1,
                              color: Colors.grey[300],
                            )),
                            headingRowColor: MaterialStateColor.resolveWith(
                                (states) => Colors.grey[200]),
                            sortColumnIndex: 0,
                            showCheckboxColumn: false,
                            columns: [
                              DataColumn(
                                  label: Container(
                                    width: 130,
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 0),
                                    child: Text(
                                      "Sublot No",
                                      style: TextStyle(
                                          fontFamily: "Arial",
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                  ),
                                  numeric: false,
                                  tooltip: "Sublot No"),
                              DataColumn(
                                label: Container(
                                    width: 50,
                                    alignment: Alignment.centerRight,
                                    child: Text(
                                      "Qty",
                                      style: TextStyle(
                                          fontFamily: "Arial",
                                          fontWeight: FontWeight.bold),
                                    )),
                                numeric: false,
                                tooltip: "Qty",
                              ),
                              DataColumn(
                                  label: Container(
                                      width: 50,
                                      child: Text(
                                        "Unit",
                                        style: TextStyle(
                                            fontFamily: "Arial",
                                            fontWeight: FontWeight.bold),
                                      )),
                                  numeric: false,
                                  tooltip: "Unit"),
                            ],
                            rows: searchdata
                                .map(
                                  (idata) => DataRow(cells: [
                                    DataCell(
                                      Container(
                                        width: 120,
                                        child: Text(idata.sublotNo),
                                        alignment: Alignment.centerLeft,
                                      ),
                                    ),
                                    DataCell(
                                      Container(
                                        width: 60,
                                        child: Text(idata.qty),
                                        alignment: Alignment.centerRight,
                                      ),
                                    ),
                                    DataCell(
                                      Container(
                                        width: 50,
                                        child: Text(idata.unit),
                                        alignment: Alignment.centerLeft,
                                      ),
                                    ),
                                  ]),
                                )
                                .toList(),
                          ),
                        ),
                      ),
                    ),
  */
}
