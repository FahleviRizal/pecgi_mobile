import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:panasonic/models/clsSyncLamp.dart';
import 'package:panasonic/models/clsagingcoolingstorage.dart';
import 'package:panasonic/screens/agingcoolingstorage/_detail.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/services/agingcoolingstorageservice.dart';
import 'package:panasonic/utilities/message.dart';
import 'package:usb_serial/transaction.dart';
import 'package:usb_serial/usb_serial.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aging Cooling Storage',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class AgingCoolingStorage extends StatefulWidget {
  const AgingCoolingStorage({Key key, this.userid}) : super(key: key);

  final String userid;

  @override
  _AgingCoolingStorage createState() => _AgingCoolingStorage();
}

class _AgingCoolingStorage extends State<AgingCoolingStorage> {
  AgingCoolingStorageService api = AgingCoolingStorageService();
  TextEditingController iScanCarryNo = TextEditingController();
  TextEditingController iLocAddressH = TextEditingController();
  TextEditingController iScanLocDest = TextEditingController();
  TextEditingController iLocAddressD = TextEditingController();
  TextEditingController txtLampID = TextEditingController();
  TextEditingController txtStatusLamp = TextEditingController();

  List<ClsAgingCoolingStorage> listH;
  List<ClsAgingCoolingStorage> searchdata = [];

  FocusNode nodeCarryNo = FocusNode();
  FocusNode nodeLocationDestination = FocusNode();
  String _searchCarry = '';
  String _scanLoc = '';

  UsbPort __port;
  List<Widget> __ports = [];
  UsbDevice __device;
  List<Widget> __serialData = [];
  StreamSubscription<String> _subscription;
  Transaction<String> __transaction;
  String __cmdReturn = "";
  String __lampID = "";
  Timer __timer;
  String __datereceive = "";
  String __datesend = "";
  String __ipaddress = "";
  bool __statuscall = false;
  // ignore: unused_field
  String __status = "Idle";
  String __remarks = "";
  String __deepsleepvalue = "0";

  bool isProcessing = false;

  @override
  void initState() {
    UsbSerial.usbEventStream.listen((UsbEvent event) {
      __getPorts();
      __loadConnect();
    });

    super.initState();
  }

  void __getPorts() async {
    __ports = [];
    bool __flagConnect = true;

    List<UsbDevice> _devices = await UsbSerial.listDevices();
    if (!_devices.contains(__device)) {
      __connectTo(null);
    }
    for (var _device in _devices) {
      __ports.add(ListTile(
          leading: const Icon(Icons.usb),
          title: Text('Product : ' + _device.productName),
          subtitle: Text('Dev ID : ' + _device.deviceId.toString()),
          trailing: ElevatedButton.icon(
            label: Text(__device == _device ? "Disconnect" : "Connect"),
            icon: Icon(
              __device == _device ? Icons.sensors_off : Icons.sensors,
              color: __device == _device ? Colors.white : Colors.yellow,
              size: 20.0,
            ),
            style: ElevatedButton.styleFrom(
              primary: Colors.indigo,
              onPrimary: __device == _device ? Colors.white : Colors.yellow,
              textStyle: TextStyle(
                color: Colors.blue,
                fontSize: 12,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(32.0)),
              minimumSize: Size(130, 40),
            ),
            onPressed: () {
              __flagConnect = !__flagConnect;

              __connectTo(__device == _device ? null : _device).then((res) {
                __getPorts();
              });
            },
          )));

      __flagConnect = !__flagConnect;

      __connectTo(__device == _device ? null : _device).then((res) {
        // _getPorts();
      });
      // }

    }

    setState(() {
      // print(_ports);
    });
  }

  void __loadConnect() async {
    List<UsbDevice> devices = await UsbSerial.listDevices();
    bool __flagConnect = true;
    for (var device in devices) {
      __flagConnect = !__flagConnect;
      print('onPressed - connect to device');
      print(__flagConnect);
      __connectTo(__device == device ? null : device).then((res) {});
    }
    __getPorts();
  }

  Future<bool> __connectTo(device) async {
    __serialData.clear();

    if (_subscription != null) {
      _subscription.cancel();
      _subscription = null;
    }

    if (__transaction != null) {
      __transaction.dispose();
      __transaction = null;
    }

    if (__port != null) {
      __port.close();
      __port = null;
    }

    if (device == null) {
      __device = null;
      setState(() {});
      return true;
    }

    __port = await device.create();
    if (await (__port.open()) != true) {
      setState(() {});
      return false;
    }
    __device = device;

    await __port.setDTR(true);
    await __port.setRTS(true);
    await __port.setPortParameters(
        9600, UsbPort.DATABITS_8, UsbPort.STOPBITS_1, UsbPort.PARITY_NONE);

    __transaction = Transaction.stringTerminated(
        // ignore: unnecessary_cast
        __port.inputStream as Stream<Uint8List>,
        Uint8List.fromList([13, 10]));

    _subscription = __transaction.stream.listen((String line) {
      setState(() async {
        __cmdReturn = line;
        String dataFeedback = __lampID + ":SLEEPON";

        if ((__cmdReturn.contains(dataFeedback) == true) &&
            __statuscall == true) {
          __timer.cancel();
          __statuscall = false;

          if (__datesend != "") {
            __datesend = "";
            DateTime now = DateTime.now();
            __datereceive = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

            // ignore: unused_local_variable
            List<ClsSyncLamp> _listinsdatalamp = [];
            _listinsdatalamp = await ClsSyncLamp.insTowerlampHistory(
                iScanCarryNo.text,
                __lampID,
                __ipaddress,
                "1",
                __cmdReturn,
                __datereceive,
                widget.userid);

            // ignore: unused_local_variable
            List<ClsSyncLamp> _listupdmscarry = [];
            _listupdmscarry = await ClsSyncLamp.updMSCarry(
                '3',
                iScanCarryNo.text,
                '',
                '',
                '',
                'Sleep',
                __deepsleepvalue,
                widget.userid);

            setState(() {
              Navigator.of(context).pop();
            });
          }
        }

        if (__serialData.length > 20) {
          __serialData.removeAt(0);
        }
      });
    });

    setState(() {
      __status = "Connected";
    });
    return true;
  }

  Future<void> __setDeepSleep(_flag) async {
    String data = "";
    if (__port == null) {
      UsbSerial.usbEventStream.listen((UsbEvent event) {
        __getPorts();
      });
      __getPorts();
      return;
    }

    if (__statuscall == true) {
      final info = NetworkInfo();
      __ipaddress = await info.getWifiIP();
      // __lampID = "00002";
      __lampID = txtLampID.text;

      if (_flag == "DeepSleep") {
        data = __lampID + ":JS:" + __deepsleepvalue;
        __remarks = __lampID + ":JS:" + __deepsleepvalue;
      } else {
        __timer.cancel();
      }

      data = data + "\r\n";

      if (data.length >= 12 && data.contains(':')) {
        __cmdReturn = "";
      } else {
        __cmdReturn = "";
      }

      await __port.write(Uint8List.fromList(data.codeUnits));

      if (__datesend == "") {
        __datereceive = "";
        DateTime now = DateTime.now();
        __datesend = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

        // ignore: unused_local_variable
        List<ClsSyncLamp> _listinsdatalamp = [];
        _listinsdatalamp = await ClsSyncLamp.insTowerlampHistory(
            iScanCarryNo.text,
            __lampID,
            __ipaddress,
            "0",
            __remarks,
            __datesend,
            widget.userid);
      }
    }
  }

  void _getAddressLocation(String code, String type) async {
    List<ClsAgingCoolingStorage> list;
    list = await api.getLocationAddress(code, type);

    if (list.length > 0) {
      if (type == "0") {
        txtLampID.text = "";
        txtStatusLamp.text = "";
        iLocAddressH.text = list[0].locationAddress1;
        txtLampID.text = list[0].lampid;
        txtStatusLamp.text = list[0].statuslamp;
        FocusScope.of(context).requestFocus(nodeLocationDestination);
      } else {
        iLocAddressD.text = list[0].locationAddress2;
      }
    } else {
      if (type == "0") {
        Message.box(
            type: "2",
            message: "Carry No is not found",
            top: 0,
            position: "0",
            context: context);

        // var dialog = CustomAlertDialog(
        //   type: "4",
        //   title: "",
        //   message: "Carry No is not found",
        //   okBtnText: 'Close',
        // );
        // showDialog(context: context, builder: (BuildContext context) => dialog);
        iScanCarryNo.text = "";
        iLocAddressH.text = "";
      } else {
        Message.box(
            type: "2",
            message: "Location is not found",
            top: 0,
            position: "0",
            context: context);

        // var dialog = CustomAlertDialog(
        //   type: "4",
        //   title: "",
        //   message: "Location is not found",
        //   okBtnText: 'Close',
        // );
        // showDialog(context: context, builder: (BuildContext context) => dialog);
        iScanLocDest.text = "";
        iLocAddressD.text = "";
      }
    }
  }

  void asynclistGetData(String carryNo) async {
    listH = await api.getData(carryNo);
    searchdata = listH;
    setState(() {});
  }

  void _detail(
      String carryNo, String _itemCode, String _itemName, String _lotNo) {
    setState(() {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => AgingStorageDetail(
              carryNo: carryNo,
              itemCode: _itemCode,
              itemName: _itemName,
              lotNo: _lotNo),
          settings: RouteSettings(),
        ),
      );
    });
  }

  void asyncsubmit(String carryNo, String loc) async {
    try {
      setState(() {
        isProcessing = true;
      });

      if (carryNo == "" || carryNo == null) {
        Message.box(
            type: "2",
            message: "Please scan carry no!",
            top: 0,
            position: "0",
            context: context);

        // String message = "Please scan carry no!";
        // var dialog = CustomAlertDialog(
        //   type: "4",
        //   title: "",
        //   message: message,
        //   okBtnText: 'Close',
        // );
        // showDialog(context: context, builder: (BuildContext context) => dialog);

        return;
      }

      if (iScanLocDest.text == "" || iScanLocDest.text == null) {
        String message = "Please scan location destination!";
        Message.box(
            type: "2",
            message: message,
            top: 0,
            position: "0",
            context: context);
        // var dialog = CustomAlertDialog(
        //   type: "4",
        //   title: "",
        //   message: message,
        //   okBtnText: 'Close',
        // );
        // showDialog(context: context, builder: (BuildContext context) => dialog);

        return;
      }

      // String message;
      // message = await api.submit(carryNo, loc, widget.userid);
      List<ClsAgingCoolingStorage> list;
      list = await api.submit(carryNo, loc, widget.userid);

      // var dialog = CustomAlertDialog(
      //   type: (list[0].id == "200") ? "3" : "2",
      //   title: "",
      //   message: (list[0].id == "200")
      //       ? "Data saved successfully!"
      //       : list[0].description,
      //   okBtnText: 'Close',
      // );

      if (list[0].id == "200") {
        __deepsleepvalue = "0";
        List<ClsAgingCoolingStorage> listdeepsleep;
        listdeepsleep = await api.deepsleep(carryNo, loc);

        if (txtLampID.text == "") {
          iScanCarryNo.text = "";
          iLocAddressH.text = "";
          asynclistGetData("");

          iScanLocDest.text = "";
          iLocAddressD.text = "";

          FocusScope.of(context).requestFocus(nodeCarryNo);
        } else if (txtStatusLamp.text != "Stand By") {
          iScanCarryNo.text = "";
          iLocAddressH.text = "";
          asynclistGetData("");

          iScanLocDest.text = "";
          iLocAddressD.text = "";

          FocusScope.of(context).requestFocus(nodeCarryNo);
        } else if (listdeepsleep.length == 0) {
          iScanCarryNo.text = "";
          iLocAddressH.text = "";
          asynclistGetData("");

          iScanLocDest.text = "";
          iLocAddressD.text = "";

          FocusScope.of(context).requestFocus(nodeCarryNo);
        } else if (listdeepsleep[0].deepsleeptimehour == "0") {
          iScanCarryNo.text = "";
          iLocAddressH.text = "";
          asynclistGetData("");

          iScanLocDest.text = "";
          iLocAddressD.text = "";

          FocusScope.of(context).requestFocus(nodeCarryNo);
        } else {
          __deepsleepvalue = listdeepsleep[0].deepsleeptimehour;

          final controller = TextEditingController();
          showDialog(
            context: context,
            builder: (_) => MyDialog(controller),
          ).then((val) {
            //showDialog(
            //    context: context, builder: (BuildContext context) => dialog);
            Message.box(
                type: (list[0].id == "200") ? "1" : "2",
                message: (list[0].id == "200")
                    ? "Data saved successfully!"
                    : list[0].description,
                top: 0,
                position: "0",
                context: context);

            iScanCarryNo.text = "";
            iLocAddressH.text = "";
            asynclistGetData("");

            iScanLocDest.text = "";
            iLocAddressD.text = "";

            FocusScope.of(context).requestFocus(nodeCarryNo);
          });

          __statuscall = true;
          int _timer = 8000;
          List<ClsSyncLamp> _listgetdatatimer = [];
          _listgetdatatimer = await ClsSyncLamp.timer();
          if (_listgetdatatimer.length > 0) {
            __timer = Timer.periodic(Duration(milliseconds: _timer),
                (Timer t5) => __setDeepSleep("DeepSleep"));
          }

          // __timer = Timer.periodic(Duration(milliseconds: 1500),
          //     (Timer t5) => __setDeepSleep("DeepSleep"));
        }
      } else {
        Message.box(
            type: "2",
            message: list[0].description,
            top: 0,
            position: "0",
            context: context);
        // showDialog(context: context, builder: (BuildContext context) => dialog);
      }
    } catch (e) {
      // print('error caught: $e');
      var dialog = CustomAlertDialog(
        type: "2",
        title: "",
        message: e,
        okBtnText: 'Close',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
  }

  void submit() async {
    for (int i = 0; i < searchdata.length; i++) {
      print(searchdata[i].itemCode);
      print(searchdata[i].lotNo);
      print(searchdata[i].qty);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: const Text("Aging Cooling Storage"),
            ),
            body: //SingleChildScrollView(
                // padding: EdgeInsets.symmetric(
                // horizontal: 0,
                //),
                //child:
                Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
              SizedBox(height: 10.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "1. Scan Carry No",
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextFormField(
                  autofocus: true,
                  showCursor: true,
                  enableInteractiveSelection: false,
                  textInputAction: TextInputAction.none,
                  controller: iScanCarryNo,
                  focusNode: nodeCarryNo,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    labelText: '',
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          iScanCarryNo.clear();
                          _searchCarry = '';
                          searchdata.clear();

                          FocusScope.of(context).requestFocus(nodeCarryNo);
                        });
                      },
                      icon: Icon(_searchCarry == ''
                          ? Icons.cancel
                          : Icons.cancel_outlined),
                      color: _searchCarry == ''
                          ? Colors.white12.withOpacity(1)
                          : Colors.grey[600],
                    ),
                  ),
                  onChanged: (value) {
                    _searchCarry = value;
                    setState(() {});
                  },
                  onEditingComplete: () {
                    _getAddressLocation(iScanCarryNo.text, "0");
                    asynclistGetData(iScanCarryNo.text);
                  },

                  // controller: iScanCarryNo,
                  // focusNode: nodeCarryNo,
                  // decoration: InputDecoration(
                  //   border: OutlineInputBorder(),
                  //   contentPadding: new EdgeInsets.symmetric(
                  //       vertical: 10.0, horizontal: 10.0),
                  // ),
                  // onEditingComplete: () {
                  //   _getAddressLocation(iScanCarryNo.text, "0");
                  //   asynclistGetData(iScanCarryNo.text);
                  // },
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Location / Address",
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextFormField(
                  controller: iLocAddressH,
                  enabled: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    filled: true,
                    fillColor: disabledColor,
                  ),
                ),
              ),
              SizedBox(height: 10),
              Container(
                child: _dataTableWidget(),
                height: MediaQuery.of(context).size.height / 5,
              ),
              Divider(
                color: Colors.grey,
                height: 20,
              ),
              SizedBox(height: 10.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "2. Scan Location Destination",
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextFormField(
                  autofocus: true,
                  showCursor: true,
                  focusNode: nodeLocationDestination,
                  enableInteractiveSelection: false,
                  textInputAction: TextInputAction.none,
                  controller: iScanLocDest,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    labelText: '',
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          iScanLocDest.clear();
                          _scanLoc = '';
                          iLocAddressD.text = '';
                          searchdata.clear();
                          FocusScope.of(context)
                              .requestFocus(nodeLocationDestination);
                        });
                      },
                      icon: Icon(_scanLoc == ''
                          ? Icons.cancel
                          : Icons.cancel_outlined),
                      color: _scanLoc == ''
                          ? Colors.white12.withOpacity(1)
                          : Colors.grey[600],
                    ),
                  ),
                  onChanged: (value) {
                    _scanLoc = value;
                    setState(() {});
                  },
                  onEditingComplete: () {
                    _getAddressLocation(iScanLocDest.text, "1");
                  },
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Location / Address",
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Container(
                  //height: MediaQuery.of(context).size.height / 4.5,
                  child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextFormField(
                  controller: iLocAddressD,
                  enabled: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    filled: true,
                    fillColor: disabledColor,
                  ),
                ),
              )),
              Expanded(
                  child: SizedBox(
                      //height: MediaQuery.of(context).size.height,
                      )),
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 0),
                  child: Container(
                    //padding: EdgeInsets.only(top: 0),
                    width: MediaQuery.of(context).size.width * 0.90,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: FloatingActionButton.extended(
                      backgroundColor: (isProcessing)
                          ? disabledButtonColor
                          : enabledButtonColor,
                      // onPressed: () {
                      //   asyncsubmit(iScanCarryNo.text, iScanLocDest.text);
                      // },
                      onPressed: (isProcessing == false)
                          ? () async {
                              //if buttonenabled == true then pass a function otherwise pass "null"
                              asyncsubmit(iScanCarryNo.text, iScanLocDest.text);
                              //plug delayed for wait while processing data
                              await Future.delayed(const Duration(seconds: 3),
                                  () {
                                setState(() {
                                  isProcessing = false;
                                });
                              });
                            }
                          : null,
                      elevation: 0,
                      label: Text(
                        "SUBMIT",
                        style: TextStyle(
                            fontSize: 18.0,
                            color: (isProcessing)
                                ? disabledColorButtonText
                                : enabledColorButtonText),
                      ),
                    ),
                  )),
              SizedBox(
                height: 10,
              )
            ])));
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 2.5,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Item', MediaQuery.of(context).size.width / 2.5),
      _getTitleItemWidget('LotNo', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Unit', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget(' ', 20),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(searchdata[index].itemName),
      width: MediaQuery.of(context).size.width / 2.5,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].lotNo),
          width: MediaQuery.of(context).size.width / 2.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(value.format(double.parse(searchdata[index].qty))),
          width: MediaQuery.of(context).size.width / 4.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(searchdata[index].unit),
          width: MediaQuery.of(context).size.width / 3.8,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: 30,
          height: 45,
          child: IconButton(
            onPressed: () {
              _detail(iScanCarryNo.text, searchdata[index].itemCode,
                  searchdata[index].itemName, searchdata[index].lotNo);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            color: Colors.black,
            alignment: Alignment.centerRight,
          ),
        ),
        // Container(
        //   padding: EdgeInsets.fromLTRB(20, 0, 5, 0),
        //   width: 20,
        //   height: 45,
        //   child: IconButton(
        //     onPressed: () {
        //       _detail(iScanCarryNo.text, searchdata[index].itemCode,
        //           searchdata[index].itemName, searchdata[index].lotNo);
        //     },
        //     icon: Icon(
        //       Icons.arrow_forward_ios,
        //       size: 15,
        //     ),
        //     color: Colors.black,
        //     alignment: Alignment.centerRight,
        //   ),
        // ),
      ],
    );
  }
}

class MyDialog extends StatelessWidget {
  MyDialog(this.controller);

  final TextEditingController controller;

  void showAlert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 5), () {
      return;
    });
    // Future.delayed(Duration.zero, () => showAlert(context));
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}

/*
Container(
                  height: 200,
                  padding: const EdgeInsets.only(top: 10),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(
                        columnSpacing: 48.0,
                        decoration: BoxDecoration(
                            border: Border.all(
                          width: 1,
                          color: Colors.grey[300],
                        )),
                        headingRowColor: MaterialStateColor.resolveWith(
                            (states) => Colors.grey[200]),
                        sortColumnIndex: 0,
                        showCheckboxColumn: false,
                        columns: [
                          DataColumn(
                              label: Container(
                                padding: EdgeInsets.symmetric(horizontal: 0),
                                child: Text(
                                  "Item",
                                  style: TextStyle(
                                      fontFamily: "Arial",
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                ),
                              ),
                              numeric: false,
                              tooltip: "Item"),
                          DataColumn(
                              label: Container(
                                padding: EdgeInsets.symmetric(horizontal: 0),
                                child: Text(
                                  "Lot No",
                                  style: TextStyle(
                                      fontFamily: "Arial",
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                ),
                              ),
                              numeric: false,
                              tooltip: "Lot No"),
                          DataColumn(
                            label: Container(
                                child: Text(
                              "Qty",
                              style: TextStyle(
                                  fontFamily: "Arial",
                                  fontWeight: FontWeight.bold),
                            )),
                            numeric: false,
                            tooltip: "Qty",
                          ),
                          DataColumn(
                              label: Container(
                                  child: Text(
                                "Unit",
                                style: TextStyle(
                                    fontFamily: "Arial",
                                    fontWeight: FontWeight.bold),
                              )),
                              numeric: false,
                              tooltip: "Unit"),
                          DataColumn(
                              label: Container(width: 5, child: Text(""))),
                        ],
                        rows: searchdata
                            .map(
                              (idata) => DataRow(cells: [
                                DataCell(
                                    Container(
                                      //width: 80,
                                      child: Text(idata.itemName),
                                    ), onTap: () {
                                  _detail(iScanCarryNo.text, idata.itemCode,
                                      idata.itemName, idata.lotNo);
                                }),
                                //Container(width: 75, child: Text(idata.materialName)),
                                // ),
                                DataCell(
                                  Container(
                                    child: Text(idata.lotNo),
                                    alignment: Alignment.centerLeft,
                                  ),
                                ),
                                DataCell(
                                  Container(
                                    height: 42,
                                    decoration: BoxDecoration(
                                        color: Colors.yellow[100]),
                                    child: TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder:
                                              const OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                color: Color.fromRGBO(
                                                    88, 87, 94, 0.2),
                                                width: 0.0),
                                          ),
                                          border: const OutlineInputBorder(),
                                          //filled: true,
                                          //fillColor: Colors.yellow[200],
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 5.0,
                                                  horizontal: 10.0),
                                        ),
                                        controller: TextEditingController(
                                            text: idata.qty),
                                        keyboardType: TextInputType.number,
                                        textAlign: TextAlign.right,
                                        onFieldSubmitted: (val) {
                                          final newval = searchdata.firstWhere(
                                              (item) =>
                                                  item.itemCode ==
                                                      idata.itemCode &&
                                                  item.lotNo == idata.lotNo,
                                              orElse: () => null);
                                          if (newval != null)
                                            setState(() => newval.qty = val);

                                          print(searchdata);
                                        }),
                                    // Text(idata.qty),
                                    alignment: Alignment.centerRight,
                                  ),
                                ),
                                DataCell(
                                  Container(
                                    child: Text(idata.unit),
                                    alignment: Alignment.centerLeft,
                                  ),
                                ),
                                DataCell(
                                  Container(
                                    width: 5,
                                    child: IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                      ),
                                      color: Colors.black,
                                    ),
                                  ),
                                  onTap: () {
                                    _detail(iScanCarryNo.text, idata.itemCode,
                                        idata.itemName, idata.lotNo);
                                  },
                                  //IconButton(),
                                ),
                              ]),
                            )
                            .toList(),
                      ),
                    ),
                  ),
                ),

*/