import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsassigntocarrybylot.dart';
import 'package:panasonic/services/assigntocarrybylotservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/message.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Assign To Carry By Lot',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class AssignToCarryByLot extends StatefulWidget {
  const AssignToCarryByLot({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _AssignToCarryByLot createState() => _AssignToCarryByLot();
}

class _AssignToCarryByLot extends State<AssignToCarryByLot> {
  AssignToCarryByLotService api = AssignToCarryByLotService();

  TextEditingController iScanCarryFrom = TextEditingController();
  TextEditingController iCarryFromTemp = TextEditingController();
  TextEditingController iScanCarryTo = TextEditingController();
  TextEditingController iCarryToTemp = TextEditingController();
  TextEditingController iScanLot = TextEditingController();
  TextEditingController iSearch = TextEditingController();

  FocusNode nodeCarryNoFrom = FocusNode();
  FocusNode nodeCarryNoTo = FocusNode();
  FocusNode nodeScanLot = FocusNode();
  FocusNode nodeSearch = FocusNode();
  FocusNode nodeButton = FocusNode();

  String _scanCarryNoFrom = '';
  String _scanCarryNoTo = '';
  String _scanLot = '';
  String _search = '';

  List<ClsAssignToCarryByLot> listH;
  List<ClsAssignToCarryByLot> searchdata = [];

  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
  }

  void _getCarryName(String code, String call) async {
    List<ClsAssignToCarryByLot> list;
    list = await AssignToCarryByLotService.getCarryDesc(code);
    if (list.length > 0) {
      if (call == "0") {
        iScanCarryFrom.text = list[0].carryName;
        iCarryFromTemp.text = list[0].carryNo;
        FocusScope.of(context).requestFocus(nodeCarryNoTo);
        setState(() {});
      } else {
        iScanCarryTo.text = list[0].carryName;
        iCarryToTemp.text = list[0].carryNo;
        setState(() {});
      }
    } else {
      if (call == "0") {
        Message.box(
            type: "2",
            message: "Carry No From is not found",
            top: 0,
            position: "0",
            context: context);
        iCarryFromTemp.text = "";
        iScanCarryFrom.clear();
      } else {
        Message.box(
            type: "2",
            message: "Carry No To is not found",
            top: 0,
            position: "0",
            context: context);
        iCarryToTemp.text = "";
        iScanCarryTo.clear();
      }
    }
  }

  void asynclistGetData(
      String fromcarry, String tocarry, String scanlot) async {
    listH = await api.scanDataBarcode(
        fromcarry, tocarry, scanlot, widget.warehouse);
    if (listH[0].id == "200") {
      searchdata = listH;
    } else {
      Message.box(
          type: "2",
          message: listH[0].description,
          top: 0,
          position: "0",
          context: context);
    }
    setState(() {});
  }

  // void _detail(
  //     String _itemCode, String _lotNo,String _barcode) {
  //   setState(() {
  //     Navigator.of(context).push(
  //       MaterialPageRoute(
  //         builder: (context) => AssignToCarryByLotDetail(
  //             carryNo: carryNo,
  //             itemCode: _itemCode,
  //             itemName: _itemName,
  //             lotNo: _lotNo),
  //         settings: RouteSettings(),
  //       ),
  //     );
  //   });
  // }

  void asyncsubmit(String fromcarry, tocarry, String scanlot) async {
    try {
      setState(() {
        isProcessing = true;
      });
      if (fromcarry == "" || fromcarry == null) {
        String message = "Please scan carry no!";
        Message.box(
            type: "2",
            message: message,
            top: 0,
            position: "0",
            context: context);
        FocusScope.of(context).requestFocus(nodeCarryNoFrom);
        return;
      }

      if (tocarry == "" || tocarry == null) {
        String message = "Please scan carry no!";
        Message.box(
            type: "2",
            message: message,
            top: 0,
            position: "0",
            context: context);
        FocusScope.of(context).requestFocus(nodeCarryNoTo);
        return;
      }

      if (scanlot == "" || scanlot == null) {
        String message = "Please scan lot card!";
        Message.box(
            type: "2",
            message: message,
            top: 0,
            position: "0",
            context: context);
        return;
      }

      List<ClsAssignToCarryByLot> list;
      list = await AssignToCarryByLotService.submit(
          fromcarry, tocarry, scanlot, widget.userid, widget.warehouse);
      Message.box(
          type: (list[0].id == "200") ? "1" : "2",
          message: (list[0].id == "200")
              ? "Data saved successfully!"
              : list[0].description,
          top: 0,
          position: "0",
          context: context);

      iScanCarryFrom.clear();
      iCarryFromTemp.text = "";

      iScanCarryTo.clear();
      iCarryToTemp.text = "";

      iScanLot.clear();
      searchdata.clear();

      //asynclistGetData(fromcarry,tocarry,scanlot);

      FocusScope.of(context).requestFocus(nodeCarryNoFrom);
    } catch (e) {
      var dialog = CustomAlertDialog(
        type: "2",
        title: "",
        message: e,
        okBtnText: 'Close',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: kAppBarColor,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text("Assign To Carry By Lot"),
          ),
          body: Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "1. Scan From Carry ",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                autofocus: true,
                showCursor: true,
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                controller: iScanCarryFrom,
                focusNode: nodeCarryNoFrom,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iScanCarryFrom.clear();
                        _scanCarryNoFrom = '';
                        iCarryFromTemp.text = '';
                        searchdata.clear();
                        FocusScope.of(context).requestFocus(nodeCarryNoFrom);
                      });
                    },
                    icon: Icon(_scanCarryNoFrom == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _scanCarryNoFrom == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _scanCarryNoFrom = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  _getCarryName(iScanCarryFrom.text, "0");
                  //asynclistGetData(iScanCarryNo.text);
                  FocusScope.of(context).requestFocus(nodeCarryNoTo);
                },
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "2. Scan Destination Carry ",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                scrollPadding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                controller: iScanCarryTo,
                focusNode: nodeCarryNoTo,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iScanCarryTo.clear();
                        _scanCarryNoTo = '';
                        iCarryToTemp.text = '';
                      });
                    },
                    icon: Icon(_scanCarryNoTo == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _scanCarryNoTo == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _scanCarryNoTo = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  _getCarryName(iScanCarryTo.text, "1");
                  FocusScope.of(context).requestFocus(nodeScanLot);
                },
              ),
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: <Widget>[
            //     Container(
            //       width: MediaQuery.of(context).size.width / 2,
            //       padding: EdgeInsets.symmetric(horizontal: 10),
            //       child: TextFormField(
            //         autofocus: true,
            //         showCursor: true,
            //         enableInteractiveSelection: false,
            //         textInputAction: TextInputAction.none,
            //         controller: iScanCarryFrom,
            //         focusNode: nodeCarryNoFrom,
            //         decoration: InputDecoration(
            //           border: OutlineInputBorder(),
            //           contentPadding: new EdgeInsets.symmetric(
            //               vertical: 10.0, horizontal: 10.0),
            //           labelText: '',
            //           suffixIcon: IconButton(
            //             onPressed: () {
            //               setState(() {
            //                 iScanCarryFrom.clear();
            //                 _scanCarryNoFrom = '';
            //                 iCarryNameFrom.text = '';
            //                 searchdata.clear();
            //                 FocusScope.of(context)
            //                     .requestFocus(nodeCarryNoFrom);
            //               });
            //             },
            //             icon: Icon(_scanCarryNoFrom == ''
            //                 ? Icons.cancel
            //                 : Icons.cancel_outlined),
            //             color: _scanCarryNoFrom == ''
            //                 ? Colors.white12.withOpacity(1)
            //                 : Colors.grey[600],
            //           ),
            //         ),
            //         onChanged: (value) {
            //           _scanCarryNoFrom = value;
            //           setState(() {});
            //         },
            //         onEditingComplete: () {
            //           _getCarryName(iScanCarryFrom.text, "0");
            //           //asynclistGetData(iScanCarryNo.text);
            //           FocusScope.of(context).requestFocus(nodeCarryNoTo);
            //         },
            //       ),
            //     ),
            //     Container(
            //       width: MediaQuery.of(context).size.width / 2,
            //       padding: EdgeInsets.symmetric(horizontal: 10),
            //       child: TextFormField(
            //         controller: iCarryNameFrom,
            //         enabled: false,
            //         decoration: InputDecoration(
            //           border: OutlineInputBorder(),
            //           contentPadding: new EdgeInsets.symmetric(
            //               vertical: 10.0, horizontal: 10.0),
            //           filled: true,
            //           fillColor: disabledColor,
            //         ),
            //       ),
            //     ),
            //   ],
            // ),
            // SizedBox(height: 5.0),
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 20),
            //   child: Align(
            //     alignment: Alignment.centerLeft,
            //     child: Text(
            //       "2. Scan Destination Carry ",
            //       style: TextStyle(
            //           fontFamily: 'Arial', fontWeight: FontWeight.bold),
            //     ),
            //   ),
            // ),
            // SizedBox(height: 5.0),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: <Widget>[
            //     Container(
            //       width: MediaQuery.of(context).size.width / 2,
            //       padding: EdgeInsets.symmetric(horizontal: 10),
            //       child: TextFormField(
            //         scrollPadding: EdgeInsets.only(
            //             bottom: MediaQuery.of(context).viewInsets.bottom),
            //         enableInteractiveSelection: false,
            //         textInputAction: TextInputAction.none,
            //         controller: iScanCarryTo,
            //         focusNode: nodeCarryNoTo,
            //         decoration: InputDecoration(
            //           border: OutlineInputBorder(),
            //           contentPadding: new EdgeInsets.symmetric(
            //               vertical: 10.0, horizontal: 10.0),
            //           labelText: '',
            //           suffixIcon: IconButton(
            //             onPressed: () {
            //               setState(() {
            //                 iScanCarryTo.clear();
            //                 _scanCarryNoTo = '';
            //                 iCarryNameTo.text = '';
            //               });
            //             },
            //             icon: Icon(_scanCarryNoTo == ''
            //                 ? Icons.cancel
            //                 : Icons.cancel_outlined),
            //             color: _scanCarryNoTo == ''
            //                 ? Colors.white12.withOpacity(1)
            //                 : Colors.grey[600],
            //           ),
            //         ),
            //         onChanged: (value) {
            //           _scanCarryNoTo = value;
            //           setState(() {});
            //         },
            //         onEditingComplete: () {
            //           _getCarryName(iScanCarryTo.text, "1");
            //           FocusScope.of(context).requestFocus(nodeScanLot);
            //         },
            //       ),
            //     ),
            //     Container(
            //       width: MediaQuery.of(context).size.width / 2,
            //       padding: EdgeInsets.symmetric(horizontal: 10),
            //       child: TextFormField(
            //         controller: iCarryNameTo,
            //         enabled: false,
            //         decoration: InputDecoration(
            //           border: OutlineInputBorder(),
            //           contentPadding: new EdgeInsets.symmetric(
            //               vertical: 10.0, horizontal: 10.0),
            //           filled: true,
            //           fillColor: disabledColor,
            //         ),
            //       ),
            //     ),
            //   ],
            // ),
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "3. Scan Lot Card ",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                scrollPadding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                controller: iScanLot,
                focusNode: nodeScanLot,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iScanLot.clear();
                        _scanLot = '';
                      });
                    },
                    icon: Icon(
                        _scanLot == '' ? Icons.cancel : Icons.cancel_outlined),
                    color: _scanLot == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _scanLot = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  asynclistGetData(
                      iCarryFromTemp.text, iCarryToTemp.text, iScanLot.text);
                  FocusScope.of(context).requestFocus(nodeSearch);
                  setState(() {});
                },
              ),
            ),
            Divider(color: Colors.grey, height: 20),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                  controller: iSearch,
                  decoration: InputDecoration(
                    labelText: 'Search',
                    prefixIcon: IconButton(
                      onPressed: () {},
                      icon: Icon(_search == '' ? Icons.search : Icons.search),

                      //Icons.search,
                    ),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          iSearch.clear();
                          _search = '';
                          searchdata = listH;
                        });
                      },
                      icon: Icon(
                          _search == '' ? Icons.cancel : Icons.cancel_outlined),
                      color: _search == ''
                          ? Colors.white12.withOpacity(1)
                          : Colors.grey[600],
                    ),
                    border: OutlineInputBorder(),
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                  ),
                  onChanged: (value) {
                    setState(() {
                      _search = value;
                      searchdata = listH
                          .where((user) =>
                              user.barcode
                                  .toLowerCase()
                                  .contains(_search.toLowerCase()) ||
                              user.itemName
                                  .toLowerCase()
                                  .contains(_search.toLowerCase()) ||
                              user.lotNo
                                  .toLowerCase()
                                  .contains(_search.toLowerCase()) ||
                              user.qty
                                  .toLowerCase()
                                  .contains(_search.toLowerCase()))
                          .toList();
                    });
                  }),
            ),
            SizedBox(height: 10),
            Expanded(
              //height: MediaQuery.of(context).size.height * 0.55,
              child: _dataTableWidget(),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: (isProcessing)
                          ? disabledButtonColor
                          : enabledButtonColor,
                      minimumSize:
                          Size(MediaQuery.of(context).size.width * 0.90, 55),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                    ),
                    // onPressed: () {
                    //   asyncsubmit(iCarryFromTemp.text, iCarryToTemp.text,
                    //       iScanLot.text);
                    // },
                    onPressed: (isProcessing == false)
                        ? () async {
                            //if buttonenabled == true then pass a function otherwise pass "null"
                            asyncsubmit(iCarryFromTemp.text, iCarryToTemp.text,
                                iScanLot.text);
                            //plug delayed for wait while processing data
                            await Future.delayed(const Duration(seconds: 3),
                                () {
                              setState(() {
                                isProcessing = false;
                              });
                            });
                          }
                        : null,
                    child: Text(
                      "SUBMIT",
                      style: TextStyle(
                        fontSize: 18,
                        color: (isProcessing)
                            ? disabledColorButtonText
                            : enabledColorButtonText,
                      ),
                    ),
                  ),
                  // ),
                ]),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
          ])),
    );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Barcode No', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Item', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(' ', 20),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(searchdata[index].barcode),
      width: MediaQuery.of(context).size.width / 3,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].itemName),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(searchdata[index].lotNo),
          width: MediaQuery.of(context).size.width / 2.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(value.format(double.parse(searchdata[index].qty))),
          width: MediaQuery.of(context).size.width / 4.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        // Container(
        //   width: 30,
        //   height: 45,
        //   child: IconButton(
        //     onPressed: () {
        //       _detail(iScanCarryNo.text, searchdata[index].itemCode,
        //           searchdata[index].itemName, searchdata[index].lotNo);
        //     },
        //     icon: Icon(
        //       Icons.arrow_forward_ios,
        //       size: 15,
        //     ),
        //     color: Colors.black,
        //     alignment: Alignment.centerRight,
        //   ),
        // ),
      ],
    );
  }
}
