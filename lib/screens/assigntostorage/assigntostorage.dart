import 'package:flutter/material.dart';
import 'package:panasonic/models/clsassigntostorage.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/services/assigntostorageservice.dart';
import 'package:panasonic/utilities/message.dart';

class AssignToStorage extends StatefulWidget {
  const AssignToStorage({Key key, this.userid, this.warehouse})
      : super(key: key);
  final String userid;
  final String warehouse;

  @override
  _AssignToStorageState createState() => _AssignToStorageState();
}

class _AssignToStorageState extends State<AssignToStorage> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  AssignToStorageService api = AssignToStorageService();

  List<ClsAssignToStorage> listGetData = [];
  List<ClsAssignToStorage> listGetDataFiltered = [];
  List<ClsAssignToStorage> listData = [];

  List<ClsAssignToStorage> listLocationCarry;
  List<ClsAssignToStorage> listLocationCarryFiltered;

  final TextEditingController txtLocationCarry = new TextEditingController();
  final TextEditingController txtLocCarryName = new TextEditingController();
  final TextEditingController txtRequestDate = new TextEditingController();
  final TextEditingController txtBarcode = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();

  FocusNode nodeBarcode = FocusNode();
  FocusNode nodeLoc = FocusNode();

  String _searchLocCarry = '';
  String _searchDataResult = '';
  // ignore: unused_field
  String _searchLocationCarryResult = '';
  String _scanBarcode = '';
  //String _selectedLocationCarry = '';
  //String _scanBarcode = '';
  bool isProcessing = false;

  String message;
  String type;

  @override
  void initState() {
    // super.initState();
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd MMMM yyyy');
    final String formatted = formatter.format(now);
    txtRequestDate.text = formatted;

    super.initState();
  }

  // void asynclistGetData() async {
  //   listGetData = await api.getData(_selectedLocationCarry);
  //   listGetDataFiltered = listGetData;
  //   setState(() {});
  // }

  void asyncScanBarcode(
      String fromwarehouse, String locationCarry, String barcode) async {
    listData =
        (await api.scanDataBarcode(fromwarehouse, locationCarry, barcode));

    for (int i = 0; i < listGetData.length; i++) {
      if (listData[0].barcode == listGetData[i].barcode) {
        //asyncAlert("barcode has been scanned", "4");
        Message.box(
            type: "2",
            message: "barcode has been scanned",
            top: 0,
            position: "0",
            context: context);
        txtBarcode.clear();
        return;
      }
    }

    ClsAssignToStorage tempData;
    if (listData[0].id == "200") {
      tempData = ClsAssignToStorage(
          fromLocation: listData[0].fromLocation,
          warehouseCode: widget.warehouse,
          locationCarryCode: locationCarry,
          barcode: listData[0].barcode,
          itemCode: listData[0].itemCode,
          itemName: listData[0].itemName,
          lotNo: listData[0].lotNo,
          qty: listData[0].qty);
      type = "3";
      message = "";
    } else if (listData[0].id == "400") {
      message = listData[0].description;
      type = '2';
    } else {
      message = listData[0].description;
      type = '2';
    }

    if ((message != "") && (type != "")) {
      Message.box(
          type: type,
          message: message,
          top: 0,
          position: "0",
          context: context);

      // var dialog = CustomAlertDialog(
      //   type: type,
      //   title: '',
      //   message: message,
      //   okBtnText: '',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
      txtBarcode.clear();
    }

    setState(() {
      if (message == "") {
        listGetData.add(tempData);
        listGetDataFiltered = listGetData;
      }
      message = "";
      type = "";
      txtBarcode.clear();
    });
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void asyncsubmit(String locationCarry) async {
    setState(() {
      isProcessing = true;
    });
    if (listData.length == 0) {
      // asyncAlert("Data is empty", "2");
      Message.box(
          type: "2",
          message: "Data is empty",
          top: 0,
          position: "0",
          context: context);
      return;
    }

    for (int index = 0; index < listGetData.length; index++) {
      final newval = listGetDataFiltered.firstWhere(
          (item) =>
              item.warehouseCode == listGetDataFiltered[index].warehouseCode &&
              item.barcode == listGetDataFiltered[index].barcode &&
              item.itemCode == listGetDataFiltered[index].itemCode &&
              item.lotNo == listGetDataFiltered[index].lotNo,
          orElse: () => null);
      if (newval != null)
        setState(() {
          newval.locationCarryCode = locationCarry.toString();
        });
    }

    try {
      if (locationCarry == "") {
        //asyncAlert("Please select Location / Carry No", "2");
        Message.box(
            type: "2",
            message: "Please select Location / Carry No",
            top: 0,
            position: "0",
            context: context);
        //FocusScope.of(context).requestFocus(nodeLoc);
        return;
      }

      if (listGetDataFiltered.length == 0 || listGetDataFiltered.isEmpty) {
        Message.box(
            type: "2",
            message: "Please scan barcode",
            top: 0,
            position: "0",
            context: context);
        //FocusScope.of(context).requestFocus(nodeLoc);
        return;
      }

      listData = await AssignToStorageService.submitData(
          listGetDataFiltered, widget.userid);
      if (listData[0].id == "200") {
        //asyncAlert(listData[0].description, "3");
        Message.box(
            type: "1",
            message: listData[0].description,
            top: 0,
            position: "0",
            context: context);

        setState(() {
          txtBarcode.clear();
          listGetDataFiltered.clear();
          txtLocationCarry.clear();
          txtLocCarryName.text = "";
          FocusScope.of(context).requestFocus(nodeLoc);
        });
      } else {
        Message.box(
            type: "2",
            message: listData[0].description,
            top: 0,
            position: "0",
            context: context);
        // listGetDataFiltered.clear();
        //asyncAlert(listData[0].description, "2");
      }
    } catch (e) {
      asyncAlert(e.toString(), "2");
    }
  }

  // void asyncSubmit() async {
  //   listData = await ClsAssignToStorage.submit(_selectedLocationCarry);
  //   // if (listData[0].ID == "200") {
  //   //   asynclistGetData();
  //   //   _onBasicAlertPressed(context, "SUCCESS", "Submit data successfully");
  //   //   txtBarcode.text = '';
  //   // } else if (listData[0].ID == "400") {
  //   //   _onBasicAlertPressed(context, "WARNING", listData[0].Description);
  //   // } else {
  //   //   _onBasicAlertPressed(context, "WARNING", listData[0].Description);
  //   // }
  //   setState(() {});
  // }

  @override
  Widget build(BuildContext context) {
    listGetData ??= <ClsAssignToStorage>[];

    var nodeLoc2 = nodeLoc;
    var txtLocationCarry2 = this.txtLocationCarry;
    var txtLocationCarry22 = txtLocationCarry2;
    var txtLocationCarry222 = txtLocationCarry22;
    // ignore: unused_local_variable
    var textFieldConfiguration2 = TextFieldConfiguration(
        textInputAction: TextInputAction.done,
        onSubmitted: (value) {
          final newval = listLocationCarryFiltered.firstWhere(
              (item) => item.locationCarryCode == value,
              orElse: () => null);

          txtLocationCarry.text = value;
          _searchLocCarry = value;
          listLocationCarryFiltered
              .where((user) => user.locationCarryCode
                  .toLowerCase()
                  .contains(value.toLowerCase()))
              .toList();

          //return listLocationCarryFiltered;

          setState(() {
            txtLocCarryName.text = newval.locationCarryDescription;
            FocusScope.of(context).requestFocus(nodeBarcode);
          });
        },
        focusNode: nodeLoc2,
        controller: txtLocationCarry222,
        style: TextStyle(fontFamily: 'Arial'),
        decoration: InputDecoration(
          suffixIcon: IconButton(
            onPressed: () {
              setState(() {
                txtLocationCarry.clear();
                _searchLocCarry = '';
                txtLocCarryName.text = '';
                FocusScope.of(context).requestFocus(nodeLoc2);
              });
            },
            icon: Icon(
                _searchLocCarry == '' ? Icons.cancel : Icons.cancel_outlined),
            color: _searchLocCarry == ''
                ? Colors.white12.withOpacity(1)
                : Colors.grey[600],
          ),
          border: OutlineInputBorder(),
          contentPadding:
              new EdgeInsets.symmetric(vertical: -10.0, horizontal: 10.0),
        ));
    var textStyle = TextStyle(fontFamily: 'Arial', fontWeight: FontWeight.bold);

    //var sizedBox = SizedBox(height: MediaQuery.of(context).size.height * 0.01);
    var children2 = <Widget>[
      Flexible(
        flex: 1,
        child: Row(
          children: <Widget>[
            Flexible(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                  child: Form(
                    key: formkey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text("1. Select Location / Carry No",
                                style: textStyle),
                          ),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.0),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: TextFormField(
                            autofocus: true,
                            showCursor: true,
                            focusNode: nodeLoc,
                            enableInteractiveSelection: false,
                            textInputAction: TextInputAction.none,
                            controller: txtLocationCarry,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              contentPadding: new EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              labelText: '',
                              suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    txtLocationCarry.clear();
                                    txtLocCarryName.text = '';
                                    _searchLocCarry = '';
                                    //listGetDataFiltered.clear();
                                    FocusScope.of(context)
                                        .requestFocus(nodeLoc);
                                  });
                                },
                                icon: Icon(_searchLocCarry == ''
                                    ? Icons.cancel
                                    : Icons.cancel_outlined),
                                color: _searchLocCarry == ''
                                    ? Colors.white12.withOpacity(1)
                                    : Colors.grey[600],
                              ),
                            ),
                            onChanged: (value) {
                              _searchLocCarry = value;
                              setState(() {});
                            },
                            onFieldSubmitted: (value) async {
                              listLocationCarry = await AssignToStorageService
                                  .getLocationCarryDesc(value);

                              // final newval = listLocationCarry.firstWhere(
                              //     (item) => item.locationCarryCode == value,
                              //     orElse: () => null);

                              // txtLocationCarry.text = value;
                              // _searchLocCarry = value;
                              // listLocationCarry
                              //     .where((user) => user.locationCarryCode
                              //         .toLowerCase()
                              //         .contains(value.toLowerCase()))
                              //     .toList();

                              //return listLocationCarryFiltered;

                              setState(() {
                                if (listLocationCarry.length > 0) {
                                  txtLocCarryName.text = listLocationCarry[0]
                                      .locationCarryDescription;
                                  FocusScope.of(context)
                                      .requestFocus(nodeBarcode);
                                } else {
                                  Message.box(
                                      type: "2",
                                      message: "location not found",
                                      top: 0,
                                      position: "0",
                                      context: context);
                                  txtLocationCarry.clear();
                                  FocusScope.of(context).requestFocus(nodeLoc);
                                }
                              });
                            },
                            onEditingComplete: () async {
                              // listLocationCarry = await AssignToStorageService
                              //     .getLocationCarryDesc(txtLocationCarry.text);

                              // if (listLocationCarry.length > 0) {
                              //   txtLocCarryName.text = listLocationCarry[0]
                              //       .locationCarryDescription;
                              // }
                              // setState(() {
                              //   if (listLocationCarry.length > 0) {
                              //     txtLocCarryName.text = listLocationCarry[0]
                              //         .locationCarryDescription;
                              //     FocusScope.of(context)
                              //         .requestFocus(nodeBarcode);
                              //   }
                              // });
                            },
                          ),
                        ),
                        //   child: TypeAheadField(
                        //     hideSuggestionsOnKeyboardHide: false,
                        //     textFieldConfiguration: textFieldConfiguration2,
                        //     suggestionsCallback: (pattern) async {
                        //       listLocationCarry =
                        //           await AssignToStorageService.getLocationCarry(
                        //               "ALL");
                        //       _searchLocationCarryResult = pattern;
                        //       _searchcombo = pattern;
                        //       listLocationCarryFiltered = listLocationCarry
                        //           .where((user) =>
                        //               user.locationCarryCode
                        //                   .toLowerCase()
                        //                   .contains(_searchLocationCarryResult
                        //                       .toLowerCase()) ||
                        //               user.locationCarryDescription
                        //                   .toLowerCase()
                        //                   .contains(_searchLocationCarryResult
                        //                       .toLowerCase()))
                        //           .toList();
                        //       return listLocationCarryFiltered;
                        //     },
                        //     itemBuilder: (context, suggestion) {
                        //       return ListTile(
                        //         title: Text(suggestion.locationCarryCode),
                        //         subtitle: Text(
                        //             '${suggestion.locationCarryDescription}'),
                        //       );
                        //     },
                        //     onSuggestionSelected: (suggestion) async {
                        //       // asyncSelectLocationCarry();
                        //       this.txtLocationCarry.text =
                        //           suggestion.locationCarryCode;
                        //       this.txtLocCarryName.text =
                        //           suggestion.locationCarryDescription;

                        //       _searchcombo = suggestion.locationCarryCode;

                        //       // _selectedLocationCarry =
                        //       //     suggestion.locationCarryCode;
                        //       FocusScope.of(context).requestFocus(nodeBarcode);
                        //     },
                        //   ),
                        // ),
                        SizedBox(height: 5.0),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Location / Carry Name",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        SizedBox(height: 5.0),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: TextFormField(
                            controller: txtLocCarryName,
                            enabled: false,
                            //style: TextStyle(color: disabledColor),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              contentPadding: new EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              filled: true,
                              fillColor: disabledColor,
                            ),
                          ),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                            child: Text(
                              "Request Date",
                              style: TextStyle(
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.0),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: TextFormField(
                            controller: txtRequestDate,
                            enabled: false,
                            //style: TextStyle(color: disabledColor),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              contentPadding: new EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              filled: true,
                              fillColor: disabledColor,
                            ),
                          ),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "2. Scan Barcode",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.00),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: TextFormField(
                            focusNode: nodeBarcode,
                            enableInteractiveSelection: false,
                            textInputAction: TextInputAction.none,
                            controller: txtBarcode,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              contentPadding: new EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              labelText: '',
                              suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    txtBarcode.clear();
                                    _scanBarcode = '';
                                    //listGetDataFiltered.clear();
                                    FocusScope.of(context)
                                        .requestFocus(nodeBarcode);
                                  });
                                },
                                icon: Icon(_scanBarcode == ''
                                    ? Icons.cancel
                                    : Icons.cancel_outlined),
                                color: _scanBarcode == ''
                                    ? Colors.white12.withOpacity(1)
                                    : Colors.grey[600],
                              ),
                            ),
                            onChanged: (value) {
                              _scanBarcode = value;
                              setState(() {});
                            },
                            onFieldSubmitted: (value) {
                              //_scanBarcode = value;
                              asyncScanBarcode(widget.warehouse,
                                  txtLocationCarry.text, txtBarcode.text);
                              FocusScope.of(context).requestFocus(nodeBarcode);

                              // print(value);
                            },
                            onEditingComplete: () {
                              print('value');
                            },
                          ),
                        ),
                        Divider(color: Colors.grey, height: 20),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: TextFormField(
                              controller: txtSearch,
                              decoration: InputDecoration(
                                labelText: 'Search',
                                prefixIcon: IconButton(
                                  onPressed: () {},
                                  icon: Icon(_searchDataResult == ''
                                      ? Icons.search
                                      : Icons.search),

                                  //Icons.search,
                                ),
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      txtSearch.clear();
                                      _searchDataResult = '';
                                      listGetDataFiltered = listGetData;
                                    });
                                  },
                                  icon: Icon(_searchDataResult == ''
                                      ? Icons.cancel
                                      : Icons.cancel_outlined),
                                  color: _searchDataResult == ''
                                      ? Colors.white12.withOpacity(1)
                                      : Colors.grey[600],
                                ),
                                border: OutlineInputBorder(),
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                              ),
                              onChanged: (value) {
                                setState(() {
                                  _searchDataResult = value;
                                  listGetDataFiltered = listGetData
                                      .where((user) =>
                                          user.barcode.toLowerCase().contains(
                                              _searchDataResult
                                                  .toLowerCase()) ||
                                          user.itemName.toLowerCase().contains(
                                              _searchDataResult
                                                  .toLowerCase()) ||
                                          user.lotNo.toLowerCase().contains(
                                              _searchDataResult
                                                  .toLowerCase()) ||
                                          user.qty.toLowerCase().contains(
                                              _searchDataResult.toLowerCase()))
                                      .toList();
                                });
                              }),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01),
                        Expanded(
                          // height: MediaQuery.of(context).size.height * 0.4,
                          child: Container(child: _dataTableWidget()),
                          // SingleChildScrollView(
                          //   scrollDirection: Axis.horizontal,
                          //   child: SingleChildScrollView(
                          //     scrollDirection: Axis.vertical,
                          //     // children: <Widget>[
                          //     child: DataTable(
                          //         columns: const <DataColumn>[
                          //           DataColumn(
                          //             label: Text(
                          //               'Barcode No',
                          //               style: TextStyle(
                          //                 fontFamily: 'Arial',
                          //                 fontWeight: FontWeight.bold,
                          //               ),
                          //             ),
                          //           ),
                          //           DataColumn(
                          //             label: Text(
                          //               'Item',
                          //               style: TextStyle(
                          //                 fontFamily: 'Arial',
                          //                 fontWeight: FontWeight.bold,
                          //               ),
                          //             ),
                          //           ),
                          //           DataColumn(
                          //             label: Text(
                          //               'Lot No',
                          //               style: TextStyle(
                          //                 fontFamily: 'Arial',
                          //                 fontWeight: FontWeight.bold,
                          //               ),
                          //             ),
                          //           ),
                          //           DataColumn(
                          //             label: Text(
                          //               'Qty',
                          //               style: TextStyle(
                          //                 fontFamily: 'Arial',
                          //                 fontWeight: FontWeight.bold,
                          //               ),
                          //             ),
                          //           ),
                          //         ],
                          //         rows: List.generate(
                          //           listGetDataFiltered.length,
                          //           (index) => DataRow(
                          //             cells: <DataCell>[
                          //               DataCell(Text(
                          //                   listGetDataFiltered[index]
                          //                       .Barcode)),
                          //               DataCell(Text(
                          //                   listGetDataFiltered[index]
                          //                       .Item)),
                          //               DataCell(Text(
                          //                   listGetDataFiltered[index]
                          //                       .Lot)),
                          //               DataCell(Text(
                          //                   listGetDataFiltered[index]
                          //                       .Qty)),
                          //             ],
                          //           ),
                          //         )),
                          //     // ]
                          //   ),
                          // ),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.02),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              ElevatedButton(
                                onPressed: (isProcessing == false)
                                    ? () async {
                                        //if buttonenabled == true then pass a function otherwise pass "null"
                                        asyncsubmit(txtLocationCarry.text);
                                        //plug delayed for wait while processing data
                                        await Future.delayed(
                                            const Duration(seconds: 3), () {
                                          setState(() {
                                            isProcessing = false;
                                          });
                                        });
                                      }
                                    : null,
                                style: TextButton.styleFrom(
                                  backgroundColor: (isProcessing)
                                      ? disabledButtonColor
                                      : enabledButtonColor,
                                  minimumSize: Size(
                                      MediaQuery.of(context).size.width * 0.90,
                                      55),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50.0),
                                  ),
                                ),
                                child: Text(
                                  "SUBMIT",
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: (isProcessing)
                                          ? disabledColorButtonText
                                          : enabledColorButtonText),
                                ),
                              )
                              // TextButton(
                              //   style: TextButton.styleFrom(
                              //     backgroundColor: Colors.blue[800],
                              //     minimumSize: Size(
                              //         MediaQuery.of(context).size.width * 0.90,
                              //         55),
                              //     shape: RoundedRectangleBorder(
                              //       borderRadius: BorderRadius.circular(50.0),
                              //     ),
                              //   ),

                              //   onPressed: null, // () async {

                              //   // (_isButtonDisabled == true)
                              //   //     ? asyncsubmit(txtLocationCarry.text)
                              //   //     // ignore: unnecessary_statements
                              //   //     : null;
                              //   //},
                              //   child: Text(
                              //     "SUBMIT",
                              //     style: TextStyle(
                              //       fontSize: 18,
                              //       color: Colors.white,
                              //     ),
                              //   ),
                              // ),
                            ]),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01)
                      ],
                    ),
                  ),
                )),
          ],
        ),
      ),
    ];
    var willPopScope = WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text("Assign To Storage"),
            ),
            body: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: children2)));
    var willPopScope2 = willPopScope;
    return willPopScope2;
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 1),
      _getTitleItemWidget('Barcode No', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('Item', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 4),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].barcode),
      width: 0,
      //height: 45,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(listGetDataFiltered[index].barcode),
          width: MediaQuery.of(context).size.width / 4,
          //height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(listGetDataFiltered[index].itemName),
          width: MediaQuery.of(context).size.width / 4,
          //height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(listGetDataFiltered[index].lotNo),
          width: MediaQuery.of(context).size.width / 3,
          //height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(listGetDataFiltered[index].qty),
          ),
          width: MediaQuery.of(context).size.width / 6,
          //height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
