import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsbarcodetracebility.dart';
import 'package:panasonic/services/barcodetracebilityservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/message.dart';
import 'package:intl/intl.dart';

class BarcodeTracebility extends StatefulWidget {
  const BarcodeTracebility({Key key, String userid, String warehouse})
      : super(key: key);

  @override
  State<BarcodeTracebility> createState() => _BarcodeTracebilityState();
}

class _BarcodeTracebilityState extends State<BarcodeTracebility> {
  BarcodeTracebilityService api = new BarcodeTracebilityService();
  TextEditingController barcodeController = TextEditingController();
  TextEditingController stockController = TextEditingController();
  List<ClsBarcodeTracebility> listConsumpt = [];
  String currentStock = "0";

  final value = new NumberFormat("#,##0.00", "en_US");

  void getDataBarcode(pBarcode) async {
    // listConsumpt = [];
    if (pBarcode == "") {
      Message.box(
          type: '2',
          message: 'Please scan Barcode No',
          top: 0,
          position: '0',
          context: context);

      return;
    }

    try {
      currentStock = await api.getCurrentStock(pBarcode);      
      stockController.text = value.format(double.parse(currentStock));
      
      List<ClsBarcodeTracebility> response = await api.getDataDetail(pBarcode);
      if (response.length > 0) {
        listConsumpt = response;
        setState(() {
          
        });
      } else {
        Message.box(
            type: '2',
            message: "Data Consumption didn't exists",
            top: 0,
            position: '0',
            context: context);
        return;
      }
    } catch (e) {
      var dialog = CustomAlertDialog(
        type: '2',
        title: '',
        message: e.toString(),
        okBtnText: '',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: Text("Barcode Tracebility"),
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
            child: 
            Text(
              "Scan Barcode",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: TextFormField(
              controller: barcodeController,
              autofocus: true,
              autocorrect: false,
              enableSuggestions: false,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.all(10),
                  suffixIcon: IconButton(
                    icon: Icon(barcodeController.text == ""
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    onPressed: () {
                      barcodeController.clear();
                      stockController.clear();
                      listConsumpt = [];
                      setState(() {});
                    },
                  )),
              onEditingComplete: () {
                listConsumpt.clear();
                setState(() {
                  getDataBarcode(barcodeController.text);
                });                

                FocusScope.of(context).unfocus();
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
            child: 
            Text(
              "Current Stock",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: TextFormField(
              controller: stockController,
              autocorrect: false,
              enableSuggestions: false,
              enabled: false,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.all(10),
                    fillColor: disabledColor,
                    filled: true              
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(child: _dataTable()),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.02,
          )
        ]),
      ),
    );
  }

  Widget _dataTable() {
    return Container(
      child: HorizontalDataTable(
        isFixedHeader: true,
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        rowSeparatorWidget: Divider(color: Colors.black54, height: 1),
        headerWidgets: _headers(),
        leftSideChildren: [],
        rightSideItemBuilder: _rightRows,
        itemCount: listConsumpt.length,
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _headers() {
    return [
      _header("", 0),
      // _header("", 0),

      _header("Date", MediaQuery.of(context).size.width / 4),
      _header("Qty", MediaQuery.of(context).size.width / 4),
      _header("Used", MediaQuery.of(context).size.width / 4),
      _header("Remaining", MediaQuery.of(context).size.width / 4),
      // _header("Current Stock Qty", MediaQuery.of(context).size.width * 3 / 4),
      // _header(currentStock, MediaQuery.of(context).size.width / 4)
    ];
  }

  Widget _header(String label, double width) {
    return Container(
      child: Text(
        label,
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
      ),
      width: width,
      height: 45,
      padding: EdgeInsets.all(5),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(top: BorderSide(color: Colors.grey, width: 1))),
    );
  }

  Widget _rightRows(BuildContext context, int index) {
    if(listConsumpt.length > 0){
      
      return Row(
        children: [
          _row(listConsumpt[index].ConsumptDate,
              MediaQuery.of(context).size.width / 4),
          _row(value.format(double.parse(listConsumpt[index].Qty)), MediaQuery.of(context).size.width / 4),
          _row(
              value.format(double.parse(listConsumpt[index].QtyUsed)), MediaQuery.of(context).size.width / 4),
          _row(value.format(double.parse(listConsumpt[index].QtyRemaining)),
              MediaQuery.of(context).size.width / 4),
        ],
      );
    } 
  }

  Widget _row(String label, double width) {
    return Container(
      child: Text(
        label,
        style: TextStyle(color: Colors.black),
      ),
      width: width,
      height: 45,
      padding: EdgeInsets.all(5),
      alignment: Alignment.center,
    );
  }
}
