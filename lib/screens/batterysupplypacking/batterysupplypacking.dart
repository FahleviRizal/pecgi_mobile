import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsbatterysupplypacking.dart';
import 'package:panasonic/services/batterysupplypackingservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/message.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Battery Supply To Packing',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class BatterySupplyPacking extends StatefulWidget {
  const BatterySupplyPacking({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _BatterySupplyPacking createState() => _BatterySupplyPacking();
}

class _BatterySupplyPacking extends State<BatterySupplyPacking> {
  BatterySupplyPackingService api = BatterySupplyPackingService();

  final TextEditingController txtSupplyNo = new TextEditingController();
  TextEditingController txtCarryNo = TextEditingController();

  String _searchSupplyNo = '';

  FocusNode nodeSupplyNo = FocusNode();
  FocusNode nodeCarryNo = FocusNode();

  String _scanCarryNo = '';

  List<ClsBatterySupplyPacking> listSupplyNo;
  List<ClsBatterySupplyPacking> listSupplyNoFiltered;
  List<ClsBatterySupplyPacking> listH;
  List<ClsBatterySupplyPacking> searchdata = [];
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
  }

  void asynclistGetData(String supplyNo, String carryNo) async {
    listH =
        await BatterySupplyPackingService.getDataListHeader(supplyNo, carryNo);
    searchdata = listH;

    if (searchdata[0].id.toString() != "200" && carryNo != "") {
      Message.box(
          type: "2",
          message: searchdata[0]
              .description
              .toString(), // "Carry/Sublot No not found",
          top: 0,
          position: "0",
          context: context);
    }

    setState(() {});
  }

  void asyncsubmit(String supplyNo, String carryNo) async {
    try {
      setState(() {
        isProcessing = true;
      });

      if (supplyNo == "" || supplyNo == null) {
        String message = "Please scan supply no!";
        Message.box(
            type: "2",
            message: message,
            top: 0,
            position: "0",
            context: context);

        return;
      }

      if (carryNo == "" || carryNo == null) {
        String message = "Please scan carry no!";
        Message.box(
            type: "2",
            message: message,
            top: 0,
            position: "0",
            context: context);

        return;
      }

      List<ClsBatterySupplyPacking> list;
      list = await BatterySupplyPackingService.submit(
          supplyNo, carryNo, widget.userid);
      Message.box(
          type: (list[0].id == "200") ? "1" : "2",
          message: (list[0].id == "200")
              ? "Data saved successfully!"
              : list[0].description,
          top: 0,
          position: "0",
          context: context);

      txtSupplyNo.text = "";
      txtCarryNo.text = "";
      asynclistGetData("", "");

      FocusScope.of(context).requestFocus(nodeCarryNo);
    } catch (e) {
      var dialog = CustomAlertDialog(
        type: "2",
        title: "",
        message: e,
        okBtnText: 'Close',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: kAppBarColor,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text("Battery Supply To Packing"),
          ),
          body: Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                child: Text(
                  "1. Select / Scan Supply No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TypeAheadField(
                hideSuggestionsOnKeyboardHide: false,
                textFieldConfiguration: TextFieldConfiguration(
                    textInputAction: TextInputAction.done,
                    onSubmitted: (value) {
                      txtSupplyNo.text = value;
                      _searchSupplyNo = value;
                      setState(() {
                        listSupplyNoFiltered
                            .where((result) => result.supplyNo
                                .toLowerCase()
                                .contains(value.toLowerCase()))
                            .toList();
                      });
                      asynclistGetData(value, txtCarryNo.text);
                      FocusScope.of(context).requestFocus(nodeCarryNo);
                    },
                    focusNode: nodeSupplyNo,
                    controller: this.txtSupplyNo,
                    style: TextStyle(fontFamily: 'Arial'),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            txtSupplyNo.clear();
                            searchdata.clear();
                            _searchSupplyNo = '';
                            FocusScope.of(context).requestFocus(nodeSupplyNo);
                          });
                        },
                        icon: Icon(_searchSupplyNo == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _searchSupplyNo == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                      ),
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: -10.0, horizontal: 10.0),
                    )),
                suggestionsCallback: (pattern) async {
                  listSupplyNo =
                      await BatterySupplyPackingService.getSupplyNo();
                  _searchSupplyNo = pattern;
                  listSupplyNoFiltered = listSupplyNo
                      .where((user) => user.supplyNo
                          .toLowerCase()
                          .contains(_searchSupplyNo.toLowerCase()))
                      .toList();
                  return listSupplyNoFiltered;
                },
                itemBuilder: (context, suggestion) {
                  return ListTile(
                    title: Text(suggestion.typeDesc),
                    subtitle: Text(suggestion.supplyNo),
                  );
                },
                onSuggestionSelected: (suggestion) async {
                  this.txtSupplyNo.text = suggestion.supplyNo;
                  _searchSupplyNo = suggestion.supplyNo;
                  asynclistGetData(suggestion.supplyNo, txtCarryNo.text);
                  FocusScope.of(context).requestFocus(nodeCarryNo);
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "2. Scan Carry No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                showCursor: true,
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                controller: txtCarryNo,
                focusNode: nodeCarryNo,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        txtCarryNo.clear();
                        _scanCarryNo = '';

                        searchdata.clear();
                        FocusScope.of(context).requestFocus(nodeCarryNo);
                      });
                    },
                    icon: Icon(_scanCarryNo == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _scanCarryNo == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _scanCarryNo = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  asynclistGetData(txtSupplyNo.text, txtCarryNo.text);
                },
              ),
            ),
            SizedBox(height: 10),
            Expanded(
              child: Container(child: _dataTableWidget()),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 0),
                child: Container(
                  padding: EdgeInsets.only(top: 0),
                  width: MediaQuery.of(context).size.width * 0.90,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: FloatingActionButton.extended(
                    backgroundColor: (isProcessing)
                        ? disabledButtonColor
                        : enabledButtonColor,
                    onPressed: (isProcessing == false)
                        ? () async {
                            //if buttonenabled == true then pass a function otherwise pass "null"
                            asyncsubmit(txtSupplyNo.text, txtCarryNo.text);
                            //plug delayed for wait while processing data
                            await Future.delayed(const Duration(seconds: 3),
                                () {
                              setState(() {
                                isProcessing = false;
                              });
                            });
                          }
                        : null,
                    elevation: 0,
                    label: Text(
                      "SUBMIT",
                      style: TextStyle(
                          fontSize: 18.0,
                          color: (isProcessing)
                              ? disabledColorButtonText
                              : enabledColorButtonText),
                    ),
                  ),
                )),
            SizedBox(
              height: 10,
            )
          ])),
    );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Barcode No', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Item', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('LotNo', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 5),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(searchdata[index].barcode),
      width: MediaQuery.of(context).size.width / 3,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  // final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].itemName),
          width: MediaQuery.of(context).size.width / 2.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(searchdata[index].lot),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(searchdata[index].qty),
          width: MediaQuery.of(context).size.width / 5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
