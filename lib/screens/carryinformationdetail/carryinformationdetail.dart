import 'package:flutter/material.dart';
import 'package:panasonic/models/clsCarryInformationDetail.dart';
import 'package:panasonic/models/clsassigntocarrybylot.dart';
import 'package:panasonic/screens/carryinformationdetail/carryinformationsublotdetail.dart';
import 'package:panasonic/services/assigntocarrybylotservice.dart';
//import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

class CarryInformationDetail extends StatefulWidget {
  const CarryInformationDetail({Key key, this.userid}) : super(key: key);

  final String userid;
  @override
  _CarryInformationDetailState createState() => _CarryInformationDetailState();
}

class _CarryInformationDetailState extends State<CarryInformationDetail> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsCarryInformationDetail> listGetData = [];
  List<ClsCarryInformationDetail> listGetDataFiltered = [];

  final TextEditingController txtScanCarry = new TextEditingController();
  final TextEditingController txtCarryName = new TextEditingController();
  final TextEditingController txtCountLot = new TextEditingController();
  final TextEditingController txtTotalQty = new TextEditingController();

  final TextEditingController iSearch = new TextEditingController();
  FocusNode nodeCarry = FocusNode();

  String _searchDataResult = '';
  String _sarchCarry = '';

  @override
  void initState() {
    // asynclistGetData();
    super.initState();
  }

  void asynclistGetData(String carryNo) async {
    listGetData = await ClsCarryInformationDetail.getData(carryNo);
    listGetDataFiltered = listGetData;
    String message;
    String type;
    if (listGetDataFiltered.length == 0) {
      message = "Data not found";
      type = '2';
      Message.box(
          type: type,
          message: message,
          top: 0,
          position: "0",
          context: context);

      // var dialog = CustomAlertDialog(
      //   type: type,
      //   title: '',
      //   message: message,
      //   okBtnText: '',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
    }

    setState(() {});
  }

  void _recalculate() async {
    int totallot = 1;
    double totalqty = 0;
    String oldVal = "";
    for (int index = 0; index < listGetDataFiltered.length; index++) {
      // if (listGetDataFiltered[index].isflag == "1") {
      //   scan += 1;
      //   txtTotalScan.text = scan.toString();
      //   if (scan == listGetDataFiltered.length) {
      //     txtRemainScan.text = "0";
      //   }
      // } else if (listGetDataFiltered[index].isflag == "0" ||
      //     listGetDataFiltered[index].isflag == null) {
      //   remain += 1;
      //   txtRemainScan.text = remain.toString();
      // }
      if (oldVal != listGetDataFiltered[index].lotNo && oldVal != "") {
        totallot += 1;
      }

      totalqty = totalqty + double.parse(listGetDataFiltered[index].qty);

      oldVal = listGetDataFiltered[index].lotNo;
      setState(() {});
    }

    setState(() {
      txtCountLot.text = totallot.toString();
      txtTotalQty.text = totalqty.toString();
    });
  }

  void materialDetail(String barcodeNo, String type, String lotNo, String qty,
      String unit, String expDate, String registerUser, String lastUpdate) {
    setState(() {
      ClsCarryInformationDetail listData = new ClsCarryInformationDetail();
      listData.carryNo = txtScanCarry.text;
      listData.barcodeNo = barcodeNo;
      listData.type = type;
      listData.lotNo = lotNo;
      listData.qty = qty;
      listData.unitName = unit;
      listData.expDate = expDate;
      listData.registerUser = registerUser;
      listData.lastUpdate = lastUpdate;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => CarryInformationSublotDetail(),
          settings: RouteSettings(
            arguments: listData,
          ),
        ),
      ).then((value) => setState(() {
            asynclistGetData(txtScanCarry.text);
          }));
    });
  }

  void _getCarryName(String code) async {
    List<ClsAssignToCarryByLot> list;
    list = await AssignToCarryByLotService.getCarryDesc(code);
    if (list.length > 0) {
      txtCarryName.text = list[0].carryName;
      txtScanCarry.text = list[0].carryNo;
      setState(() {});
    } else {
      Message.box(
          type: "2",
          message: "Carry No is not found",
          top: 0,
          position: "0",
          context: context);
      txtCarryName.text = "";
      txtScanCarry.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    //final double width = MediaQuery.of(context).size.width;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Carry Information Detail"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Scan Carry No",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                autofocus: true,
                                showCursor: true,
                                controller: txtScanCarry,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  labelText: '',
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        txtScanCarry.clear();
                                        txtCarryName.text = '';
                                        txtCountLot.text = '';
                                        txtTotalQty.text = '';
                                        _sarchCarry = '';
                                        listGetDataFiltered.clear();

                                        FocusScope.of(context)
                                            .requestFocus(nodeCarry);
                                      });
                                    },
                                    icon: Icon(_sarchCarry == ''
                                        ? Icons.cancel
                                        : Icons.cancel_outlined),
                                    color: _sarchCarry == ''
                                        ? Colors.white12.withOpacity(1)
                                        : Colors.grey[600],
                                  ),
                                ),
                                onChanged: (value) {
                                  _sarchCarry = value;
                                },
                                onFieldSubmitted: (value) async {
                                  // MaterialDetail(value);
                                  //_selectedCarryNo = value;
                                  _getCarryName(value);
                                  await asynclistGetData(txtScanCarry.text);
                                  await _recalculate();
                                },
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Carry Name",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtCarryName,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  filled: true,
                                  fillColor: disabledColor,
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Total Lot : ' + ' ' + txtCountLot.text,
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.bold),
                                      )),
                                ),
                                // Container(
                                //   width: MediaQuery.of(context).size.width / 4,
                                //   padding: EdgeInsets.symmetric(horizontal: 10),
                                //   child: Align(
                                //       alignment: Alignment.centerLeft,
                                //       child: Text(
                                //         txtCountLot.text,
                                //         style: TextStyle(
                                //             fontFamily: 'Arial',
                                //             fontWeight: FontWeight.bold),
                                //       )),
                                // ),
                                Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  padding: EdgeInsets.symmetric(horizontal: 0),
                                  child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Total Qty : ' + ' ' + txtTotalQty.text,
                                        style: TextStyle(
                                            fontFamily: 'Arial',
                                            fontWeight: FontWeight.bold),
                                      )),
                                ),
                                // Container(
                                //   width: MediaQuery.of(context).size.width / 4,
                                //   padding: EdgeInsets.symmetric(horizontal: 10),
                                //   child: Align(
                                //       alignment: Alignment.centerLeft,
                                //       child: Text(
                                //         txtTotalQty.text,
                                //         style: TextStyle(
                                //             fontFamily: 'Arial',
                                //             fontWeight: FontWeight.bold),
                                //       )),
                                // ),
                              ],
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //   children: <Widget>[
                            //     Column(
                            //       children: <Widget>[
                            //         Padding(
                            //           padding:
                            //               EdgeInsets.symmetric(horizontal: 0),
                            //           child: Container(
                            //               alignment: Alignment.centerLeft,
                            //               padding: EdgeInsets.only(
                            //                   right: MediaQuery.of(context)
                            //                           .size
                            //                           .height *
                            //                       0.10),
                            //               child: Align(
                            //                 alignment: Alignment.center,
                            //                 child: Text(
                            //                   "Total Lot",
                            //                   style: TextStyle(
                            //                       fontFamily: 'Arial',
                            //                       fontWeight: FontWeight.bold),
                            //                 ),
                            //               )),
                            //         ),
                            //         SizedBox(height: 5),
                            //         Container(
                            //           height: 40,
                            //           width:
                            //               MediaQuery.of(context).size.width / 2,
                            //           padding:
                            //               EdgeInsets.symmetric(horizontal: 10),
                            //           child: Text(
                            //             txtCountLot.text,
                            //             style: TextStyle(
                            //                 fontFamily: 'Arial',
                            //                 fontWeight: FontWeight.normal),
                            //           ),
                            //           // child: TextFormField(
                            //           //   controller: txtCountLot,
                            //           //   keyboardType: TextInputType.number,
                            //           //   textAlign: TextAlign.right,
                            //           //   enabled: false,
                            //           //   decoration: InputDecoration(
                            //           //     border: OutlineInputBorder(),
                            //           //     contentPadding:
                            //           //         new EdgeInsets.symmetric(
                            //           //             vertical: 10.0,
                            //           //             horizontal: 10.0),
                            //           //     filled: true,
                            //           //     fillColor: disabledColor,
                            //           //   ),
                            //           // ),
                            //         ),
                            //       ],
                            //     ),
                            //     Column(
                            //       children: <Widget>[
                            //         Padding(
                            //           padding:
                            //               EdgeInsets.symmetric(horizontal: 20),
                            //           child: Container(
                            //               padding: EdgeInsets.only(
                            //                   left: MediaQuery.of(context)
                            //                           .size
                            //                           .height *
                            //                       0.10),
                            //               child: Align(
                            //                 alignment: Alignment.center,
                            //                 child: Text(
                            //                   "Total Qty",
                            //                   style: TextStyle(
                            //                       fontFamily: 'Arial',
                            //                       fontWeight: FontWeight.bold),
                            //                 ),
                            //               )),
                            //         ),
                            //         SizedBox(height: 5),
                            //         Container(
                            //           width:
                            //               MediaQuery.of(context).size.width / 2,
                            //           height: 40,
                            //           padding:
                            //               EdgeInsets.symmetric(horizontal: 10),
                            //           child: Text(
                            //             txtTotalQty.text,
                            //             style: TextStyle(
                            //                 fontFamily: 'Arial',
                            //                 fontWeight: FontWeight.normal),
                            //           ),
                            //           // child: TextFormField(
                            //           //   controller: txtTotalQty,
                            //           //   keyboardType: TextInputType.number,
                            //           //   textAlign: TextAlign.right,
                            //           //   enabled: false,
                            //           //   decoration: InputDecoration(
                            //           //     border: OutlineInputBorder(),
                            //           //     contentPadding:
                            //           //         new EdgeInsets.symmetric(
                            //           //             vertical: 10.0,
                            //           //             horizontal: 10.0),
                            //           //     filled: true,
                            //           //     fillColor: disabledColor,
                            //           //   ),
                            //           // ),
                            //         ),
                            //       ],
                            //     )
                            //   ],
                            // ),
                            Divider(height: 20, color: Colors.grey),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                  controller: iSearch,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 15.0),
                                    labelText: 'Search',
                                    prefixIcon: IconButton(
                                      onPressed: () {},
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.search
                                          : Icons.search),
                                      //Icons.search,
                                    ),
                                    suffixIcon: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          iSearch.clear();
                                          _searchDataResult = '';
                                          listGetDataFiltered = listGetData;
                                        });
                                      },
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.cancel
                                          : Icons.cancel_outlined),
                                      color: _searchDataResult == ''
                                          ? Colors.white12.withOpacity(1)
                                          : Colors.grey[600],
                                    ),
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      _searchDataResult = value;
                                      listGetDataFiltered = listGetData
                                          .where((user) =>
                                              user.type.toLowerCase().contains(
                                                  _searchDataResult
                                                      .toLowerCase()) ||
                                              user.sublotNo
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()) ||
                                              user.barcodeNo
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()) ||
                                              user.lotNo.toLowerCase().contains(
                                                  _searchDataResult
                                                      .toLowerCase()) ||
                                              user.qty.toLowerCase().contains(
                                                  _searchDataResult
                                                      .toLowerCase()) ||
                                              user.unitName.toLowerCase().contains(
                                                  _searchDataResult.toLowerCase()))
                                          .toList();
                                    });
                                  }),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Expanded(
                              child: Container(child: _dataTableWidget()),
                            ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3.5,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width * 1.2,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  // List<Widget> _getTitleWidget() {
  //   return [
  //     _getTitleItemWidget('Type', MediaQuery.of(context).size.width / 3.2),
  //     _getTitleItemWidget('Sublot No', MediaQuery.of(context).size.width / 3.5),
  //     _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 3.5),
  //     _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 5.5),
  //     _getTitleItemWidget('Unit', MediaQuery.of(context).size.width / 5),
  //     _getTitleItemWidget('', 40),
  //   ];
  // }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Type', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget(
          'Sublot No', MediaQuery.of(context).size.width / 3.5), //, 100),
      _getTitleItemWidget(
          'LotNo', MediaQuery.of(context).size.width / 3), // 120),
      _getTitleItemWidget(
          'Qty', MediaQuery.of(context).size.width / 4), // 100),
      _getTitleItemWidget(
          'Unit', MediaQuery.of(context).size.width / 5), // 100),
      _getTitleItemWidget(' ', 20),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].type),
      width: MediaQuery.of(context).size.width / 3.5,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(listGetDataFiltered[index].barcodeNo),
          width: MediaQuery.of(context).size.width / 3.5, //100,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(listGetDataFiltered[index].lotNo),
          width: MediaQuery.of(context).size.width / 2.5, //120,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child:
              Text(value.format(double.parse(listGetDataFiltered[index].qty))),
          width: MediaQuery.of(context).size.width / 4.5, //100,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(listGetDataFiltered[index].unitName),
          width: MediaQuery.of(context).size.width / 5, //110,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: 20,
          height: 45,
          child: IconButton(
            onPressed: () {
              materialDetail(
                  listGetDataFiltered[index].barcodeNo,
                  listGetDataFiltered[index].type,
                  listGetDataFiltered[index].lotNo,
                  listGetDataFiltered[index].qty,
                  listGetDataFiltered[index].unitName,
                  listGetDataFiltered[index].expDate,
                  listGetDataFiltered[index].registerUser,
                  listGetDataFiltered[index].lastUpdate);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            color: Colors.black,
            alignment: Alignment.center,
          ),
        ),
      ],
    );
  }
}
