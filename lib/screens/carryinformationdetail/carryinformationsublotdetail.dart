// import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:panasonic/models/clsCarryInformationDetail.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:flutter/services.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:intl/intl.dart';

class CarryInformationSublotDetail extends StatefulWidget {
  @override
  _CarryInformationSublotDetailState createState() =>
      _CarryInformationSublotDetailState();
}

class _CarryInformationSublotDetailState
    extends State<CarryInformationSublotDetail> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  bool init = false;

  List<ClsCarryInformationDetail> listGetData = [];

  final TextEditingController txtScanBarcodeNo = new TextEditingController();
  final TextEditingController txtType = new TextEditingController();
  final TextEditingController txtLotNo = new TextEditingController();
  final TextEditingController txtQty = new TextEditingController();
  final TextEditingController txtUnit = new TextEditingController();
  final TextEditingController txtExpDate = new TextEditingController();
  final TextEditingController txtRegisterUser = new TextEditingController();
  final TextEditingController txtLastUpdate = new TextEditingController();

  String carryNo = '';
  //String _searchSublot = '';
  FocusNode nodeBarcode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  void clearData() {
    txtScanBarcodeNo.text = "";
    txtType.text = "";
    txtLotNo.text = "";
    txtQty.text = "";
    txtUnit.text = "";
    txtExpDate.text = "";
    txtRegisterUser.text = "";
    txtLastUpdate.text = "";
  }

  void asyncSubmit() async {
    listGetData = await ClsCarryInformationDetail.submit(
        carryNo, txtScanBarcodeNo.text, txtQty.text);
    if (listGetData.length > 0) {
      String message;
      String type;

      if (listGetData[0].id == "200") {
        message = 'Data saved successfully';
        type = '3';
      } else if (listGetData[0].id == "400") {
        message = listGetData[0].description;
        type = '2';
      } else {
        message = listGetData[0].description;
        type = '2';
      }

      var dialog = CustomAlertDialog(
        type: type,
        title: '',
        message: message,
        okBtnText: '',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
    setState(() {});
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  @override
  Widget build(BuildContext context) {
    final pModels = ModalRoute.of(context).settings.arguments;
    ClsCarryInformationDetail model = pModels;
    carryNo = model.carryNo;
    txtScanBarcodeNo.text = model.barcodeNo;
    txtType.text = model.type;
    txtLotNo.text = model.lotNo;
    txtQty.text = value.format(double.parse(model.qty));
    txtUnit.text = model.unitName;
    txtExpDate.text = model.expDate;
    txtRegisterUser.text = model.registerUser;
    txtLastUpdate.text = model.lastUpdate;

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Sublot Detail"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Barcode Material",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: TextFormField(
                                // autofocus: true,
                                // showCursor: true,
                                // focusNode: nodeBarcode,
                                //enableInteractiveSelection: false,
                                //textInputAction: TextInputAction.none,
                                enabled: false,
                                controller: txtScanBarcodeNo,
                                decoration: InputDecoration(
                                  fillColor: disabledColor,
                                  filled: true,
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  labelText: '',
                                  // suffixIcon: IconButton(
                                  //   onPressed: () {
                                  //     setState(() {
                                  //       txtScanBarcodeNo.clear();
                                  //       _searchSublot = '';
                                  //       clearData();

                                  //       FocusScope.of(context)
                                  //           .requestFocus(nodeBarcode);
                                  //     });
                                  //   },
                                  //   icon: Icon(_searchSublot == ''
                                  //       ? Icons.cancel
                                  //       : Icons.cancel_outlined),
                                  //   color: _searchSublot == ''
                                  //       ? Colors.white12.withOpacity(1)
                                  //       : Colors.grey[600],
                                  // ),
                                ),
                                onChanged: (value) {
                                  // _searchSublot = value;

                                  setState(() {});
                                },
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Type",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: TextFormField(
                                controller: txtType,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Lot No",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: TextFormField(
                                controller: txtLotNo,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Qty",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: TextFormField(
                                controller: txtQty,
                                enabled: false,
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.right,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Unit",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: TextFormField(
                                controller: txtUnit,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Expired Date",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: TextFormField(
                                controller: txtExpDate,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Register User",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: TextFormField(
                                controller: txtRegisterUser,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Register User",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: TextFormField(
                                controller: txtLastUpdate,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            // SizedBox(height: 80.0),
                            // Expanded(
                            //   child: Container(
                            //     alignment: Alignment.centerRight,
                            //     child: Padding(
                            //       padding: EdgeInsets.symmetric(
                            //           horizontal: 15, vertical: 15),
                            //       child: TextButton(
                            //         style: TextButton.styleFrom(
                            //           backgroundColor: Colors.blue,
                            //           minimumSize: Size(150, 40),
                            //         ),
                            //         onPressed: () {
                            //           asyncSubmit();
                            //         },
                            //         child: Text(
                            //           "Submit",
                            //           style: TextStyle(
                            //             fontSize: 16,
                            //             color: Colors.white,
                            //           ),
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }

  @override
  void dispose() {
    nodeBarcode.dispose();
    super.dispose();
  }
}
