import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clscarrymovinglocation.dart';
import 'package:panasonic/screens/carrymovinglocation/_detail.dart';
import 'package:panasonic/services/carrymovinglocationservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/message.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Carry Moving Location',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class CarryMovingLocation extends StatefulWidget {
  const CarryMovingLocation({Key key, this.userid}) : super(key: key);

  final String userid;

  @override
  _CarryMovingLocation createState() => _CarryMovingLocation();
}

class _CarryMovingLocation extends State<CarryMovingLocation> {
  CarryMovingLocationService api = CarryMovingLocationService();

  TextEditingController iScanCarryNo = TextEditingController();
  TextEditingController iLocAddressH = TextEditingController();
  TextEditingController iScanLocDest = TextEditingController();
  TextEditingController iLocAddressD = TextEditingController();

  FocusNode nodeCarryNo = FocusNode();
  FocusNode nodeLocationDestination = FocusNode();

  String _scanCarryNo = '';
  String _scanLoc = '';

  List<ClsCarryMovingLocation> listH;
  List<ClsCarryMovingLocation> searchdata = [];
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
  }

  void _getAddressLocation(String code, String type) async {
    List<ClsCarryMovingLocation> list;
    list = await api.getLocationAddress(code, type);

    if (list.length > 0) {
      if (type == "0") {
        iLocAddressH.text = list[0].locationAddress1;
        FocusScope.of(context).requestFocus(nodeLocationDestination);
      } else {
        iLocAddressD.text = list[0].locationAddress2;
      }
    } else {
      if (type == "0") {
        Message.box(
            type: "2",
            message: "Carry No is not found",
            top: 0,
            position: "0",
            context: context);

        // var dialog = CustomAlertDialog(
        //   type: "4",
        //   title: "",
        //   message: "Carry No is not found",
        //   okBtnText: 'Close',
        // );
        // showDialog(context: context, builder: (BuildContext context) => dialog);
        iScanCarryNo.text = "";
        iLocAddressH.text = "";
      } else {
        Message.box(
            type: "2",
            message: "Location is not found",
            top: 0,
            position: "0",
            context: context);
        // var dialog = CustomAlertDialog(
        //   type: "4",
        //   title: "",
        //   message: "Location is not found",
        //   okBtnText: 'Close',
        // );
        // showDialog(context: context, builder: (BuildContext context) => dialog);
        iScanLocDest.text = "";
        iLocAddressD.text = "";
      }
    }
  }

  void asynclistGetData(String carryNo) async {
    listH = await api.getData(carryNo);
    searchdata = listH;
    setState(() {});
  }

  void _detail(
      String carryNo, String _itemCode, String _itemName, String _lotNo) {
    setState(() {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => CarryMovingLocatitonDetail(
              carryNo: carryNo,
              itemCode: _itemCode,
              itemName: _itemName,
              lotNo: _lotNo),
          settings: RouteSettings(),
        ),
      );
    });
  }

  void asyncsubmit(String carryNo, String loc) async {
    try {
      setState(() {
        isProcessing = true;
      });
      if (carryNo == "" || carryNo == null) {
        String message = "Please scan carry no!";
        Message.box(
            type: "2",
            message: message,
            top: 0,
            position: "0",
            context: context);

        // var dialog = CustomAlertDialog(
        //   type: "4",
        //   title: "",
        //   message: message,
        //   okBtnText: 'Close',
        // );
        // showDialog(context: context, builder: (BuildContext context) => dialog);

        return;
      }

      if (iScanLocDest.text == "" || iScanLocDest.text == null) {
        String message = "Please scan location destination!";
        Message.box(
            type: "2",
            message: message,
            top: 0,
            position: "0",
            context: context);
        // var dialog = CustomAlertDialog(
        //   type: "4",
        //   title: "",
        //   message: message,
        //   okBtnText: 'Close',
        // );
        // showDialog(context: context, builder: (BuildContext context) => dialog);

        return;
      }

      // String message;
      // message = await api.submit(carryNo, loc, widget.userid);
      List<ClsCarryMovingLocation> list;
      list = await api.submit(carryNo, loc, widget.userid);
      Message.box(
          type: (list[0].id == "200") ? "31" : "2",
          message: (list[0].id == "200")
              ? "Data saved successfully!"
              : list[0].description,
          top: 0,
          position: "0",
          context: context);

      // var dialog = CustomAlertDialog(
      //   type: (list[0].id == "200") ? "3" : "2",
      //   title: "",
      //   message: (list[0].id == "200")
      //       ? "Data saved successfully!"
      //       : list[0].description,
      //   okBtnText: 'Close',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);

      iScanCarryNo.text = "";
      iLocAddressH.text = "";
      asynclistGetData("");

      iScanLocDest.text = "";
      iLocAddressD.text = "";
      FocusScope.of(context).requestFocus(nodeCarryNo);
    } catch (e) {
      // print('error caught: $e');
      var dialog = CustomAlertDialog(
        type: "2",
        title: "",
        message: e,
        okBtnText: 'Close',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: kAppBarColor,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text("Carry Moving Location"),
          ),
          body: //SingleChildScrollView(
              // physics: ClampingScrollPhysics(),
              //  child: IntrinsicHeight(
              // child:
              Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "1. Scan Carry No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                autofocus: true,
                showCursor: true,
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                controller: iScanCarryNo,
                focusNode: nodeCarryNo,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iScanCarryNo.clear();
                        _scanCarryNo = '';
                        iLocAddressH.text = '';
                        searchdata.clear();
                        FocusScope.of(context).requestFocus(nodeCarryNo);
                      });
                    },
                    icon: Icon(_scanCarryNo == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _scanCarryNo == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _scanCarryNo = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  _getAddressLocation(iScanCarryNo.text, "0");
                  asynclistGetData(iScanCarryNo.text);
                  FocusScope.of(context).requestFocus(nodeLocationDestination);
                },
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Location / Address",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iLocAddressH,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: 10),
            Container(
                height: MediaQuery.of(context).size.height / 5,
                child: _dataTableWidget()),
            Divider(
              color: Colors.black,
              height: 20,
            ),
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "2. Scan Location Destination",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                scrollPadding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                controller: iScanLocDest,
                focusNode: nodeLocationDestination,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iScanLocDest.clear();
                        _scanLoc = '';
                        iLocAddressD.text = '';
                      });
                    },
                    icon: Icon(
                        _scanLoc == '' ? Icons.cancel : Icons.cancel_outlined),
                    color: _scanLoc == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _scanLoc = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  _getAddressLocation(iScanLocDest.text, "1");
                },
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Location / Address",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Container(
                height: MediaQuery.of(context).size.height / 4.5,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: iLocAddressD,
                    enabled: false,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      filled: true,
                      fillColor: disabledColor,
                    ),
                  ),
                )),
            Expanded(
                child: SizedBox(
                    //height: MediaQuery.of(context).size.height,
                    )),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 0),
                child: Container(
                  padding: EdgeInsets.only(top: 0),
                  width: MediaQuery.of(context).size.width * 0.90,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: FloatingActionButton.extended(
                    backgroundColor: (isProcessing)
                        ? disabledButtonColor
                        : enabledButtonColor,
                    onPressed: (isProcessing == false)
                        ? () async {
                            //if buttonenabled == true then pass a function otherwise pass "null"
                            asyncsubmit(iScanCarryNo.text, iScanLocDest.text);
                            //plug delayed for wait while processing data
                            await Future.delayed(const Duration(seconds: 3),
                                () {
                              setState(() {
                                isProcessing = false;
                              });
                            });
                          }
                        : null,
                    elevation: 0,
                    label: Text(
                      "SUBMIT",
                      style: TextStyle(
                          fontSize: 18.0,
                          color: (isProcessing)
                              ? disabledColorButtonText
                              : enabledColorButtonText),
                    ),
                  ),
                )),
            SizedBox(
              height: 10,
            )
          ])),
    );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 2.5,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Item', MediaQuery.of(context).size.width / 2.5),
      _getTitleItemWidget('LotNo', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Unit', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget(' ', 20),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(searchdata[index].itemName),
      width: MediaQuery.of(context).size.width / 2.5,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].lotNo),
          width: MediaQuery.of(context).size.width / 2.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(value.format(double.parse(searchdata[index].qty))),
          width: MediaQuery.of(context).size.width / 4.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(searchdata[index].unit),
          width: MediaQuery.of(context).size.width / 3.8,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: 30,
          height: 45,
          child: IconButton(
            onPressed: () {
              _detail(iScanCarryNo.text, searchdata[index].itemCode,
                  searchdata[index].itemName, searchdata[index].lotNo);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            color: Colors.black,
            alignment: Alignment.centerRight,
          ),
        ),
        // Container(
        //   padding: EdgeInsets.fromLTRB(20, 0, 5, 0),
        //   width: 20,
        //   height: 45,
        //   child: IconButton(
        //     onPressed: () {
        //       _detail(iScanCarryNo.text, searchdata[index].itemCode,
        //           searchdata[index].itemName, searchdata[index].lotNo);
        //     },
        //     icon: Icon(
        //       Icons.arrow_forward_ios,
        //       size: 15,
        //     ),
        //     color: Colors.black,
        //     alignment: Alignment.centerRight,
        //   ),
        // ),
      ],
    );
  }
}

/*

Container(
                  height: 200,
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(
                        columnSpacing: (searchdata.length == 0 ||
                                searchdata == null)
                            ? (MediaQuery.of(context).size.width / 5) * 0.67
                            : (MediaQuery.of(context).size.width / 5) * 0.47,
                        headingRowHeight: 50,
                        decoration: BoxDecoration(
                            border: Border.all(
                          width: 1,
                          color: Colors.grey[300],
                        )),
                        headingRowColor: MaterialStateColor.resolveWith(
                            (states) => Colors.grey[200]),
                        sortColumnIndex: 0,
                        showCheckboxColumn: false,
                        columns: [
                          DataColumn(
                              label: Container(
                                padding: EdgeInsets.symmetric(horizontal: 0),
                                child: Text(
                                  "Item",
                                  style: TextStyle(
                                      fontFamily: "Arial",
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                ),
                              ),
                              numeric: false,
                              tooltip: "Item"),
                          DataColumn(
                              label: Container(
                                padding: EdgeInsets.symmetric(horizontal: 0),
                                child: Text(
                                  "Lot No",
                                  style: TextStyle(
                                      fontFamily: "Arial",
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                ),
                              ),
                              numeric: false,
                              tooltip: "Lot No"),
                          DataColumn(
                            label: Container(
                                child: Text(
                              "Qty",
                              style: TextStyle(
                                  fontFamily: "Arial",
                                  fontWeight: FontWeight.bold),
                            )),
                            numeric: false,
                            tooltip: "Qty",
                          ),
                          DataColumn(
                              label: Container(
                                  child: Text(
                                "Unit",
                                style: TextStyle(
                                    fontFamily: "Arial",
                                    fontWeight: FontWeight.bold),
                              )),
                              numeric: false,
                              tooltip: "Unit"),
                          DataColumn(label: Container(child: Text(""))),
                        ],
                        rows: searchdata
                            .map(
                              (idata) => DataRow(cells: [
                                DataCell(
                                    Container(
                                      child: Text(idata.itemName),
                                    ), onTap: () {
                                  _detail(iScanCarryNo.text, idata.itemCode,
                                      idata.itemName, idata.lotNo);
                                }),
                                DataCell(
                                  Container(
                                    child: Text(idata.lotNo),
                                    alignment: Alignment.centerLeft,
                                  ),
                                ),
                                DataCell(
                                  Container(
                                    child: Text(idata.qty),
                                    alignment: Alignment.centerRight,
                                  ),
                                ),
                                DataCell(
                                  Container(
                                    child: Text(idata.unit),
                                    alignment: Alignment.centerLeft,
                                  ),
                                ),
                                DataCell(
                                  Container(
                                    width: 5,
                                    child: IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                      ),
                                      color: Colors.black,
                                    ),
                                  ),
                                  onTap: () {
                                    _detail(iScanCarryNo.text, idata.itemCode,
                                        idata.itemName, idata.lotNo);
                                  },
                                  //IconButton(),
                                ),
                              ]),
                            )
                            .toList(),
                      ),
                    ),
                  ),
                ),

                */
