import 'package:flutter/material.dart';
import 'package:panasonic/models/clsCarryResetStatus.dart';
import 'package:panasonic/models/clsassigntocarrybylot.dart';
import 'package:panasonic/screens/carryresetstatus/carryresetdetailsublotdetail.dart';
import 'package:panasonic/services/assigntocarrybylotservice.dart';
// import 'package:panasonic/screens/carryresetstatus/carryresetstatus.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

class CarryResetStatus extends StatefulWidget {
  const CarryResetStatus({Key key, this.userid}) : super(key: key);

  final String userid;
  @override
  _CarryResetStatusState createState() => _CarryResetStatusState();
}

class _CarryResetStatusState extends State<CarryResetStatus> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsCarryResetStatus> listGetData = [];
  List<ClsCarryResetStatus> listGetDataFiltered = [];

  final TextEditingController txtScanCarry = new TextEditingController();
  final TextEditingController txtCarryName = new TextEditingController();

  final TextEditingController txtSearch = new TextEditingController();
  FocusNode nodeCarry = FocusNode();

  String _searchDataResult = '';
  String _searchCarry = '';
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
  }

  void asynclistGetData(String carryNo) async {
    listGetData = await ClsCarryResetStatus.getData(carryNo);
    listGetDataFiltered = listGetData;
    // String message;
    // String type;
    // if (listGetDataFiltered.length == 0) {
    //   message = "Data not found";
    //   type = '2';
    //   Message.box(
    //       type: type,
    //       message: message,
    //       top: 0,
    //       position: "0",
    //       context: context);

    // var dialog = CustomAlertDialog(
    //   type: type,
    //   title: '',
    //   message: message,
    //   okBtnText: '',
    // );
    // showDialog(context: context, builder: (BuildContext context) => dialog);
    //}

    setState(() {});
  }

  void _getCarryName(String code) async {
    List<ClsAssignToCarryByLot> list;
    list = await AssignToCarryByLotService.getCarryDesc(code);
    if (list.length > 0) {
      txtCarryName.text = list[0].carryName;
      txtScanCarry.text = list[0].carryNo;
      setState(() {});
    } else {
      Message.box(
          type: "2",
          message: "Carry No is not found",
          top: 0,
          position: "0",
          context: context);
      txtCarryName.text = "";
      txtScanCarry.clear();
    }
  }

  void sublotDetail(String barcodeNo, String type, String lotNo, String qty,
      String unit, String expDate, String registerUser, String lastUpdate) {
    setState(() {
      ClsCarryResetStatus listData = new ClsCarryResetStatus();
      listData.carryNo = txtScanCarry.text;
      listData.barcodeNo = barcodeNo;
      listData.type = type;
      listData.lotNo = lotNo;
      listData.qty = qty;
      listData.unitName = unit;
      listData.expDate = expDate;
      listData.registerUser = registerUser;
      listData.lastUpdate = lastUpdate;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              CarryResetStatusSublotDetail(), //CarryInformationSublotDetail(),
          settings: RouteSettings(
            arguments: listData,
          ),
        ),
      ).then((value) => setState(() {
            asynclistGetData(txtScanCarry.text);
          }));
    });
  }

  void asyncSubmit(String carryno) async {
    setState(() {
      isProcessing = true;
    });

    if (carryno == "") {
      Message.box(
          type: "2",
          message: "Pleasae scan carry",
          top: 0,
          position: "0",
          context: context);
      return;
    }

    listGetData = await ClsCarryResetStatus.submit(carryno, widget.userid);
    if (listGetData.length > 0) {
      String message;
      String type;

      if (listGetData[0].id == "200") {
        message = 'Data saved successfully';
        type = '1';
      } else if (listGetData[0].id == "400") {
        message = listGetData[0].description;
        type = '2';
      } else {
        message = listGetData[0].description;
        type = '2';
      }

      Message.box(
          type: type,
          message: message,
          top: 0,
          position: "0",
          context: context);

      // var dialog = CustomAlertDialog(
      //   type: type,
      //   title: '',
      //   message: message,
      //   okBtnText: '',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    //final double width = MediaQuery.of(context).size.width;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text("Carry Reset Status"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "1. Scan Carry No",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                autofocus: true,
                                showCursor: true,
                                enableInteractiveSelection: false,
                                textInputAction: TextInputAction.none,
                                controller: txtScanCarry,
                                focusNode: nodeCarry,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  labelText: '',
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        txtScanCarry.clear();
                                        txtCarryName.text = '';
                                        _searchCarry = '';
                                        listGetDataFiltered.clear();

                                        FocusScope.of(context)
                                            .requestFocus(nodeCarry);
                                      });
                                    },
                                    icon: Icon(_searchCarry == ''
                                        ? Icons.cancel
                                        : Icons.cancel_outlined),
                                    color: _searchCarry == ''
                                        ? Colors.white12.withOpacity(1)
                                        : Colors.grey[600],
                                  ),
                                ),
                                onChanged: (value) {
                                  _searchCarry = value;
                                },
                                onFieldSubmitted: (value) {
                                  // MaterialDetail(value);
                                  //_selectedCarryNo = value;
                                  _getCarryName(value);
                                  asynclistGetData(value);
                                },
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Carry Name",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtCarryName,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  filled: true,
                                  fillColor: disabledColor,
                                ),
                              ),
                            ),
                            Divider(height: 20, color: Colors.grey),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                  controller: txtSearch,
                                  decoration: InputDecoration(
                                    labelText: 'Search',
                                    prefixIcon: IconButton(
                                      onPressed: () {},
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.search
                                          : Icons.search),

                                      //Icons.search,
                                    ),
                                    suffixIcon: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          txtSearch.clear();
                                          _searchDataResult = '';
                                          listGetDataFiltered = listGetData;
                                        });
                                      },
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.cancel
                                          : Icons.cancel_outlined),
                                      color: _searchDataResult == ''
                                          ? Colors.white12.withOpacity(1)
                                          : Colors.grey[600],
                                    ),
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      _searchDataResult = value;
                                      listGetDataFiltered = listGetData
                                          .where((user) =>
                                              user.type.toLowerCase().contains(
                                                  _searchDataResult
                                                      .toLowerCase()) ||
                                              user.sublotNo
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()) ||
                                              user.lotNo.toLowerCase().contains(
                                                  _searchDataResult
                                                      .toLowerCase()) ||
                                              user.qty.toLowerCase().contains(
                                                  _searchDataResult
                                                      .toLowerCase()) ||
                                              user.unitName
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()))
                                          .toList();
                                    });
                                  }),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Expanded(
                              child: Container(child: _dataTableWidget()),
                            ),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  TextButton(
                                    style: TextButton.styleFrom(
                                      backgroundColor: (isProcessing)
                                          ? disabledButtonColor
                                          : enabledButtonColor,
                                      minimumSize: Size(
                                          MediaQuery.of(context).size.width *
                                              0.90,
                                          55),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50.0),
                                      ),
                                    ),
                                    onPressed: (isProcessing == false)
                                        ? () async {
                                            //if buttonenabled == true then pass a function otherwise pass "null"
                                            asyncSubmit(txtScanCarry.text);
                                            //plug delayed for wait while processing data
                                            await Future.delayed(
                                                const Duration(seconds: 3), () {
                                              setState(() {
                                                isProcessing = false;
                                              });
                                            });
                                          }
                                        : null,
                                    child: Text(
                                      "SUBMIT",
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: (isProcessing)
                                            ? disabledColorButtonText
                                            : enabledColorButtonText,
                                      ),
                                    ),
                                  ),
                                  // ),
                                ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3.5,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width * 1.2,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Type', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget(
          'Sublot No', MediaQuery.of(context).size.width / 3.5), //, 100),
      _getTitleItemWidget(
          'LotNo', MediaQuery.of(context).size.width / 3), // 120),
      _getTitleItemWidget(
          'Qty', MediaQuery.of(context).size.width / 4), // 100),
      _getTitleItemWidget(
          'Unit', MediaQuery.of(context).size.width / 5), // 100),
      _getTitleItemWidget(' ', 20),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].type),
      width: MediaQuery.of(context).size.width / 3.5,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(listGetDataFiltered[index].barcodeNo),
          width: MediaQuery.of(context).size.width / 3.5, //100,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(listGetDataFiltered[index].lotNo),
          width: MediaQuery.of(context).size.width / 2.5, //120,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child:
              Text(value.format(double.parse(listGetDataFiltered[index].qty))),
          width: MediaQuery.of(context).size.width / 4.5, //100,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(listGetDataFiltered[index].unitName),
          width: MediaQuery.of(context).size.width / 5, //110,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: 20,
          height: 45,
          child: IconButton(
            onPressed: () {
              sublotDetail(
                  listGetDataFiltered[index].barcodeNo,
                  listGetDataFiltered[index].type,
                  listGetDataFiltered[index].lotNo,
                  listGetDataFiltered[index].qty,
                  listGetDataFiltered[index].unitName,
                  listGetDataFiltered[index].expDate,
                  listGetDataFiltered[index].registerUser,
                  listGetDataFiltered[index].lastUpdate);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            color: Colors.black,
            alignment: Alignment.center,
          ),
        ),
      ],
    );
  }
}
