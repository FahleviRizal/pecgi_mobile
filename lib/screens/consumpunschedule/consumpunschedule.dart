import 'package:flutter/material.dart';
import 'package:panasonic/models/clsconsumpunschedule.dart';
import 'package:panasonic/services/consumpunscheduleservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Consump Unschedule',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class ConsumpUnschedule extends StatefulWidget {
  const ConsumpUnschedule({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _ConsumpUnschedule createState() => _ConsumpUnschedule();
}

class _ConsumpUnschedule extends State<ConsumpUnschedule> {
  ConsumpUnscheduleService api = ConsumpUnscheduleService();

  TextEditingController iBarcodeNo = TextEditingController();
  TextEditingController iMaterialCode = TextEditingController();
  TextEditingController iMaterialName = TextEditingController();
  TextEditingController iLotNo = TextEditingController();
  TextEditingController iBeforeQty = TextEditingController();
  TextEditingController iQtyTemp = TextEditingController();
  TextEditingController iActualQty = TextEditingController();
  TextEditingController iUnit = TextEditingController();
  TextEditingController iExpDate = TextEditingController();
  TextEditingController iLastUser = TextEditingController();
  TextEditingController iLastUpdate = TextEditingController();

  //final value = new NumberFormat("#,##0.00", "en_US");

  FocusNode nodeBarcode = FocusNode();
  FocusNode nodeActualQty = FocusNode();
  FocusNode nodeButton = FocusNode();

  String _scanBarcode = '';
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
  }

  void asyncGetData(String barcodeNo) async {
    List<ClsConsumpUnschedule> list = [];

    list =
        await ConsumpUnscheduleService.getDataScan(barcodeNo, widget.warehouse);

    if (list[0].id == "200") {
      iMaterialCode.text = list[0].materialCode;
      iMaterialName.text = list[0].materialDesc;
      iLotNo.text = list[0].lotNo;
      iBeforeQty.text = list[0].qtyBefore;
      iActualQty.text = list[0].qtyBefore;
      iQtyTemp.text = list[0].qtyBefore;
      iUnit.text = list[0].unitName;
      iExpDate.text = list[0].expDate;
      iLastUser.text = list[0].lastUser;
      iLastUpdate.text = list[0].lastUpdate;
    } else {
      _clear();

      Message.box(
          type: '2',
          message: list[0].description.toString(),
          top: 0,
          position: "0",
          context: context);
    }
  }

  List<ClsConsumpUnschedule> listData;
  void asyncSubmit(String barcode, String item, String lotNo, String qtyBefore,
      String qtyActual) async {
    setState(() {
      isProcessing = true;
    });
    if (barcode == "") {
      Message.box(
          type: '2',
          message: "Please scan barcode",
          top: 0,
          position: "0",
          context: context);
      return;
    }
    if (qtyActual == "" || qtyActual == "0") {
      Message.box(
          type: '2',
          message: "Actual Qty must greater than zero",
          top: 0,
          position: "0",
          context: context);
      return;
    }
    qtyBefore = iQtyTemp.text;
    // if (double.parse(qtyActual) > double.parse(qtyBefore)) {
    //   Message.box(
    //       type: '2',
    //       message: "Actual Qty must less than Qty",
    //       top: 0,
    //       position: "0",
    //       context: context);
    //   return;
    // }

    try {
      //String message;
      listData = await ConsumpUnscheduleService.submit(barcode, item, lotNo,
          qtyBefore, qtyActual, widget.userid, widget.warehouse);
      if (listData[0].id == "200") {
        Message.box(
            type: "1",
            message: "Submit data successfully",
            top: 0,
            position: "0",
            context: context);

        iBarcodeNo.clear();
        _clear();

        FocusScope.of(context).requestFocus(nodeBarcode);
      } else {
        Message.box(
            type: "2",
            message: listData[0].description.toString(),
            top: 0,
            position: "0",
            context: context);
        iBarcodeNo.clear();
        _clear();
        FocusScope.of(context).requestFocus(nodeBarcode);
      }
    } catch (e) {
      var dialog = CustomAlertDialog(
        type: "2",
        title: "",
        message: e.toString(),
        okBtnText: 'Close',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
  }

  void _clear() {
    iMaterialCode.text = "";
    iMaterialName.text = "";
    iLotNo.text = "";
    iBeforeQty.text = "0";
    iQtyTemp.text = "";
    iActualQty.text = "0";
    iUnit.text = "";
    iExpDate.text = "";
    iLastUpdate.text = "";
    iLastUser.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text("Consump Unschedule"),
        ),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: <Widget>[
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Scan Barcode No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                autofocus: true,
                showCursor: true,
                controller: iBarcodeNo,
                focusNode: nodeBarcode,
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iBarcodeNo.clear();
                        _scanBarcode = '';
                        _clear();
                        FocusScope.of(context).requestFocus(nodeBarcode);
                      });
                    },
                    icon: Icon(_scanBarcode == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _scanBarcode == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _scanBarcode = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  asyncGetData(iBarcodeNo.text);
                  FocusScope.of(context).requestFocus(nodeButton);
                },
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Item Code",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iMaterialCode,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Item Name",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iMaterialName,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Lot No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iLotNo,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      child: Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(
                              right: MediaQuery.of(context).size.height * 0.16),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Qty",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                    ),
                    SizedBox(height: 5),
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.right,
                        controller: iBeforeQty,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height * 0.10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Consump Qty",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                    ),
                    SizedBox(height: 5),
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.right,
                        controller: iActualQty,
                        focusNode: nodeActualQty,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        onEditingComplete: () {
                          FocusScope.of(context).requestFocus(nodeButton);
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Unit",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iUnit,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  fillColor: disabledColor,
                  filled: true,
                ),
              ),
            ),
            // SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 20),
            //   child: Align(
            //     alignment: Alignment.centerLeft,
            //     child: Text(
            //       "Expired Date",
            //       style: TextStyle(
            //           fontFamily: 'Arial', fontWeight: FontWeight.bold),
            //     ),
            //   ),
            // ),
            // SizedBox(height: MediaQuery.of(context).size.height * 0.0),
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 10),
            //   child: TextFormField(
            //     controller: iExpDate,
            //     enabled: false,
            //     decoration: InputDecoration(
            //       border: OutlineInputBorder(),
            //       contentPadding: new EdgeInsets.symmetric(
            //           vertical: 10.0, horizontal: 10.0),
            //       fillColor: disabledColor,
            //       filled: true,
            //     ),
            //   ),
            // ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Last User",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: iLastUser,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  fillColor: disabledColor,
                  filled: true,
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Last Update",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: iLastUpdate,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  fillColor: disabledColor,
                  filled: true,
                ),
              ),
            ),
            // Divider(
            //   height: 10,
            //   color: Colors.grey,
            // ),
            SizedBox(height: 10),
            Expanded(
              child: Text(''),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                // height: MediaQuery.of(context).size.height * 0.1,
                // alignment: Alignment.center,
                children: <Widget>[
                  // Padding(
                  //   padding: EdgeInsets.symmetric(vertical: 5),
                  //   child:
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: (isProcessing)
                          ? disabledButtonColor
                          : enabledButtonColor,
                      minimumSize:
                          Size(MediaQuery.of(context).size.width * 0.90, 55),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                    ),
                    // onPressed: () {
                    //   asyncSubmit(iBarcodeNo.text, iMaterialCode.text,
                    //       iLotNo.text, iBeforeQty.text, iActualQty.text);
                    // },
                    onPressed: (isProcessing == false)
                        ? () async {
                            //if buttonenabled == true then pass a function otherwise pass "null"
                            asyncSubmit(iBarcodeNo.text, iMaterialCode.text,
                                iLotNo.text, iBeforeQty.text, iActualQty.text);
                            //plug delayed for wait while processing data
                            await Future.delayed(const Duration(seconds: 3),
                                () {
                              setState(() {
                                isProcessing = false;
                              });
                            });
                          }
                        : null,
                    child: Text(
                      "SUBMIT",
                      style: TextStyle(
                        fontSize: 18,
                        color: (isProcessing)
                            ? disabledColorButtonText
                            : enabledColorButtonText,
                      ),
                    ),
                  ),
                  // ),
                ]),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
          ],
        ),
      ),
    );
  }
}
