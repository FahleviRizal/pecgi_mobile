import 'package:flutter/material.dart';
import 'package:panasonic/models/clsDeliveryScan.dart';
//import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/screens/deliveryscan/deliveryscandetail.dart';
import 'package:panasonic/screens/deliveryscan/deliveryscanlist.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/utilities/message.dart';

class DeliveryScan extends StatefulWidget {
  const DeliveryScan({Key key, this.userid}) : super(key: key);

  final String userid;
  @override
  _DeliveryScanState createState() => _DeliveryScanState();
}

class _DeliveryScanState extends State<DeliveryScan> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsDeliveryScan> listGetData = [];
  List<ClsDeliveryScan> listGetDataFiltered = [];
  List<ClsDeliveryScan> listInvoiceNo;
  List<ClsDeliveryScan> listInvoiceNoFiltered;

  final TextEditingController txtInvoiceNo = new TextEditingController();
  final TextEditingController txtCustomer = new TextEditingController();
  final TextEditingController txtPONo = new TextEditingController();
  final TextEditingController txtScanBarcode = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();

  String _searchcombo = '';
  String _searchDataResult = '';
  String _searchInvoiceNo = '';
  String _scanBarcode = '';
  //String _selectedSuratJalanNo = '';

  FocusNode nodeInvoice = FocusNode();
  FocusNode nodeBarcode = FocusNode();

  @override
  void initState() {
    asynclistGetData("");
    super.initState();
  }

  void asynclistGetData(String invoiceNo) async {
    listGetData = await ClsDeliveryScan.getData(invoiceNo);
    listGetDataFiltered = listGetData;
    setState(() {});
  }

  void deliveryList(String type) {
    setState(() {
      ClsDeliveryScan listData = new ClsDeliveryScan();
      listData.invoiceNo = txtInvoiceNo.text;
      listData.customerName = txtCustomer.text;
      listData.poNo = txtPONo.text;
      listData.type = type;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DeliveryScanList(),
          settings: RouteSettings(
            arguments: listData,
          ),
        ),
      );
    });
  }

  void deliveryDetail(String barcodeno) {
    if (txtInvoiceNo.text == '') {
      Message.box(
          type: '2',
          message: 'Please input/scan invoice',
          top: 0,
          position: "0",
          context: context);

      FocusScope.of(context).requestFocus(nodeInvoice);
      return;
    }

    if (barcodeno == '') {
      Message.box(
          type: '2',
          message: 'Please scan barcode no',
          top: 0,
          position: "0",
          context: context);

      FocusScope.of(context).requestFocus(nodeBarcode);
      return;
    }
    setState(() {
      ClsDeliveryScan listData = new ClsDeliveryScan();
      listData.invoiceNo = txtInvoiceNo.text;
      listData.barcodeNo = txtScanBarcode.text;

      Navigator.of(context)
          .push(
            MaterialPageRoute(
              builder: (context) => DeliveryScanDetail(userid: widget.userid),
              settings: RouteSettings(
                arguments: listData,
              ),
            ),
          )
          .then((value) => setState(() {
                asynclistGetData(txtInvoiceNo.text);
                txtScanBarcode.text = '';
              }));

      // Navigator.of(context)
      //     .push(
      //       context,
      //       MaterialPageRoute(
      //         builder: (context) => DeliveryScanDetail(userid: widget.userid),
      //         settings: RouteSettings(
      //           arguments: listData,
      //         ),
      //       ),
      //     )
      //     .then((value) => setState(() {
      //           asynclistGetData(txtInvoiceNo.text);
      //           txtScanBarcode.text = '';
      //         }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Delivery Scan"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Invoice No",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                textInputAction: TextInputAction.none,
                                controller: txtInvoiceNo,
                                focusNode: nodeInvoice,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  labelText: '',
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        if (txtInvoiceNo.text != "") {
                                          txtInvoiceNo.clear();
                                          _searchInvoiceNo = '';
                                          //listGetDataFiltered.clear();

                                          FocusScope.of(context)
                                              .requestFocus(nodeBarcode);
                                        }
                                      });
                                    },
                                    icon: Icon(_searchInvoiceNo == ''
                                        ? Icons.cancel
                                        : Icons.cancel_outlined),
                                    color: _searchInvoiceNo == ''
                                        ? Colors.white12.withOpacity(1)
                                        : Colors.grey[600],
                                  ),
                                ),
                                onChanged: (value) {
                                  _searchInvoiceNo = value;

                                  setState(() {});
                                },
                                onFieldSubmitted: (value) {
                                  asynclistGetData(value);
                                },
                              ),
                              // child: TypeAheadField(
                              //   hideSuggestionsOnKeyboardHide: false,
                              //   textFieldConfiguration: TextFieldConfiguration(
                              //       textInputAction: TextInputAction.done,
                              //       onSubmitted: (value) {
                              //         txtInvoiceNo.text = value;
                              //         _searchcombo = value;

                              //         listInvoiceNoFiltered
                              //             .where((result) => result.invoiceNo
                              //                 .toLowerCase()
                              //                 .contains(value.toLowerCase()))
                              //             .toList();

                              //         if (listInvoiceNoFiltered.length > 0) {
                              //           this.txtCustomer.text =
                              //               listInvoiceNoFiltered[0]
                              //                   .customerName;
                              //           this.txtPONo.text =
                              //               listInvoiceNoFiltered[0].poNo;
                              //         }
                              //         FocusScope.of(context)
                              //             .requestFocus(nodeBarcode);
                              //         asynclistGetData(value);
                              //       },
                              //       focusNode: node,
                              //       controller: this.txtInvoiceNo,
                              //       style: TextStyle(fontFamily: 'Arial'),
                              //       decoration: InputDecoration(
                              //         suffixIcon: IconButton(
                              //           onPressed: () {
                              //             setState(() {
                              //               txtInvoiceNo.clear();
                              //               _searchcombo = '';
                              //               txtCustomer.text = '';
                              //               txtPONo.text = '';
                              //               listGetDataFiltered.clear();
                              //             });
                              //           },
                              //           icon: Icon(_searchcombo == ''
                              //               ? Icons.cancel
                              //               : Icons.cancel_outlined),
                              //           color: _searchcombo == ''
                              //               ? Colors.white12.withOpacity(1)
                              //               : Colors.grey[600],
                              //         ),
                              //         border: OutlineInputBorder(),
                              //         contentPadding: new EdgeInsets.symmetric(
                              //             vertical: -10.0, horizontal: 10.0),
                              //       )),
                              //   suggestionsCallback: (pattern) async {
                              //     listInvoiceNo =
                              //         await ClsDeliveryScan.getInvoiceNo();
                              //     _searchInvoiceNo = pattern;
                              //     listInvoiceNoFiltered = listInvoiceNo
                              //         .where((user) => user.invoiceNo
                              //             .toLowerCase()
                              //             .contains(
                              //                 _searchInvoiceNo.toLowerCase()))
                              //         .toList();
                              //     _searchcombo = pattern;
                              //     return listInvoiceNoFiltered;
                              //   },
                              //   itemBuilder: (context, suggestion) {
                              //     return ListTile(
                              //       title: Text(suggestion.invoiceNo),
                              //     );
                              //   },
                              //   onSuggestionSelected: (suggestion) async {
                              //     this.txtInvoiceNo.text = suggestion.invoiceNo;
                              //     // _selectedSuratJalanNo =
                              //     //     suggestion.suratJalanNo;
                              //     this.txtCustomer.text =
                              //         suggestion.customerName;
                              //     this.txtPONo.text = suggestion.poNo;
                              //     _searchcombo = suggestion.invoiceNo;
                              //     FocusScope.of(context)
                              //         .requestFocus(nodeBarcode);

                              //     asynclistGetData(suggestion.invoiceNo);
                              //   },
                              // ),
                            ),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.01),
                            // Align(
                            //   alignment: Alignment.centerLeft,
                            //   child: Padding(
                            //     padding:
                            //         EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                            //     child: Text(
                            //       "Customer Name",
                            //       style: TextStyle(
                            //           fontFamily: 'Arial',
                            //           fontWeight: FontWeight.bold),
                            //     ),
                            //   ),
                            // ),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.0),
                            // Padding(
                            //   padding: EdgeInsets.symmetric(horizontal: 10),
                            //   child: TextFormField(
                            //     controller: txtCustomer,
                            //     enabled: false,
                            //     decoration: InputDecoration(
                            //       border: OutlineInputBorder(),
                            //       contentPadding: new EdgeInsets.symmetric(
                            //           vertical: 10.0, horizontal: 10.0),
                            //       fillColor: disabledColor,
                            //       filled: true,
                            //     ),
                            //   ),
                            // ),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.01),
                            // Align(
                            //   alignment: Alignment.centerLeft,
                            //   child: Padding(
                            //     padding:
                            //         EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                            //     child: Text(
                            //       "PO No",
                            //       style: TextStyle(
                            //           fontFamily: 'Arial',
                            //           fontWeight: FontWeight.bold),
                            //     ),
                            //   ),
                            // ),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.0),
                            // Padding(
                            //   padding: EdgeInsets.symmetric(horizontal: 10),
                            //   child: TextFormField(
                            //     controller: txtPONo,
                            //     enabled: false,
                            //     decoration: InputDecoration(
                            //       border: OutlineInputBorder(),
                            //       contentPadding: new EdgeInsets.symmetric(
                            //           vertical: 10.0, horizontal: 10.0),
                            //       fillColor: disabledColor,
                            //       filled: true,
                            //     ),
                            //   ),
                            // ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Scan Label Barcode",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                textInputAction: TextInputAction.none,
                                controller: txtScanBarcode,
                                focusNode: nodeBarcode,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  labelText: '',
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        if (txtScanBarcode.text != "") {
                                          txtScanBarcode.clear();
                                          _scanBarcode = '';
                                          //listGetDataFiltered.clear();

                                          FocusScope.of(context)
                                              .requestFocus(nodeBarcode);
                                        }
                                      });
                                    },
                                    icon: Icon(_scanBarcode == ''
                                        ? Icons.cancel
                                        : Icons.cancel_outlined),
                                    color: _scanBarcode == ''
                                        ? Colors.white12.withOpacity(1)
                                        : Colors.grey[600],
                                  ),
                                ),
                                onChanged: (value) {
                                  _scanBarcode = value;

                                  setState(() {});
                                },
                                onFieldSubmitted: (value) {
                                  deliveryDetail(value);
                                },
                              ),
                            ),
                            Divider(
                              color: Colors.black,
                              height: 20,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                  controller: txtSearch,
                                  decoration: InputDecoration(
                                    labelText: 'Search',
                                    prefixIcon: IconButton(
                                      onPressed: () {},
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.search
                                          : Icons.search),

                                      //Icons.search,
                                    ),
                                    suffixIcon: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          txtSearch.clear();
                                          _searchDataResult = '';
                                          listGetDataFiltered = listGetData;
                                        });
                                      },
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.cancel
                                          : Icons.cancel_outlined),
                                      color: _searchDataResult == ''
                                          ? Colors.white12.withOpacity(1)
                                          : Colors.grey[600],
                                    ),
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      _searchDataResult = value;
                                      // listGetDataFiltered = listGetData
                                      //     .where((user) =>
                                      //         user.MaterialDesc.toLowerCase()
                                      //             .contains(_searchDataResult
                                      //                 .toLowerCase()) ||
                                      //         user.PlanQty.toLowerCase()
                                      //             .contains(_searchDataResult
                                      //                 .toLowerCase()) ||
                                      //         user.ResultQty.toLowerCase()
                                      //             .contains(_searchDataResult
                                      //                 .toLowerCase()) ||
                                      //         user.Location.toLowerCase()
                                      //             .contains(_searchDataResult
                                      //                 .toLowerCase()))
                                      //     .toList();
                                    });
                                  }),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Expanded(
                              child: Container(child: _dataTableWidget()),
                            ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 6,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('No', MediaQuery.of(context).size.width / 6),
      _getTitleItemWidget('Type', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('PlanQty', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget('ScanQty', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('', MediaQuery.of(context).size.width * 0.12),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    if (listGetDataFiltered[index] != null) {
      return Container(
        child: Text(listGetDataFiltered[index].no),
        width: MediaQuery.of(context).size.width / 6,
        height: 45,
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        alignment: Alignment.center,
      );
    }
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    if (listGetDataFiltered[index] != null) {
      return Row(
        children: <Widget>[
          Container(
            child: Text(listGetDataFiltered[index].typeName),
            width: MediaQuery.of(context).size.width / 3,
            height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
            alignment: Alignment.center,
          ),
          Container(
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(listGetDataFiltered[index].planQty),
            ),
            width: MediaQuery.of(context).size.width / 4,
            height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
            alignment: Alignment.centerRight,
          ),
          Container(
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(listGetDataFiltered[index].scanQty),
            ),
            width: MediaQuery.of(context).size.width / 4,
            height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
            alignment: Alignment.centerRight,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.12,
            child: IconButton(
              onPressed: () {
                deliveryList(listGetDataFiltered[index].type);
              },
              icon: Icon(
                Icons.arrow_forward_ios,
                size: 15,
              ),
              color: Colors.black,
              alignment: Alignment.centerRight,
            ),
          ),
          // Container(
          //   child: Row(
          //     children: <Widget>[
          //       IconButton(
          //           onPressed: () {
          //             deliveryList(listGetDataFiltered[index].type);
          //           },
          //           icon: Icon(
          //             Icons.arrow_forward_ios,
          //           )),
          //     ],
          //   ),
          //   width: 10,
          //   height: 45,
          //   padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          //   alignment: Alignment.centerLeft,
          // ),
        ],
      );
    }
  }
}
