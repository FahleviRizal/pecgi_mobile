// import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:panasonic/models/clsDeliveryScan.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:flutter/services.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Man Power Detail',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class DeliveryScanDetail extends StatefulWidget {
  const DeliveryScanDetail({Key key, this.userid}) : super(key: key);

  final String userid;
  @override
  _DeliveryScanDetailState createState() => _DeliveryScanDetailState();
}

class _DeliveryScanDetailState extends State<DeliveryScanDetail> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  bool init = false;
  List<ClsDeliveryScan> listGetData = [];
  String message;
  final TextEditingController txtLotNo = new TextEditingController();
  final TextEditingController txtPalletNo = new TextEditingController();
  final TextEditingController txtQty = new TextEditingController();
  final TextEditingController txtScanBarcodeNo = new TextEditingController();
  final TextEditingController txtInvoiceNo = new TextEditingController();
  final TextEditingController txtType = new TextEditingController();
  final TextEditingController txtTypeName = new TextEditingController();

  String type;
  String _scanBarcode = '';

  FocusNode nodeScanBarcode = FocusNode();
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
  }

  void asynclistGetData() async {
    init = true;
    listGetData = await ClsDeliveryScan.getDataDetail(
        txtScanBarcodeNo.text, txtInvoiceNo.text);

    if (listGetData.length == 0) {
      message = "Data not found";
      type = '2';
      // asyncAlert(message, type);
      Message.box(
          type: type,
          message: message,
          top: 0,
          position: "0",
          context: context);

      FocusScope.of(context).requestFocus(nodeScanBarcode);
      return;
    } else {
      if (listGetData[0].id == "400") {
        message = listGetData[0].description;
        type = '2';
        Message.box(
            type: type,
            message: listGetData[0].description,
            top: 0,
            position: "0",
            context: context);
        //asyncAlert(message, type);

        txtPalletNo.text = "";
        txtType.text = "";
        txtTypeName.text = "";
        txtLotNo.text = "";
        txtQty.text = "";

        FocusScope.of(context).requestFocus(nodeScanBarcode);
      } else {
        txtPalletNo.text = listGetData[0].palletNo;
        txtType.text = listGetData[0].type;
        txtTypeName.text = listGetData[0].typeName;
        txtLotNo.text = listGetData[0].lotNo;
        txtQty.text = listGetData[0].scanQty;

        FocusScope.of(context).requestFocus(nodeScanBarcode);
      }
    }
    setState(() {});
  }

  void asyncSubmit() async {
    setState(() {
      isProcessing = true;
    });

    listGetData = await ClsDeliveryScan.submit(
        txtInvoiceNo.text, txtScanBarcodeNo.text, widget.userid);

    if (listGetData.length == 0) {
      message = "Data not found";
      type = '2';
      Message.box(
          type: type,
          message: message,
          top: 0,
          position: "0",
          context: context);
      //asyncAlert(message, type);
      return;
    } else {
      if (listGetData[0].id == "200") {
        message = "Submit data successfully";
        type = '1';
        //asyncAlert(message, type);
        Message.box(
            type: type,
            message: message,
            top: 0,
            position: "0",
            context: context);

        //txtScanBarcodeNo.text = "";
        txtPalletNo.text = "";
        txtType.text = "";
        txtTypeName.text = "";
        txtLotNo.text = "";
        txtQty.text = "";

        FocusScope.of(context).requestFocus(nodeScanBarcode);
      } else {
        message = listGetData[0].description;
        type = '2';
        Message.box(
            type: type,
            message: message,
            top: 0,
            position: "0",
            context: context);
        //asyncAlert(message, type);
      }
    }

    setState(() {});
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  @override
  Widget build(BuildContext context) {
    final pModels = ModalRoute.of(context).settings.arguments;
    ClsDeliveryScan model = pModels;

    if (init == false) {
      txtScanBarcodeNo.text = model.barcodeNo;
      txtInvoiceNo.text = model.invoiceNo;
      asynclistGetData();
    }
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Scan Detail"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "1. Scan Barcode No",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                autofocus: true,
                                showCursor: true,
                                enableInteractiveSelection: false,
                                textInputAction: TextInputAction.none,
                                controller: txtScanBarcodeNo,
                                focusNode: nodeScanBarcode,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  labelText: '',
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        txtScanBarcodeNo.clear();
                                        _scanBarcode = '';
                                        txtPalletNo.text = "";
                                        txtType.text = "";
                                        txtLotNo.text = "";
                                        txtQty.text = "";

                                        FocusScope.of(context)
                                            .requestFocus(nodeScanBarcode);
                                      });
                                    },
                                    icon: Icon(_scanBarcode == ''
                                        ? Icons.cancel
                                        : Icons.cancel_outlined),
                                    color: _scanBarcode == ''
                                        ? Colors.white12.withOpacity(1)
                                        : Colors.grey[600],
                                  ),
                                ),
                                onChanged: (value) {
                                  _scanBarcode = value;

                                  setState(() {});
                                },
                                onFieldSubmitted: (value) {
                                  asynclistGetData();
                                },
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Pallet No",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtPalletNo,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                    fillColor: disabledColor,
                                    filled: true),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Surat Jalan",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtInvoiceNo,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Type",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtTypeName,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Lot No",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtLotNo,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Qty",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtQty,
                                enabled: false,
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.right,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            Expanded(
                              child: SizedBox(
                                  height: MediaQuery.of(context).size.height),
                            ),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  TextButton(
                                    style: TextButton.styleFrom(
                                      backgroundColor: (isProcessing)
                                          ? disabledButtonColor
                                          : enabledButtonColor,
                                      minimumSize: Size(
                                          MediaQuery.of(context).size.width *
                                              0.90,
                                          55),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50.0),
                                      ),
                                    ),
                                    onPressed: (isProcessing == false)
                                        ? () async {
                                            //if buttonenabled == true then pass a function otherwise pass "null"
                                            asyncSubmit();

                                            //plug delayed for wait while processing data
                                            await Future.delayed(
                                                const Duration(seconds: 3), () {
                                              setState(() {
                                                isProcessing = false;
                                                txtScanBarcodeNo.text = "";
                                              });
                                            });
                                          }
                                        : null,
                                    child: Text(
                                      "SUBMIT",
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: (isProcessing)
                                              ? disabledColorButtonText
                                              : enabledColorButtonText),
                                    ),
                                  ),
                                  // ),
                                ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            // Container(
                            //   alignment: Alignment.centerRight,
                            //   child: Padding(
                            //     padding: EdgeInsets.symmetric(
                            //         horizontal: 15, vertical: 15),
                            //     child: TextButton(
                            //       style: TextButton.styleFrom(
                            //         backgroundColor: Colors.blue,
                            //         minimumSize: Size(150, 40),
                            //       ),
                            //       onPressed: () {
                            //         asyncSubmit();
                            //       },
                            //       child: Text(
                            //         "Submit",
                            //         style: TextStyle(
                            //           fontSize: 16,
                            //           color: Colors.white,
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }
}
