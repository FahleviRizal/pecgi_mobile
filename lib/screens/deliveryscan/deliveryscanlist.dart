// import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:panasonic/models/clsDeliveryScan.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';

class DeliveryScanList extends StatefulWidget {
  @override
  _DeliveryScanListState createState() => _DeliveryScanListState();
}

class _DeliveryScanListState extends State<DeliveryScanList> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsDeliveryScan> listGetData = [];
  List<ClsDeliveryScan> listGetDataFiltered = [];
  List<ClsDeliveryScan> listScanBarcode = [];
  List<ClsDeliveryScan> listSupplyNo;
  List<ClsDeliveryScan> listSupplyNoFiltered;

  final TextEditingController txtInvoiceNo = new TextEditingController();
  final TextEditingController txtCustomer = new TextEditingController();
  final TextEditingController txtPONo = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();

  String _searchDataResult = '';
  String type = '';
  // String _searchSupplyNo = '';
  // String _selectedSupplyNo = '';
  bool init = false;

  @override
  void initState() {
    // asynclistGetData();
    super.initState();
  }

  void asynclistGetData() async {
    init = true;
    listGetData = await ClsDeliveryScan.getDataList(txtInvoiceNo.text, type);
    listGetDataFiltered = listGetData;
    if (listGetDataFiltered.length > 0) {
      // txtInstructionNo.text = listGetDataFiltered[0].InstNo;
      // txtUnit.text = listGetDataFiltered[0].UnitName;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final pModels = ModalRoute.of(context).settings.arguments;
    ClsDeliveryScan model = pModels;

    // txtSuratJalanNo.text = "SJ0001";
    // txtCustomer.text = "Customer 1";
    // txtPONo.text = "Customer 1";
    // Type = "TYP0001";

    if (init == false) {
      txtInvoiceNo.text = model.invoiceNo;
      txtCustomer.text = model.customerName;
      txtPONo.text = model.poNo;
      type = model.type;
      asynclistGetData();
    }
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Delivery Scan Detail"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Invoice No",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtInvoiceNo,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  fillColor: disabledColor,
                                  filled: true,
                                ),
                              ),
                            ),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.01),
                            // Align(
                            //   alignment: Alignment.centerLeft,
                            //   child: Padding(
                            //     padding:
                            //         EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                            //     child: Text(
                            //       "Customer Name",
                            //       style: TextStyle(
                            //         fontFamily: 'Arial',
                            //         fontWeight: FontWeight.bold,
                            //       ),
                            //     ),
                            //   ),
                            // ),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.0),
                            // Padding(
                            //   padding: EdgeInsets.symmetric(horizontal: 10),
                            //   child: TextFormField(
                            //     controller: txtCustomer,
                            //     enabled: false,
                            //     decoration: InputDecoration(
                            //       border: OutlineInputBorder(),
                            //       contentPadding: new EdgeInsets.symmetric(
                            //           vertical: 10.0, horizontal: 10.0),
                            //       fillColor: disabledColor,
                            //       filled: true,
                            //     ),
                            //   ),
                            // ),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.01),
                            // Align(
                            //   alignment: Alignment.centerLeft,
                            //   child: Padding(
                            //     padding:
                            //         EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                            //     child: Text(
                            //       "PO No",
                            //       style: TextStyle(
                            //         fontFamily: 'Arial',
                            //         fontWeight: FontWeight.bold,
                            //       ),
                            //     ),
                            //   ),
                            // ),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.0),
                            // Padding(
                            //   padding: EdgeInsets.symmetric(horizontal: 10),
                            //   child: TextFormField(
                            //     controller: txtPONo,
                            //     enabled: false,
                            //     decoration: InputDecoration(
                            //       border: OutlineInputBorder(),
                            //       contentPadding: new EdgeInsets.symmetric(
                            //           vertical: 10.0, horizontal: 10.0),
                            //       fillColor: disabledColor,
                            //       filled: true,
                            //     ),
                            //   ),
                            // ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                  controller: txtSearch,
                                  decoration: InputDecoration(
                                    labelText: 'Search',
                                    prefixIcon: IconButton(
                                      onPressed: () {},
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.search
                                          : Icons.search),

                                      //Icons.search,
                                    ),
                                    suffixIcon: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          txtSearch.clear();
                                          _searchDataResult = '';
                                          listGetDataFiltered = listGetData;
                                        });
                                      },
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.cancel
                                          : Icons.cancel_outlined),
                                      color: _searchDataResult == ''
                                          ? Colors.white12.withOpacity(1)
                                          : Colors.grey[600],
                                    ),
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      _searchDataResult = value;
                                      listGetDataFiltered = listGetData
                                          .where((user) =>
                                              user.palletNo
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()) ||
                                              user.barcodeNo
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()) ||
                                              user.lotNo.toLowerCase().contains(
                                                  _searchDataResult
                                                      .toLowerCase()) ||
                                              user.scanQty
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()))
                                          .toList();
                                    });
                                  }),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Expanded(
                              child: Container(child: _dataTableWidget()),
                            ),
                            // SizedBox(
                            //   height: 300,
                            //   child: SingleChildScrollView(
                            //     scrollDirection: Axis.horizontal,
                            //     child: SingleChildScrollView(
                            //       scrollDirection: Axis.vertical,
                            //       // children: <Widget>[
                            //       child: DataTable(
                            //           columns: <DataColumn>[
                            //             DataColumn(
                            //               label: Container(
                            //                 width: 65.00,
                            //                 child: Text(
                            //                   'Pallet No',
                            //                   style: TextStyle(
                            //                       fontFamily: 'Arial',
                            //                       fontWeight: FontWeight.bold),
                            //                 ),
                            //               ),
                            //             ),
                            //             DataColumn(
                            //               label: Text(
                            //                 'Barcode No',
                            //                 style: TextStyle(
                            //                     fontFamily: 'Arial',
                            //                     fontWeight: FontWeight.bold),
                            //               ),
                            //             ),
                            //             DataColumn(
                            //               label: Text(
                            //                 'Lot No',
                            //                 style: TextStyle(
                            //                     fontFamily: 'Arial',
                            //                     fontWeight: FontWeight.bold),
                            //               ),
                            //             ),
                            //             DataColumn(
                            //               label: Container(
                            //                 width: 50.00,
                            //                 child: Text(
                            //                   'Qty',
                            //                   style: TextStyle(
                            //                       fontFamily: 'Arial',
                            //                       fontWeight: FontWeight.bold),
                            //                 ),
                            //               ),
                            //             ),
                            //           ],
                            //           rows: List.generate(
                            //             listGetDataFiltered.length,
                            //             (index) => DataRow(
                            //               cells: <DataCell>[
                            //                 DataCell(Text(
                            //                     listGetDataFiltered[index]
                            //                         .PalletNo)),
                            //                 DataCell(Text(
                            //                     listGetDataFiltered[index]
                            //                         .BarcodeNo)),
                            //                 DataCell(Text(
                            //                     listGetDataFiltered[index]
                            //                         .LotNo)),
                            //                 DataCell(
                            //                   Container(
                            //                     alignment:
                            //                         Alignment.centerRight,
                            //                     child: Text(
                            //                       listGetDataFiltered[index]
                            //                           .ScanQty,
                            //                       textAlign: TextAlign.right,
                            //                     ),
                            //                   ),
                            //                 ),
                            //               ],
                            //             ),
                            //           )),
                            //       // ]
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Pallet No', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Barcode No', MediaQuery.of(context).size.width / 2.5),
      _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 4.5),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].palletNo),
      width: MediaQuery.of(context).size.width / 3,
      height: 45,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(listGetDataFiltered[index].barcodeNo),
          width: MediaQuery.of(context).size.width / 2.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(listGetDataFiltered[index].lotNo),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(listGetDataFiltered[index].scanQty),
          ),
          width: MediaQuery.of(context).size.width / 4.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
