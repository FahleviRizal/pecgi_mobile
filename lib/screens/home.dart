// Dropdown Button
// import 'dart:async';
// import 'dart:typed_data';
// import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
//import 'package:network_info_plus/network_info_plus.dart';
//import 'package:panasonic/models/clsSyncLamp.dart';
import 'package:panasonic/models/clsmenu.dart';
import 'package:panasonic/models/clswarehouse.dart';
import 'package:panasonic/screens/login.dart';
import 'package:panasonic/screens/warehouseselect.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/controlbutton.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/services/userservice.dart';
import 'package:panasonic/utilities/message.dart';
//import 'package:usb_serial/transaction.dart';
//import 'package:usb_serial/usb_serial.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: MyHomePage(title: ''),
    );
  }
}

//Start Sync Towerlamp
// class SyncTowerlamp extends StatefulWidget {
//   const SyncTowerlamp({Key key, this.title, this.userid}) : super(key: key);
//   final String title;
//   final String userid;
//   @override
//   _SyncTowerlampState createState() => _SyncTowerlampState();
// }

// class _SyncTowerlampState extends State<SyncTowerlamp> {
//   bool _lampstatus = false;

//   List<clsSyncLamp> _listgetdatalamp = [];
//   List<Widget> _serialData = [];
//   List<Widget> _ports = [];

//   StreamSubscription<String> _subscription;
//   String _cmdReturn = "";
//   String _status = "Idle";
//   String _datesend = "";
//   String _datereceive = "";
//   String _carryno = "";
//   String _ipaddress = "";
//   String _lampID = "";
//   String _remarks = "";
//   String _user = "";

//   Timer timeron;
//   Timer timeroff;

//   Transaction<String> _transaction;

//   UsbPort _port;
//   UsbDevice _device;

//   @override
//   void initState() {
//     _user = widget.userid;
//     UsbSerial.usbEventStream.listen((UsbEvent event) {
//       _getPorts();
//       _loadConnect();
//     });

//     _getPorts();
//     _loadConnect();

//     super.initState();

//     timeron = Timer.periodic(
//         Duration(milliseconds: 1500), (Timer t1) => _firstload());
//     timeroff = Timer.periodic(
//         Duration(milliseconds: 1500), (Timer t2) => _setPatlite(false));
//   }

//   Future<void> _firstload() async {
//     print("test 1");
//     await _setPatlite(true);
//   }

//   // @override
//   // void dispose() {
//   //   timeroff?.cancel();
//   //   timeron?.cancel();
//   //   super.dispose();
//   // }

//   Future<bool> _connectTo(device) async {
//     _serialData.clear();

//     if (_subscription != null) {
//       _subscription.cancel();
//       _subscription = null;
//     }

//     if (_transaction != null) {
//       _transaction.dispose();
//       _transaction = null;
//     }

//     if (_port != null) {
//       _port.close();
//       _port = null;
//     }

//     if (device == null) {
//       _device = null;
//       setState(() {});
//       return true;
//     }

//     _port = await device.create();
//     if (await (_port.open()) != true) {
//       setState(() {});
//       return false;
//     }
//     _device = device;

//     await _port.setDTR(true);
//     await _port.setRTS(true);
//     await _port.setPortParameters(
//         9600, UsbPort.DATABITS_8, UsbPort.STOPBITS_1, UsbPort.PARITY_NONE);

//     _transaction = Transaction.stringTerminated(
//         _port.inputStream as Stream<Uint8List>, Uint8List.fromList([13, 10]));

//     _subscription = _transaction.stream.listen((String line) {
//       setState(() async {
//         _cmdReturn = line;
//         String dataFeedbackON = _lampID + ":LEDON";
//         String dataFeedbackOFF = _lampID + ":LEDOFF";

//         if ((_cmdReturn.contains(dataFeedbackON) == true ||
//                 _cmdReturn == dataFeedbackON) &&
//             _lampstatus == false) {
//           _lampstatus = true;

//           if (_datesend != "") {
//             _datesend = "";
//             DateTime now = DateTime.now();
//             _datereceive = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

//             List<clsSyncLamp> _listinsdatalamp = [];
//             _listinsdatalamp = await clsSyncLamp.insTowerlampHistory(_carryno,
//                 _lampID, _ipaddress, "1", _cmdReturn, _datereceive, _user);
//           }
//         }
//         if ((_cmdReturn.contains(dataFeedbackOFF) == true ||
//                 _cmdReturn == dataFeedbackOFF) &&
//             _lampstatus == true) {
//           timeron.cancel();
//           timeroff.cancel();

//           _lampstatus = false;

//           if (_datesend != "") {
//             _datesend = "";
//             DateTime now = DateTime.now();
//             _datereceive = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

//             List<clsSyncLamp> _listinsdatalamp = [];
//             _listinsdatalamp = await clsSyncLamp.insTowerlampHistory(_carryno,
//                 _lampID, _ipaddress, "1", _cmdReturn, _datereceive, _user);

//             List<clsSyncLamp> _listupdmscarry = [];
//             _listupdmscarry =
//                 await clsSyncLamp.updMSCarry('1', _carryno, "2", '', '', _user);
//             _lampID = "";
//             Future.delayed(const Duration(seconds: 2), () {
//               timeron = Timer.periodic(
//                   Duration(milliseconds: 1500), (Timer t3) => _firstload());
//               timeroff = Timer.periodic(Duration(milliseconds: 1500),
//                   (Timer t4) => _setPatlite(false));
//             });
//           }
//         }

//         if (_serialData.length > 20) {
//           _serialData.removeAt(0);
//         }
//       });
//     });

//     setState(() {
//       _status = "Connected";
//     });
//     return true;
//   }

//   void _loadConnect() async {
//     List<UsbDevice> devices = await UsbSerial.listDevices();
//     bool _flagConnect = true;
//     for (var device in devices) {
//       _flagConnect = !_flagConnect;
//       print('onPressed - connect to device');
//       print(_flagConnect);
//       _connectTo(_device == device ? null : device).then((res) {});
//     }
//     _getPorts();
//   }

//   void _getPorts() async {
//     _ports = [];
//     bool _flagConnect = true;

//     List<UsbDevice> devices = await UsbSerial.listDevices();
//     if (!devices.contains(_device)) {
//       _connectTo(null);
//     }
//     for (var device in devices) {
//       _ports.add(ListTile(
//           leading: const Icon(Icons.usb),
//           title: Text('Product : ' + device.productName),
//           subtitle: Text('Dev ID : ' + device.deviceId.toString()),
//           trailing: ElevatedButton.icon(
//             label: Text(_device == device ? "Disconnect" : "Connect"),
//             icon: Icon(
//               _device == device ? Icons.sensors_off : Icons.sensors,
//               color: _device == device ? Colors.white : Colors.yellow,
//               size: 20.0,
//             ),
//             style: ElevatedButton.styleFrom(
//               primary: Colors.indigo,
//               onPrimary: _device == device ? Colors.white : Colors.yellow,
//               textStyle: TextStyle(
//                 color: Colors.blue,
//                 fontSize: 12,
//               ),
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(32.0)),
//               minimumSize: Size(130, 40),
//             ),
//             onPressed: () {
//               _flagConnect = !_flagConnect;

//               _connectTo(_device == device ? null : device).then((res) {
//                 _getPorts();
//               });
//             },
//           )));

//       _flagConnect = !_flagConnect;

//       _connectTo(_device == device ? null : device).then((res) {
//         // _getPorts();
//       });
//       // }

//     }

//     setState(() {
//       // print(_ports);
//     });
//   }

//   Future<void> _setPatlite(_flagLampON) async {
//     String data = "";
//     if (_port == null) {
//       UsbSerial.usbEventStream.listen((UsbEvent event) {
//         _getPorts();
//       });
//       _getPorts();
//       return;
//     }

//     if ((_lampstatus == false && _flagLampON == true ||
//         _lampstatus == true && _flagLampON == false)) {
//       if (_lampID == "") {
//         final info = NetworkInfo();
//         _ipaddress = await info.getWifiIP();

//         _listgetdatalamp = await clsSyncLamp.getDataCarry(_ipaddress);
//         if (_listgetdatalamp.length > 0) {
//           _lampID = _listgetdatalamp[0].LampID;
//           _carryno = _listgetdatalamp[0].CarryNo;
//         } else {
//           return;
//         }
//       }

//       if (_flagLampON == true) {
//         data = _lampID + ":LED1:ON";
//         _remarks = _lampID + ":LED1:ON";
//       } else {
//         data = _lampID + ":LED1:OFF";
//         _remarks = _lampID + ":LED1:OFF";
//       }

//       data = data + "\r\n";

//       if (data.length >= 12 && data.contains(':')) {
//         _cmdReturn = "";
//       } else {
//         _cmdReturn = "";
//       }

//       await _port.write(Uint8List.fromList(data.codeUnits));

//       if (_datesend == "") {
//         _datereceive = "";
//         DateTime now = DateTime.now();
//         _datesend = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

//         List<clsSyncLamp> _listinsdatalamp = [];
//         _listinsdatalamp = await clsSyncLamp.insTowerlampHistory(
//             _carryno, _lampID, _ipaddress, "0", _remarks, _datesend, _user);
//       }
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter 100 Days',
//       theme: ThemeData(
//         primarySwatch: Colors.indigo,
//       ),
//       home: MyHomePage(
//         title: 'Menu ROW',
//         userid: widget.userid,
//         port: _port,
//         device: _device,
//       ),
//     );
//   }
// }
//End Sync Towerlamp

class MyHomePage extends StatefulWidget {
  const MyHomePage(
      {Key key,
      this.title,
      this.userid,
      this.whcode,
      this.whname,
      this.version})
      : super(key: key);
  final String title;
  final String userid;
  final String whcode;
  final String whname;
  final String version;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  APIService api = APIService();
  List<ClsMenu> listMenu = [];

  void asyncListMenu() async {
    listMenu = await api.getMenu(widget.userid).catchError((error, stackTrace) {
      //asyncAlert(error.toString(), "1");
      // ignore: invalid_return_type_for_catch_error
      return Future.error(Message.box(
          type: "2",
          message: error.toString(),
          position: "1",
          top: 0,
          context: context));
    });
  }

  @override
  void initState() {
    super.initState();
    // asyncListMenu();
  }

  Future<void> selectedItem(BuildContext context, item) async {
    switch (item) {
      case 0:
        List<ClsWarehouse> listD =
            await ClsWarehouse.getComboWarehouse(widget.userid)
                .catchError((error, stackTrace) {
          // ignore: invalid_return_type_for_catch_error
          return Future.error(Message.box(
              type: "2",
              message: error.toString(),
              position: "0",
              top: 45,
              context: context));
        });
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => WarehouseSelect(
                  userid: widget.userid,
                  list: listD,
                  version: widget.version,
                )));
        break;
      case 1:
        print("Logged out");
        var dialog = CustomAlertDialog(
            type: "1",
            title: "Logout",
            message: "Are you sure, do you want to logout?",
            onOkPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) => LoginScreen(
                            lastuserid: widget.userid,
                          )),
                  (route) => false);
            },
            okBtnText: 'Yes',
            noBtnText: 'No');
        showDialog(context: context, builder: (BuildContext context) => dialog);

        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    //final userid = ModalRoute.of(context).settings.arguments;
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: kAppBarColor,
              title: Text(
                widget.userid + " | " + widget.whname,
                overflow: TextOverflow.clip,
                style: TextStyle(
                    fontFamily: 'Arial',
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
              actions: <Widget>[
                PopupMenuButton<int>(
                  icon: Icon(Icons.more_vert, color: Colors.white),
                  itemBuilder: (context) => [
                    PopupMenuItem<int>(
                      value: 0,
                      child: Row(
                        children: [
                          Icon(
                            Icons.home,
                            color: Colors.lightBlue,
                          ),
                          const SizedBox(
                            width: 7,
                          ),
                          Text("Change Warehouse")
                        ],
                      ),
                    ),
                    PopupMenuItem<int>(
                      value: 1,
                      child: Row(
                        children: [
                          Icon(
                            Icons.logout,
                            color: Colors.lightBlue,
                          ),
                          const SizedBox(
                            width: 7,
                          ),
                          Text("Logout")
                        ],
                      ),
                    ),
                    PopupMenuItem<int>(
                      value: 2,
                      child: Row(
                        children: [
                          Icon(
                            Icons.info,
                            color: Colors.lightBlue,
                          ),
                          const SizedBox(
                            width: 7,
                          ),
                          Text('Version ' + widget.version)
                        ],
                      ),
                    ),
                  ],
                  onSelected: (item) => selectedItem(context, item),
                ),
              ],
              bottom: PreferredSize(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                  child: Text(
                    "Main Menu",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24.0,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w900),
                  ),
                ),
                preferredSize: Size(0.0, 40.0),
              ),
            ),
            body:
                //GridView.count(
                //padding: EdgeInsets.only(top: 10),
                //crossAxisCount: 3,
                //mainAxisSpacing: 3,
                //children: List.generate(listMenu.length, (index) {
                //           var assetsImage = new AssetImage(
                //               'assets/icons/menu/i-receipt.png',); //<- Creates an object that fetches an image.
                //           var image = new Image(
                //             image: Image.asset('assets/icons/menu/i-receipt.png',height: 150,
                // width: 150); //assetsImage,
                //             fit: BoxFit.cover,
                //             color: Colors.lightBlue,
                //           );

                // String imagename =
                //     (listMenu[index].imageName.toString() == "")
                //         ? 'i-receipt.png'
                //         : listMenu[index].imageName.toString();

                FutureBuilder(
                    future: api
                        .getMenu(widget.userid)
                        .catchError((error, stackTrace) {
                      Future.delayed(Duration(seconds: 3));
                      // ignore: invalid_return_type_for_catch_error
                      return Future.error(Message.box(
                          type: "2",
                          message: error.toString(),
                          position: "0",
                          top: 45,
                          context: context));
                    }),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return Center(child: CircularProgressIndicator());
                      }
                      //if (snapshot.hasData) {
                      return GridView.count(
                          padding: EdgeInsets.only(top: 10),
                          crossAxisCount: 3,
                          mainAxisSpacing: 3,
                          children:
                              List.generate(snapshot.data.length, (index) {
                            listMenu = snapshot.data;
                            String imagename =
                                (listMenu[index].imageName.toString() == "")
                                    ? 'i-receipt.png'
                                    : listMenu[index].imageName.toString();

                            return Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: size.width * 0.06),
                                child: Padding(
                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    child: GestureDetector(
                                      child: ControlButton(
                                        size: size,
                                        title: listMenu[index].menuDesc,
                                        icon: Image.asset(
                                          'assets/icons/menu/' + imagename,
                                          height: 10,
                                          width: 100,
                                          color: Colors.blue[800],
                                        ), // Icons.receipt_long_sharp,
                                        idxMenu: listMenu[index].menuIndex,
                                        userid: widget.userid,
                                        warehouse: widget.whcode,
                                        version: widget.version,
                                      ),
                                    )));
                          }));
                    })));
  }
}


/*
ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: size.width * 0.06),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      //SizedBox(height: size.height * 0.02),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ControlButton(
                              size: size,
                              title: 'Receipt\nSchedule',
                              icon: Icons.receipt_long_sharp,
                              idxMenu: 0,
                              userid: userid,
                            ),
                            ControlButton(
                              size: size,
                              title: 'Receipt\nUnschedule',
                              icon: Icons.assignment,
                              idxMenu: 1,
                              userid: userid,
                            ),
                            ControlButton(
                                size: size,
                                title: 'Assign\nTo Storage',
                                icon: Icons.storage_sharp,
                                idxMenu: 2),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ControlButton(
                                size: size,
                                title: 'Supply \nBy Request',
                                icon: Icons.support_outlined,
                                idxMenu: 3),
                            ControlButton(
                                size: size,
                                title: 'Material \nConsump',
                                icon: Icons.data_usage,
                                idxMenu: 4),
                            ControlButton(
                                size: size,
                                title: 'Manpower \nPreparation',
                                icon: Icons.person_pin_sharp,
                                idxMenu: 5,
                                userid: userid),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ControlButton(
                                size: size,
                                title: 'Process Time \nLogging',
                                icon: Icons.timer,
                                idxMenu: 6,
                                userid: userid),
                            ControlButton(
                                size: size,
                                title: 'Material\nRequest Scan',
                                icon: Icons.request_quote,
                                idxMenu: 7,
                                userid: userid),
                            ControlButton(
                                size: size,
                                title: 'Sublot\nGroup. Carry',
                                icon: Icons.group,
                                idxMenu: 8,
                                userid: userid),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ControlButton(
                                size: size,
                                title: 'Aging Cooling \nStorage',
                                icon: Icons.store,
                                idxMenu: 9,
                                userid: userid),
                            ControlButton(
                                size: size,
                                title: 'Delivery \nScan',
                                icon: Icons.delivery_dining,
                                idxMenu: 10,
                                userid: userid),
                            ControlButton(
                                size: size,
                                title: 'Stuffing\nCheck',
                                icon: Icons.check_box,
                                idxMenu: 11,
                                userid: userid),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ControlButton(
                                size: size,
                                title: 'Carry \nInformation',
                                icon: Icons.info_outline,
                                idxMenu: 12),
                            ControlButton(
                                size: size,
                                title: 'Carry Reset \nStatus',
                                icon: Icons.reset_tv_sharp,
                                idxMenu: 13),
                            ControlButton(
                                size: size,
                                title: 'Physical\nInv. Barcode',
                                icon: Icons.qr_code_scanner,
                                idxMenu: 14,
                                userid: userid),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ControlButton(
                                size: size,
                                title: 'Physical\nInv. Carry',
                                icon: Icons.inventory,
                                idxMenu: 15,
                                userid: userid),
                            ControlButton(
                              size: size,
                              title: 'Carry Moving\nLocation',
                              icon: Icons.moving,
                              idxMenu: 16,
                              userid: userid,
                            ),
                            ControlButton(
                                size: size,
                                title: 'Towerlamp\nInformation',
                                icon: Icons.lightbulb,
                                idxMenu: 17),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            */