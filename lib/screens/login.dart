import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:panasonic/models/clswarehouse.dart';
//import 'package:panasonic/screens/prodresultpacking/prodresultpacking.dart';
//import 'package:panasonic/screens/home.dart';
import 'package:panasonic/screens/warehouseselect.dart';
import 'package:panasonic/utilities/curvedleft.dart';
import 'package:panasonic/utilities/curvedleftshadow.dart';
import 'package:panasonic/utilities/curvedright.dart';
import 'package:panasonic/utilities/curvedrightshadow.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:panasonic/models/user.dart';
import 'package:panasonic/services/userservice.dart';
import 'package:panasonic/utilities/message.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key, this.lastuserid}) : super(key: key);
  final String lastuserid;
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class _LoginScreenState extends State<LoginScreen> {
  //bool _rememberMe = false;
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  User loginRequest = new User();
  bool isApiCallProcess = false;
  bool hidePassword = true;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final myUsernameController = TextEditingController();
  final myPasswordController = TextEditingController();
  String nUsername, nPassword;
  //for update latest version
  final String version = "v.0.8";

  void initState() {
    //HttpOverrides.global = new MyHttpOverrides();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    super.initState();
    loginRequest = new User();
    if (widget.lastuserid != '') {
      myUsernameController.text = widget.lastuserid;
    }
  }

  // void validate() {
  //   if (formkey.currentState.validate()) {
  //     print('valid');
  //     Navigator.push(
  //       context,
  //       MaterialPageRoute(
  //         builder: (context) => MyHomePage(
  //           userid: "rizal",
  //         ),
  //         settings: RouteSettings(
  //           arguments: myUsernameController.text,
  //         ),
  //       ),
  //     );
  //   } else {
  //     print('not valid');
  //   }
  // }

  bool validateAndSave() {
    final form = formkey.currentState;
    if (form.validate()) {
      form.save();

      return true;
    }
    return false;
  }

  Future<bool> loginAction() async {
    //replace the below line of code with your login request
    await new Future.delayed(const Duration(seconds: 2));
    return true;
  }

  List<ClsWarehouse> ddlWarehouse = [];
  void listofCombo(String user) async {
    ddlWarehouse = await ClsWarehouse.getComboWarehouse(user);
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  Widget _buildUser() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "User ID",
              style: TextStyle(
                  color: Colors.indigo[800],
                  fontFamily: 'Arial',
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          child: TextFormField(
            onSaved: (input) => loginRequest.userid = input,
            controller: myUsernameController,
            validator: RequiredValidator(errorText: "user is required"),
            keyboardType: TextInputType.text,
            style: TextStyle(
              color: Colors.indigo[800],
              fontFamily: 'Arial',
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.indigo[300].withOpacity(0.1),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.person_rounded,
                color: Colors.indigo[800],
              ),
              hintText: 'Enter your ID',
              hintStyle: TextStyle(
                color: Colors.indigo[800],
                fontWeight: FontWeight.bold,
                fontFamily: 'Arial',
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPassword() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Password",
              style: TextStyle(
                  color: Colors.indigo[800],
                  fontFamily: 'Arial',
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          child: TextFormField(
            onSaved: (input) => loginRequest.password = input,
            validator: RequiredValidator(errorText: "password is required"),
            obscureText: hidePassword,
            style: TextStyle(
              color: Colors.indigo[800],
              fontFamily: 'Arial',
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.indigo[300].withOpacity(0.1),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.indigo[800],
              ),
              suffixIcon: IconButton(
                onPressed: () {
                  setState(() {
                    hidePassword = !hidePassword;
                  });
                },
                color: Theme.of(context)
                    .colorScheme
                    .secondary
                    .withOpacity(0.4), // .accentColor.withOpacity(0.4),
                icon: Icon(
                    hidePassword ? Icons.visibility_off : Icons.visibility),
              ),
              hintText: 'Enter your Password',
              hintStyle: TextStyle(
                color: Colors.indigo[800],
                fontWeight: FontWeight.bold,
                fontFamily: 'Arial',
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * 0.04),
      width: double.infinity,
      child: ElevatedButton(
        //onPressed: () => validate(),
        onPressed: () {
          if (validateAndSave()) {
            setState(() {
              isApiCallProcess = true;
            });

            APIService apiService = new APIService();
            apiService.login(loginRequest).catchError((error, stackTrace) {
              //asyncAlert(error.toString(), "1");
              // ignore: invalid_return_type_for_catch_error
              return Future.error(Message.box(
                  type: "2",
                  message: error.toString(),
                  position: "1",
                  top: 0,
                  context: context));
            }).then((value) async {
              if (value != null) {
                setState(() {
                  isApiCallProcess = false;
                });

                if (value.status == 'OK') {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      });
                  await loginAction();
                  List<ClsWarehouse> listD =
                      await ClsWarehouse.getComboWarehouse(
                              myUsernameController.text)
                          .catchError((error, stackTrace) {
                    // ignore: invalid_return_type_for_catch_error
                    return Future.error(Message.box(
                        type: "2",
                        message: error.toString(),
                        position: "0",
                        top: 45,
                        context: context));
                  });

                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      // builder: (context) => ReceiptUnschedule(
                      //       userid: myUsernameController.text,
                      //       warehouse: '',
                      //     )
                      builder: (context) => WarehouseSelect(
                        version: version,
                        userid: myUsernameController.text,
                        list: listD,
                      ),
                    ),
                  );
                } else {
                  // final snackBar = SnackBar(
                  //   content: Text(value.status),
                  //   backgroundColor: Colors.orangeAccent,
                  // );
                  // scaffoldKey.currentState.showSnackBar(snackBar);

                  Message.box(
                      type: "2",
                      message: value.status,
                      position: "1",
                      top: 0,
                      context: context);
                }
              } else {
                Message.box(
                    type: "2",
                    message: value.status,
                    position: "1",
                    top: 0,
                    context: context);
              }
            });
          }
        },
        style: ButtonStyle(
            padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(0.0)),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
            )),
        child: Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(84, 171, 255, 1),
                  Color.fromRGBO(0, 24, 46, 1),
                  Color.fromRGBO(84, 171, 255, 1),
                ],
              ),
              borderRadius: BorderRadius.all(Radius.circular(80.0))),
          padding: const EdgeInsets.symmetric(
              horizontal: 20), //EdgeInsets.fromLTRB(20, 10, 20, 10),
          width: 400,
          height: MediaQuery.of(context).size.height * 0.07, //55,
          child: Align(
              child: Text(
            'LOGIN',
            style: TextStyle(
              color: Colors.white,
              letterSpacing: 1.8,
              fontSize: 20.0,
              fontWeight: FontWeight.w900,
              fontFamily: 'Arial',
            ),
          )),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: false,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Form(
          key: formkey,
          child: Stack(
            children: <Widget>[
              Container(
                height: double.maxFinite,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [
                      Colors.grey.shade50,
                      Colors.grey.shade50,
                      Colors.grey.shade50,
                      Colors.grey.shade50,
                    ],
                    stops: [0.1, 0.3, 0.6, 0.8],
                  ),
                ),
              ),
              Positioned(top: 0, left: 0, child: CurvedLeft()),
              Positioned(top: 0, left: 0, child: CurvedLeftShadow()),
              Positioned(bottom: 0, left: 0, child: CurvedRight()),
              Positioned(bottom: 0, left: 0, child: CurvedRightShadow()),
              Container(
                height: double.infinity,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.05,
                      vertical: MediaQuery.of(context).size.height * 0.20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Panasonic',
                        style: TextStyle(
                          color: Colors.indigo[900],
                          //Color.fromRGBO(0, 0, 118, 2),
                          fontFamily: 'Arial Helvetica',
                          fontSize: 57.0,
                          fontWeight: FontWeight.w800,
                          shadows: <Shadow>[
                            Shadow(
                              offset: Offset(5.0, 5.0),
                              blurRadius: 8.0,
                              color: Colors.grey,
                            ),
                          ],
                        ),
                      ),
                      Text(
                        'Smart Factory',
                        style: TextStyle(
                          color: Colors.grey.shade500,
                          fontFamily: 'Arial',
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 45.0),
                      _buildUser(),
                      SizedBox(
                        height: 20.0,
                      ),
                      _buildPassword(),
                      _buildLoginBtn(),
                      Text(
                        version,
                        style: TextStyle(
                          color: Colors.grey.shade500,
                          fontFamily: 'Arial Helvetica',
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
