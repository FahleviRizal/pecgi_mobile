import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsmanpowervisual.dart';
import 'package:panasonic/screens/manpowervisual/manpowervisualdetail.dart';
import 'package:panasonic/services/manpowervisualservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Man Power Preparation Visual',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class ManPowerPreparationVisual extends StatefulWidget {
  const ManPowerPreparationVisual({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _ManPowerPreparationVisual createState() => _ManPowerPreparationVisual();
}

class _ManPowerPreparationVisual extends State<ManPowerPreparationVisual> {
  final ManpowerPreparationVisualService api =
      ManpowerPreparationVisualService();
  List<ClsManpowerPreparationVisual> listH;
  List<ClsManpowerPreparationVisual> listLine;
  List<ClsManpowerPreparationVisual> listLineNoFiltered;

  List<ClsManpowerPreparationVisual> searchdata = [
    // ClsManpowerPreparation(
    //     skillName: "Check", totalManpower: "2", remainingscan: "2"),
    // ClsManpowerPreparation(
    //     skillName: "Mixing & Kinding", totalManpower: "4", remainingscan: "10"),
  ];

  TextEditingController iInsNo = TextEditingController();
  TextEditingController iLine = TextEditingController();
  TextEditingController iDate = TextEditingController();
  TextEditingController iLotCard = TextEditingController();
  TextEditingController iEmpId = TextEditingController();
  TextEditingController iSearch = TextEditingController();

  TextEditingController iLineCode = TextEditingController();

  String _searchCombo = '';
  String _searchResult = '';
  String _ddlFilterResult = '';
  String _scanLot = '';
  String _scanEmpID = '';
  String _searchLine = '';

  FocusNode nodeins = FocusNode();
  FocusNode nodeLot = FocusNode();
  FocusNode nodeEmpId = FocusNode();
  FocusNode nodeLine = new FocusNode();

  bool _isEnabled = false;

  @override
  void initState() {
    super.initState();

    asynclistGetData("1", "1", "x");
    print(widget.userid);
  }

  List<ClsManpowerPreparationVisual> ddlInsNo = [];
  List<ClsManpowerPreparationVisual> ddlInsNoFilter = [];

  void asynclistGetData(String insNo, String lineCode, String lot) async {
    listH = await api.getData(insNo, lineCode, lot);
    searchdata = listH;
    setState(() {});
  }

  void _detail(String _insNo, String _line, String _date, String _skillcode,
      String _skillName) {
    setState(() {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ManPowerPreparationDetail(
            insNo: _insNo,
            lineName: _line,
            date: _date,
            skillCode: _skillcode,
            skillName: _skillName,
          ),
          settings: RouteSettings(),
        ),
      );
    });
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void _scanLotCard(String lineCode, String instNo, String lotCard) async {
    if (lineCode == "") {
      Message.box(
          type: "2",
          message: "please scan/select machine process",
          top: 0,
          position: "0",
          context: context);
      FocusScope.of(context).requestFocus(nodeLine);
      return;
    } else if (instNo == "") {
      Message.box(
          type: "2",
          message: "please scan/select instruction",
          top: 0,
          position: "0",
          context: context);
      FocusScope.of(context).requestFocus(nodeins);
      return;
    } else if (lotCard == "") {
      Message.box(
          type: "2",
          message: "please scan lot card",
          top: 0,
          position: "0",
          context: context);
      FocusScope.of(context).requestFocus(nodeLot);
      return;
    }

    List<ClsManpowerPreparationVisual> listScanData;
    listScanData =
        await ManpowerPreparationVisualService.scanLotCard(instNo, lineCode);

    if (listScanData.length > 0) {
      if (listScanData[0].id == "200") {
        setState(() {
          _isEnabled = true;
        });
        setState(() {
          FocusScope.of(context).requestFocus(nodeEmpId);
        });
      } else {
        _isEnabled = false;
        Message.box(
            type: "2",
            message: listScanData[0].description,
            top: 0,
            position: "0",
            context: context);
        iLotCard.clear();
        FocusScope.of(context).requestFocus(nodeLot);
        setState(() {
          _isEnabled = false;
        });
      }
    }
  }

  void _scanEmployee(
      String empID, String insNo, String lineCd, String lotCard) async {
    if (empID == "") {
      Message.box(
          type: "2",
          message: "please select employee",
          top: 0,
          position: "0",
          context: context);
      FocusScope.of(context).requestFocus(nodeEmpId);
      return;
    } else if (insNo == "") {
      Message.box(
          type: "2",
          message: "please select instruction",
          top: 0,
          position: "0",
          context: context);
      FocusScope.of(context).requestFocus(nodeins);
      return;
    } else if (lineCd == "") {
      Message.box(
          type: "2",
          message: "please select line",
          top: 0,
          position: "0",
          context: context);
      FocusScope.of(context).requestFocus(nodeLine);
      return;
    } else if (lotCard == "") {
      Message.box(
          type: "2",
          message: "please scan lot card",
          top: 0,
          position: "0",
          context: context);
      FocusScope.of(context).requestFocus(nodeLine);
      return;
    }

    List<ClsManpowerPreparationVisual> listScanData;
    listScanData = await ManpowerPreparationVisualService.scanSubmitEmployee(
        insNo, lineCd, empID, lotCard, widget.userid);

    if (listScanData.length > 0) {
      if (listScanData[0].id == "200") {
        Message.box(
            type: "1",
            message: listScanData[0].description,
            top: 0,
            position: "0",
            context: context);
        iEmpId.clear();
        _scanEmpID = '';
        FocusScope.of(context).requestFocus(nodeEmpId);
      } else if (listScanData[0].id == "400") {
        Message.box(
            type: "2",
            message: listScanData[0].description,
            top: 0,
            position: "0",
            context: context);
        iEmpId.clear();
        _scanEmpID = '';
        FocusScope.of(context).requestFocus(nodeEmpId);
        //asyncAlert(listGetData[0].description, "2");
      } else {
        Message.box(
            type: "2",
            message: listScanData[0].description,
            top: 0,
            position: "0",
            context: context);
        iEmpId.clear();
        _scanEmpID = '';
        FocusScope.of(context).requestFocus(nodeEmpId);
        //asyncAlert(listGetData[0].description, "2");
      }
    }
    asynclistGetData(iInsNo.text, iLineCode.text, "");
    setState(() {});
  }

  //await api.scanEmployee(insNo, lineCd, empID, widget.userid);

  //     Message.box(
  //         type: (message == "success") ? "1" : "2",
  //         message: (message == "success") ? "Data scan successfully!" : message,
  //         top: 0,
  //         position: "0",
  //         context: context);

  //     //var dialog = CustomAlertDialog(
  //     //  type: (message == "success") ? "3" : "2",
  //     //  title: "",
  //     //  message: (message == "success") ? "Data scan successfully!" : message,
  //     //  okBtnText: 'Close',
  //     //);
  //     //showDialog(context: context, builder: (BuildContext context) => dialog);
  //   } catch (e) {
  //     asyncAlert(e.toString(), "2");
  //     //print('error caught: $e');
  //   }
  // }

  List<ClsManpowerPreparationVisualDetil> listDetail;

  @override
  Widget build(BuildContext context) {
    listH ??= <ClsManpowerPreparationVisual>[];
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text("Man Power Preparation Visual"),
        ),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "1. Select/Scan Machine Process",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TypeAheadField(
                hideSuggestionsOnKeyboardHide: false,
                textFieldConfiguration: TextFieldConfiguration(
                    textInputAction: TextInputAction.done,
                    onSubmitted: (value) {
                      _searchLine = value;
                      iLineCode.text = value;
                      listLineNoFiltered
                          .where((result) => result.lineCode
                              .toLowerCase()
                              .contains(value.toLowerCase()))
                          .toList();

                      final newval = listLineNoFiltered.firstWhere(
                          (item) => item.lineCode == value,
                          orElse: () => null);

                      setState(() {
                        iLine.text = newval.lineName;
                      });
                      FocusScope.of(context).requestFocus(nodeins);
                    },
                    focusNode: nodeLine,
                    controller: this.iLine,
                    style: TextStyle(fontFamily: 'Arial'),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            iLineCode.clear();
                            iLine.text = '';
                            iInsNo.text = '';
                            iDate.text = '';
                            iLotCard.clear();
                            searchdata.clear();
                            _searchLine = '';
                            _searchCombo = '';
                            _isEnabled = false;
                            FocusScope.of(context).requestFocus(nodeLine);
                          });
                        },
                        icon: Icon(_searchLine == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _searchLine == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                      ),
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: -10.0, horizontal: 10.0),
                    )),
                suggestionsCallback: (pattern) async {
                  listLine = await ManpowerPreparationVisualService.getLine(
                      widget.userid);

                  _searchLine = pattern;

                  listLineNoFiltered = listLine
                      .where((user) =>
                          user.lineCode
                              .toLowerCase()
                              .contains(_searchLine.toLowerCase()) ||
                          user.lineName
                              .toLowerCase()
                              .contains(_searchLine.toLowerCase()))
                      .toList();
                  return listLineNoFiltered;
                },
                itemBuilder: (context, suggestion) {
                  return ListTile(
                    title: Text(suggestion.lineCode),
                    subtitle: Text(suggestion.lineName),
                  );
                },
                onSuggestionSelected: (suggestion) async {
                  this.iLineCode.text = suggestion.lineCode;
                  this.iLine.text = suggestion.lineName;
                  _searchLine = suggestion.lineCode;
                  FocusScope.of(context).requestFocus(nodeins);
                },
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "3. Scan Lot Card",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                focusNode: nodeLot,
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                controller: iLotCard,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iLotCard.clear();
                        _scanLot = '';
                        FocusScope.of(context).requestFocus(nodeLot);
                        _isEnabled = false;
                      });
                    },
                    icon: Icon(
                        _scanLot == '' ? Icons.cancel : Icons.cancel_outlined),
                    color: _scanLot == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _scanLot = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  _scanLotCard(iLineCode.text, iInsNo.text, iLotCard.text);
                },
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "2. Scan/Select Instruction No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TypeAheadField(
                hideSuggestionsOnKeyboardHide: false,
                textFieldConfiguration: TextFieldConfiguration(
                    controller: iInsNo,
                    textInputAction: TextInputAction.done,
                    onSubmitted: (value) {
                      iInsNo.text = value;
                      _searchCombo = value;

                      ddlInsNoFilter
                          .where((result) => result.instructionNo
                              .toLowerCase()
                              .contains(value.toLowerCase()))
                          .toList();

                      // final DateTime now =
                      //     DateTime.parse(ddlInsNoFilter[0].prodDate);
                      // final DateFormat formatter = DateFormat('dd MMM yyyy');
                      // final String formatted = formatter.format(now);
                      iDate.text = ddlInsNoFilter[0].prodDate;
                      iInsNo.text = ddlInsNoFilter[0].instructionNo;
                      //iLine.text = ddlInsNoFilter[0].lineName;
                      //iLineCode.text = ddlInsNoFilter[0].lineCode;
                      asynclistGetData(ddlInsNoFilter[0].instructionNo,
                          ddlInsNoFilter[0].lineCode, "");
                      FocusScope.of(context).requestFocus(nodeLot);
                    },
                    focusNode: nodeins,
                    style: TextStyle(fontFamily: 'Arial'),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            iInsNo.clear();
                            iDate.text = '';
                            searchdata.clear();
                            _searchCombo = '';
                            _scanLot = '';
                            iLotCard.clear();
                            _isEnabled = false;
                            FocusScope.of(context).requestFocus(nodeins);
                          });
                        },
                        icon: Icon(_searchCombo == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _searchCombo == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                      ),
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: -10.0, horizontal: 10.0),
                    )),
                suggestionsCallback: (pattern) async {
                  ddlInsNo =
                      await ManpowerPreparationVisualService.getInstructionNo(
                          iLineCode.text, iLotCard.text, widget.userid);
                  _ddlFilterResult = pattern;
                  ddlInsNoFilter = ddlInsNo
                      .where((result) => result.instructionNo
                          .toLowerCase()
                          .contains(_ddlFilterResult.toLowerCase()))
                      .toList();
                  return ddlInsNoFilter;
                },
                itemBuilder: (context, suggestion) {
                  print(suggestion);
                  return ListTile(
                    title: Text(suggestion.typeDesc),
                    subtitle: Text(suggestion.instructionNo),
                  );
                },
                onSuggestionSelected: (suggestion) {
                  print(suggestion);

                  // final DateTime now = DateTime.parse(suggestion.prodDate);
                  // final DateFormat formatter = DateFormat('dd MMM yyyy');
                  // final String formatted = formatter.format(now);
                  iDate.text = suggestion.prodDate;
                  iInsNo.text = suggestion.instructionNo;
                  //iLine.text = suggestion.lineName;
                  //iLineCode.text = suggestion.lineCode;
                  _searchCombo = suggestion.instructionNo;

                  asynclistGetData(iInsNo.text, iLineCode.text, "");
                  FocusScope.of(context).requestFocus(nodeLot);
                },
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Production Date",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iDate,
                enabled: false,
                //style: TextStyle(color: disabledColor),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "4. Scan Employee ID",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                enabled: _isEnabled,
                focusNode: nodeEmpId,
                //enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                controller: iEmpId,
                decoration: InputDecoration(
                  filled: (_isEnabled == false) ? true : false,
                  fillColor:
                      (_isEnabled == false) ? disabledColor : Colors.white10,
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iEmpId.clear();
                        _scanEmpID = '';
                        searchdata.clear();
                        FocusScope.of(context).requestFocus(nodeEmpId);
                      });
                    },
                    icon: Icon(_scanEmpID == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _scanEmpID == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _scanEmpID = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  _scanEmployee(
                      iEmpId.text, iInsNo.text, iLineCode.text, iLotCard.text);
                },
              ),
            ),
            Divider(
              color: Colors.black,
              height: 20,
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iSearch,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: 'Search',
                  prefixIcon: IconButton(
                    onPressed: () {},
                    icon:
                        Icon(_searchResult == '' ? Icons.search : Icons.search),
                    //Icons.search,
                  ),
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iSearch.clear();
                        _searchResult = '';
                        searchdata = listH;
                      });
                    },
                    icon: Icon(_searchResult == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _searchResult == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  setState(() {
                    if (value != null) {
                      _searchResult = value;
                      searchdata = listH
                          .where((rec) =>
                              rec.skillName
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()) ||
                              rec.totalManpower
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()) ||
                              rec.remainingscan
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()))
                          .toList();
                    } else {
                      searchdata = listH;
                    }
                  });
                },
              ),
            ),
            SizedBox(height: 10),
            Expanded(child: _dataTableWidget()),
          ],
        ),
      ),
    );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 1),
      _getTitleItemWidget('Skill', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Total \nMan Power', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget(
          'Remaining \nScan', MediaQuery.of(context).size.width / 3.6),
      _getTitleItemWidget(' ', MediaQuery.of(context).size.width * 0.1),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      //child: Text(searchdata[index].skillName),
      width: 1,
      //height: 45,
      //padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      //alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].skillName),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(searchdata[index].totalManpower),
          width: MediaQuery.of(context).size.width / 4,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(searchdata[index].remainingscan),
          width: MediaQuery.of(context).size.width / 3.6,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: 45,
          height: 45,
          child: IconButton(
            onPressed: () {
              _detail(iInsNo.text, iLine.text, iDate.text,
                  searchdata[index].skillCode, searchdata[index].skillName);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            color: Colors.black,
            alignment: Alignment.centerRight,
          ),
        ),
      ],
    );
  }
}


/*
Container(
                padding: const EdgeInsets.only(top: 10),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      columnSpacing: 25.0,
                      decoration: BoxDecoration(
                          border: Border.all(
                        width: 1,
                        color: Colors.grey[300],
                      )),
                      headingRowColor: MaterialStateColor.resolveWith(
                          (states) => Colors.grey[200]),
                      sortColumnIndex: 0,
                      showCheckboxColumn: false,
                      columns: [
                        DataColumn(
                            label: Container(
                              //width: 60,
                              padding: EdgeInsets.symmetric(horizontal: 0),
                              child: Text(
                                "Skill",
                                style: TextStyle(
                                    fontFamily: "Arial",
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ),
                            numeric: false,
                            tooltip: "Skill Name"),
                        DataColumn(
                          label: Container(

                              //width: 50,
                              child: Text(
                            "Total ManPower",
                            style: TextStyle(
                                fontFamily: "Arial",
                                fontWeight: FontWeight.bold),
                          )),
                          numeric: false,
                          tooltip: "Total Man Power",
                        ),
                        DataColumn(
                            label: Container(
                                //width: 40,
                                child: Text(
                              "Remaining Scan",
                              style: TextStyle(
                                  fontFamily: "Arial",
                                  fontWeight: FontWeight.bold),
                            )),
                            numeric: false,
                            tooltip: "Status"),
                        DataColumn(label: Container(width: 5, child: Text(""))),
                      ],
                      rows: searchdata
                          .map(
                            (idata) => DataRow(
                                onSelectChanged: (b) {
                                  print(idata.skillName);
                                },
                                cells: [
                                  DataCell(
                                      Container(
                                        //width: 80,
                                        child: Text(idata.skillName),
                                      ),
                                      onTap: () {}),
                                  //Container(width: 75, child: Text(idata.materialName)),
                                  // ),
                                  DataCell(
                                    Container(
                                      //width: 38,
                                      child: Text(idata.totalManpower),
                                      alignment: Alignment.centerRight,
                                    ),
                                  ),
                                  DataCell(
                                    Container(
                                      //width: 40,
                                      child: Text(idata.remainingscan),
                                      alignment: Alignment.centerRight,
                                    ),
                                  ),
                                  DataCell(
                                    Container(
                                      width: 5,
                                      child: IconButton(
                                        onPressed: () {
                                          _detail(
                                              iInsNo.text,
                                              iLine.text,
                                              iDate.text,
                                              idata.skillCode,
                                              idata.skillName);
                                          print('onpress');
                                        },
                                        icon: Icon(
                                          Icons.arrow_forward_ios,
                                          size: 15,
                                        ),
                                        color: Colors.black,
                                      ),
                                    ),
                                    onTap: () {
                                      _detail(
                                          iInsNo.text,
                                          iLine.text,
                                          iDate.text,
                                          idata.skillCode,
                                          idata.skillName);
                                    },
                                    //IconButton(),
                                  ),
                                ]),
                          )
                          .toList(),
                    ),
                  ),
                ),
              ),
*/