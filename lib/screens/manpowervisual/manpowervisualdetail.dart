import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsmanpower.dart';
import 'package:panasonic/services/manpowerservice.dart';
import 'package:panasonic/utilities/constants.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Man Power Detail',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class ManPowerPreparationDetail extends StatefulWidget {
  const ManPowerPreparationDetail(
      {Key key,
      this.userid,
      this.insNo,
      this.lineName,
      this.date,
      this.skillCode,
      this.skillName})
      : super(key: key);

  final String userid;
  final String insNo;
  final String lineName;
  final String date;
  final String skillCode;
  final String skillName;

  @override
  _ManPowerPreparationDetail createState() => _ManPowerPreparationDetail();
}

class _ManPowerPreparationDetail extends State<ManPowerPreparationDetail> {
  final ManpowerPreparationService api = ManpowerPreparationService();
  List<ClsManpowerPreparationDetil> listH;

  List<ClsManpowerPreparationDetil> searchdata = [
    // ClsManpowerPreparationDetil(empID: "K08324", empName: "Teguh Mahesa"),
    // ClsManpowerPreparationDetil(
    //     empID: "K0832", empName: "Boby Kurniawan Kuncoro Subibyo Mangunkusumo"),
  ];

  TextEditingController iInsNo = TextEditingController();
  TextEditingController iLine = TextEditingController();
  TextEditingController iDate = TextEditingController();
  TextEditingController iSkill = TextEditingController();
  TextEditingController iSearch = TextEditingController();

  String _searchResult = '';

  @override
  void initState() {
    super.initState();

    iLine.text = widget.lineName;
    iDate.text = widget.date;
    iSkill.text = widget.skillName;

    asynclistGetData(widget.insNo, widget.skillCode);
  }

  List<ClsManpowerPreparationDetil> ddlInsNo = [];
  List<ClsManpowerPreparationDetil> ddlInsNoFilter = [];

  void asynclistGetData(String insNo, String skillCode) async {
    listH = await api.getDataDetail(insNo, skillCode);
    searchdata = listH;
    setState(() {});
  }

  List<ClsManpowerPreparationDetil> listDetail;

  @override
  Widget build(BuildContext context) {
    listH ??= <ClsManpowerPreparationDetil>[];
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text("Man Power Detail"),
        ),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Line",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iLine,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Production Date",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iDate,
                enabled: false,
                //style: TextStyle(color: disabledColor),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Skill",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iSkill,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            Divider(
              color: Colors.black,
              height: 20,
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iSearch,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 15.0),
                  labelText: 'Search',
                  prefixIcon: IconButton(
                    onPressed: () {},
                    icon:
                        Icon(_searchResult == '' ? Icons.search : Icons.search),
                    //Icons.search,
                  ),
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iSearch.clear();
                        _searchResult = '';
                        searchdata = listH;
                      });
                    },
                    icon: Icon(_searchResult == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _searchResult == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  setState(() {
                    if (value != null) {
                      _searchResult = value;
                      searchdata = listH
                          .where((rec) =>
                              rec.empID
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()) ||
                              rec.empName
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()))
                          .toList();
                    } else {
                      searchdata = listH;
                    }
                  });
                },
              ),
            ),
            SizedBox(height: 10),
            Expanded(
              child: _dataTableWidget(),
            ),
          ],
        ),
      ),
    );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget(' ', 1),
      _getTitleItemWidget('Employee ID', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Employee Name', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Scan', MediaQuery.of(context).size.width / 3),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      width: 1,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].empID),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(searchdata[index].empName),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          width: MediaQuery.of(context).size.width / 3,
          alignment: Alignment.centerRight,
          child: Align(
            alignment: Alignment.center,
            child: Icon(
                searchdata[index].status == "1"
                    ? Icons.check_circle
                    : Icons.cancel_sharp,
                color: searchdata[index].status == "1"
                    ? Colors.green[400]
                    : Colors.red),
          ),
        ),
      ],
    );
  }
}

/*
child: Container(
                padding: const EdgeInsets.only(top: 10),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      columnSpacing: 55.0,
                      decoration: BoxDecoration(
                          border: Border.all(
                        width: 1,
                        color: Colors.grey[300],
                      )),
                      headingRowColor: MaterialStateColor.resolveWith(
                          (states) => Colors.grey[200]),
                      sortColumnIndex: 0,
                      showCheckboxColumn: false,
                      columns: [
                        DataColumn(
                            label: Container(
                              // width: 80,
                              padding: EdgeInsets.symmetric(horizontal: 0),
                              child: Text(
                                "Employee ID",
                                style: TextStyle(
                                    fontFamily: "Arial",
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ),
                            numeric: false,
                            tooltip: "Employee ID"),
                        DataColumn(
                          label: Container(
                              // width: 120,
                              child: Text(
                            "Employee Name",
                            style: TextStyle(
                                fontFamily: "Arial",
                                fontWeight: FontWeight.bold),
                          )),
                          numeric: false,
                          tooltip: "Employee Name",
                        ),
                        DataColumn(
                          label: Container(
                              //width: 50,
                              child: Text(
                            "Scan",
                            style: TextStyle(
                                fontFamily: "Arial",
                                fontWeight: FontWeight.bold),
                          )),
                          numeric: false,
                          tooltip: "Scan",
                        ),
                      ],
                      rows: searchdata
                          .map(
                            (idata) => DataRow(
                                onSelectChanged: (b) {
                                  print(idata.empID);
                                },
                                cells: [
                                  DataCell(
                                      Container(
                                        //width: 50,
                                        child: Text(idata.empID),
                                      ),
                                      onTap: () {}),
                                  DataCell(
                                    Container(
                                      //width: 160,
                                      child: Text(idata.empName),
                                      alignment: Alignment.centerLeft,
                                    ),
                                  ),
                                  DataCell(
                                    Container(
                                      //width: 35,
                                      alignment: Alignment.center,
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Icon(
                                            idata.status == "1"
                                                ? Icons.check_circle
                                                : Icons.cancel_sharp,
                                            color: idata.status == "1"
                                                ? Colors.green[400]
                                                : Colors.red),
                                      ),
                                    ),
                                  ),
                                ]),
                          )
                          .toList(),
                    ),
                  ),
                ),
              ),
          */
