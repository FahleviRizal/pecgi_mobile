import 'package:flutter/material.dart';
import 'package:panasonic/models/clsmanpowervisualUpdate.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/services/manpowervisualserviceUpdate.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/message.dart';
import 'package:panasonic/screens/manpowervisualupdate/manpowervisualdetailupdate.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Man Power Preparation Visual',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class ManPowerPreparationVisualUpdate extends StatefulWidget {
  const ManPowerPreparationVisualUpdate({Key key, this.userid, this.warehouse})
      : super(key: key);
  final String userid;
  final String warehouse;
  @override
  _ManPowerPreparationVisualUpdate createState() =>
      _ManPowerPreparationVisualUpdate();
}

class _ManPowerPreparationVisualUpdate
    extends State<ManPowerPreparationVisualUpdate> {
  String _scanBarcodeLotcard = 'Unknown';
  String _scanBarcodeMachineNo = 'Unknown';
  String _scanBarcodeEmployeID = 'Unknown';

  final ManpowerPreparationVisualService api =
      ManpowerPreparationVisualService();

  List<ClsManpowerPreparationVisualUpdate> ddlInsNo = [];
  List<ClsManpowerPreparationVisualUpdate> ddlInsNoFilter = [];
  TextEditingController iDate = TextEditingController();
  TextEditingController iEmpId = TextEditingController();
  TextEditingController iInsNo = TextEditingController();
  TextEditingController iLine = TextEditingController();

  TextEditingController iLineCode = TextEditingController();
  TextEditingController iLineCodeMachine = TextEditingController();
  TextEditingController iLotCard = TextEditingController();
  TextEditingController iLineMachine = TextEditingController();
  TextEditingController iSearch = TextEditingController();

  List<ClsManpowerPreparationVisualUpdateDetil> listDetail;
  List<ClsManpowerPreparationVisualUpdate> listH;
  List<ClsManpowerPreparationVisualUpdate> listG;
  List<ClsManpowerPreparationVisualUpdate> listZ;

  List<ClsManpowerPreparationVisualUpdate> listLine;
  List<ClsManpowerPreparationVisualUpdate> listLineNoFiltered;
  List<ClsManpowerPreparationVisualUpdate> listLotCardNoFiltered;

  FocusNode nodeEmpId = FocusNode();
  FocusNode nodeLine = new FocusNode();
  FocusNode nodeLot = FocusNode();
  FocusNode nodeins = FocusNode();
  FocusNode nodeMachine = FocusNode();

  List<ClsManpowerPreparationVisualUpdate> searchdata = [];
  List<ClsManpowerPreparationVisualUpdate> searchdataBarcode = [];

  String _searchLine = '';
  String _searchLotCard = '';
  String _searchLineMachine = '';
  //String _searchResult = '';

  @override
  void initState() {
    //syntag pertama kali load
    super.initState();
    //asynclistGetData("1", "1", "x");
    //asynclistSubmitData("")
    print(widget.userid);
    //asyncScanbarcode('A18', 'L.220804.018.1.0001');
  }

  void scanQR(String namevar) async {
    if (namevar == 'LotCard') {
      String barcodeScanResL;
      try {
        barcodeScanResL = await FlutterBarcodeScanner.scanBarcode(
            '#ff6666', 'Cancel', true, ScanMode.QR);
      } on PlatformException {
        barcodeScanResL = 'Failed to get platform version.';
      }
      if (!mounted) return;
      setState(() {
        if (barcodeScanResL != "-1") {
          _scanBarcodeLotcard = barcodeScanResL;
        } else {
          _scanBarcodeLotcard = "";
        }

        asyncScanbarcode(iLineCode.text, _scanBarcodeLotcard);

        iLotCard.text = _scanBarcodeLotcard;
        //_searchLotCard = _scanBarcodeLotcard;
      });
    } else if (namevar == 'MachineNo') {
      String barcodeScanResM;
      try {
        barcodeScanResM = await FlutterBarcodeScanner.scanBarcode(
            '#ff6666', 'Cancel', true, ScanMode.QR);
      } on PlatformException {
        barcodeScanResM = 'Failed to get platform version.';
      }
      if (!mounted) return;
      setState(() {
        if (barcodeScanResM != "-1") {
          _scanBarcodeMachineNo = barcodeScanResM;
        } else {
          _scanBarcodeMachineNo = "";
        }
        iLineMachine.text = _scanBarcodeMachineNo;
        FocusScope.of(context).requestFocus(nodeMachine);
      });
    } else if (namevar == 'EmployID') {
      String barcodeScanResE;
      try {
        barcodeScanResE = await FlutterBarcodeScanner.scanBarcode(
            '#ff6666', 'Cancel', true, ScanMode.QR);
        print(barcodeScanResE);
        FocusScope.of(context).requestFocus(nodeEmpId);
      } on PlatformException {
        barcodeScanResE = 'Failed to get platform version.';
      }
      if (!mounted) return;
      setState(() {
        if (barcodeScanResE != "-1") {
          _scanBarcodeEmployeID = barcodeScanResE;
        } else {
          _scanBarcodeEmployeID = "";
        }
        iEmpId.text = _scanBarcodeEmployeID;

        this.iEmpId.text = _scanBarcodeEmployeID;
        _scanEmployee(iEmpId.text, iInsNo.text, iLineCode.text, iLotCard.text,
            iLineMachine.text);
      });
    }
  }

  void asynclistGetData(String insNo, String lineCode, String lot) async {
    listH = await api.getData(insNo, lineCode, lot);
    searchdata = listH;
    setState(() {});
  }

  void asyncScanbarcode(String lineCode, String lot) async {
    List<ClsManpowerPreparationVisualUpdate> listScanData;
    listScanData = await api.getBarcodeData(lineCode, lot);
    print(listScanData.length);
    if (listScanData.length == 0) {
      Message.box(
          type: "2",
          message: 'Lot Card Tidak Ditemukan',
          top: 0,
          position: "0",
          context: context);
      iLotCard.clear();
      iLotCard.text = '';
      iInsNo.text = '';
      iDate.text = '';
      iEmpId.clear();
      iEmpId.text = '';
      searchdata.clear();
      _searchLotCard = '';
    } else {
      listScanData
          .forEach((item) => this.iInsNo.text = "${item.instructionNo}");
      listScanData.forEach((item) => this.iDate.text = "${item.prodDate}");

      asynclistGetData(iInsNo.text, iLineCode.text, "");
    }
    //listScanData.forEach((item) => this.iInsNo.text = "${item.instructionNo}");
    //print("Branch : ${item.instructionNo}, xyz : ${item.prodDate}"));

    //this.iInsNo.text = ${item.instructionNo}
    //this.iDate.text = ${item.prodDate};
    //print(listH);
    setState(() {});
  }

  void _detail(String _insNo, String _line, String _date, String _skillcode,
      String _skillName) {
    setState(() {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ManPowerPreparationDetail(
            insNo: _insNo,
            lineName: _line,
            date: _date,
            skillCode: _skillcode,
            skillName: _skillName,
          ),
          settings: RouteSettings(),
        ),
      );
    });
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void _scanEmployee(String empID, String insNo, String lineCd, String lotCard,
      String MachineNo) async {
    //vino input data FINAL
    List<ClsManpowerPreparationVisualUpdate> listScanData;
    listScanData = await ManpowerPreparationVisualService.scanSubmitEmployee(
        insNo, lineCd, empID, lotCard, widget.userid, MachineNo);

    if (listScanData[0].id == "200") {
      Message.box(
          type: "1",
          message: listScanData[0].description,
          top: 0,
          position: "0",
          context: context);
      iEmpId.clear();
      FocusScope.of(context).requestFocus(nodeEmpId);
    } else if (listScanData[0].id == "400") {
      Message.box(
          type: "2",
          message: listScanData[0].description,
          top: 0,
          position: "0",
          context: context);
      iEmpId.clear();
      FocusScope.of(context).requestFocus(nodeEmpId);
      //asyncAlert(listGetData[0].description, "2");
    } else {
      Message.box(
          type: "2",
          message: listScanData[0].description,
          top: 0,
          position: "0",
          context: context);
      iEmpId.clear();
      //FocusScope.of(context).requestFocus(nodeEmpId);
      //asyncAlert(listGetData[0].description, "2");
    }
    asynclistGetData(iInsNo.text, iLineCode.text, "");
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    listH ??= <ClsManpowerPreparationVisualUpdate>[];
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text("Man Power Preparation Visual Update"),
        ),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "1. Select Line",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TypeAheadField(
                hideSuggestionsOnKeyboardHide: true,
                textFieldConfiguration: TextFieldConfiguration(
                    textInputAction: TextInputAction.done,
                    onSubmitted: (value) {
                      _searchLine = value;
                      iLineCode.text = value;
                      listLineNoFiltered
                          .where((result) =>
                              result.lineCode
                                  .toLowerCase()
                                  .contains(value.toLowerCase()) ||
                              result.lineName
                                  .toLowerCase()
                                  .contains(value.toLowerCase()))
                          .toList();
                      final newval = listLineNoFiltered.firstWhere(
                          (item) => item.lineName == value,
                          orElse: () => null);

                      setState(() {
                        iLine.text = newval.lineName;
                      });
                      FocusScope.of(context).requestFocus(nodeLot);
                    },
                    //function focus dan merubah tampilan textbox
                    focusNode: nodeLine,
                    controller: this.iLine,
                    style: TextStyle(fontFamily: 'Arial'),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            iLineCode.clear();
                            iLine.text = '';
                            iLotCard.text = '';
                            iInsNo.text = '';
                            iDate.text = '';
                            iEmpId.text = '';
                            iEmpId.clear();
                            iLineCodeMachine.clear();
                            iLineMachine.text = '';
                            searchdata.clear();
                            _searchLine = '';
                            FocusScope.of(context).requestFocus(nodeLine);
                          });
                        },
                        icon: Icon(_searchLine == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _searchLine == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                      ),
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                    )),
                suggestionsCallback: (pattern) async {
                  listLine = await ManpowerPreparationVisualService.getLinevis(
                      widget.userid);
                  _searchLine = pattern;
                  //memunculkan list autocomplete
                  listLineNoFiltered = listLine
                      .where((user) =>
                          user.lineCode
                              .toLowerCase()
                              .contains(_searchLine.toLowerCase()) ||
                          user.lineName
                              .toLowerCase()
                              .contains(_searchLine.toLowerCase()))
                      .toList();
                  return listLineNoFiltered;
                },
                itemBuilder: (context, suggestion) {
                  //tampilan data dropdown
                  return ListTile(
                    title: Text(suggestion.lineName),
                    //subtitle: Text(suggestion.lineCode),
                  );
                },
                //function setelah memilih dropdown
                onSuggestionSelected: (suggestion) async {
                  this.iLineCode.text = suggestion.lineCode;
                  this.iLine.text = suggestion.lineName;
                  _searchLine = suggestion.lineCode;
                  FocusScope.of(context).requestFocus(nodeLot);
                },
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "2. Select / Scan Lot Card",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TypeAheadField(
                hideSuggestionsOnKeyboardHide: false,
                textFieldConfiguration: TextFieldConfiguration(
                    //enabled: iLineCode.text == '' ? false : true,
                    textInputAction: TextInputAction.done,
                    onSubmitted: (value) {
                      _searchLotCard = value;
                      iLotCard.text = value;
                      listLotCardNoFiltered
                          .where((result) =>
                              result.lineCode
                                  .toLowerCase()
                                  .contains(value.toLowerCase()) ||
                              result.lineName
                                  .toLowerCase()
                                  .contains(value.toLowerCase()))
                          .toList();
                      final newval = listLotCardNoFiltered.firstWhere(
                          (item) => item.lineName == value,
                          orElse: () => null);

                      setState(() {
                        iLotCard.text = newval.lineName;
                      });
                      FocusScope.of(context).requestFocus(nodeLot);
                    },
                    //function focus dan merubah tampilan textbox
                    focusNode: nodeLot,
                    controller: this.iLotCard,
                    style: TextStyle(fontFamily: 'Arial'),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            iLotCard.clear();
                            iLotCard.text = '';
                            iInsNo.text = '';
                            iDate.text = '';
                            iEmpId.clear();
                            iEmpId.text = '';
                            searchdata.clear();
                            _searchLotCard = '';
                            FocusScope.of(context).requestFocus(nodeLot);
                          });
                        },
                        icon: Icon(_searchLotCard == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _searchLotCard == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                      ),
                      prefixIcon: IconButton(
                          icon: const Icon(Icons.qr_code_scanner),
                          onPressed: () => scanQR('LotCard')),
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                    )),
                suggestionsCallback: (pattern) async {
                  listLine =
                      await ManpowerPreparationVisualService.getLotCardvis(
                          iLineCode.text, '1');
                  _searchLotCard = pattern;
                  listLotCardNoFiltered = listLine
                      .where((user) =>
                          user.LotCard.toLowerCase()
                              .contains(_searchLotCard.toLowerCase()) ||
                          user.InstructionNo.toLowerCase()
                              .contains(_searchLotCard.toLowerCase()))
                      .toList();
                  return listLotCardNoFiltered;
                },
                itemBuilder: (context, suggestion) {
                  return ListTile(
                    title: Text(suggestion.LotCard),
                    //subtitle: Text(suggestion.InstructionNo),
                  );
                },
                //function setelah memilih dropdown
                onSuggestionSelected: (suggestion) async {
                  this.iLotCard.text = suggestion.LotCard;
                  this.iInsNo.text = suggestion.InstructionNo;
                  this.iDate.text = suggestion.prodDate;
                  _searchLotCard = suggestion.LotCard;

                  //menampilkan gridview
                  asynclistGetData(iInsNo.text, iLineCode.text, "");
                },
              ),
            ),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      child: Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(
                              right: MediaQuery.of(context).size.height * 0.10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Instruction No",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                    ),
                    SizedBox(height: 5),
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        enableInteractiveSelection: false,
                        textInputAction: TextInputAction.none,
                        controller: iInsNo,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      child: Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height * 0.10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Product Date",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                    ),
                    SizedBox(height: 5),
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        controller: iDate,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "4. Scan Machine No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TypeAheadField(
                hideSuggestionsOnKeyboardHide: false ,
                textFieldConfiguration: TextFieldConfiguration(
                    enabled: iLineCode.text == '' ? false : true,
                    textInputAction: TextInputAction.done,
                    onSubmitted: (value) {
                      _searchLineMachine = value;
                      iLineMachine.text = value;
                      listLineNoFiltered
                          .where((result) =>
                              result.Line_Code.toLowerCase()
                                  .contains(value.toLowerCase()) ||
                              result.MachineNo.toLowerCase()
                                  .contains(value.toLowerCase()))
                          .toList();
                      //fungsi pilihan kolom yang dapat di search (contoh lineName)
                      final newval = listLineNoFiltered.firstWhere(
                          (item) => item.MachineNo == value,
                          orElse: () => null);
                      setState(() {
                        iLineMachine.text = newval.MachineNo;
                      });
                      FocusScope.of(context).requestFocus(nodeLot);
                    },
                    //function focus dan merubah tampilan textbox
                    focusNode: nodeMachine,
                    controller: this.iLineMachine,
                    style: TextStyle(fontFamily: 'Arial'),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            iLineMachine.clear();
                            iLineMachine.text = '';
                            _searchLineMachine = '';
                            FocusScope.of(context).requestFocus(nodeMachine);
                          });
                        },
                        icon: Icon(_searchLineMachine == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _searchLineMachine == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                      ),
                      prefixIcon: IconButton(
                          icon: const Icon(Icons.qr_code_scanner),
                          onPressed: () => scanQR('MachineNo')),
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                    )),
                suggestionsCallback: (pattern) async {
                  listLine =
                      await ManpowerPreparationVisualService.getMachineID(
                          iLineCode.text);
                  _searchLineMachine = pattern;

                  listLineNoFiltered = listLine
                      .where((user) =>
                          user.Line_Code.toLowerCase()
                              .contains(_searchLineMachine.toLowerCase()) ||
                          user.MachineNo.toLowerCase()
                              .contains(_searchLineMachine.toLowerCase()))
                      .toList();
                  return listLineNoFiltered;
                },
                itemBuilder: (context, suggestion) {
                  return ListTile(title: Text(suggestion.MachineNo));
                },
                //function setelah memilih dropdown
                onSuggestionSelected: (suggestion) async {
                  this.iLineMachine.text = suggestion.MachineNo;
                  //FocusScope.of(context).requestFocus(nodeEmpId);
                },
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "5. Scan Employee ID",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 10),
            //   child: TypeAheadField(
            //     hideSuggestionsOnKeyboardHide: true,
            //     textFieldConfiguration: TextFieldConfiguration(
            //         enabled: iLineCode.text == '' ? false : true,
            //         textInputAction: TextInputAction.done,
            //         onSubmitted: (value) {
            //           _scanEmpID = value;
            //           iEmpId.text = value;
            //           listLineNoFiltered
            //               .where((result) =>
            //                   result.lineCode
            //                       .toLowerCase()
            //                       .contains(value.toLowerCase()) ||
            //                   result.lineName
            //                       .toLowerCase()
            //                       .contains(value.toLowerCase()))
            //               .toList();
            //           final newval = listLineNoFiltered.firstWhere(
            //               (item) => item.lineName == value,
            //               orElse: () => null);

            //           setState(() {
            //             iEmpId.text = newval.lineName;
            //           });
            //           //FocusScope.of(context).requestFocus(nodeLot);
            //         },
            //         focusNode: nodeEmpId,
            //         controller: this.iEmpId,
            //         style: TextStyle(fontFamily: 'Arial'),
            //         decoration: InputDecoration(
            //           suffixIcon: IconButton(
            //             onPressed: () {
            //               setState(() {
            //                 iEmpId.clear();
            //                 iEmpId.text = '';
            //                 _scanEmpID = '';
            //                 FocusScope.of(context).requestFocus(nodeEmpId);
            //               });
            //             },
            //             icon: Icon(_scanEmpID == ''
            //                 ? Icons.cancel
            //                 : Icons.cancel_outlined),
            //             color: _scanEmpID == ''
            //                 ? Colors.white12.withOpacity(1)
            //                 : Colors.grey[600],
            //           ),
            //           prefixIcon: IconButton(
            //               icon: const Icon(Icons.qr_code_scanner),
            //               onPressed: () => scanQR('EmployID')),
            //           border: OutlineInputBorder(),
            //           contentPadding: new EdgeInsets.symmetric(
            //               vertical: 10.0, horizontal: 10.0),
            //         )),
            //     suggestionsCallback: (pattern) async {
            //       listLine = await ManpowerPreparationVisualService.getEmpID(
            //           iLineCode.text);

            //       _scanEmpID = pattern;
            //       //memunculkan list autocomplete
            //       listLineNoFiltered = listLine
            //           .where((user) =>
            //               user.skillCode
            //                   .toLowerCase()
            //                   .contains(_scanEmpID.toLowerCase()) ||
            //               user.empID
            //                   .toLowerCase()
            //                   .contains(_scanEmpID.toLowerCase()))
            //           .toList();
            //       return listLineNoFiltered;
            //     },
            //     itemBuilder: (context, suggestion) {
            //       return ListTile(
            //         title: Text(suggestion.empID),
            //         //subtitle: Text(suggestion.skillCode),
            //       );
            //     },
            //     //function setelah memilih dropdown
            //     onSuggestionSelected: (suggestion) async {
            //       this.iEmpId.text = suggestion.empID;
            //       _scanEmpID = suggestion.skillCode;
            //       //proses input
            //       _scanEmployee(iEmpId.text, iInsNo.text, iLineCode.text,
            //           iLotCard.text, iLineMachine.text);
            //     },
            //   ),
            // ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: this.iEmpId,
                enabled: iLineCode.text == '' ? false : true,
                decoration: InputDecoration(
                  prefixIcon: IconButton(
                      icon: const Icon(Icons.qr_code_scanner),
                      onPressed: () => scanQR('EmployID')),
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: 5.0),

            /*
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iSearch,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: 'Search',
                  prefixIcon: IconButton(
                    onPressed: () {},
                    icon:
                        Icon(_searchResult == '' ? Icons.search : Icons.search),
                    //Icons.search,
                  ),
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iSearch.clear();
                        _searchResult = '';
                        searchdata = listH;
                      });
                    },
                    icon: Icon(_searchResult == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _searchResult == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  setState(() {
                    if (value != null) {
                      _searchResult = value;
                      searchdata = listH
                          .where((rec) =>
                              rec.skillName
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()) ||
                              rec.totalManpower
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()) ||
                              rec.remainingscan
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()))
                          .toList();
                    } else {
                      searchdata = listH;
                    }
                  });
                },
              ),
            ),
            */
            SizedBox(height: 10),
            Expanded(child: _dataTableWidget()),
          ],
        ),
      ),
    );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 1),
      _getTitleItemWidget('Skill', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(' ', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget(
          'Total \n Man Power Scan', MediaQuery.of(context).size.width / 3.6),
      _getTitleItemWidget(' ', MediaQuery.of(context).size.width * 0.1),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      //child: Text(searchdata[index].skillName),
      width: 1,
      //height: 45,
      //padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      //alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].skillName),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(''),
          width: MediaQuery.of(context).size.width / 4,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(searchdata[index].remainingscan),
          width: MediaQuery.of(context).size.width / 3.6,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: 45,
          height: 45,
          child: IconButton(
            onPressed: () {
              _detail(iInsNo.text, iLine.text, iDate.text,
                  searchdata[index].skillCode, searchdata[index].skillName);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            color: Colors.black,
            alignment: Alignment.centerRight,
          ),
        ),
      ],
    );
  }
}
