import 'package:flutter/material.dart';
import 'package:panasonic/models/ClsMaterialConsumption.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/screens/materialconsumption/materialconsumptiondetail.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/services/materialconsumptionservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

class MaterialConsumption extends StatefulWidget {
  const MaterialConsumption({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;
  @override
  _MaterialConsumptionState createState() => _MaterialConsumptionState();
}

class _MaterialConsumptionState extends State<MaterialConsumption> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsMaterialConsumption> listGetData = [];

  List<ClsMaterialConsumption> listGetDataFiltered = [];
  List<ClsMaterialConsumption> listInstructionNo;
  List<ClsMaterialConsumption> listInstructionNoFiltered;
  List<ClsMaterialConsumption> listLine;
  List<ClsMaterialConsumption> listLineNoFiltered;

  final TextEditingController txtInstructionNo = new TextEditingController();
  final TextEditingController txtLine = new TextEditingController();
  final TextEditingController txtScanBarcode = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();

  TextEditingController iLineCode = TextEditingController();

  String _searchCombo = '';
  String _searchDataResult = '';
  String _searchInstructionNo = '';
  String _scanMaterial = '';
  String _searchLine = '';

  //String _selectedInstructionNoNo = '';
  FocusNode nodeins = FocusNode();
  FocusNode nodeMaterial = FocusNode();
  FocusNode nodeLine = new FocusNode();

  @override
  void initState() {
    // asynclistGetData();
    super.initState();
  }

  void asynclistGetData(String instNo) async {
    listGetData = await MaterialConsumptionService.getData(instNo);
    listGetDataFiltered = listGetData;
    setState(() {});
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void materialDetail(String labelBarcode) async {
    List<ClsMaterialConsumption> listGetData2 = [];
    listGetData2 = await MaterialConsumptionService.getDataDetail(
        txtInstructionNo.text, txtScanBarcode.text);

    if (listGetData2[0].id == "200") {
      setState(() {
        ClsMaterialConsumption listData = new ClsMaterialConsumption();
        listData.instNo = txtInstructionNo.text;
        listData.labelBarcode = labelBarcode;

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MaterialConsumptionDetail(
                userid: widget.userid, warehouse: widget.warehouse),
            settings: RouteSettings(
              arguments: listData,
            ),
          ),
        ).then((value) => setState(() {
              asynclistGetData(txtInstructionNo.text);
              txtScanBarcode.text = '';
            }));
      });
    } else {
      Message.box(
          type: "2",
          message: listGetData2[0].description,
          top: 0,
          position: "0",
          context: context);
      //asyncAlert(listGetData2[0].description, "2");
      txtScanBarcode.text = "";
      FocusScope.of(context).requestFocus(nodeMaterial);
      return;
    }
  }

  void asyncSubmit(String barcode) async {
    listGetData = await MaterialConsumptionService.submit(
        txtInstructionNo.text, barcode, "X", "X", "0", widget.userid);
    if (listGetData.length > 0) {
      if (listGetData[0].id == "200") {
        Message.box(
            type: "1",
            message: listGetData[0].description,
            top: 0,
            position: "0",
            context: context);
        asynclistGetData(txtInstructionNo.text);
        txtScanBarcode.clear();
        FocusScope.of(context).requestFocus(nodeMaterial);
      } else if (listGetData[0].id == "400") {
        Message.box(
            type: "2",
            message: listGetData[0].description,
            top: 0,
            position: "0",
            context: context);
      } else {
        Message.box(
            type: "2",
            message: listGetData[0].description,
            top: 0,
            position: "0",
            context: context);
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text("Material Consumption"),
            ),
            body: Column(children: <Widget>[
              Flexible(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                          child: Form(
                            key: formkey,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "1. Select/Scan Machine Process",
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TypeAheadField(
                                    hideSuggestionsOnKeyboardHide: false,
                                    textFieldConfiguration:
                                        TextFieldConfiguration(
                                            textInputAction:
                                                TextInputAction.done,
                                            onSubmitted: (value) {
                                              _searchLine = value;
                                              iLineCode.text = value;
                                              listLineNoFiltered
                                                  .where((result) => result
                                                      .linecode
                                                      .toLowerCase()
                                                      .contains(
                                                          value.toLowerCase()))
                                                  .toList();

                                              final newval =
                                                  listLineNoFiltered.firstWhere(
                                                      (item) =>
                                                          item.linecode ==
                                                          value,
                                                      orElse: () => null);

                                              setState(() {
                                                txtLine.text = newval.linename;
                                              });
                                            },
                                            focusNode: nodeLine,
                                            controller: this.txtLine,
                                            style:
                                                TextStyle(fontFamily: 'Arial'),
                                            decoration: InputDecoration(
                                              suffixIcon: IconButton(
                                                onPressed: () {
                                                  setState(() {
                                                    iLineCode.clear();
                                                    txtLine.text = '';
                                                    txtInstructionNo.text = '';
                                                    listGetDataFiltered.clear();
                                                    _searchLine = '';
                                                    _searchInstructionNo = '';
                                                    FocusScope.of(context)
                                                        .requestFocus(nodeLine);
                                                  });
                                                },
                                                icon: Icon(_searchLine == ''
                                                    ? Icons.cancel
                                                    : Icons.cancel_outlined),
                                                color: _searchLine == ''
                                                    ? Colors.white12
                                                        .withOpacity(1)
                                                    : Colors.grey[600],
                                              ),
                                              border: OutlineInputBorder(),
                                              contentPadding:
                                                  new EdgeInsets.symmetric(
                                                      vertical: -10.0,
                                                      horizontal: 10.0),
                                            )),
                                    suggestionsCallback: (pattern) async {
                                      listLine =
                                          await MaterialConsumptionService
                                              .getLine(widget.userid);

                                      _searchLine = pattern;

                                      listLineNoFiltered = listLine
                                          .where((user) =>
                                              user.linecode
                                                  .toLowerCase()
                                                  .contains(_searchLine
                                                      .toLowerCase()) ||
                                              user.linename
                                                  .toLowerCase()
                                                  .contains(_searchLine
                                                      .toLowerCase()))
                                          .toList();
                                      return listLineNoFiltered;
                                    },
                                    itemBuilder: (context, suggestion) {
                                      return ListTile(
                                        title: Text(suggestion.linecode),
                                        subtitle: Text(suggestion.linename),
                                      );
                                    },
                                    onSuggestionSelected: (suggestion) async {
                                      this.iLineCode.text = suggestion.linecode;
                                      this.txtLine.text = suggestion.linename;
                                      _searchLine = suggestion.linecode;
                                      FocusScope.of(context)
                                          .requestFocus(nodeins);
                                    },
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Select / Scan Instruction No",
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TypeAheadField(
                                    hideSuggestionsOnKeyboardHide: false,
                                    textFieldConfiguration:
                                        TextFieldConfiguration(
                                            textInputAction:
                                                TextInputAction.done,
                                            onSubmitted: (value) {
                                              txtInstructionNo.text = value;
                                              _searchCombo = value;

                                              asynclistGetData(value);
                                              FocusScope.of(context)
                                                  .requestFocus(nodeMaterial);
                                            },
                                            focusNode: nodeins,
                                            controller: this.txtInstructionNo,
                                            style:
                                                TextStyle(fontFamily: 'Arial'),
                                            decoration: InputDecoration(
                                              suffixIcon: IconButton(
                                                onPressed: () {
                                                  setState(() {
                                                    txtInstructionNo.clear();
                                                    listGetDataFiltered.clear();
                                                    _searchCombo = '';
                                                    FocusScope.of(context)
                                                        .requestFocus(nodeins);
                                                  });
                                                },
                                                icon: Icon(_searchCombo == ''
                                                    ? Icons.cancel
                                                    : Icons.cancel_outlined),
                                                color: _searchCombo == ''
                                                    ? Colors.white12
                                                        .withOpacity(1)
                                                    : Colors.grey[600],
                                              ),
                                              border: OutlineInputBorder(),
                                              contentPadding:
                                                  new EdgeInsets.symmetric(
                                                      vertical: -10.0,
                                                      horizontal: 10.0),
                                            )),
                                    suggestionsCallback: (pattern) async {
                                      listInstructionNo =
                                          await MaterialConsumptionService
                                              .getInstructionNo(
                                                  widget.warehouse,
                                                  iLineCode.text);

                                      _searchInstructionNo = pattern;

                                      listInstructionNoFiltered =
                                          listInstructionNo
                                              .where((user) => user.instNo
                                                  .toLowerCase()
                                                  .contains(_searchInstructionNo
                                                      .toLowerCase()))
                                              .toList();
                                      return listInstructionNoFiltered;
                                    },
                                    itemBuilder: (context, suggestion) {
                                      return ListTile(
                                        title: Text(suggestion.typedesc),
                                        subtitle: Text(suggestion.instNo),
                                      );
                                    },
                                    onSuggestionSelected: (suggestion) async {
                                      this.txtInstructionNo.text =
                                          suggestion.instNo;
                                      //_selectedInstructionNoNo =
                                      //suggestion.instNo;

                                      _searchCombo = suggestion.instNo;
                                      asynclistGetData(suggestion.instNo);
                                      FocusScope.of(context)
                                          .requestFocus(nodeMaterial);
                                    },
                                  ),
                                ),
                                SizedBox(height: 5),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Scan Material",
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    focusNode: nodeMaterial,
                                    enableInteractiveSelection: false,
                                    textInputAction: TextInputAction.none,
                                    controller: txtScanBarcode,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      labelText: '',
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            txtScanBarcode.clear();
                                            _scanMaterial = '';
                                            // listGetDataFiltered.clear();
                                            FocusScope.of(context)
                                                .requestFocus(nodeMaterial);
                                          });
                                        },
                                        icon: Icon(_scanMaterial == ''
                                            ? Icons.cancel
                                            : Icons.cancel_outlined),
                                        color: _scanMaterial == ''
                                            ? Colors.white12.withOpacity(1)
                                            : Colors.grey[600],
                                      ),
                                    ),
                                    onChanged: (value) {
                                      _scanMaterial = value;
                                      setState(() {});
                                    },
                                    onFieldSubmitted: (value) {
                                      //auto submit after scan
                                      asyncSubmit(value);
                                      //if redirect to detail screen after scan (show information before submit ) uncomment here
                                      //materialDetail(value);
                                    },
                                  ),
                                ),
                                Divider(
                                  color: Colors.grey,
                                  height: 20,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                      controller: txtSearch,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        contentPadding:
                                            new EdgeInsets.symmetric(
                                                vertical: 10.0,
                                                horizontal: 10.0),
                                        labelText: 'Search',
                                        prefixIcon: IconButton(
                                          onPressed: () {},
                                          icon: Icon(_searchDataResult == ''
                                              ? Icons.search
                                              : Icons.search),

                                          //Icons.search,
                                        ),
                                        suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              txtSearch.clear();
                                              _searchDataResult = '';
                                              listGetDataFiltered = listGetData;
                                            });
                                          },
                                          icon: Icon(_searchDataResult == ''
                                              ? Icons.cancel
                                              : Icons.cancel_outlined),
                                          color: _searchDataResult == ''
                                              ? Colors.white12.withOpacity(1)
                                              : Colors.grey[600],
                                        ),
                                      ),
                                      onChanged: (value) {
                                        setState(() {
                                          _searchDataResult = value;
                                          listGetDataFiltered = listGetData
                                              .where((user) =>
                                                  user.materialDesc
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()) ||
                                                  user.lotNo
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()) ||
                                                  user.qty
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()))
                                              .toList();
                                        });
                                      }),
                                ),
                                SizedBox(height: 10),
                                Expanded(
                                  child: Container(child: _dataTableWidget()),
                                ),
                              ],
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ])));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3.5,
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      // _getTitleItemWidget('', 1),
      _getTitleItemWidget(
          'Barcode No', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget('Material', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 4),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].labelBarcode),
      width: MediaQuery.of(context).size.width / 3.5,
      height: 45,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      alignment: Alignment.centerLeft,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        // Container(
        //   child: Text(listGetDataFiltered[index].labelBarcode),
        //   width: MediaQuery.of(context).size.width / 4,
        //   //height: 45,
        //   padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
        //   alignment: Alignment.center,
        // ),
        Container(
          child: Text(listGetDataFiltered[index].materialDesc),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(listGetDataFiltered[index].lotNo),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
                value.format(double.parse(listGetDataFiltered[index].qty))),
          ),
          width: MediaQuery.of(context).size.width / 4.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
