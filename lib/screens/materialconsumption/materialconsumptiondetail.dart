// import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:panasonic/models/ClsMaterialConsumption.dart';
import 'package:panasonic/services/materialconsumptionservice.dart';
import 'package:panasonic/utilities/constants.dart';
// import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter/services.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/message.dart';

class MaterialConsumptionDetail extends StatefulWidget {
  const MaterialConsumptionDetail({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _MaterialConsumptionDetailState createState() =>
      _MaterialConsumptionDetailState();
}

class _MaterialConsumptionDetailState extends State<MaterialConsumptionDetail> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  bool init = false;

  List<ClsMaterialConsumption> listGetData = [];
  final TextEditingController txtScanBarcodeNo = new TextEditingController();
  final TextEditingController txtMaterial = new TextEditingController();
  final TextEditingController txtLotNo = new TextEditingController();
  final TextEditingController txtQty = new TextEditingController();
  final TextEditingController txtUnit = new TextEditingController();
  final TextEditingController txtSupplierName = new TextEditingController();
  final TextEditingController txtExpDate = new TextEditingController();
  final TextEditingController txtLocation = new TextEditingController();
  final TextEditingController txtMatCodeTemp = new TextEditingController();

  String instructionNo = '';
  String message;
  String type;
  FocusNode nodeQty = FocusNode();
  FocusNode nodeScanBarcode = FocusNode();
  FocusNode nodeButton = FocusNode();
  String _scanBarcode = '';
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void asynclistGetData() async {
    init = true;
    listGetData = await MaterialConsumptionService.getDataDetail(
        instructionNo, txtScanBarcodeNo.text);
    if (listGetData[0].id == "200") {
      txtMaterial.text =
          listGetData[0].materialCode + ' / ' + listGetData[0].materialDesc;
      txtLotNo.text = listGetData[0].lotNo;
      txtMatCodeTemp.text = listGetData[0].materialCode;
      txtQty.text = listGetData[0].qty;
      txtUnit.text = listGetData[0].unitName;
      txtExpDate.text = listGetData[0].expDate;
      // final DateTime now = DateTime.parse(listGetData[0].expDate);
      // final DateFormat formatter = DateFormat('dd MMM yyyy');
      // final String formatted = formatter.format(now);

      // txtExpDate.text = formatted;

      txtSupplierName.text = listGetData[0].supplierName;
      FocusScope.of(context).requestFocus(nodeButton);
    } else {
      Message.box(
          type: "2",
          message: listGetData[0].description,
          top: 0,
          position: "0",
          context: context);
      //asyncAlert(listGetData[0].description, "2");
      return;
    }
    setState(() {});
  }

  void asyncSubmit(
      String barcode, String material, String lotNo, String qty) async {
    setState(() {
      isProcessing = true;
    });

    listGetData = await MaterialConsumptionService.submit(
        instructionNo, barcode, material, lotNo, qty, widget.userid);
    if (listGetData.length > 0) {
      if (listGetData[0].id == "200") {
        Message.box(
            type: "1",
            message: listGetData[0].description,
            top: 0,
            position: "0",
            context: context);
        //asyncAlert(listGetData[0].description, "3");
        txtScanBarcodeNo.text = '';
        txtMaterial.text = '';
        txtLotNo.text = '';
        txtQty.text = '';
        txtUnit.text = '';
        txtExpDate.text = '';
        txtSupplierName.text = '';

        FocusScope.of(context).requestFocus(nodeScanBarcode);
      } else if (listGetData[0].id == "400") {
        Message.box(
            type: "2",
            message: listGetData[0].description,
            top: 0,
            position: "0",
            context: context);
        //asyncAlert(listGetData[0].description, "2");
      } else {
        Message.box(
            type: "2",
            message: listGetData[0].description,
            top: 0,
            position: "0",
            context: context);
        //asyncAlert(listGetData[0].description, "2");
      }
    }
    setState(() {});
  }

  void clearData() {
    txtMaterial.text = '';
    txtExpDate.text = '';
    txtLocation.text = '';
    txtLotNo.text = '';
    txtQty.text = '';
    txtSupplierName.text = '';
    txtUnit.text = '';
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    // final pModels = ModalRoute.of(context).settings.arguments;
    // ClsMaterialConsumption model = pModels;

    // if (init == false) {
    //   instructionNo = model.instNo;
    //   txtScanBarcodeNo.text = model.labelBarcode;
    //   asynclistGetData();
    // }
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              flexibleSpace: kAppBarColor,
              title: Text("Material Detail"),
            ),
            body: Column(children: <Widget>[
              Flexible(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                          child: Form(
                            key: formkey,
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Barcode Material",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    autofocus: true,
                                    showCursor: true,
                                    focusNode: nodeScanBarcode,
                                    enableInteractiveSelection: false,
                                    textInputAction: TextInputAction.none,
                                    controller: txtScanBarcodeNo,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      labelText: '',
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            txtScanBarcodeNo.clear();
                                            _scanBarcode = '';
                                            clearData();
                                            FocusScope.of(context)
                                                .requestFocus(nodeScanBarcode);
                                          });
                                        },
                                        icon: Icon(_scanBarcode == ''
                                            ? Icons.cancel
                                            : Icons.cancel_outlined),
                                        color: _scanBarcode == ''
                                            ? Colors.white12.withOpacity(1)
                                            : Colors.grey[600],
                                      ),
                                    ),
                                    onChanged: (value) {
                                      _scanBarcode = value;
                                      setState(() {});
                                    },
                                    onFieldSubmitted: (value) {
                                      asynclistGetData();
                                    },
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Material",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),

                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    enableInteractiveSelection: false,
                                    controller: txtMaterial,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      filled: true,
                                      fillColor: disabledColor,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Lot No",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    enableInteractiveSelection: false,
                                    controller: txtLotNo,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      filled: true,
                                      fillColor: disabledColor,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Qty",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtQty,
                                    focusNode: nodeQty,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp(r"[0-9.]")),
                                      TextInputFormatter.withFunction(
                                          (oldValue, newValue) {
                                        try {
                                          final text = newValue.text;
                                          if (text.isNotEmpty)
                                            double.parse(text);
                                          return newValue;
                                        } catch (e) {}
                                        return oldValue;
                                      }),
                                    ],

                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true, signed: false),
                                    // inputFormatters: [
                                    //   FilteringTextInputFormatter.digitsOnly
                                    // ],
                                    // keyboardType: TextInputType.number,
                                    textAlign: TextAlign.right,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Unit",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    enableInteractiveSelection: false,
                                    controller: txtUnit,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      filled: true,
                                      fillColor: disabledColor,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Expired Date",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    enableInteractiveSelection: false,
                                    controller: txtExpDate,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      filled: true,
                                      fillColor: disabledColor,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Supplier",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    enableInteractiveSelection: false,
                                    controller: txtSupplierName,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      filled: true,
                                      fillColor: disabledColor,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height),
                                ),
                                Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      TextButton(
                                        style: TextButton.styleFrom(
                                          backgroundColor: (isProcessing)
                                              ? disabledButtonColor
                                              : enabledButtonColor,
                                          minimumSize: Size(
                                              MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.90,
                                              55),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50.0),
                                          ),
                                        ),
                                        // onPressed: () {
                                        //   asyncSubmit(
                                        //       txtScanBarcodeNo.text,
                                        //       txtMatCodeTemp.text,
                                        //       txtLotNo.text,
                                        //       txtQty.text);
                                        // },
                                        onPressed: (isProcessing == false)
                                            ? () async {
                                                //if buttonenabled == true then pass a function otherwise pass "null"
                                                asyncSubmit(
                                                    txtScanBarcodeNo.text,
                                                    txtMatCodeTemp.text,
                                                    txtLotNo.text,
                                                    txtQty.text);
                                                //plug delayed for wait while processing data
                                                await Future.delayed(
                                                    const Duration(seconds: 3),
                                                    () {
                                                  setState(() {
                                                    isProcessing = false;
                                                  });
                                                });
                                              }
                                            : null,
                                        child: Text(
                                          "SUBMIT",
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: (isProcessing)
                                                  ? disabledColorButtonText
                                                  : enabledColorButtonText),
                                        ),
                                      ),
                                      // ),
                                    ]),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                // SizedBox(height: 150.0),
                                // Expanded(
                                //   child: Container(
                                //     alignment: Alignment.centerRight,
                                //     child: Padding(
                                //       padding: EdgeInsets.symmetric(
                                //           horizontal: 15, vertical: 15),
                                //       child: TextButton(
                                //         style: TextButton.styleFrom(
                                //           backgroundColor: Colors.blue,
                                //           minimumSize: Size(150, 40),
                                //         ),
                                //         onPressed: () {
                                //           asyncSubmit();
                                //         },
                                //         child: Text(
                                //           "Submit",
                                //           style: TextStyle(
                                //             fontSize: 16,
                                //             color: Colors.white,
                                //           ),
                                //         ),
                                //       ),
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ])));
  }
}

// _onBasicAlertPressed(context, title, desc) {
//   Alert(
//     context: context,
//     title: title,
//     desc: desc,
//     buttons: [
//       DialogButton(
//         child: Text(
//           "OK",
//           style: TextStyle(color: Colors.white, fontSize: 20),
//         ),
//         onPressed: () => Navigator.pop(context),
//         radius: BorderRadius.circular(0.0),
//       ),
//     ],
//   ).show();
// }
