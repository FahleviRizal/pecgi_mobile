import 'package:flutter/material.dart';
import 'package:panasonic/models/clsmaterialpreparation.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/screens/MaterialPreparation/MaterialPreparationdetail.dart';
import 'package:panasonic/services/materialpreparationservice.dart';
//import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/utilities/message.dart';

class MaterialPreparation extends StatefulWidget {
  const MaterialPreparation({Key key, this.userid}) : super(key: key);

  final String userid;
  @override
  _MaterialPreparationState createState() => _MaterialPreparationState();
}

class _MaterialPreparationState extends State<MaterialPreparation> {
  MaterialPreparationService api = MaterialPreparationService();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsMaterialpreparation> listGetData = [];
  List<ClsMaterialpreparation> listGetDataFiltered = [];
  List<ClsMaterialpreparation> listInstructionNo;
  List<ClsMaterialpreparation> listInstructionNoFiltered;
  List<ClsMaterialpreparation> listLine;
  List<ClsMaterialpreparation> listLineNoFiltered;

  final TextEditingController txtInstructionNo = new TextEditingController();
  final TextEditingController txtScanBarcode = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();

  TextEditingController iLine = TextEditingController();
  TextEditingController iDate = TextEditingController();

  TextEditingController iLineCode = TextEditingController();

  String _searchDataResult = '';
  String _searchLine = '';
  String _searchInstructionNo = '';
  String _scanMaterial = '';
  //String _selectedInstructionNoNo = '';
  String message;
  String type;

  FocusNode node = new FocusNode();
  FocusNode nodeMaterial = new FocusNode();
  FocusNode nodeLine = new FocusNode();

  @override
  void initState() {
    // asynclistGetData();
    super.initState();
  }

  void asynclistGetData(String insNo) async {
    listGetData = await MaterialPreparationService.getdata(insNo);
    listGetDataFiltered = listGetData;
    if (listGetDataFiltered.length > 0) {
      //iLine.text = listGetDataFiltered[0].linename;
      iDate.text = listGetDataFiltered[0].proddate;
    }

    setState(() {
      listGetDataFiltered = listGetData;
    });
  }

  void asyncSubmit() async {
    listGetData = await MaterialPreparationService.submit(
        txtInstructionNo.text, txtScanBarcode.text, widget.userid);
    if (listGetData.length > 0) {
      if (listGetData[0].id == "200") {
        message = 'Data saved successfully';
        type = '1';
        asynclistGetData(txtInstructionNo.text);
      } else if (listGetData[0].id == "400") {
        message = listGetData[0].description;
        type = '2';
      } else {
        message = listGetData[0].description;
        type = '2';
      }
      Message.box(
          type: type,
          message: listGetData[0].description,
          top: 0,
          position: "0",
          context: context);
      // var dialog = CustomAlertDialog(
      //   type: type,
      //   title: '',
      //   message: message,
      //   okBtnText: '',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
    }
    FocusScope.of(context).requestFocus(nodeMaterial);
    txtScanBarcode.text = "";
    setState(() {});
  }

  void materialList(String instructionNo, String materialCode) {
    setState(() {
      ClsMaterialpreparation listData = new ClsMaterialpreparation();
      listData.instNo = instructionNo;
      listData.materialcode = materialCode;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MaterialPreparationList(),
          settings: RouteSettings(
            arguments: listData,
          ),
        ),
      ).then((value) => setState(() {
            asynclistGetData(txtInstructionNo.text);
          }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Material Preparation"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 5.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "1. Select / Scan Machine Process",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TypeAheadField(
                                hideSuggestionsOnKeyboardHide: false,
                                textFieldConfiguration: TextFieldConfiguration(
                                    textInputAction: TextInputAction.done,
                                    onSubmitted: (value) {
                                      _searchLine = value;
                                      iLineCode.text = value;

                                      listLineNoFiltered
                                          .where((result) => result.linecode
                                              .toLowerCase()
                                              .contains(value.toLowerCase()))
                                          .toList();

                                      final newval =
                                          listLineNoFiltered.firstWhere(
                                              (item) => item.linecode == value,
                                              orElse: () => null);

                                      setState(() {
                                        iLine.text = newval.linename;
                                      });

                                      //FocusScope.of(context).requestFocus(node);
                                    },
                                    focusNode: nodeLine,
                                    controller: this.iLine,
                                    style: TextStyle(fontFamily: 'Arial'),
                                    decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            iLineCode.clear();
                                            iLine.clear();
                                            txtInstructionNo.text = '';
                                            iDate.text = '';
                                            listGetDataFiltered.clear();
                                            _searchLine = '';
                                            _searchInstructionNo = '';
                                          });
                                        },
                                        icon: Icon(_searchLine == ''
                                            ? Icons.cancel
                                            : Icons.cancel_outlined),
                                        color: _searchLine == ''
                                            ? Colors.white12.withOpacity(1)
                                            : Colors.grey[600],
                                      ),
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: -10.0, horizontal: 10.0),
                                    )),
                                suggestionsCallback: (pattern) async {
                                  listLine =
                                      await MaterialPreparationService.getLine(
                                          widget.userid);

                                  _searchLine = pattern;

                                  listLineNoFiltered = listLine
                                      .where((user) =>
                                          user.linecode.toLowerCase().contains(
                                              _searchLine.toLowerCase()) ||
                                          user.linename.toLowerCase().contains(
                                              _searchLine.toLowerCase()))
                                      .toList();
                                  return listLineNoFiltered;
                                },
                                itemBuilder: (context, suggestion) {
                                  return ListTile(
                                    title: Text(suggestion.linecode),
                                    subtitle: Text(suggestion.linename),
                                  );
                                },
                                onSuggestionSelected: (suggestion) async {
                                  this.iLine.text = suggestion.linename;
                                  this.iLineCode.text = suggestion.linecode;
                                  _searchLine = suggestion.linecode;
                                },
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "2. Select / Scan Instruction No",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TypeAheadField(
                                hideSuggestionsOnKeyboardHide: false,
                                textFieldConfiguration: TextFieldConfiguration(
                                    textInputAction: TextInputAction.done,
                                    onSubmitted: (value) {
                                      _searchInstructionNo = value;
                                      txtInstructionNo.text = value;
                                      final newval =
                                          listInstructionNoFiltered.firstWhere(
                                              (item) => item.instNo == value,
                                              orElse: () => null);

                                      txtInstructionNo.text = value;
                                      _searchInstructionNo = value;
                                      listInstructionNoFiltered
                                          .where((user) => user.instNo
                                              .toLowerCase()
                                              .contains(value.toLowerCase()))
                                          .toList();

                                      // listInstructionNoFiltered
                                      //     .where((result) => result.instNo
                                      //         .toLowerCase()
                                      //         .contains(value.toLowerCase()))
                                      //     .toList();

                                      setState(() {
                                        iDate.text = newval.proddate;
                                        FocusScope.of(context)
                                            .requestFocus(nodeMaterial);
                                      });
                                    },
                                    focusNode: node,
                                    controller: this.txtInstructionNo,
                                    style: TextStyle(fontFamily: 'Arial'),
                                    decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            txtInstructionNo.clear();

                                            //iLine.text = '';
                                            iDate.text = '';
                                            listGetDataFiltered.clear();
                                            _searchInstructionNo = '';
                                          });
                                        },
                                        icon: Icon(_searchInstructionNo == ''
                                            ? Icons.cancel
                                            : Icons.cancel_outlined),
                                        color: _searchInstructionNo == ''
                                            ? Colors.white12.withOpacity(1)
                                            : Colors.grey[600],
                                      ),
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: -10.0, horizontal: 10.0),
                                    )),
                                suggestionsCallback: (pattern) async {
                                  listInstructionNo =
                                      await MaterialPreparationService
                                          .getInstructionNo(
                                              iLineCode.text, widget.userid);

                                  _searchInstructionNo = pattern;

                                  listInstructionNoFiltered = listInstructionNo
                                      .where((user) => user.instNo
                                          .toLowerCase()
                                          .contains(_searchInstructionNo
                                              .toLowerCase()))
                                      .toList();
                                  return listInstructionNoFiltered;
                                },
                                itemBuilder: (context, suggestion) {
                                  return ListTile(
                                    title: Text(suggestion.typedesc),
                                    subtitle: Text(suggestion.instNo),
                                  );
                                },
                                onSuggestionSelected: (suggestion) async {
                                  this.txtInstructionNo.text =
                                      suggestion.instNo;
                                  _searchInstructionNo = suggestion.instNo;
                                  //_selectedInstructionNoNo = suggestion.instNo;
                                  this.txtInstructionNo.text =
                                      suggestion.instNo;
                                  this.iDate.text = suggestion.proddate;
                                  asynclistGetData(suggestion.instNo);
                                  FocusScope.of(context)
                                      .requestFocus(nodeMaterial);
                                },
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Production Date",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: iDate,
                                enabled: false,
                                //style: TextStyle(color: disabledColor),
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  filled: true,
                                  fillColor: disabledColor,
                                ),
                              ),
                            ),
                            SizedBox(height: 5.0),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "2. Scan Material",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                focusNode: nodeMaterial,
                                enableInteractiveSelection: false,
                                textInputAction: TextInputAction.none,
                                controller: txtScanBarcode,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  labelText: '',
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        txtScanBarcode.clear();
                                        _scanMaterial = '';
                                        //listGetDataFiltered.clear();
                                        FocusScope.of(context)
                                            .requestFocus(nodeMaterial);
                                      });
                                    },
                                    icon: Icon(_scanMaterial == ''
                                        ? Icons.cancel
                                        : Icons.cancel_outlined),
                                    color: _scanMaterial == ''
                                        ? Colors.white12.withOpacity(1)
                                        : Colors.grey[600],
                                  ),
                                ),
                                onChanged: (value) {
                                  _scanMaterial = value;
                                  setState(() {});
                                },
                                onFieldSubmitted: (value) {
                                  asyncSubmit();
                                },
                              ),
                            ),
                            Divider(
                              color: Colors.grey,
                              height: 20,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                  controller: txtSearch,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.search),
                                    suffixIcon: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          txtSearch.clear();
                                          _searchDataResult = '';
                                          listGetDataFiltered = listGetData;
                                        });
                                      },
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.cancel
                                          : Icons.cancel_outlined),
                                      color: _searchDataResult == ''
                                          ? Colors.white12.withOpacity(1)
                                          : Colors.grey[600],
                                    ),
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                    labelText: 'Search',
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      _searchDataResult = value;
                                      listGetDataFiltered = listGetData
                                          .where((user) =>
                                              user.materialcode.toLowerCase().contains(_searchDataResult.toLowerCase()) ||
                                              user.materialname
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()) ||
                                              user.scanstatus
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()))
                                          .toList();
                                    });
                                  }),
                            ),
                            SizedBox(height: 10),
                            Expanded(
                              child: Container(child: _dataTableWidget()),
                            ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 0),
      _getTitleItemWidget(
          'Material Code', MediaQuery.of(context).size.width / 3.3),
      _getTitleItemWidget(
          'Material Name', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Scan Status', MediaQuery.of(context).size.width / 3.8),
      _getTitleItemWidget(' ', MediaQuery.of(context).size.width * 0.1),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].materialcode),
      width: 0,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(listGetDataFiltered[index].materialcode),
          ),
          width: MediaQuery.of(context).size.width / 3.2,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(listGetDataFiltered[index].materialname),
          ),
          width: MediaQuery.of(context).size.width / 3,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Align(
            alignment: Alignment.center,
            child: Text(listGetDataFiltered[index].scanstatus),
          ),
          width: MediaQuery.of(context).size.width / 4,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: 40,
          height: 45,
          child: IconButton(
            onPressed: () {
              materialList(txtInstructionNo.text,
                  listGetDataFiltered[index].materialcode);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            color: Colors.black,
            alignment: Alignment.centerRight,
          ),
        ),
      ],
    );
  }
}
