// import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:panasonic/models/clsmaterialpreparation.dart';
import 'package:panasonic/services/materialpreparationservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
// import 'package:rflutter_alert/rflutter_alert.dart';

class MaterialPreparationList extends StatefulWidget {
  @override
  _MaterialPreparationListState createState() =>
      _MaterialPreparationListState();
}

class _MaterialPreparationListState extends State<MaterialPreparationList> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsMaterialpreparation> listGetData = [];
  List<ClsMaterialpreparation> listScanBarcode = [];
  List<ClsMaterialpreparation> listGetDataFiltered = [];

  final TextEditingController txtline = new TextEditingController();
  final TextEditingController txtProductionDate = new TextEditingController();
  final TextEditingController txtBarcodeNo = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();

  String message;
  String type;
  String _searchDataResult = '';
  String instructionNo = '';
  String materialCode = '';
  // String _selectedSupplyNo = '';
  bool init = false;
  FocusNode nodeScanBarcode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  void asynclistGetData(String instructionNo, String materialCode) async {
    init = true;
    listGetData = await MaterialPreparationService.getDataDetail(
        instructionNo, materialCode);

    listGetDataFiltered = listGetData;
    if (listGetDataFiltered.length > 0) {
      txtline.text = listGetDataFiltered[0].linename;
      txtProductionDate.text = listGetDataFiltered[0].proddate;
      txtBarcodeNo.text = listGetDataFiltered[0].itemtype;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final pModels = ModalRoute.of(context).settings.arguments;
    ClsMaterialpreparation model = pModels;

    // InstructionNo = 'S20211001.01';
    // MaterialCode = 'ITM001';
    if (init == false) {
      instructionNo = model.instNo;
      materialCode = model.materialcode;

      asynclistGetData(instructionNo, materialCode);
    }
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Material Preparation Detail"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Line",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtline,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  filled: true,
                                  fillColor: disabledColor,
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Production Date",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtProductionDate,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  filled: true,
                                  fillColor: disabledColor,
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Type",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtBarcodeNo,
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  filled: true,
                                  fillColor: disabledColor,
                                ),
                              ),
                            ),
                            Divider(color: Colors.grey, height: 20),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                  controller: txtSearch,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.search),
                                    suffixIcon: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          txtSearch.clear();
                                          _searchDataResult = '';
                                          listGetDataFiltered = listGetData;
                                        });
                                      },
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.cancel
                                          : Icons.cancel_outlined),
                                      color: _searchDataResult == ''
                                          ? Colors.white12.withOpacity(1)
                                          : Colors.grey[600],
                                    ),
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                    labelText: 'Search',
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      _searchDataResult = value;
                                      listGetDataFiltered = listGetData
                                          .where((user) =>
                                              user.materialname
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()) ||
                                              user.registerby
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()) ||
                                              user.registerdate
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()))
                                          .toList();
                                    });
                                  }),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Expanded(
                              child: Container(child: _dataTableWidget()),
                            ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 0),
      _getTitleItemWidget(
          'Material Name', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Register By', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget(
          'Register Date', MediaQuery.of(context).size.width / 2.5),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].materialname),
      width: 0,
      height: 45,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(listGetDataFiltered[index].materialname),
          ),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(listGetDataFiltered[index].registerby),
          ),
          width: MediaQuery.of(context).size.width / 3.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(listGetDataFiltered[index].registerdate),
          ),
          width: MediaQuery.of(context).size.width / 2.8,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
