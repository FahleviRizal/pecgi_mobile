import 'package:flutter/material.dart';
import 'package:panasonic/models/clsMaterialRequest.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/screens/materialrequest/materialrequestdetail.dart';
import 'package:panasonic/screens/materialrequest/materialrequestproductioninformation.dart';
import 'package:panasonic/screens/materialrequest/materialrequestlist.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:intl/intl.dart';

class MaterialRequest extends StatefulWidget {
  const MaterialRequest({Key key, this.userid}) : super(key: key);

  final String userid;
  @override
  _MaterialRequestState createState() => _MaterialRequestState();
}

class _MaterialRequestState extends State<MaterialRequest> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsMaterialRequest> listGetData = [];
  List<ClsMaterialRequest> listGetDataFiltered = [];
  List<ClsMaterialRequest> listInstructionNo;
  List<ClsMaterialRequest> listInstructionNoFiltered;

  final TextEditingController txtInstructionNo = new TextEditingController();
  final TextEditingController txtLine = new TextEditingController();
  final TextEditingController txtScanBarcode = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();

  String _searchcombo = '';
  String _searchDataResult = '';
  String _searchInstructionNo = '';
  //String _selectedInstructionNoNo = '';
  String message;
  String type;
  FocusNode nodeInsNo = new FocusNode();

  @override
  void initState() {
    // asynclistGetData();
    super.initState();
  }

  void asynclistGetData(String insNo) async {
    listGetData = await ClsMaterialRequest.getDataHeader(insNo);
    listGetDataFiltered = listGetData;
    setState(() {});
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void materialDetail(String labelBarcode) async {
    if (txtInstructionNo.text == null || txtInstructionNo.text == '') {
      asyncAlert('Please select / scan Instruction No', '2');
      return;
    }

    listGetData =
        await ClsMaterialRequest.getDetail(txtInstructionNo.text, labelBarcode);
    if (listGetData.length == 0) {
      asyncAlert('Barcode not found', '2');
      return;
    }

    setState(() {
      ClsMaterialRequest listData = new ClsMaterialRequest();
      listData.instNo = txtInstructionNo.text;
      listData.labelBarcode = labelBarcode;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MaterialRequestDetail(),
          settings: RouteSettings(
            arguments: listData,
          ),
        ),
      ).then((value) => setState(() {
            asynclistGetData(txtInstructionNo.text);
            txtScanBarcode.text = '';
          }));
    });
  }

  void materialList(String materialCode) {
    setState(() {
      ClsMaterialRequest listData = new ClsMaterialRequest();
      listData.instNo = txtInstructionNo.text;
      listData.materialCode = materialCode;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MaterialRequestList(),
          settings: RouteSettings(
            arguments: listData,
          ),
        ),
      ).then((value) => setState(() {
            asynclistGetData(txtInstructionNo.text);
          }));
    });
  }

  void materialProductionInformation(String instructionNo) {
    if (txtInstructionNo.text == null || txtInstructionNo.text == '') {
      asyncAlert('Please select / scan Instruction No', '2');
      return;
    }

    setState(() {
      ClsMaterialRequest listData = new ClsMaterialRequest();
      listData.instNo = txtInstructionNo.text;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MaterialRequestProductionInformation(),
          settings: RouteSettings(
            arguments: listData,
          ),
        ),
      ).then((value) => setState(() {
            asynclistGetData(txtInstructionNo.text);
          }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Material Request Scan"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "1. Select / Scan Instruction No",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.005),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TypeAheadField(
                                hideSuggestionsOnKeyboardHide: false,
                                textFieldConfiguration: TextFieldConfiguration(
                                    textInputAction: TextInputAction.done,
                                    onSubmitted: (value) {
                                      txtInstructionNo.text = value;
                                      asynclistGetData(value);
                                    },
                                    focusNode: nodeInsNo,
                                    controller: this.txtInstructionNo,
                                    style: TextStyle(fontFamily: 'Arial'),
                                    decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            txtInstructionNo.clear();
                                            listGetDataFiltered.clear();
                                            _searchcombo = '';
                                          });
                                        },
                                        icon: Icon(_searchcombo == ''
                                            ? Icons.cancel
                                            : Icons.cancel_outlined),
                                        color: _searchcombo == ''
                                            ? Colors.white12.withOpacity(1)
                                            : Colors.grey[600],
                                      ),
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: -10.0, horizontal: 10.0),
                                    )),
                                suggestionsCallback: (pattern) async {
                                  listInstructionNo = await ClsMaterialRequest
                                      .getInstructionNo();

                                  _searchInstructionNo = pattern;
                                  _searchcombo = pattern;

                                  listInstructionNoFiltered = listInstructionNo
                                      .where((user) => user.instNo
                                          .toLowerCase()
                                          .contains(_searchInstructionNo
                                              .toLowerCase()))
                                      .toList();
                                  return listInstructionNoFiltered;
                                },
                                itemBuilder: (context, suggestion) {
                                  return ListTile(
                                    title: Text(suggestion.instNo),
                                  );
                                },
                                onSuggestionSelected: (suggestion) async {
                                  this.txtInstructionNo.text =
                                      suggestion.instNo;
                                  //_selectedInstructionNoNo = suggestion.instNo;
                                  this.txtInstructionNo.text =
                                      suggestion.instNo;
                                  _searchcombo = suggestion.instNo;
                                  asynclistGetData(suggestion.instNo);
                                },
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.010),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: InkWell(
                                  child: Text(
                                    "Production Information",
                                    style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                        fontStyle: FontStyle.italic),
                                  ),
                                  onTap: () {
                                    materialProductionInformation(
                                        txtInstructionNo.text);
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.010),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "2. Scan Material",
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtScanBarcode,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                ),
                                onFieldSubmitted: (value) {
                                  materialDetail(value);
                                },
                              ),
                            ),
                            Divider(height: 20, color: Colors.grey),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                  controller: txtSearch,
                                  decoration: InputDecoration(
                                    labelText: 'Search',
                                    prefixIcon: IconButton(
                                      onPressed: () {},
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.search
                                          : Icons.search),

                                      //Icons.search,
                                    ),
                                    suffixIcon: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          txtSearch.clear();
                                          _searchDataResult = '';
                                          listGetDataFiltered = listGetData;
                                        });
                                      },
                                      icon: Icon(_searchDataResult == ''
                                          ? Icons.cancel
                                          : Icons.cancel_outlined),
                                      color: _searchDataResult == ''
                                          ? Colors.white12.withOpacity(1)
                                          : Colors.grey[600],
                                    ),
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      _searchDataResult = value;
                                      listGetDataFiltered = listGetData
                                          .where((user) =>
                                              user.materialDesc
                                                  .toLowerCase()
                                                  .contains(_searchDataResult
                                                      .toLowerCase()) ||
                                              user.lotNo.toLowerCase().contains(
                                                  _searchDataResult
                                                      .toLowerCase()) ||
                                              user.qty.toLowerCase().contains(
                                                  _searchDataResult
                                                      .toLowerCase()))
                                          .toList();
                                    });
                                  }),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Expanded(
                              child: Container(child: _dataTableWidget()),
                            ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 0),
      _getTitleItemWidget('Material', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Request Qty', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget('Result', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget('', MediaQuery.of(context).size.width * 0.095),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].labelBarcode),
      width: 0,
      height: 45,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      alignment: Alignment.centerLeft,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(listGetDataFiltered[index].materialDesc),
          ),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
                value.format(double.parse(listGetDataFiltered[index].qty))),
          ),
          width: MediaQuery.of(context).size.width / 3.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(value
                .format(double.parse(listGetDataFiltered[index].resultQty))),
          ),
          width: MediaQuery.of(context).size.width / 4,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.12,
          child: IconButton(
            onPressed: () {
              materialList(listGetDataFiltered[index].materialCode);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            color: Colors.black,
            alignment: Alignment.centerRight,
          ),
        ),
      ],
    );
  }
}
