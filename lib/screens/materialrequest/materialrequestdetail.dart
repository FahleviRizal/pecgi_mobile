// import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:panasonic/models/clsMaterialRequest.dart';
import 'package:panasonic/utilities/constants.dart';
// import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter/services.dart';
import 'package:panasonic/utilities/alertdialog.dart';

class MaterialRequestDetail extends StatefulWidget {
  @override
  _MaterialRequestDetailState createState() => _MaterialRequestDetailState();
}

class _MaterialRequestDetailState extends State<MaterialRequestDetail> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  bool init = false;

  List<ClsMaterialRequest> listGetData = [];
  final TextEditingController txtScanBarcodeNo = new TextEditingController();
  final TextEditingController txtMaterial = new TextEditingController();
  final TextEditingController txtLotNo = new TextEditingController();
  final TextEditingController txtQty = new TextEditingController();
  final TextEditingController txtUnit = new TextEditingController();
  final TextEditingController txtSupplierName = new TextEditingController();
  final TextEditingController txtExpDate = new TextEditingController();
  final TextEditingController txtLocation = new TextEditingController();

  String message;
  String type;
  String instructionNo = '';
  FocusNode nodeQty = FocusNode();
  FocusNode nodeScanBarcode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  void asynclistGetData() async {
    init = true;
    listGetData = await ClsMaterialRequest.getDetail(
        instructionNo, txtScanBarcodeNo.text);
    if (listGetData.length > 0) {
      txtMaterial.text =
          listGetData[0].materialCode + ' / ' + listGetData[0].materialDesc;
      txtLotNo.text = listGetData[0].lotNo;
      txtQty.text = listGetData[0].qty;
      txtUnit.text = listGetData[0].unitName;
      txtExpDate.text = listGetData[0].expDate;
      txtSupplierName.text = listGetData[0].supplierName;
      FocusScope.of(context).requestFocus(nodeQty);
    } else {
      txtMaterial.text = '';
      txtLotNo.text = '';
      txtQty.text = '';
      txtUnit.text = '';
      txtExpDate.text = '';
      txtSupplierName.text = '';
      FocusScope.of(context).requestFocus(nodeScanBarcode);

      message = 'Barcode not found';
      type = '2';

      var dialog = CustomAlertDialog(
        type: type,
        title: '',
        message: message,
        okBtnText: '',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
    setState(() {});
  }

  void asyncSubmit() async {
    listGetData = await ClsMaterialRequest.submitData(
        instructionNo, txtScanBarcodeNo.text, txtQty.text);
    if (listGetData.length > 0) {
      if (listGetData[0].id == "200") {
        message = 'Data saved successfully';
        type = '3';

        txtScanBarcodeNo.text = '';
        txtMaterial.text = '';
        txtLotNo.text = '';
        txtQty.text = '';
        txtUnit.text = '';
        txtExpDate.text = '';
        txtSupplierName.text = '';
        FocusScope.of(context).requestFocus(nodeScanBarcode);
      } else if (listGetData[0].id == "400") {
        message = listGetData[0].description;
        type = '2';
      } else {
        message = listGetData[0].description;
        type = '2';
      }

      var dialog = CustomAlertDialog(
        type: type,
        title: '',
        message: message,
        okBtnText: '',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final pModels = ModalRoute.of(context).settings.arguments;
    ClsMaterialRequest model = pModels;

    if (init == false) {
      instructionNo = model.instNo;
      txtScanBarcodeNo.text = model.labelBarcode;
      asynclistGetData();
    }
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Material Detail"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Barcode Material",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtScanBarcodeNo,
                                focusNode: nodeScanBarcode,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                ),
                                onFieldSubmitted: (value) {
                                  asynclistGetData();
                                },
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Material",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtMaterial,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                    fillColor: disabledColor,
                                    filled: true),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Lot No",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtLotNo,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                    fillColor: disabledColor,
                                    filled: true),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Qty",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtQty,
                                focusNode: nodeQty,
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.right,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Unit",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtUnit,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                    fillColor: disabledColor,
                                    filled: true),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Expired Date",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtExpDate,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                    fillColor: disabledColor,
                                    filled: true),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Supplier",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: TextFormField(
                                controller: txtSupplierName,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                    fillColor: disabledColor,
                                    filled: true),
                              ),
                            ),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.01),
                            Expanded(
                              child: SizedBox(
                                  height: MediaQuery.of(context).size.height),
                            ),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  TextButton(
                                    style: TextButton.styleFrom(
                                      backgroundColor: Colors.blue[800],
                                      minimumSize: Size(
                                          MediaQuery.of(context).size.width *
                                              0.90,
                                          55),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50.0),
                                      ),
                                    ),
                                    onPressed: () {
                                      asyncSubmit();
                                    },
                                    child: Text(
                                      "SUBMIT",
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  // ),
                                ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            // Expanded(
                            //   child: Container(
                            //     alignment: Alignment.centerRight,
                            //     child: Padding(
                            //       padding: EdgeInsets.symmetric(
                            //           horizontal: 15, vertical: 15),
                            //       child: TextButton(
                            //         style: TextButton.styleFrom(
                            //           backgroundColor: Colors.blue,
                            //           minimumSize: Size(150, 40),
                            //         ),
                            //         onPressed: () {
                            //           asyncSubmit();
                            //         },
                            //         child: Text(
                            //           "Submit",
                            //           style: TextStyle(
                            //             fontSize: 16,
                            //             color: Colors.white,
                            //           ),
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }
}
