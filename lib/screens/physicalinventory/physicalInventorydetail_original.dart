import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsphysicalinventory.dart';
import 'package:panasonic/services/physicalinventoryservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/message.dart';
// import 'package:intl/intl.dart';
//import 'package:keyboard_visibility/keyboard_visibility.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lot Detail',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class PhysicalInventoryDetail extends StatefulWidget {
  const PhysicalInventoryDetail(
      {Key key,
      this.carryNo,
      this.type,
      this.name,
      this.lotNo,
      this.date,
      this.userid})
      : super(key: key);

  final String carryNo;
  final String type;
  final String name;
  final String lotNo;
  final String date;
  final String userid;

  @override
  _PhysicalInventoryDetail createState() => _PhysicalInventoryDetail();
}

class _PhysicalInventoryDetail extends State<PhysicalInventoryDetail> {
  bool keyboardOpen = false;
  PhysicalInventoryService api = PhysicalInventoryService();

  TextEditingController iCarryNo = TextEditingController();
  TextEditingController iType = TextEditingController();
  TextEditingController iCode = TextEditingController();

  TextEditingController iLotNo = TextEditingController();
  TextEditingController iDate = TextEditingController();
  TextEditingController iSearch = TextEditingController();

  TextEditingController iInvQty = TextEditingController();

  FocusNode nodeInvQty = FocusNode();

  //declare
  List<ClsPhysicalInventoryByCarry> list = [];
  List<ClsPhysicalInventoryByCarry> searchdata = [];
  String _searchResult = '';
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
    // KeyboardVisibilityNotification().addNewListener(
    //   onChange: (bool visible) {
    //     setState(() => keyboardOpen = visible);
    //     print(keyboardOpen);
    //   },
    // );

    iCarryNo.text = widget.carryNo;
    iType.text = widget.name;
    iCode.text = widget.type;
    iLotNo.text = widget.lotNo;

    // final DateTime now = DateTime.parse(widget.date);
    // final DateFormat formatter = DateFormat('dd MMM yyyy');
    // final String formatted = formatter.format(now);
    iDate.text = widget.date;
    _getList();
  }

  void _getList() async {
    searchdata = await api.getListDetail(
        iCarryNo.text, iCode.text, iLotNo.text, iDate.text);
    list = searchdata;
    setState(() {});
  }

  List<ClsPhysicalInventoryByCarry> listData = [];
  void asyncsubmit() async {
    try {
      setState(() {
        isProcessing = true;
      });
      //String message;
      listData =
          await PhysicalInventoryService.submitCarry(searchdata, widget.userid);

      if (listData[0].id == "200") {
        //asyncAlert(listData[0].description, "3");
        Message.box(
            type: "1",
            message: listData[0].message,
            top: 0,
            position: "0",
            context: context);

        // await Navigator.pop(context, "1");
      } else {
        Message.box(
            type: "2",
            message: listData[0].message,
            top: 0,
            position: "0",
            context: context);

        //asyncAlert(listData[0].description, "2");
      }

      // var dialog = CustomAlertDialog(
      //   type: (message == "success") ? "3" : "2",
      //   title: "",
      //   message: (message == "success") ? "Data saved successfully!" : message,
      //   okBtnText: 'Close',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
    } catch (e) {
      var dialog = CustomAlertDialog(
        type: "2",
        title: "",
        message: e,
        okBtnText: 'Close',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            //resizeToAvoidBottomInset: false,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: const Text("Lot Detail"),
            ),
            body: //FutureBuilder(
                // future: api.getListDetail(
                //     iCarryNo.text, iCode.text, iLotNo.text, iDate.text),
                // builder: (context, snapshot) {
                //   //final items = snapshot.data;
                //   //List<ClsPhysicalInventoryByCarry> rdlist = items;
                //   if (searchdata == null || searchdata.length == 0) {
                //     list = snapshot.data;
                //     searchdata = list;
                //   }

                //   if (!snapshot.hasData) {
                //     return Center(child: CircularProgressIndicator());
                //   }
                SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: IntrinsicHeight(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    // CONTENT HERE
                    SizedBox(height: 10.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Type",
                          style: TextStyle(
                              fontFamily: 'Arial', fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        enabled: false,
                        controller: iType,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            filled: true,
                            fillColor: disabledColor),
                      ),
                    ),
                    SizedBox(height: 5),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Lot No",
                          style: TextStyle(
                              fontFamily: 'Arial', fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        enabled: false,
                        controller: iLotNo,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            filled: true,
                            fillColor: disabledColor),
                      ),
                    ),
                    SizedBox(height: 5),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Production Date",
                          style: TextStyle(
                              fontFamily: 'Arial', fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        enabled: false,
                        controller: iDate,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            filled: true,
                            fillColor: disabledColor),
                      ),
                    ),
                    Divider(color: Colors.grey, height: 20),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        controller: iSearch,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          labelText: 'Search',
                          prefixIcon: IconButton(
                            onPressed: () {},
                            icon: Icon(_searchResult == ''
                                ? Icons.search
                                : Icons.search),
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                iSearch.clear();
                                _searchResult = '';
                                searchdata = list;
                              });
                            },
                            icon: Icon(_searchResult == ''
                                ? Icons.cancel
                                : Icons.cancel_outlined),
                            color: _searchResult == ''
                                ? Colors.white12.withOpacity(1)
                                : Colors.grey[600],
                          ),
                        ),
                        onChanged: (value) {
                          setState(() {
                            if (value != null) {
                              _searchResult = value;
                              searchdata = list
                                  .where((rec) =>
                                      rec.barcodeNo.toUpperCase().contains(
                                          _searchResult.toUpperCase()) ||
                                      rec.sublotNo.toUpperCase().contains(
                                          _searchResult.toUpperCase()) ||
                                      rec.currentQty.toUpperCase().contains(
                                          _searchResult.toUpperCase()) ||
                                      rec.inventory.toUpperCase().contains(
                                          _searchResult.toUpperCase()) ||
                                      rec.difference.toUpperCase().contains(
                                          _searchResult.toUpperCase()))
                                  .toList();
                            } else {
                              searchdata = list;
                            }
                          });
                        },
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                          width: 1,
                          color: Colors.grey[100],
                        )),
                        height: MediaQuery.of(context).size.height / 2.3,
                        child: _dataTableWidget()),
                    SizedBox(height: 10),
                    Padding(
                        padding: EdgeInsets.symmetric(vertical: 0),
                        child: Container(
                          padding: EdgeInsets.only(bottom: 10),
                          width: MediaQuery.of(context).size.width * 0.90,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: FloatingActionButton.extended(
                            backgroundColor: (isProcessing)
                                ? disabledButtonColor
                                : enabledButtonColor,
                            onPressed: (isProcessing == false)
                                ? () async {
                                    //if buttonenabled == true then pass a function otherwise pass "null"
                                    asyncsubmit();
                                    //plug delayed for wait while processing data
                                    await Future.delayed(
                                        const Duration(seconds: 3), () {
                                      setState(() {
                                        isProcessing = false;
                                      });
                                    });
                                  }
                                : null,
                            elevation: 0,
                            label: Text(
                              "SUBMIT",
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: (isProcessing)
                                      ? disabledColorButtonText
                                      : enabledColorButtonText),
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            )));
    //}),
    //); // This trailing comma makes auto-formatting nicer for build methods.
    // );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: (MediaQuery.of(context).size.width / 4),
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Barcode No', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('Sublot No', MediaQuery.of(context).size.width / 5),
      _getTitleItemWidget('Current', MediaQuery.of(context).size.width / 4.5),
      _getTitleItemWidget('Inventory', MediaQuery.of(context).size.width / 4.5),
      _getTitleItemWidget('Difference', MediaQuery.of(context).size.width / 5),
      // _getTitleItemWidget('  ', 20),
    ];
  }

  Widget _getTitleItemWidget(String label, double pwidth) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      height: 45,
      width: pwidth,
      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(searchdata[index].barcodeNo),
      width: MediaQuery.of(context).size.width / 4,
      height: 45,
      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Expanded(
      child: Row(
        children: <Widget>[
          Container(
            child: Text(searchdata[index].sublotNo),
            width: MediaQuery.of(context).size.width / 5.5,
            height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
            alignment: Alignment.center,
          ),
          Container(
            width: MediaQuery.of(context).size.width / 4.5,
            child: Text(searchdata[index].currentQty),
            height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
            alignment: Alignment.centerRight,
          ),
          Container(
            width: MediaQuery.of(context).size.width / 4.5,
            height: 42,
            decoration: BoxDecoration(color: Colors.yellow[100]),
            child: TextFormField(
                enabled: true,
                controller: TextEditingController(
                  text: (searchdata[index].inventory == '0.00')
                      ? ""
                      : searchdata[index].inventory,
                ),
                //enableInteractiveSelection: false,
                textInputAction: TextInputAction.next,
                textAlign: TextAlign.right,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r"[0-9.]")),
                  TextInputFormatter.withFunction((oldValue, newValue) {
                    try {
                      final text = newValue.text;
                      if (text.isNotEmpty) double.parse(text);
                      return newValue;
                    } catch (e) {}
                    return oldValue;
                  }),
                ],
                //keyboardType: TextInputType.number,
                keyboardType: TextInputType.numberWithOptions(
                    decimal: true, signed: false),
                decoration: InputDecoration(
                  enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: Color.fromRGBO(88, 87, 94, 0.2), width: 0.0),
                  ),
                  border: const OutlineInputBorder(),
                  contentPadding:
                      new EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                ),
                onFieldSubmitted: (val) {
                  final newval = searchdata.firstWhere(
                      (item) =>
                          item.barcodeNo == searchdata[index].barcodeNo &&
                          item.sublotNo == searchdata[index].sublotNo,
                      orElse: () => null);
                  if (newval != null)
                    setState(() {
                      double current =
                          double.parse(searchdata[index].currentQty);
                      if (val != "" && val != null) {
                        double inv = double.parse(val);
                        double diff = current - inv;

                        newval.inventory = val;
                        newval.difference = diff.toString();
                      }
                    });
                }),
          ),
          // Container(
          //   width: MediaQuery.of(context).size.width / 4.5,
          //   child: Text(searchdata[index].inventory),
          //   height: 45,
          //   padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          //   alignment: Alignment.centerRight,
          // ),
          Container(
            width: MediaQuery.of(context).size.width / 5,
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(searchdata[index].difference),
            ),
            height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
            alignment: Alignment.centerRight,
          ),
          // Container(
          //   width: 20,
          // )
        ],
      ),
    );
  }
}

/*
SingleChildScrollView(
                          physics: ClampingScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          child: SingleChildScrollView(
                            physics: ClampingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            child: DataTable(
                              columnSpacing: 20.0,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                width: 1,
                                color: Colors.grey[300],
                              )),
                              headingRowColor: MaterialStateColor.resolveWith(
                                  (states) => Colors.grey[200]),
                              sortColumnIndex: 0,
                              showCheckboxColumn: false,
                              columns: [
                                DataColumn(
                                    label: Container(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 0),
                                      child: Text(
                                        "Barcode No",
                                        style: TextStyle(
                                            fontFamily: "Arial",
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black),
                                      ),
                                    ),
                                    numeric: false),
                                DataColumn(
                                  label: Container(
                                      child: Text(
                                    "Sublot No",
                                    style: TextStyle(
                                        fontFamily: "Arial",
                                        fontWeight: FontWeight.bold),
                                  )),
                                  numeric: false,
                                ),
                                DataColumn(
                                  label: Container(
                                      child: Text(
                                    "Current",
                                    style: TextStyle(
                                        fontFamily: "Arial",
                                        fontWeight: FontWeight.bold),
                                  )),
                                  numeric: false,
                                ),
                                DataColumn(
                                  label: Container(
                                      child: Text(
                                    "Inventory",
                                    style: TextStyle(
                                        fontFamily: "Arial",
                                        fontWeight: FontWeight.bold),
                                  )),
                                  numeric: false,
                                ),
                                DataColumn(
                                  label: Container(
                                      child: Text(
                                    "Difference",
                                    style: TextStyle(
                                        fontFamily: "Arial",
                                        fontWeight: FontWeight.bold),
                                  )),
                                  numeric: false,
                                ),
                              ],
                              rows: searchdata
                                  .map(
                                    (idata) => DataRow(cells: [
                                      DataCell(
                                        Container(
                                          child: Text(idata.barcodeNo),
                                          alignment: Alignment.centerLeft,
                                        ),
                                      ),
                                      DataCell(
                                        Container(
                                          child: Text(idata.sublotNo),
                                          alignment: Alignment.centerLeft,
                                        ),
                                      ),
                                      DataCell(
                                        Container(
                                          child: Text(idata.currentQty),
                                          alignment: Alignment.centerRight,
                                        ),
                                      ),
                                      DataCell(
                                        Container(
                                          height: 42,
                                          decoration: BoxDecoration(
                                              color: Colors.yellow[100]),
                                          child: TextFormField(
                                              decoration: InputDecoration(
                                                enabledBorder:
                                                    const OutlineInputBorder(
                                                  borderSide: const BorderSide(
                                                      color: Color.fromRGBO(
                                                          88, 87, 94, 0.2),
                                                      width: 0.0),
                                                ),
                                                border:
                                                    const OutlineInputBorder(),
                                                contentPadding:
                                                    new EdgeInsets.symmetric(
                                                        vertical: 5.0,
                                                        horizontal: 10.0),
                                              ),
                                              controller: TextEditingController(
                                                text: idata.inventory,
                                              ),
                                              keyboardType:
                                                  TextInputType.number,
                                              // textDirection:
                                              //     TextDirection.rtl,
                                              textInputAction:
                                                  TextInputAction.newline,
                                              textAlign: TextAlign.right,
                                              // onChanged: (val) {
                                              //   final newval =
                                              //       searchdata.firstWhere(
                                              //           (item) =>
                                              //               item.carryNo ==
                                              //                   iCarryNo
                                              //                       .text &&
                                              //               item.lotNo ==
                                              //                   iLotNo
                                              //                       .text &&
                                              //               item.barcodeNo ==
                                              //                   idata
                                              //                       .barcodeNo &&
                                              //               item.sublotNo ==
                                              //                   idata
                                              //                       .sublotNo,
                                              //           orElse: () => null);
                                              //   if ((val != "0") &&
                                              //       (newval != null)) {
                                              //     setState(() {
                                              //       int current = int.parse(
                                              //           idata.currentQty);
                                              //       int inv =
                                              //           int.parse(val);
                                              //       int diff =
                                              //           current - inv;
                                              //       newval.inventory = val;
                                              //       newval.difference =
                                              //           diff.toString();
                                              //     });
                                              //   }
                                              // },
                                              onFieldSubmitted: (val) {
                                                final newval =
                                                    searchdata.firstWhere(
                                                        (item) =>
                                                            item.barcodeNo ==
                                                                idata
                                                                    .barcodeNo &&
                                                            item.sublotNo ==
                                                                idata.sublotNo,
                                                        orElse: () => null);
                                                if (newval != null)
                                                  setState(() {
                                                    double current =
                                                        double.parse(
                                                            idata.currentQty);
                                                    double inv =
                                                        double.parse(val);
                                                    double diff = current - inv;

                                                    newval.inventory = val;
                                                    newval.difference =
                                                        diff.toString();
                                                  });

                                                // print(searchdata);
                                                // FocusScope.of(context)
                                                //     .requestFocus(focus);
                                              }),
                                          // Text(idata.qty),
                                          // alignment: Alignment.centerRight,
                                        ),
                                      ),
                                      DataCell(
                                        Container(
                                          child: Text(idata.difference),
                                          alignment: Alignment.centerRight,
                                        ),
                                      ),
                                    ]),
                                  )
                                  .toList(),
                            ),
                          ),
                        ),
*/