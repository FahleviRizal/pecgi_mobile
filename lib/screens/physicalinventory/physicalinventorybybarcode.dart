import 'package:flutter/material.dart';
import 'package:panasonic/models/clsphysicalinventory.dart';
import 'package:panasonic/services/physicalinventoryservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/message.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Physical Inventory By Barcode',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class PhysicalInventoryByBarcode extends StatefulWidget {
  const PhysicalInventoryByBarcode({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _PhysicalInventoryByBarcode createState() => _PhysicalInventoryByBarcode();
}

class _PhysicalInventoryByBarcode extends State<PhysicalInventoryByBarcode> {
  PhysicalInventoryService api = PhysicalInventoryService();

  TextEditingController iPeriod = TextEditingController();
  TextEditingController iBarcodeNo = TextEditingController();
  TextEditingController iItemCode = TextEditingController();
  TextEditingController iItemName = TextEditingController();
  TextEditingController iLotNo = TextEditingController();
  TextEditingController iQty = TextEditingController();
  TextEditingController iQtyTemp = TextEditingController();

  TextEditingController iActualQty = TextEditingController();
  TextEditingController iWarehouse = TextEditingController();
  TextEditingController iLocation = TextEditingController();
  TextEditingController iScanActualLoc = TextEditingController();
  TextEditingController iLocationName = TextEditingController();
  final value = new NumberFormat("#,##0.00", "en_US");

  FocusNode node = FocusNode();
  FocusNode nodeActualQty = FocusNode();
  FocusNode nodeLoc = FocusNode();
  FocusNode nodeButton = FocusNode();
  bool isProcessing = false;
  String _scanBarcode = '';

  @override
  void initState() {
    super.initState();

    asyncGetPeriod();
  }

  void asyncGetPeriod() async {
    String value = "";
    value = await api.getPeriod();
    iPeriod.text = value;
  }

  void asyncGetData(String period, String barcodeNo) async {
    List<ClsPhysicalInventoryByBarcode> list = [];

    list = await api.getData(period, barcodeNo, widget.warehouse);

    if (list.length > 0) {
      iItemCode.text = list[0].itemCode;
      iItemName.text = list[0].itemName;
      iLotNo.text = list[0].lotNo;
      iQty.text = value.format(double.parse(list[0].qty));
      iQtyTemp.text = list[0].qty;
      iWarehouse.text = list[0].warehouse;
      iLocation.text = list[0].location;
    } else {
      iItemCode.text = "";
      iItemName.text = "";
      iLotNo.text = "";
      iQty.text = "0";
      iQtyTemp.text = "0";
      iWarehouse.text = "";
      iLocation.text = "";

      Message.box(
          type: '2',
          message: "Barcode No is not found",
          top: 0,
          position: "0",
          context: context);

      // var dialog = CustomAlertDialog(
      //   type: "4",
      //   title: "",
      //   message: "Barcode No is not found",
      //   okBtnText: 'Close',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
    }
  }

  void asyncGetLocation(String locCode) async {
    List<ClsPhysicalInventoryByBarcode> list = [];
    if (locCode.isNotEmpty || locCode == "") {
      list = await api.actualLocation(locCode);

      if (list.length > 0) {
        iLocationName.text = list[0].actualLocName;
      } else {
        iLocationName.text = "";

        Message.box(
            type: '2',
            message: "Location No is not found",
            top: 0,
            position: "0",
            context: context);

        // var dialog = CustomAlertDialog(
        //   type: "4",
        //   title: "",
        //   message: "Location No is not found",
        //   okBtnText: 'Close',
        // );
        // showDialog(context: context, builder: (BuildContext context) => dialog);
      }
    }
  }

  List<ClsPhysicalInventoryByBarcode> listData;
  void asyncSubmit(String period, String barcode, String item, String lotNo,
      String actualQty, String actualLoc) async {
    setState(() {
      isProcessing = true;
    });
    if (barcode == "") {
      Message.box(
          type: '2',
          message: "Please scan barcode",
          top: 0,
          position: "0",
          context: context);
      return;
    }
    if (actualQty == "" || actualQty == "0") {
      Message.box(
          type: '2',
          message: "Actual Qty must greater than zero",
          top: 0,
          position: "0",
          context: context);

      // var dialog = CustomAlertDialog(
      //   type: "2",
      //   title: "",
      //   message: "Actual Qty must greater than zero",
      //   okBtnText: 'Close',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
      return;
    }

    // if (int.parse(actualQty) > int.parse(iQtyTemp.text)) {
    //   Message.box(
    //       type: '2',
    //       message: "Actual Qty must less than Qty",
    //       top: 0,
    //       position: "0",
    //       context: context);
    //   return;
    // }

    try {
      //String message;
      listData = await PhysicalInventoryService.submit(period, barcode, item,
          lotNo, actualQty, actualLoc, widget.userid, widget.warehouse);
      if (listData[0].id == "200") {
        Message.box(
            type: "1",
            message: "Submit data successfully",
            top: 0,
            position: "0",
            context: context);
        iBarcodeNo.clear();
        iItemCode.text = '';
        iItemName.text = '';
        iLotNo.text = '';
        iQty.text = '';
        iLocation.text = '';
        iLocationName.text = '';
        iWarehouse.text = '';
        iActualQty.text = '';

        FocusScope.of(context).requestFocus(node);
      } else {
        Message.box(
            type: "2",
            message: listData[0].message.toString(),
            top: 0,
            position: "0",
            context: context);
        iBarcodeNo.clear();
        FocusScope.of(context).requestFocus(node);
        //asyncAlert(listcheck[0].message.toString(), "4");
      }
      // var dialog = CustomAlertDialog(
      //   type: (message == "success") ? "3" : "2",
      //   title: "",
      //   message: (message == "success") ? "Data saved successfully!" : message,
      //   okBtnText: 'Close',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
    } catch (e) {
      var dialog = CustomAlertDialog(
        type: "2",
        title: "",
        message: e.toString(),
        okBtnText: 'Close',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text("Physical Inventory By Barcode"),
        ),
        body: ListView(
          scrollDirection: Axis.vertical,
          // mainAxisSize: MainAxisSize.min,
          // mainAxisAlignment: MainAxisAlignment.center,
          // verticalDirection: VerticalDirection.down,
          children: <Widget>[
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  "Period",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                textAlign: TextAlign.center,
                controller: iPeriod,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Scan Barcode No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                autofocus: true,
                showCursor: true,
                controller: iBarcodeNo,
                focusNode: node,
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iBarcodeNo.clear();
                        _scanBarcode = '';
                        iItemCode.text = "";
                        iItemName.text = "";
                        iLotNo.text = "";
                        iQty.text = "0";
                        iWarehouse.text = "";
                        iLocation.text = "";

                        FocusScope.of(context).requestFocus(node);
                      });
                    },
                    icon: Icon(_scanBarcode == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _scanBarcode == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _scanBarcode = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  asyncGetData(iPeriod.text, iBarcodeNo.text);
                  FocusScope.of(context).requestFocus(nodeButton);
                },
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Item Code",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iItemCode,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Item Name",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iItemName,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Lot No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iLotNo,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      child: Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(
                              right: MediaQuery.of(context).size.height * 0.16),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Qty",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                    ),
                    SizedBox(height: 5),
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.right,
                        controller: iQty,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height * 0.10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Actual Qty",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                    ),
                    SizedBox(height: 5),
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.right,
                        controller: iActualQty,
                        focusNode: nodeActualQty,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                        ),
                        onEditingComplete: () {
                          FocusScope.of(context).requestFocus(nodeLoc);
                          FocusScope.of(context).requestFocus(nodeButton);
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      child: Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(
                              right: MediaQuery.of(context).size.height * 0.10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Warehouse",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                    ),
                    SizedBox(height: 5),
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        controller: iWarehouse,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height * 0.10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Location",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                    ),
                    SizedBox(height: 5),
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        controller: iLocation,
                        enabled: false,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            filled: true,
                            fillColor: disabledColor),
                      ),
                    ),
                  ],
                )
              ],
            ),

            SizedBox(height: 5),
            Divider(
              height: 10,
              color: Colors.grey,
            ),

            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 20),
            //   child: Align(
            //     alignment: Alignment.centerLeft,
            //     child: Text(
            //       "Scan Actual Location",
            //       style: TextStyle(
            //           fontFamily: 'Arial', fontWeight: FontWeight.bold),
            //     ),
            //   ),
            // ),
            // SizedBox(height: 5.0),
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 10),
            //   child: TextFormField(
            //     controller: iScanActualLoc,
            //     focusNode: nodeLoc,
            //     decoration: InputDecoration(
            //       border: OutlineInputBorder(),
            //       contentPadding: new EdgeInsets.symmetric(
            //           vertical: 10.0, horizontal: 10.0),
            //     ),
            //     onEditingComplete: () {
            //       asyncGetLocation(iScanActualLoc.text);
            //     },
            //   ),
            // ),
            // SizedBox(height: 5.0),
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 20),
            //   child: Align(
            //     alignment: Alignment.centerLeft,
            //     child: Text(
            //       "Location Name",
            //       style: TextStyle(
            //           fontFamily: 'Arial', fontWeight: FontWeight.bold),
            //     ),
            //   ),
            // ),
            // SizedBox(height: 5.0),
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 10),
            //   child: TextFormField(
            //     controller: iLocationName,
            //     enabled: false,
            //     decoration: InputDecoration(
            //       border: OutlineInputBorder(),
            //       contentPadding: new EdgeInsets.symmetric(
            //           vertical: 10.0, horizontal: 10.0),
            //       filled: true,
            //       fillColor: disabledColor,
            //     ),
            //   ),
            // ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                // height: MediaQuery.of(context).size.height * 0.1,
                // alignment: Alignment.center,
                children: <Widget>[
                  // Padding(
                  //   padding: EdgeInsets.symmetric(vertical: 5),
                  //   child:
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: (isProcessing)
                          ? disabledButtonColor
                          : enabledButtonColor,
                      minimumSize:
                          Size(MediaQuery.of(context).size.width * 0.90, 55),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                    ),
                    // onPressed: () {
                    //   asyncSubmit(iPeriod.text, iBarcodeNo.text, iItemCode.text,
                    //       iLotNo.text, iActualQty.text, iScanActualLoc.text);
                    // },
                    onPressed: (isProcessing == false)
                        ? () async {
                            //if buttonenabled == true then pass a function otherwise pass "null"
                            asyncSubmit(
                                iPeriod.text,
                                iBarcodeNo.text,
                                iItemCode.text,
                                iLotNo.text,
                                iActualQty.text,
                                iScanActualLoc.text);
                            //plug delayed for wait while processing data
                            await Future.delayed(const Duration(seconds: 3),
                                () {
                              setState(() {
                                isProcessing = false;
                              });
                            });
                          }
                        : null,
                    child: Text(
                      "SUBMIT",
                      style: TextStyle(
                        fontSize: 18,
                        color: (isProcessing)
                            ? disabledColorButtonText
                            : enabledColorButtonText,
                      ),
                    ),
                  ),
                  // ),
                ]),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
            // SizedBox(height: 10),
            // Padding(
            //     padding: EdgeInsets.symmetric(horizontal: 10),
            //     child: Container(
            //       //padding: EdgeInsets.only(top: 0),
            //       width: MediaQuery.of(context).size.width * 0.90,
            //       decoration: BoxDecoration(
            //         borderRadius: BorderRadius.circular(10.0),
            //       ),
            //       child: FloatingActionButton.extended(
            //         backgroundColor: Colors.blue[800],
            //         onPressed: () {
            //           asyncSubmit(iPeriod.text, iBarcodeNo.text, iItemCode.text,
            //               iLotNo.text, iActualQty.text, iScanActualLoc.text);
            //         },
            //         elevation: 0,
            //         label: Text(
            //           "SUBMIT",
            //           style: TextStyle(fontSize: 18.0),
            //         ),
            //       ),
            //     )),
            // Expanded(
            //   child: Padding(
            //       padding: EdgeInsets.symmetric(horizontal: 10),
            //       child: Align(
            //           alignment: Alignment.bottomRight,
            //           child: ElevatedButton(
            //             child: Container(
            //               child: Container(
            //                   height: 40,
            //                   width: 80,
            //                   alignment: Alignment.center,
            //                   child: Text(' Submit ')),
            //             ),
            //             onPressed: () {
            //               asyncSubmit(
            //                   iPeriod.text,
            //                   iBarcodeNo.text,
            //                   iItemCode.text,
            //                   iLotNo.text,
            //                   iActualQty.text,
            //                   iScanActualLoc.text);
            //             },
            //           ))),
            // ),
          ],
          //),
          //),
        ),
      ),
    );
  }
}
