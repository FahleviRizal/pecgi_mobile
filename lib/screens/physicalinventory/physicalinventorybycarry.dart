import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsphysicalinventory.dart';
import 'package:panasonic/screens/physicalinventory/physicalInventorydetail.dart';
import 'package:panasonic/services/physicalinventoryservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Physical Inventory By Carry',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class PhysicalInventoryByCarry extends StatefulWidget {
  const PhysicalInventoryByCarry({Key key, this.userid}) : super(key: key);

  final String userid;

  @override
  _PhysicalInventoryByCarry createState() => _PhysicalInventoryByCarry();
}

class _PhysicalInventoryByCarry extends State<PhysicalInventoryByCarry> {
  PhysicalInventoryService api = PhysicalInventoryService();

  TextEditingController iCarryNo = TextEditingController();
  TextEditingController iCarryName = TextEditingController();

  TextEditingController iSearch = TextEditingController();

  //declare
  List<ClsPhysicalInventoryByCarry> list = [];
  List<ClsPhysicalInventoryByCarry> searchdata = [];
  String _combosearch = '';
  String _searchResult = '';
  String _ddlFilterResult = '';
  List<ClsPhysicalInventoryByCarry> ddlCarryNo = [];
  List<ClsPhysicalInventoryByCarry> ddlCarryNoFilter = [];

  FocusNode node = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  void asyncGetList(String carryNo) async {
    list = await api.getListHeader(carryNo);
    searchdata = list;
    setState(() {});
  }

  void _detail(String _carryNo, String _type, String _name, String _lotNo,
      String _date) {
    setState(() {
      Navigator.of(context)
          .push(
            MaterialPageRoute(
              builder: (context) => PhysicalInventoryDetail(
                carryNo: _carryNo,
                type: _type,
                name: _name,
                lotNo: _lotNo,
                date: _date,
                userid: widget.userid,
              ),
              settings: RouteSettings(),
            ),
          )
          .then((value) => setState(() {
                asyncGetList(iCarryNo.text);
              }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: const Text("Physical Inventory By Carry"),
            ),
            body: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                SizedBox(height: 10.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Select/Scan Carry",
                      style: TextStyle(
                          fontFamily: 'Arial', fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TypeAheadField(
                    hideSuggestionsOnKeyboardHide: false,
                    textFieldConfiguration: TextFieldConfiguration(
                        textInputAction: TextInputAction.done,
                        onSubmitted: (value) {
                          iCarryNo.text = value;

                          asyncGetList(value);
                          ddlCarryNo
                              .where((result) => result.carryNo
                                  .toLowerCase()
                                  .contains(value.toLowerCase()))
                              .toList();

                          final newval = ddlCarryNo.firstWhere(
                              (item) => item.carryNo == value,
                              orElse: () => null);

                          setState(() {
                            iCarryName.text = newval.carryName;
                          });
                        },
                        focusNode: node,
                        controller: iCarryNo,
                        style: TextStyle(fontFamily: 'Arial'),
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                iCarryNo.clear();
                                iCarryName.text = '';
                                ddlCarryNoFilter.clear();
                                searchdata.clear();
                                _combosearch = '';
                              });
                            },
                            icon: Icon(_combosearch == ''
                                ? Icons.cancel
                                : Icons.cancel_outlined),
                            color: _combosearch == ''
                                ? Colors.white12.withOpacity(1)
                                : Colors.grey[600],
                          ),
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: -10.0, horizontal: 10.0),
                        )),
                    suggestionsCallback: (pattern) async {
                      ddlCarryNo = await api.getCarryNo();
                      _ddlFilterResult = pattern;
                      _combosearch = pattern;
                      ddlCarryNoFilter = ddlCarryNo
                          .where((result) => result.carryNo
                              .toLowerCase()
                              .contains(_ddlFilterResult.toLowerCase()))
                          .toList();
                      return ddlCarryNoFilter;
                    },
                    itemBuilder: (context, suggestion) {
                      //print(suggestion);
                      return ListTile(
                        title: Text(suggestion.carryNo),
                        subtitle: Text('${suggestion.carryName}'),
                      );
                    },
                    onSuggestionSelected: (suggestion) {
                      iCarryNo.text = suggestion.carryNo;
                      iCarryName.text = suggestion.carryName;
                      _combosearch = suggestion.carryNo;
                      asyncGetList(suggestion.carryNo);
                    },
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Carry Name",
                      style: TextStyle(
                          fontFamily: 'Arial', fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: iCarryName,
                    enabled: false,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      filled: true,
                      fillColor: disabledColor,
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Divider(color: Colors.grey, height: 20),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: iSearch,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      labelText: 'Search',
                      prefixIcon: IconButton(
                        onPressed: () {},
                        icon: Icon(
                            _searchResult == '' ? Icons.search : Icons.search),
                      ),
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            iSearch.clear();
                            _searchResult = '';
                            searchdata = list;
                          });
                        },
                        icon: Icon(_searchResult == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _searchResult == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                      ),
                    ),
                    onChanged: (value) {
                      setState(() {
                        if (value != null) {
                          _searchResult = value;
                          searchdata = list
                              .where((rec) =>
                                  rec.type
                                      .toUpperCase()
                                      .contains(_searchResult.toUpperCase()) ||
                                  rec.lotNo
                                      .toUpperCase()
                                      .contains(_searchResult.toUpperCase()) ||
                                  rec.currentQty
                                      .toUpperCase()
                                      .contains(_searchResult.toUpperCase()) ||
                                  rec.inventory
                                      .toUpperCase()
                                      .contains(_searchResult.toUpperCase()) ||
                                  rec.difference
                                      .toUpperCase()
                                      .contains(_searchResult.toUpperCase()))
                              .toList();
                        } else {
                          searchdata = list;
                        }
                      });
                    },
                  ),
                ),
                SizedBox(height: 10),
                Expanded(
                  child: _dataTableWidget(),
                ),
              ],
            )));
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: (MediaQuery.of(context).size.width / 4),
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      //_getTitleItemWidget('', 1),
      _getTitleItemWidget('Type', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('LotNo', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('Current', MediaQuery.of(context).size.width / 4.5),
      _getTitleItemWidget('Inventory', MediaQuery.of(context).size.width / 4.5),
      _getTitleItemWidget(
          'Difference', MediaQuery.of(context).size.width / 4.5),
      _getTitleItemWidget('  ', 40),
    ];
  }

  Widget _getTitleItemWidget(String label, double pwidth) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      height: 45,
      width: pwidth,
      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(searchdata[index].type),
      width: MediaQuery.of(context).size.width / 4,
      height: 45,
      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
      alignment: Alignment.centerLeft,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        // Container(
        //   child: Text(searchdata[index].type),
        //   width: MediaQuery.of(context).size.width / 4,
        //   height: 45,
        //   padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
        //   alignment: Alignment.centerLeft,
        // ),
        Container(
          child: Text(searchdata[index].lotNo),
          width: MediaQuery.of(context).size.width / 4,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.center,
        ),
        Container(
          width: MediaQuery.of(context).size.width / 5,
          child: Text(value.format(double.parse(searchdata[index].currentQty))),
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: MediaQuery.of(context).size.width / 4.5,
          child: Text(value.format(double.parse(searchdata[index].inventory))),
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: MediaQuery.of(context).size.width / 4.5,
          child: Align(
            alignment: Alignment.centerRight,
            child:
                Text(value.format(double.parse(searchdata[index].difference))),
          ),
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: 40,
          height: 45,
          child: Align(
              alignment: Alignment.center,
              child: IconButton(
                onPressed: () {
                  _detail(
                      iCarryNo.text,
                      searchdata[index].type,
                      searchdata[index].typedesc,
                      searchdata[index].lotNo,
                      searchdata[index].prodDate);
                },
                icon: Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                ),
                color: Colors.black,
                alignment: Alignment.centerRight,
              )),
        ),
      ],
    );
  }
}


/*
                    //padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: DataTable(
                          columnSpacing: 15.0,
                          decoration: BoxDecoration(
                              border: Border.all(
                            width: 1,
                            color: Colors.grey[300],
                          )),
                          headingRowColor: MaterialStateColor.resolveWith(
                              (states) => Colors.grey[200]),
                          sortColumnIndex: 0,
                          showCheckboxColumn: false,
                          columns: [
                            DataColumn(
                                label: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 0),
                                  child: Text(
                                    "Type",
                                    style: TextStyle(
                                        fontFamily: "Arial",
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                                numeric: false),
                            DataColumn(
                              label: Container(
                                  child: Text(
                                "Lot No",
                                style: TextStyle(
                                    fontFamily: "Arial",
                                    fontWeight: FontWeight.bold),
                              )),
                              numeric: false,
                            ),
                            DataColumn(
                              label: Container(
                                  child: Text(
                                "Current",
                                style: TextStyle(
                                    fontFamily: "Arial",
                                    fontWeight: FontWeight.bold),
                              )),
                              numeric: false,
                            ),
                            DataColumn(
                              label: Container(
                                  child: Text(
                                "Inventory",
                                style: TextStyle(
                                    fontFamily: "Arial",
                                    fontWeight: FontWeight.bold),
                              )),
                              numeric: false,
                            ),
                            DataColumn(
                              label: Container(
                                  child: Text(
                                "Difference",
                                style: TextStyle(
                                    fontFamily: "Arial",
                                    fontWeight: FontWeight.bold),
                              )),
                              numeric: false,
                            ),
                            DataColumn(
                                label: Container(
                              child: Text(""),
                            ))
                          ],
                          rows: searchdata
                              .map(
                                (idata) => DataRow(cells: [
                                  DataCell(
                                    Container(
                                      child: Text(idata.typedesc),
                                      alignment: Alignment.centerLeft,
                                    ),
                                  ),
                                  DataCell(
                                    Container(
                                      child: Text(idata.lotNo),
                                      alignment: Alignment.centerLeft,
                                    ),
                                  ),
                                  DataCell(
                                    Container(
                                      child: Text(idata.currentQty),
                                      alignment: Alignment.centerRight,
                                    ),
                                  ),
                                  DataCell(
                                    Container(
                                      child: Text(idata.inventory),
                                      alignment: Alignment.centerRight,
                                    ),
                                  ),
                                  DataCell(
                                    Container(
                                      child: Text(idata.difference),
                                      alignment: Alignment.centerRight,
                                    ),
                                  ),
                                  DataCell(
                                    Container(
                                      child: IconButton(
                                        onPressed: () {
                                          _detail(
                                              iCarryNo.text,
                                              idata.type,
                                              idata.typedesc,
                                              idata.lotNo,
                                              idata.prodDate);
                                        },
                                        icon: Icon(
                                          Icons.arrow_forward_ios,
                                          size: 15,
                                        ),
                                        color: Colors.black,
                                      ),
                                    ),
                                    onTap: () {
                                      _detail(
                                          iCarryNo.text,
                                          idata.type,
                                          idata.typedesc,
                                          idata.lotNo,
                                          idata.prodDate);
                                    },
                                    //IconButton(),
                                  ),
                                ]),
                              )
                              .toList(),
                        ),
                      ),
                    ),
                    */