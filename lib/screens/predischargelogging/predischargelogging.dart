import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/models/clspredischargelogging.dart';
import 'package:panasonic/services/predischargeloggingservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

class PredischargeLogging extends StatefulWidget {
  const PredischargeLogging({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;
  @override
  _PredischargeLogging createState() => _PredischargeLogging();
}

class _PredischargeLogging extends State<PredischargeLogging> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsPredischargeLogging> listGetData = [];
  List<ClsPredischargeLogging> listGetDataFiltered = [];
  List<ClsPredischargeLogging> ddlInsNo = [];
  List<ClsPredischargeLogging> ddlInsNoFilter = [];

  final TextEditingController txtInsNo = new TextEditingController();
  final TextEditingController txtScanBarcode = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();

  String _scanInsNo = '';
  String _scanBarcode = '';
  String _searchDataResult = '';
  bool isProcessing = false;

  FocusNode nodeBarcode = FocusNode();
  FocusNode nodeInsNo = FocusNode();

  @override
  void initState() {
    //asynclistGetData();
    super.initState();
  }

  void asynclistGetData(String insNo) async {
    listGetData = await PredischargeLoggingService.getListHeader(insNo);
    listGetDataFiltered = listGetData;
    setState(() {});
  }

  void _submit(String insno, String barcodeNo) async {
    setState(() {
      isProcessing = true;
    });

    if (insno == "") {
      Message.box(
          type: "2",
          message: "please select/scan instruction",
          top: 0,
          position: "0",
          context: context);
      return;
    }
    if (barcodeNo == "") {
      Message.box(
          type: "2",
          message: "please scan barcode no",
          top: 0,
          position: "0",
          context: context);
      return;
    }

    List<ClsPredischargeLogging> listScanData;
    listScanData = await PredischargeLoggingService.submit(
        insno, barcodeNo, widget.userid, widget.warehouse);

    if (listScanData.length > 0) {
      if (listScanData[0].id == "200") {
        Message.box(
            type: "1",
            message: listScanData[0].description,
            top: 0,
            position: "0",
            context: context);
        txtScanBarcode.clear();
        _scanBarcode = '';
        FocusScope.of(context).requestFocus(nodeBarcode);
      } else if (listScanData[0].id == "400") {
        Message.box(
            type: "2",
            message: listScanData[0].description,
            top: 0,
            position: "0",
            context: context);
        txtScanBarcode.clear();
        _scanBarcode = '';
        FocusScope.of(context).requestFocus(nodeBarcode);
      } else {
        Message.box(
            type: "2",
            message: listScanData[0].description,
            top: 0,
            position: "0",
            context: context);
        txtScanBarcode.clear();
        _scanBarcode = '';
        FocusScope.of(context).requestFocus(nodeBarcode);
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text("Predischarge Logging"),
            ),
            body: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: <Widget>[
                  SizedBox(height: 10.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "2. Scan/Select Instruction No",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TypeAheadField(
                      hideSuggestionsOnKeyboardHide: false,
                      textFieldConfiguration: TextFieldConfiguration(
                          controller: txtInsNo,
                          textInputAction: TextInputAction.done,
                          onSubmitted: (value) {
                            txtInsNo.text = value;
                            _scanInsNo = value;

                            ddlInsNoFilter
                                .where((result) => result.instructionNo
                                    .toLowerCase()
                                    .contains(value.toLowerCase()))
                                .toList();
                            asynclistGetData(
                              ddlInsNoFilter[0].instructionNo,
                            );
                            FocusScope.of(context).requestFocus(nodeBarcode);
                          },
                          focusNode: nodeInsNo,
                          style: TextStyle(fontFamily: 'Arial'),
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              onPressed: () {
                                setState(() {
                                  txtInsNo.clear();
                                  listGetDataFiltered.clear();
                                  _scanInsNo = '';
                                  _scanBarcode = '';
                                  txtScanBarcode.clear();

                                  FocusScope.of(context)
                                      .requestFocus(nodeInsNo);
                                });
                              },
                              icon: Icon(_scanInsNo == ''
                                  ? Icons.cancel
                                  : Icons.cancel_outlined),
                              color: _scanInsNo == ''
                                  ? Colors.white12.withOpacity(1)
                                  : Colors.grey[600],
                            ),
                            border: OutlineInputBorder(),
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: -10.0, horizontal: 10.0),
                          )),
                      suggestionsCallback: (pattern) async {
                        ddlInsNo =
                            await PredischargeLoggingService.getInstructionNo(
                                '018', widget.userid);
                        _scanInsNo = pattern;
                        ddlInsNoFilter = ddlInsNo
                            .where((result) => result.instructionNo
                                .toLowerCase()
                                .contains(_scanInsNo.toLowerCase()))
                            .toList();
                        return ddlInsNoFilter;
                      },
                      itemBuilder: (context, suggestion) {
                        print(suggestion);
                        return ListTile(
                          title: Text(suggestion.type),
                          subtitle: Text(suggestion.instructionNo),
                        );
                      },
                      onSuggestionSelected: (suggestion) {
                        print(suggestion);
                        txtInsNo.text = suggestion.instructionNo;
                        _scanInsNo = suggestion.instructionNo;

                        asynclistGetData(txtInsNo.text);
                        FocusScope.of(context).requestFocus(nodeBarcode);
                      },
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "1. Scan Barcode No",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      focusNode: nodeBarcode,
                      enableInteractiveSelection: false,
                      textInputAction: TextInputAction.none,
                      controller: txtScanBarcode,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        labelText: '',
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              txtScanBarcode.clear();
                              _scanBarcode = '';
                              //listGetDataFiltered.clear();
                              FocusScope.of(context).requestFocus(nodeBarcode);
                            });
                          },
                          icon: Icon(_scanBarcode == ''
                              ? Icons.cancel
                              : Icons.cancel_outlined),
                          color: _scanBarcode == ''
                              ? Colors.white12.withOpacity(1)
                              : Colors.grey[600],
                        ),
                      ),
                      onChanged: (value) {
                        _scanBarcode = value;
                        setState(() {});
                      },
                      onEditingComplete: () {
                        _submit(txtInsNo.text, txtScanBarcode.text);

                        setState(() {
                          asynclistGetData(txtInsNo.text);
                        });
                      },
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                    height: 20,
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: txtSearch,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        labelText: 'Search',
                        prefixIcon: IconButton(
                          onPressed: () {},
                          icon: Icon(_searchDataResult == ''
                              ? Icons.search
                              : Icons.search),
                          //Icons.search,
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              txtSearch.clear();
                              _searchDataResult = '';
                              listGetDataFiltered = listGetData;
                            });
                          },
                          icon: Icon(_searchDataResult == ''
                              ? Icons.cancel
                              : Icons.cancel_outlined),
                          color: _searchDataResult == ''
                              ? Colors.white12.withOpacity(1)
                              : Colors.grey[600],
                        ),
                      ),
                      onChanged: (value) {
                        setState(() {
                          if (value != null) {
                            _searchDataResult = value;
                            listGetDataFiltered = listGetData
                                .where((rec) =>
                                    rec.itemcode.toUpperCase().contains(
                                        _searchDataResult.toUpperCase()) ||
                                    rec.itemname.toUpperCase().contains(
                                        _searchDataResult.toUpperCase()) ||
                                    rec.barcodeno.toUpperCase().contains(
                                        _searchDataResult.toUpperCase()) ||
                                    rec.lotno.toUpperCase().contains(
                                        _searchDataResult.toUpperCase()) ||
                                    rec.sublotno.toUpperCase().contains(
                                        _searchDataResult.toUpperCase()))
                                .toList();
                          } else {
                            listGetDataFiltered = listGetData;
                          }
                        });
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Expanded(child: _dataTableWidget()),
                ])));
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3.5,
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Item', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Barcode No', MediaQuery.of(context).size.width / 3.5),
      // _getTitleItemWidget('Sublot No', MediaQuery.of(context).size.width / 5),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 3.5),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].itemname),
      width: MediaQuery.of(context).size.width / 3,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.centerLeft,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(listGetDataFiltered[index].lotno),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(listGetDataFiltered[index].barcodeno),
          width: MediaQuery.of(context).size.width / 3.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        // Container(
        //   child: Text(listGetDataFiltered[index].sublotno),
        //   width: MediaQuery.of(context).size.width / 4.5,
        //   height: 45,
        //   padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
        //   alignment: Alignment.centerRight,
        // ),
        Container(
          child:
              Text(value.format(double.parse(listGetDataFiltered[index].qty))),
          width: MediaQuery.of(context).size.width / 4,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
