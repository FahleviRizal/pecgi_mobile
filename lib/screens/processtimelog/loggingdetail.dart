import 'package:flutter/material.dart';
import 'package:panasonic/models/clsmixingsublotgroupcarry.dart';
import 'package:panasonic/models/clsprocesstimelogging.dart';
import 'package:panasonic/services/mixingsublotgroupcarryservice.dart';
import 'package:panasonic/services/processtimeloggingservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:intl/intl.dart';
//import 'package:panasonic/utilities/message.dart';

class LoggingDetail extends StatefulWidget {
  const LoggingDetail({Key key, this.userid, this.warehouse, this.carryNo})
      : super(key: key);
  final String userid;
  final String warehouse;
  final String carryNo;

  @override
  _LoggingDetail createState() => _LoggingDetail();
}

class _LoggingDetail extends State<LoggingDetail> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  ProcessTimeLoggingService api = new ProcessTimeLoggingService();
  List<ClsProcessTimeLogging> listGetData = [];
  List<ClsProcessTimeLogging> listGetDataFiltered = [];

  final TextEditingController iCarryNo = new TextEditingController();
  final TextEditingController iCarryName = new TextEditingController();
  final TextEditingController iSearch = new TextEditingController();
  String warehouseLogin = '';
  String _searchCarry = '';
  String _searchDataResult = '';

  FocusNode nodeCarry = FocusNode();
  FocusNode nodeSearch = FocusNode();

  @override
  void initState() {
    asynclistGetData(widget.carryNo);
    super.initState();
  }

  void asynclistGetData(String carryno) async {
    listGetData = await api.getDataDetail(carryno);
    listGetDataFiltered = listGetData;
    iCarryNo.text = carryno;
    _getCarryName(carryno);
    setState(() {});
  }

  void _getCarryName(String code) async {
    List<ClsMixingSublotGroupCarry> list;
    MixingSublotGroupingCarryService apis =
        new MixingSublotGroupingCarryService();
    list = await apis.getCarryName(code);

    if (list.length > 0) {
      iCarryName.text = list[0].carryName;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text("Mixing Sublot Grouping"),
            ),
            body: Column(children: <Widget>[
              Flexible(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                          child: Form(
                            key: formkey,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "1. Scan Carry No",
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    // autofocus: true,
                                    // showCursor: true,
                                    enableInteractiveSelection: false,
                                    textInputAction: TextInputAction.none,
                                    controller: iCarryNo,
                                    focusNode: nodeCarry,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      labelText: '',
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            iCarryNo.clear();
                                            iCarryName.text = '';
                                            _searchCarry = '';
                                            listGetDataFiltered.clear();
                                            FocusScope.of(context)
                                                .requestFocus(nodeCarry);
                                          });
                                        },
                                        icon: Icon(_searchCarry == ''
                                            ? Icons.cancel
                                            : Icons.cancel_outlined),
                                        color: _searchCarry == ''
                                            ? Colors.white12.withOpacity(1)
                                            : Colors.grey[600],
                                      ),
                                    ),
                                    onChanged: (value) {
                                      _searchCarry = value;

                                      setState(() {});
                                    },
                                    onEditingComplete: () {
                                      _getCarryName(iCarryNo.text);
                                      asynclistGetData(iCarryNo.text);
                                    },
                                  ),
                                ),
                                SizedBox(height: 5.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Carry Name",
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: iCarryName,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      filled: true,
                                      fillColor: disabledColor,
                                    ),
                                  ),
                                ),
                                Divider(color: Colors.grey, height: 20),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                      controller: iSearch,
                                      decoration: InputDecoration(
                                        labelText: 'Search',
                                        prefixIcon: IconButton(
                                          onPressed: () {},
                                          icon: Icon(_searchDataResult == ''
                                              ? Icons.search
                                              : Icons.search),

                                          //Icons.search,
                                        ),
                                        suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              iSearch.clear();
                                              _searchDataResult = '';
                                              listGetDataFiltered = listGetData;
                                            });
                                          },
                                          icon: Icon(_searchDataResult == ''
                                              ? Icons.cancel
                                              : Icons.cancel_outlined),
                                          color: _searchDataResult == ''
                                              ? Colors.white12.withOpacity(1)
                                              : Colors.grey[600],
                                        ),
                                        border: OutlineInputBorder(),
                                        contentPadding:
                                            new EdgeInsets.symmetric(
                                                vertical: 10.0,
                                                horizontal: 10.0),
                                      ),
                                      onChanged: (value) {
                                        setState(() {
                                          _searchDataResult = value;
                                          listGetDataFiltered = listGetData
                                              .where((user) =>
                                                  user.barcode
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()) ||
                                                  user.lotNo
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()) ||
                                                  user.qty
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()))
                                              .toList();
                                        });
                                      }),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Expanded(
                                  child: Container(child: _dataTableWidget()),
                                ),
                                // SizedBox(
                                //     height: MediaQuery.of(context).size.height *
                                //         0.02),
                                // Column(
                                //     crossAxisAlignment:
                                //         CrossAxisAlignment.center,
                                //     mainAxisSize: MainAxisSize.max,
                                //     mainAxisAlignment: MainAxisAlignment.end,
                                //     // height: MediaQuery.of(context).size.height * 0.1,
                                //     // alignment: Alignment.center,
                                //     children: <Widget>[
                                //       // Padding(
                                //       //   padding: EdgeInsets.symmetric(vertical: 5),
                                //       //   child:
                                //       TextButton(
                                //         style: TextButton.styleFrom(
                                //           backgroundColor: Colors.blue[800],
                                //           minimumSize: Size(
                                //               MediaQuery.of(context)
                                //                       .size
                                //                       .width *
                                //                   0.90,
                                //               55),
                                //           shape: RoundedRectangleBorder(
                                //             borderRadius:
                                //                 BorderRadius.circular(50.0),
                                //           ),
                                //         ),
                                //         onPressed: () {
                                //           _submit(iCarryNo.text);
                                //         },
                                //         child: Text(
                                //           "SUBMIT",
                                //           style: TextStyle(
                                //             fontSize: 18,
                                //             color: Colors.white,
                                //           ),
                                //         ),
                                //       ),
                                //       // ),
                                //     ]),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                              ],
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ])));
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3, //100,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width, //470,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Type', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Sublot No', MediaQuery.of(context).size.width / 4), //, 100),
      _getTitleItemWidget(
          'LotNo', MediaQuery.of(context).size.width / 4), // 120),
      _getTitleItemWidget(
          'Qty', MediaQuery.of(context).size.width / 4), // 100),
      _getTitleItemWidget(
          'Unit', MediaQuery.of(context).size.width / 5), // 100),
      _getTitleItemWidget(' ', 20),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].materialName),
      width: MediaQuery.of(context).size.width / 3,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  // final value = new NumberFormat("#,##0.00000", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(listGetDataFiltered[index].sublot),
          width: MediaQuery.of(context).size.width / 4, //100,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(listGetDataFiltered[index].lotNo),
          width: MediaQuery.of(context).size.width / 4, //120,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(listGetDataFiltered[index].qty),
          width: MediaQuery.of(context).size.width / 5, //100,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(listGetDataFiltered[index].unit),
          width: MediaQuery.of(context).size.width / 5, //110,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
