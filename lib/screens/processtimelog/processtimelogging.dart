import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsprocesstimelogging.dart';
import 'package:panasonic/screens/processtimelog/loggingdetail.dart';
import 'package:panasonic/services/processtimeloggingservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
//import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Process Time Logging',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class ProcessTimeLogging extends StatefulWidget {
  const ProcessTimeLogging({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _ProcessTimeLogging createState() => _ProcessTimeLogging();
}

class _ProcessTimeLogging extends State<ProcessTimeLogging>
    with TickerProviderStateMixin {
  final ProcessTimeLoggingService api = ProcessTimeLoggingService();
  TextEditingController iLocCode = TextEditingController();
  TextEditingController iLocName = TextEditingController();
  TextEditingController iBarcode = TextEditingController();
  TextEditingController iStatusScan = TextEditingController();
  TextEditingController iType = TextEditingController();
  TextEditingController iTypeDesc = TextEditingController();
  TextEditingController iCarryNo = TextEditingController();

  List<ClsProcessTimeLogging> list;
  List<ClsProcessTimeLogging> searchdata = [];
  String _ddlFilterResult = '';
  String _scanBarcode = '';
  List<ClsProcessTimeLogging> ddlLoc = [];
  List<ClsProcessTimeLogging> ddlLocFilter = [];

  FocusNode nodeLoc = new FocusNode();
  FocusNode nodeBarcode = new FocusNode();
  FocusNode nodeCarry = new FocusNode();
  FocusNode nodeButton = new FocusNode();
  String _searchLoc = '';
  String _searchCarry = '';
  bool _isEnabled = false;
  bool _isEnabledBarcode = true;

  // List<ClsProcessTimeLogging> searchdata = [
  //   ClsProcessTimeLogging(
  //       scanIn: "12 Jan 2022 09:00",
  //       leadtimehour: "1",
  //       scanOut: "12 Jan 2022 10:00"),
  // ];
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();

    // SpinKitSquareCircle(
    //   //color: kPrimaryColor,
    //   size: 50.0,
    //   controller: AnimationController(
    //       vsync: this, duration: const Duration(milliseconds: 1200)),
    // );
  }

  void _detail(String carryNo) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => LoggingDetail(
          userid: widget.userid,
          warehouse: widget.warehouse,
          carryNo: carryNo,
        ),
      ),
    );
  }

  void asynclistGetData(String location, String barcode, String carry) async {
    barcode = (carry != "" && carry != "xxx") ? carry : barcode;
    list = await api.getData(location, barcode);
    searchdata = list;
    setState(() {});
  }

  List<ClsProcessTimeLogging> data = [];
  void asyncSetData(String location, String barcode) async {
    data = await api.autoFillData(location, barcode);

    if (data.length > 0) {
      iStatusScan.text = data[0].statusScan.toString();
      iType.text = data[0].type.toString();
      iTypeDesc.text = data[0].typeDesc.toString();
    } else {
      iStatusScan.text = "";
      iType.text = "";
      iTypeDesc.text = "";
    }
    setState(() {});
  }

  void asyncSubmitData(String loc, String carry, String barcode) async {
    setState(() {
      isProcessing = true;
    });

    if (loc == "") {
      Message.box(
          type: "2",
          message: "please select location",
          top: 0,
          position: "0",
          context: context);
      FocusScope.of(context).requestFocus(nodeLoc);
      return;
    }
    //set di SP
    //  else if (barcode == "" &&
    //     (loc != "052" && loc != "052B" && loc != "NT01")) {
    //   Message.box(
    //       type: "2",
    //       message: "please scan barcode no",
    //       top: 0,
    //       position: "0",
    //       context: context);
    //   FocusScope.of(context).requestFocus(nodeBarcode);
    //   return;
    // } else if ((iTypeDesc.text == "" || iType.text == "") &&
    //     (loc != "052" && loc != "052B" && loc != "NT01")) {
    //   Message.box(
    //       type: "2",
    //       message: "type cannot be empty",
    //       top: 0,
    //       position: "0",
    //       context: context);
    // }

    //set di SP
    //jika lokasi oven bubuk (051) atau naturral dry (NT01 ) sebelumnya 076
    // if (((loc == "052" && loc == "052B") || loc == "NT01") && carry == "") {
    //   Message.box(
    //       type: "2",
    //       message: "carry no cannot be empty",
    //       top: 0,
    //       position: "0",
    //       context: context);
    // }

    List<ClsProcessTimeLogging> listScanData;
    listScanData = await ProcessTimeLoggingService.submit(
        loc, carry, barcode, iType.text, widget.userid);
    if (listScanData.length > 0) {
      if (listScanData[0].id == "200") {
        Message.box(
            type: "1",
            message: listScanData[0].description,
            top: 0,
            position: "0",
            context: context);
        iLocName.text = '';
        iBarcode.clear();
        iCarryNo.clear();
        iStatusScan.text = '';
        iType.text = '';
        iTypeDesc.text = '';
        iLocCode.clear();
        _searchLoc = '';

        FocusScope.of(context).requestFocus(nodeLoc);
      } else if (listScanData[0].id == "400") {
        Message.box(
            type: "2",
            message: listScanData[0].description,
            top: 0,
            position: "0",
            context: context);
        iBarcode.clear();
        _scanBarcode = '';
        FocusScope.of(context).requestFocus(nodeBarcode);
      } else {
        Message.box(
            type: "2",
            message: listScanData[0].description,
            top: 0,
            position: "0",
            context: context);
        //iBarcode.clear();
        //_scanBarcode = '';
        FocusScope.of(context).requestFocus(nodeBarcode);
      }
    }
    asynclistGetData(loc, barcode, carry);
    setState(() {});
  }

  asyncCheckLocation(String location) async {
    String retVal;

    retVal = await api.checkLocation(location);

    return retVal;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: const Text("Process Time Logging"),
            ),
            body: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                // SpinKitSquareCircle(
                //   color: Colors.green,
                //   size: 50.0,
                //   controller: AnimationController(
                //       vsync: this, duration: Duration(milliseconds: 200)),
                // ),
                SizedBox(height: 10.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "1. Scan/Select Location Process",
                      style: TextStyle(
                          fontFamily: 'Arial', fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TypeAheadField(
                    hideSuggestionsOnKeyboardHide: false,
                    textFieldConfiguration: TextFieldConfiguration(
                        textInputAction: TextInputAction.done,
                        onSubmitted: (value) {
                          _searchLoc = value;
                          iLocCode.text = value;
                          searchdata
                              .where((result) =>
                                  result.locCode
                                      .toLowerCase()
                                      .contains(value.toLowerCase()) ||
                                  result.locName
                                      .toLowerCase()
                                      .contains(value.toLowerCase()))
                              .toList();
                          asynclistGetData(
                              iLocCode.text,
                              (iBarcode.text == "") ? "xxx" : iBarcode.text,
                              (iCarryNo.text == "") ? "xxx" : iCarryNo.text);
                          FocusScope.of(context).requestFocus(nodeBarcode);
                        },
                        focusNode: nodeLoc,
                        controller: iLocCode,
                        style: TextStyle(fontFamily: 'Arial'),
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                iLocCode.clear();
                                iLocCode.text = '';
                                iLocName.text = '';
                                _isEnabled = false;
                                searchdata.clear();
                                iCarryNo.clear();
                                _isEnabledBarcode = true;
                                _searchLoc = '';
                                iStatusScan.text = '';
                                iType.text = '';
                                iTypeDesc.text = '';
                                FocusScope.of(context).requestFocus(nodeLoc);
                              });
                            },
                            icon: Icon(_searchLoc == ''
                                ? Icons.cancel
                                : Icons.cancel_outlined),
                            color: _searchLoc == ''
                                ? Colors.white12.withOpacity(1)
                                : Colors.grey[600],
                          ),
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: -10.0, horizontal: 10.0),
                        )),
                    suggestionsCallback: (pattern) async {
                      ddlLoc = await ProcessTimeLoggingService.getLocation(
                          widget.userid);
                      _ddlFilterResult = pattern;
                      _searchLoc = pattern;
                      ddlLocFilter = ddlLoc
                          .where((result) =>
                              result.locCode
                                  .toLowerCase()
                                  .contains(_ddlFilterResult.toLowerCase()) ||
                              result.locName
                                  .toLowerCase()
                                  .contains(_ddlFilterResult))
                          .toList();
                      return ddlLocFilter;
                    },
                    itemBuilder: (context, suggestion) {
                      print(suggestion);
                      return ListTile(
                          title: Text(suggestion.locCode),
                          subtitle: Text(suggestion.locName));
                    },
                    onSuggestionSelected: (suggestion) async {
                      iLocCode.text = suggestion.locCode;
                      iLocName.text = suggestion.locName;
                      _searchLoc = suggestion.locCode;

                      String rval =
                          await asyncCheckLocation(suggestion.locCode);

                      if (rval == "1") {
                        setState(() {
                          _isEnabled = true;
                          _isEnabledBarcode = false;
                        });

                        // if ((suggestion.locCode == "052" ||
                        //         suggestion.locName
                        //             .toString()
                        //             .contains("Oven Bubuk 1")) ||
                        //     (suggestion.locCode == "052B" ||
                        //         suggestion.locName
                        //             .toString()
                        //             .contains("Oven Bubuk 2")) ||
                        //     (suggestion.locCode == "NT01" ||
                        //         suggestion.locName
                        //             .toString()
                        //             .contains("Natural Dry"))) {
                        //   setState(() {
                        //     _isEnabled = true;
                        //     _isEnabledBarcode = false;
                        //   });
                        FocusScope.of(context).requestFocus(nodeCarry);
                      } else {
                        setState(() {
                          _isEnabled = false;
                          _isEnabledBarcode = true;
                        });
                        FocusScope.of(context).requestFocus(nodeBarcode);
                      }

                      asynclistGetData(
                          iLocCode.text,
                          (iBarcode.text == "") ? "xxx" : iBarcode.text,
                          (iCarryNo.text == "") ? "xxx" : iCarryNo.text);
                    },
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Location Process Name",
                      style: TextStyle(
                          fontFamily: 'Arial', fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    controller: iLocName,
                    enabled: false,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      filled: true,
                      fillColor: disabledColor,
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Scan Carry No",
                      style: TextStyle(
                          fontFamily: 'Arial', fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    enableInteractiveSelection: false,
                    textInputAction: TextInputAction.none,
                    controller: iCarryNo,
                    focusNode: nodeCarry,
                    enabled: _isEnabled,
                    decoration: InputDecoration(
                      filled: (_isEnabled == false) ? true : false,
                      fillColor: (_isEnabled == false)
                          ? disabledColor
                          : Colors.white10,
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      labelText: '',
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            iCarryNo.clear();
                            _searchCarry = '';
                            searchdata.clear();

                            FocusScope.of(context).requestFocus(nodeCarry);
                          });
                        },
                        icon: Icon(_searchCarry == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _searchCarry == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                      ),
                    ),
                    onChanged: (value) {
                      _searchCarry = value;

                      setState(() {});
                    },
                    onEditingComplete: () async {
                      await asyncSetData(
                          (iLocCode.text == "") ? "xxx" : iLocCode.text,
                          (iCarryNo.text == "") ? "xxx" : iCarryNo.text);

                      await asynclistGetData(
                          iLocCode.text,
                          (iBarcode.text == "") ? "xxx" : iBarcode.text,
                          (iCarryNo.text == "") ? "xxx" : iCarryNo.text);
                      _detail(iCarryNo.text);
                    },
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Scan Barcode No",
                      style: TextStyle(
                          fontFamily: 'Arial', fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    focusNode: nodeBarcode,
                    enableInteractiveSelection: false,
                    textInputAction: TextInputAction.none,
                    controller: iBarcode,
                    enabled: _isEnabledBarcode,
                    decoration: InputDecoration(
                      filled: (_isEnabledBarcode == false) ? true : false,
                      fillColor: (_isEnabledBarcode == false)
                          ? disabledColor
                          : Colors.white10,
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      labelText: '',
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            iBarcode.clear();
                            _scanBarcode = '';
                            iStatusScan.text = '';
                            iType.text = '';
                            iTypeDesc.text = '';
                            searchdata.clear();
                            FocusScope.of(context).requestFocus(nodeBarcode);
                          });
                        },
                        icon: Icon(_scanBarcode == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _scanBarcode == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                      ),
                    ),
                    onChanged: (value) {
                      _scanBarcode = value;
                      setState(() {});
                    },
                    onEditingComplete: () {
                      asyncSetData(
                          (iLocCode.text == "") ? "xxx" : iLocCode.text,
                          (iBarcode.text == "") ? "xxx" : iBarcode.text);
                      asynclistGetData(
                        (iLocCode.text == "") ? "xxx" : iLocCode.text,
                        (iBarcode.text == "") ? "xxx" : iBarcode.text,
                        (iCarryNo.text == "") ? "xxx" : iCarryNo.text,
                      );
                    },
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Status Scan",
                      style: TextStyle(
                          fontFamily: 'Arial', fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Container(
                      height: 30,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 0.0, horizontal: 10.0),
                            child: Row(
                              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Text(
                                  iStatusScan.text,
                                  style: TextStyle(
                                      color: (iStatusScan.text == "SCAN IN")
                                          ? Colors.green
                                          : Colors.red,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.normal),
                                ),
                                Container(
                                    alignment: Alignment.centerRight,
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 2),
                                    child: Visibility(
                                        visible: (iStatusScan.text == "")
                                            ? false
                                            : true,
                                        child: Icon(
                                          (iStatusScan.text == "SCAN IN")
                                              ? Icons.login_rounded
                                              : Icons.logout_sharp,
                                          color: (iStatusScan.text == "SCAN IN")
                                              ? Colors.green
                                              : Colors.red,
                                        ))),
                              ],
                            )),
                      ),
                    )),
                SizedBox(height: 10.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Type",
                      style: TextStyle(
                          fontFamily: 'Arial', fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        iTypeDesc.text,
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.normal),
                      )),
                ),
                SizedBox(height: 10.0),
                Expanded(child: Container(child: _dataTableWidget())),
                Padding(
                    padding: EdgeInsets.symmetric(vertical: 0),
                    child: Container(
                      padding: EdgeInsets.only(bottom: 5),
                      width: MediaQuery.of(context).size.width * 0.90,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: FloatingActionButton.extended(
                        backgroundColor: (isProcessing)
                            ? disabledButtonColor
                            : enabledButtonColor,
                        // onPressed: () {
                        //   asyncSubmitData(
                        //       iLocCode.text, iCarryNo.text, this.iBarcode.text);
                        // },
                        onPressed: (isProcessing == false)
                            ? () async {
                                //if buttonenabled == true then pass a function otherwise pass "null"
                                asyncSubmitData(iLocCode.text, iCarryNo.text,
                                    this.iBarcode.text);
                                //plug delayed for wait while processing data
                                await Future.delayed(const Duration(seconds: 3),
                                    () {
                                  setState(() {
                                    isProcessing = false;
                                  });
                                });
                              }
                            : null,
                        elevation: 0,
                        label: Text(
                          "SUBMIT",
                          style: TextStyle(
                              fontSize: 18.0,
                              color: (isProcessing)
                                  ? disabledColorButtonText
                                  : enabledColorButtonText),
                        ),
                      ),
                    )),
                // Padding(
                //     padding: EdgeInsets.symmetric(horizontal: 20),
                //     child: Align(
                //         alignment: Alignment.bottomRight,
                //         child: ElevatedButton(
                //           child: Container(
                //             child: Container(
                //                 height: 40,
                //                 width: 60,
                //                 alignment: Alignment.center,
                //                 child: Text(' Submit ')),
                //           ),
                //           onPressed: () {
                //             // asyncSubmitData();
                //             print('submit button');
                //           },
                //         ))),
                // SizedBox(height: 10.0),
              ],
            )));
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget(' ', 1),
      _getTitleItemWidget(
          'Time Scan In', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Lead Time(Hour)', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Time Scan Out', MediaQuery.of(context).size.width / 3),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      width: 1,
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    // String dateIn = searchdata[index].scanIn;
    // final DateTime now1 = DateTime.parse(dateIn);
    // final DateFormat formatter1 = DateFormat('dd MMM yyyy HH:mm');
    // final String formatted1 = formatter1.format(now1);

    // String date = searchdata[index].scanOut;
    // final DateTime now2 = DateTime.parse(date);
    // final DateFormat formatter2 = DateFormat('dd MMM yyyy HH:mm');
    // final String formatted2 = formatter2.format(now2);

    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].scanIn,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 13, color: Colors.black)),
          width: MediaQuery.of(context).size.width / 2.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(searchdata[index].leadtimehour,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 13, color: Colors.black)),
          width: MediaQuery.of(context).size.width / 5,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(searchdata[index].scanOut,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 13, color: Colors.black)),
          width: MediaQuery.of(context).size.width / 2.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
          alignment: Alignment.center,
        ),
      ],
    );
  }
}

/*
child: Container(
                    padding: const EdgeInsets.only(top: 10),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: DataTable(
                            columnSpacing: 25.0,
                            decoration: BoxDecoration(
                                border: Border.all(
                              width: 1,
                              color: Colors.grey[300],
                            )),
                            headingRowColor: MaterialStateColor.resolveWith(
                                (states) => Colors.grey[200]),
                            sortColumnIndex: 0,
                            showCheckboxColumn: false,
                            columns: [
                              DataColumn(
                                label: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 0),
                                  child: Text(
                                    "Time Scan In",
                                    style: TextStyle(
                                        fontFamily: "Arial",
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Container(
                                    child: Text(
                                  "Lead Time (Hour)",
                                  style: TextStyle(
                                      fontFamily: "Arial",
                                      fontWeight: FontWeight.bold),
                                )),
                                numeric: false,
                              ),
                              DataColumn(
                                label: Container(
                                    child: Text(
                                  "Time Scan Out",
                                  style: TextStyle(
                                      fontFamily: "Arial",
                                      fontWeight: FontWeight.bold),
                                )),
                                numeric: false,
                              ),
                            ],
                            rows: searchdata
                                .map(
                                  (idata) => DataRow(cells: [
                                    DataCell(
                                      Container(
                                        child: Text(idata.scanIn),
                                      ),
                                    ),
                                    DataCell(
                                      Container(
                                        child: Text(idata.leadtimehour),
                                        alignment: Alignment.centerRight,
                                      ),
                                    ),
                                    DataCell(
                                      Container(
                                        child: Text(idata.scanOut),
                                      ),
                                    ),
                                  ]),
                                )
                                .toList()),
                      ),
                    ),
                  ),
*/