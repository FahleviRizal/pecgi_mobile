import 'package:flutter/material.dart';
import 'package:panasonic/models/clsprodresultpacking.dart';
import 'package:panasonic/models/clswarehouse.dart';
import 'package:panasonic/services/prodresultpackingservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/message.dart';

import '../home.dart';

class ProdResultPacking extends StatefulWidget {
  const ProdResultPacking({Key key, this.userid, this.warehouse, this.version})
      : super(key: key);
  final String userid;
  final String warehouse;
  final String version;
  @override
  _ProdResultPackingState createState() => _ProdResultPackingState();
}

class _ProdResultPackingState extends State<ProdResultPacking> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  ProdResultPackingService api = ProdResultPackingService();

  List<ClsProdResultPacking> listGetData = [];
  List<ClsProdResultPacking> listGetDataFiltered = [];
  List<ClsProdResultPacking> listData = [];
  List<ClsWarehouse> ddlWarehouse = [];
  String warehousename = '';

  final TextEditingController txtBarcode = new TextEditingController();
  final TextEditingController txtTotalScan = new TextEditingController();
  final TextEditingController txtRemainScan = new TextEditingController();

  final TextEditingController txtSearch = new TextEditingController();

  FocusNode nodeBarcode = FocusNode();
  FocusNode nodeButton = FocusNode();

  String _searchDataResult = '';
  String _scanBarcode = '';
  bool isProcessing = false;
  String message;
  String type;

  @override
  void initState() {
    super.initState();
  }

  void _getDescWarehouse(String _code) async {
    ddlWarehouse = await ClsWarehouse.getComboWarehouse(widget.userid);
    ddlWarehouse
        .where((result) =>
            result.warehouseCode.toLowerCase().contains(_code.toLowerCase()))
        .toList();
    final newval = ddlWarehouse
        .firstWhere((item) => item.warehouseCode == _code, orElse: () => null);
    warehousename = newval.warehouseName;
    setState(() {});
  }

  void asyncScanBarcode(String barcode) async {
    listData = (await api.scanDataBarcode(barcode, widget.warehouse));

    for (int i = 0; i < listGetData.length; i++) {
      if ((barcode == listGetData[i].barcodeNo) &&
          listGetData[i].isflag == "1") {
        //asyncAlert("barcode has been scanned", "4");
        Message.box(
            type: "2",
            message: "barcode has been scanned",
            top: 0,
            position: "0",
            context: context);
        //txtBarcode.clear();
        return;
      }
    }

    ClsProdResultPacking tempData;
    List<ClsProdResultPacking> listTemp = [];
    if (listData[0].id == "200") {
      // if want check new value
      for (int index = 0; index < listData.length; index++) {
        //listTemp.addAll(listData);
        tempData = ClsProdResultPacking(
            warehouselogin: widget.warehouse,
            barcodeNo: listData[index].barcodeNo,
            itemCode: listData[index].itemCode,
            itemName: listData[index].itemName,
            lotNo: listData[index].lotNo,
            sublotNo: listData[index].sublotNo,
            qty: listData[index].qty,
            unit: listData[index].unit,
            isflag: "0");

        listTemp.add(tempData);
        // listGetData = listTemp;
        // //listGetData.add(tempData);
        // listGetDataFiltered = listGetData;
        // listTemp.clear();
        // setState(() {});
        // //}

        // if (listGetData.length != listTemp.length) {
        //   setState(() {
        //     //listGetData = listTemp;
        //     listGetData.add(tempData);
        //     listGetDataFiltered = listGetData;
        //     listTemp.clear();
        //   });
        // }
      }

      type = "3";
      message = (listGetDataFiltered.isEmpty) ? "" : "R";
    } else if (listData[0].id == "400") {
      message = listData[0].description;
      type = '2';
    } else {
      message = listData[0].description;
      type = '2';
    }

    if ((message != "" && message != "R") && (type != "")) {
      Message.box(
          type: type,
          message: message,
          top: 0,
          position: "0",
          context: context);

      txtBarcode.clear();
    } else if (message == "R") {
      final newval = listGetDataFiltered.firstWhere(
          (item) => item.barcodeNo == txtBarcode.text,
          orElse: () => null);
      if (newval != null)
        setState(() {
          newval.isflag = "1";
          listGetDataFiltered = listGetData;
        });

      setState(() {});
    } else {
      listGetData = listTemp;
      listGetDataFiltered = listGetData;

      //if want check new value
      final newval = listGetDataFiltered.firstWhere(
          (item) => item.barcodeNo == txtBarcode.text,
          orElse: () => null);
      if (newval != null)
        setState(() {
          newval.isflag = "1";
          // newval.locationCarryCode = locationCarry.toString();
          listGetDataFiltered = listGetData;
        });

      setState(() {});
    }
  }

  void _recalculate() async {
    int scan = 0;
    int remain = 0;
    for (int index = 0; index < listGetDataFiltered.length; index++) {
      if (listGetDataFiltered[index].isflag == "1") {
        scan += 1;
        txtTotalScan.text = scan.toString();
        if (scan == listGetDataFiltered.length) {
          txtRemainScan.text = "0";
        }
      } else if (listGetDataFiltered[index].isflag == "0" ||
          listGetDataFiltered[index].isflag == null) {
        remain += 1;
        txtRemainScan.text = remain.toString();
      }
      setState(() {});
    }
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void asyncsubmit(String barcodeno) async {
    setState(() {
      isProcessing = true;
    });

    if (listData.length == 0) {
      // asyncAlert("Data is empty", "2");
      Message.box(
          type: "2",
          message: "Data is empty",
          top: 0,
          position: "0",
          context: context);

      return;
    }

    if ((txtRemainScan.text != "" && txtRemainScan.text != "0") &&
        (listData.length > 0)) {
      Message.box(
          type: "2",
          message: "Please complete scan to submit",
          top: 0,
          position: "0",
          context: context);

      return;
    }

    // if want check new value
    // for (int index = 0; index < listGetData.length; index++) {
    //   final newval = listGetDataFiltered.firstWhere(
    //       (item) =>
    //           item.barcode == listGetDataFiltered[index].barcode &&
    //           item.itemCode == listGetDataFiltered[index].itemCode &&
    //           item.lotNo == listGetDataFiltered[index].lotNo,
    //       orElse: () => null);
    //   if (newval != null)
    //     setState(() {
    //       // newval.locationCarryCode = locationCarry.toString();
    //     });
    // }

    try {
      if (listGetDataFiltered.length == 0 || listGetDataFiltered.isEmpty) {
        Message.box(
            type: "2",
            message: "Please scan barcode",
            top: 0,
            position: "0",
            context: context);

        //FocusScope.of(context).requestFocus(nodeLoc);
        return;
      }

      listData = await ProdResultPackingService.submitData(
          listGetDataFiltered, widget.userid);
      if (listData[0].id == "200") {
        //asyncAlert(listData[0].description, "3");
        Message.box(
            type: "1",
            message: listData[0].description,
            top: 0,
            position: "0",
            context: context);

        setState(() {
          txtBarcode.clear();
          listGetDataFiltered.clear();
          txtRemainScan.text = "";
          txtTotalScan.text = "";

          FocusScope.of(context).requestFocus(nodeButton);
        });
      } else {
        Message.box(
            type: "2",
            message: listData[0].description,
            top: 0,
            position: "0",
            context: context);
      }
    } catch (e) {
      asyncAlert(e.toString(), "2");
    }
  }

  @override
  Widget build(BuildContext context) {
    listGetData ??= <ClsProdResultPacking>[];

    var children2 = <Widget>[
      Flexible(
        flex: 1,
        child: Row(
          children: <Widget>[
            Flexible(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                  child: Form(
                    key: formkey,
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "1. Scan Barcode",
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.00),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: TextFormField(
                            focusNode: nodeBarcode,
                            enableInteractiveSelection: false,
                            textInputAction: TextInputAction.none,
                            controller: txtBarcode,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              contentPadding: new EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              labelText: '',
                              suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    txtBarcode.clear();
                                    _scanBarcode = '';
                                    //listGetDataFiltered.clear();
                                    FocusScope.of(context)
                                        .requestFocus(nodeBarcode);
                                  });
                                },
                                icon: Icon(_scanBarcode == ''
                                    ? Icons.cancel
                                    : Icons.cancel_outlined),
                                color: _scanBarcode == ''
                                    ? Colors.white12.withOpacity(1)
                                    : Colors.grey[600],
                              ),
                            ),
                            onChanged: (value) {
                              _scanBarcode = value;
                              setState(() {});
                            },
                            onFieldSubmitted: (value) async {
                              //_scanBarcode = value;
                              await asyncScanBarcode(txtBarcode.text);
                              await _recalculate();
                              txtBarcode.clear();
                              //FocusScope.of(context).requestFocus(nodeBarcode);

                              // print(value);
                            },
                            onEditingComplete: () {},
                          ),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width / 2,
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Total Scan : ' + ' ' + txtTotalScan.text,
                                    style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold),
                                  )),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 2,
                              padding: EdgeInsets.symmetric(horizontal: 0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Remaining : ' + ' ' + txtRemainScan.text,
                                  style: TextStyle(
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ],
                        ),

                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //   children: <Widget>[
                        //     Column(
                        //       children: <Widget>[
                        //         Padding(
                        //           padding: EdgeInsets.symmetric(horizontal: 0),
                        //           child: Container(
                        //               alignment: Alignment.centerLeft,
                        //               padding: EdgeInsets.only(
                        //                   right: MediaQuery.of(context)
                        //                           .size
                        //                           .height *
                        //                       0.10),
                        //               child: Align(
                        //                 alignment: Alignment.center,
                        //                 child: Text(
                        //                   "Total Scan",
                        //                   style: TextStyle(fontFamily: 'Arial'),
                        //                 ),
                        //               )),
                        //         ),
                        //         SizedBox(height: 5),
                        //         Container(
                        //           width: MediaQuery.of(context).size.width / 2,
                        //           padding: EdgeInsets.symmetric(horizontal: 10),
                        //           child: TextFormField(
                        //             controller: txtTotalScan,
                        //             keyboardType: TextInputType.number,
                        //             textAlign: TextAlign.right,
                        //             enabled: false,
                        //             decoration: InputDecoration(
                        //               border: OutlineInputBorder(),
                        //               contentPadding: new EdgeInsets.symmetric(
                        //                   vertical: 10.0, horizontal: 10.0),
                        //               filled: true,
                        //               fillColor: disabledColor,
                        //             ),
                        //           ),
                        //         ),
                        //       ],
                        //     ),
                        //     Column(
                        //       children: <Widget>[
                        //         Padding(
                        //           padding: EdgeInsets.symmetric(horizontal: 20),
                        //           child: Container(
                        //               padding: EdgeInsets.only(
                        //                   left: MediaQuery.of(context)
                        //                           .size
                        //                           .height *
                        //                       0.10),
                        //               child: Align(
                        //                 alignment: Alignment.center,
                        //                 child: Text(
                        //                   "Remaining",
                        //                   style: TextStyle(
                        //                       fontFamily: 'Arial',
                        //                       fontWeight: FontWeight.bold),
                        //                 ),
                        //               )),
                        //         ),
                        //         SizedBox(height: 5),
                        //         Container(
                        //           width: MediaQuery.of(context).size.width / 2,
                        //           padding: EdgeInsets.symmetric(horizontal: 10),
                        //           child: TextFormField(
                        //             controller: txtRemainScan,
                        //             keyboardType: TextInputType.number,
                        //             textAlign: TextAlign.right,
                        //             enabled: false,
                        //             decoration: InputDecoration(
                        //               border: OutlineInputBorder(),
                        //               contentPadding: new EdgeInsets.symmetric(
                        //                   vertical: 10.0, horizontal: 10.0),
                        //               filled: true,
                        //               fillColor: disabledColor,
                        //             ),
                        //             onEditingComplete: () {
                        //               FocusScope.of(context)
                        //                   .requestFocus(nodeButton);
                        //             },
                        //           ),
                        //         ),
                        //       ],
                        //     )
                        //   ],
                        // ),
                        Divider(color: Colors.grey, height: 20),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: TextFormField(
                              controller: txtSearch,
                              decoration: InputDecoration(
                                labelText: 'Search',
                                prefixIcon: IconButton(
                                  onPressed: () {},
                                  icon: Icon(_searchDataResult == ''
                                      ? Icons.search
                                      : Icons.search),

                                  //Icons.search,
                                ),
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      txtSearch.clear();
                                      _searchDataResult = '';
                                      listGetDataFiltered = listGetData;
                                    });
                                  },
                                  icon: Icon(_searchDataResult == ''
                                      ? Icons.cancel
                                      : Icons.cancel_outlined),
                                  color: _searchDataResult == ''
                                      ? Colors.white12.withOpacity(1)
                                      : Colors.grey[600],
                                ),
                                border: OutlineInputBorder(),
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                              ),
                              onChanged: (value) {
                                setState(() {
                                  _searchDataResult = value;
                                  listGetDataFiltered = listGetData
                                      .where((user) =>
                                          user.barcodeNo.toLowerCase().contains(
                                              _searchDataResult
                                                  .toLowerCase()) ||
                                          user.itemName.toLowerCase().contains(
                                              _searchDataResult
                                                  .toLowerCase()) ||
                                          user.lotNo.toLowerCase().contains(
                                              _searchDataResult
                                                  .toLowerCase()) ||
                                          user.qty.toLowerCase().contains(
                                              _searchDataResult.toLowerCase()))
                                      .toList();
                                });
                              }),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01),
                        Expanded(
                          // height: MediaQuery.of(context).size.height * 0.4,
                          child: Container(child: _dataTableWidget()),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.02),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              ElevatedButton(
                                onPressed: (isProcessing == false)
                                    ? () async {
                                        //if buttonenabled == true then pass a function otherwise pass "null"
                                        asyncsubmit(txtBarcode.text);
                                        //plug delayed for wait while processing data
                                        await Future.delayed(
                                            const Duration(seconds: 3), () {
                                          setState(() {
                                            isProcessing = false;
                                          });
                                        });
                                      }
                                    : null,
                                style: TextButton.styleFrom(
                                  backgroundColor: (isProcessing)
                                      ? disabledButtonColor
                                      : enabledButtonColor,
                                  minimumSize: Size(
                                      MediaQuery.of(context).size.width * 0.90,
                                      55),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50.0),
                                  ),
                                ),
                                child: Text(
                                  "SUBMIT",
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: (isProcessing)
                                          ? disabledColorButtonText
                                          : enabledColorButtonText),
                                ),
                              )
                            ]),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01)
                      ],
                    ),
                  ),
                )),
          ],
        ),
      ),
    ];
    var willPopScope = WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  if (listGetDataFiltered.length > 0) {
                    //Navigator.of(context).pop();
                    var dialog = CustomAlertDialog(
                        type: "1",
                        title: "",
                        message:
                            "Data not saved! Are you sure, do you want to clear data?",
                        onOkPressed: () async {
                          await _getDescWarehouse(widget.warehouse);
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => MyHomePage(
                                        userid: widget.userid,
                                        whcode: widget.warehouse,
                                        whname: warehousename,
                                        version: widget.version,
                                      )),
                              (route) => false);
                        },
                        okBtnText: 'Yes',
                        noBtnText: 'No');
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => dialog);
                  } else {
                    Navigator.of(context).pop();
                  }
                },
              ),
              title: Text("Production Result Packing "),
            ),
            body: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: children2)));
    var willPopScope2 = willPopScope;
    return willPopScope2;
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 1),
      _getTitleItemWidget('Barcode No', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('Item', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 4),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].barcodeNo),
      width: 0,
      //height: 45,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(listGetDataFiltered[index].barcodeNo),
          width: MediaQuery.of(context).size.width / 4,
          height: 50,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
          color: (listGetDataFiltered[index].isflag == "1"
              ? Colors.lightBlue
              : Colors.white),
        ),
        Container(
          child: Text(listGetDataFiltered[index].itemName),
          width: MediaQuery.of(context).size.width / 4,
          height: 50,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
          color: (listGetDataFiltered[index].isflag == "1"
              ? Colors.lightBlue
              : Colors.white),
        ),
        Container(
          child: Text(listGetDataFiltered[index].lotNo),
          width: MediaQuery.of(context).size.width / 3.5,
          height: 50,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
          color: (listGetDataFiltered[index].isflag == "1"
              ? Colors.lightBlue
              : Colors.white),
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(listGetDataFiltered[index].qty),
          ),
          width: MediaQuery.of(context).size.width / 5.5,
          height: 50,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
          color: (listGetDataFiltered[index].isflag == "1"
              ? Colors.lightBlue
              : Colors.white),
        ),
      ],
    );
  }
}
