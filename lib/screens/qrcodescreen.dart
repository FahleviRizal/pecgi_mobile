import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class QRCodeScreen extends StatefulWidget {
  QRCodeScreen({Key key}) : super(key: key);

  @override
  _QRCodeScreenState createState() => _QRCodeScreenState();
}

class _QRCodeScreenState extends State<QRCodeScreen> {
  FocusNode node = NoKeyboardEditableTextFocusNode();
  TextEditingController scanController = TextEditingController();

  @override
  void initState() {
    node.requestFocus();
    Future.delayed(const Duration(milliseconds: 300), () {
      SystemChannels.textInput.invokeMethod('TextInput.invisible');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: IgnorePointer(
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Column(
              children: [
                TextField(
                  controller: scanController,
                  focusNode: node,
                  autofocus: true,
                  decoration:
                      InputDecoration(hintText: "WAITING FOR QR CODE..."),
                ),
                SizedBox(height: 50),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    node.dispose();
    super.dispose();
  }
}

class NoKeyboardEditableTextFocusNode extends FocusNode {
  @override
  bool consumeKeyboardToken() {
    return false;
  }
}
