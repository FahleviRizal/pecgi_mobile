import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsreceipt.dart';
import 'package:panasonic/screens/receiptschedule/receiptschedulescan.dart';
import 'package:panasonic/services/receiptservices.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/screens/receiptschedule/receiptschedulelist.dart';
import 'package:intl/intl.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/message.dart';
//import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Receipt Schedule',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      //home: const ReceiptSchedule(title: 'Panasonic'),
      //initialRoute: '/receiptschedulelist',
      //onGenerateRoute: RouteNavigator.generateRoute,
    );
  }
}

class ReceiptSchedule extends StatefulWidget {
  const ReceiptSchedule({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _ReceiptSchedule createState() => _ReceiptSchedule();
}

class _ReceiptSchedule extends State<ReceiptSchedule> {
  final ReceiptService api = ReceiptService();
  List<ReceiptScheduleHeader> recHList;

  List<ReceiptScheduleHeader> searchdata = [];
  TextEditingController iBarcodeNo = TextEditingController();
  TextEditingController icontroller = TextEditingController();

  final iReqNo = TextEditingController();
  final iReqDate = TextEditingController();

  String _searchResult = '';
  String _searchCombo = '';
  String _scanBarcode = '';
  String _ddlFilterResult = '';
  FocusNode nodeReqno = FocusNode();
  FocusNode nodeBarcode = FocusNode();

  @override
  void initState() {
    super.initState();
    asynclistGetData("1");
    // showDialog(
    //     context: context,
    //     builder: (BuildContext context) {
    //       return Center(
    //         child: CircularProgressIndicator(),
    //       );
    //     });
    // wait();
    // Navigator.pop(context);
  }

  List<ReceiptScheduleHeader> ddlReqNo = [];
  List<ReceiptScheduleHeader> ddlReqNoFilter = [];

  void asynclistGetData(String reqNo) async {
    recHList = await api.getReceipt(reqNo, iReqDate.text);
    searchdata = recHList;
    setState(() {});
  }

  Future<bool> progressAction() async {
    await new Future.delayed(const Duration(seconds: 2));
    return true;
  }

  // Widget _circularProgressIndicator() {
  //   Future.delayed(const Duration(seconds: 2));
  //   return CircularProgressIndicator();
  // }

  List<ReceiptScheduleDetail> listScan;
  // void asyncGetScan(String barcodeNo) async {
  //   listScan = await api.getReceiptScan(barcodeNo);
  // }

  void clearText() {
    iReqDate.text = "";
    iBarcodeNo.text = "";
    icontroller.text = "";
    searchdata.clear();
  }

  void _detail(String _matCode, String _matName) {
    setState(() {
      ReceiptScheduleDetail model = new ReceiptScheduleDetail();
      model.reqNo = iReqNo.text;
      model.materialCode = _matCode;
      model.materialName = _matName;

      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ReceiptScheduleList(
              pReqNo: iReqNo.text,
              pMatCode: _matCode,
              pMatName: _matName,
              userid: widget.userid,
              warehouse: widget.warehouse),
          settings: RouteSettings(),
        ),
      );
    });
  }

  void _scanbarcode(String reqNo, String barcodeNo) async {
    //setState(() {
    //await asyncGetScan(barcodeNo);
    listScan = await api.getReceiptScan(reqNo, barcodeNo);

    List<ReceiptScheduleDetail> listcheck = [];

    listcheck = await api.checkValidate(reqNo, barcodeNo, '-');

    if (listcheck[0].id.toString() == "200") {
      ReceiptScheduleDetail model = new ReceiptScheduleDetail();

      if (listScan.length > 0) {
        model.reqNo = iReqNo.text;
        model.barcodeNo = barcodeNo;
        model.materialCode = listScan[0].materialCode;
        model.materialName = listScan[0].materialName;
        model.lotNo = listScan[0].lotNo;
        model.qty = listScan[0].qty;
        model.expiredDate = listScan[0].expiredDate;
        model.supplierName = listScan[0].supplierName;

        String callbackdata = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ReceiptScheduleScan(
              pReqNo: iReqNo.text,
              warehouse: widget.warehouse,
              pUser: widget.userid,
            ),
            settings: RouteSettings(
              arguments: model,
            ),
          ),
        );
        // .then((value) => setState(() {
        //       iBarcodeNo.text = '';
        //     }));

        if (callbackdata == "1") {
          asynclistGetData(iReqNo.text);
          iBarcodeNo.text = '';
          //asyncAlert("Submit data successfully", "3");
          Message.box(
              type: "1",
              message: "Submit data successfully",
              top: 45,
              position: "0",
              context: context);
        } else {
          iBarcodeNo.text = '';
        }

        // Navigator.of(context)
        //     .push(
        //       MaterialPageRoute(
        //         builder: (context) => ReceiptScheduleScan(
        //           warehouse: widget.warehouse,
        //         ),
        //         settings: RouteSettings(
        //           arguments: model,
        //         ),
        //       ),
        //     )
        //     .then((value) => setState(() {
        //           asynclistGetData(iReqNo.text);
        //           iBarcodeNo.text = '';
        //           asyncAlert("Submit data successfully", "3");
        //         }));
      } else {
        // var dialog = CustomAlertDialog(
        //   type: "4",
        //   title: "",
        //   message: "Barcode is not found",
        //   okBtnText: 'Close',
        // );
        // showDialog(context: context, builder: (BuildContext context) => dialog);
        Message.box(
            type: "2",
            message: "Barcode is not found",
            top: 45,
            position: "0",
            context: context);
        iBarcodeNo.text = "";
      }
    } else {
      Message.box(
          type: "2",
          message: listcheck[0].message.toString(),
          top: 45,
          position: "0",
          context: context);
      iBarcodeNo.text = "";

      //asyncAlert(listcheck[0].message.toString(), "4");
      //iBarcodeNo.text = "";
    }
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  // Future scan() async {
  //   try {
  //     String barcode = await FlutterBarcodeScanner BarcodeScanner.scan();
  //     setState(() => this.barcode = barcode);
  //   } on PlatformException catch (e) {
  //     if (e.code == BarcodeScanner.CameraAccessDenied) {
  //       setState(() {
  //         this.barcode = 'User tidak memberi akses ke kamera';
  //       });
  //     } else {
  //       setState(() => this.barcode = 'Unknown error: $e');
  //     }
  //   } on FormatException {
  //     setState(() => this.barcode = 'Tidak ada aktifitas scanning');
  //   } catch (e) {
  //     setState(() => this.barcode = 'Unknown error: $e');
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    recHList ??= <ReceiptScheduleHeader>[];

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: kAppBarColor,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text("Receipt Schedule"),
          ),
          body: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              SizedBox(height: 10.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "1. Request No",
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TypeAheadField(
                  hideSuggestionsOnKeyboardHide: false,
                  textFieldConfiguration: TextFieldConfiguration(
                      textInputAction: TextInputAction.done,
                      onSubmitted: (value) {
                        iReqNo.text = value;
                        _searchResult = value;
                        _searchCombo = value;

                        ddlReqNoFilter
                            .where((result) => result.requestNo
                                .toLowerCase()
                                .contains(value.toLowerCase()))
                            .toList();

                        final DateTime now =
                            DateTime.parse(ddlReqNoFilter[0].requestDate);
                        final DateFormat formatter = DateFormat('dd MMM yyyy');
                        final String formatted = formatter.format(now);
                        iReqDate.text = formatted;

                        asynclistGetData(value);
                      },
                      focusNode: nodeReqno,
                      controller: this.iReqNo,
                      style: TextStyle(fontFamily: 'Arial'),
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              iReqNo.clear();
                              clearText();
                              _searchCombo = '';
                            });
                          },
                          icon: Icon(_searchCombo == ''
                              ? Icons.cancel
                              : Icons.cancel_outlined),
                          color: _searchCombo == ''
                              ? Colors.white12.withOpacity(1)
                              : Colors.grey[600],
                        ),
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: -10.0, horizontal: 10.0),
                      )),
                  suggestionsCallback: (pattern) async {
                    ddlReqNo = await ReceiptService.getRequestNo();
                    _ddlFilterResult = pattern;
                    _searchCombo = pattern;
                    ddlReqNoFilter = ddlReqNo
                        .where((result) => result.requestNo
                            .toLowerCase()
                            .contains(_ddlFilterResult.toLowerCase()))
                        .toList();
                    return ddlReqNoFilter;
                  },
                  itemBuilder: (context, suggestion) {
                    print(suggestion);
                    return ListTile(
                      title: Text(suggestion.requestNo),
                      //subtitle: Text('${suggestion.requestNo}'),
                    );
                  },
                  onSuggestionSelected: (suggestion) {
                    final DateTime now = DateTime.parse(suggestion.requestDate);
                    final DateFormat formatter = DateFormat('dd MMM yyyy');
                    final String formatted = formatter.format(now);
                    iReqDate.text = formatted;

                    iReqNo.text = suggestion.requestNo;
                    asynclistGetData(iReqNo.text);

                    _searchCombo = suggestion.requestNo;

                    FocusScope.of(context).requestFocus(nodeBarcode);
                  },
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Request Date",
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextFormField(
                  controller: iReqDate,
                  enabled: false,
                  //style: TextStyle(color: disabledColor),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    filled: true,
                    fillColor: disabledColor,
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "2. Scan Label Barcode",
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextFormField(
                  focusNode: nodeBarcode,
                  enableInteractiveSelection: false,
                  textInputAction: TextInputAction.none,
                  controller: iBarcodeNo,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    labelText: '',
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          iBarcodeNo.clear();
                          _scanBarcode = '';
                          searchdata.clear();
                          FocusScope.of(context).requestFocus(nodeBarcode);
                        });
                      },
                      icon: Icon(_scanBarcode == ''
                          ? Icons.cancel
                          : Icons.cancel_outlined),
                      color: _scanBarcode == ''
                          ? Colors.white12.withOpacity(1)
                          : Colors.grey[600],
                    ),
                  ),
                  onChanged: (value) {
                    _scanBarcode = value;
                    setState(() {});
                  },
                  onEditingComplete: () {
                    _scanbarcode(iReqNo.text, iBarcodeNo.text);
                    // Navigator.of(context).push(MaterialPageRoute(
                    //     builder: (context) => ReceiptScheduleScan()));
                  },
                ),
              ),
              Divider(
                color: Colors.grey,
                height: 20,
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextFormField(
                  controller: icontroller,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    labelText: 'Search',
                    prefixIcon: IconButton(
                      onPressed: () {},
                      icon: Icon(
                          _searchResult == '' ? Icons.search : Icons.search),

                      //Icons.search,
                    ),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          icontroller.clear();
                          _searchResult = '';
                          searchdata = recHList;
                        });
                      },
                      icon: Icon(_searchResult == ''
                          ? Icons.cancel
                          : Icons.cancel_outlined),
                      color: _searchResult == ''
                          ? Colors.white12.withOpacity(1)
                          : Colors.grey[600],
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {
                      if (value != null) {
                        _searchResult = value;
                        searchdata = recHList
                            .where((rec) =>
                                rec.materialName
                                    .toUpperCase()
                                    .contains(_searchResult.toUpperCase()) ||
                                rec.qty
                                    .toUpperCase()
                                    .contains(_searchResult.toUpperCase()))
                            .toList();
                      } else {
                        searchdata = recHList;
                      }
                    });
                  },
                ),
              ),
              SizedBox(height: 5),
              Expanded(child: _dataTableWidget()),
            ],
          )),
    );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0, //(MediaQuery.of(context).size.width / 3),
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 1),
      _getTitleItemWidget('Material', MediaQuery.of(context).size.width * 0.4),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width * 0.25),
      _getTitleItemWidget('Status', MediaQuery.of(context).size.width * 0.2),
      _getTitleItemWidget('  ', MediaQuery.of(context).size.width * 0.15),
    ];
  }

  Widget _getTitleItemWidget(String label, double pwidth) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      height: 45,
      width: pwidth,
      // (label == "Material")
      //     ? MediaQuery.of(context).size.width / 3
      //     : MediaQuery.of(context).size.width / 3.5,
      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
      alignment: Alignment.center,
    );

    // return Container(
    //   decoration: BoxDecoration(
    //       color: datatableColor,
    //       border: Border(
    //           top: BorderSide(
    //         color: Colors.grey,
    //         width: 1,
    //       ))),
    //   child: Text(label,
    //       style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
    //   // width: width,
    //   height: 45,
    //   padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
    //   alignment: Alignment.center,
    // );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      // child: Text(searchdata[index].materialName),
      width: 1,
      // height: 45,
      // padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
      // alignment: Alignment.centerLeft,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Expanded(
      child: Row(
        children: <Widget>[
          Container(
            child: Text(searchdata[index].materialName),
            width: MediaQuery.of(context).size.width / 3,
            //height: 45,
            padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
            alignment: Alignment.centerLeft,
          ),
          Container(
            child: Text(value.format(double.parse(searchdata[index].qty))),
            width: MediaQuery.of(context).size.width / 4,
            //height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
            alignment: Alignment.centerRight,
          ),
          Container(
            width: MediaQuery.of(context).size.width / 3,
            child: Align(
              alignment: Alignment.center,
              child: Icon(
                  searchdata[index].status == "1"
                      ? Icons.check_circle
                      : Icons.cancel_sharp,
                  color: searchdata[index].status == "1"
                      ? Colors.green[400]
                      : Colors.red),
            ),
            //height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
            alignment: Alignment.center,
          ),
          Container(
            width: 20,
            //height: 45,
            child: IconButton(
              onPressed: () {
                _detail(searchdata[index].materialCode,
                    searchdata[index].materialName);
                print('onpress');
              },
              icon: Icon(
                Icons.arrow_forward_ios,
                size: 15,
              ),
              color: Colors.black,
              alignment: Alignment.centerRight,
            ),
          ),
        ],
      ),
    );
  }
}

  // Expanded(
  //               child: Container(
  //                 padding: const EdgeInsets.only(top: 10),
  //                 child: SingleChildScrollView(
  //                   //dataBody(List<ReceiptScheduleHeader> listData) {
  //                   //return SingleChildScrollView(
  //                   scrollDirection: Axis.vertical,
  //                   child: SingleChildScrollView(
  //                     scrollDirection: Axis.horizontal,
  //                     child: DataTable(
  //                       decoration: BoxDecoration(
  //                           border: Border.all(
  //                         width: 1,
  //                         color: Colors.grey[300],
  //                       )),
  //                       headingRowColor: MaterialStateColor.resolveWith(
  //                           (states) => Colors.grey[200]),
  //                       sortColumnIndex: 0,
  //                       showCheckboxColumn: false,
  //                       columns: [
  //                         DataColumn(
  //                             label: Container(
  //                               width: 80,
  //                               child: Text(
  //                                 "Material",
  //                                 style: TextStyle(
  //                                     fontFamily: "Arial",
  //                                     fontWeight: FontWeight.bold,
  //                                     color: Colors.black),
  //                               ),
  //                             ),
  //                             numeric: false,
  //                             tooltip: "Material Name"),
  //                         DataColumn(
  //                           label: Container(
  //                               width: 30,
  //                               child: Text(
  //                                 "Qty",
  //                                 style: TextStyle(
  //                                     fontFamily: "Arial",
  //                                     fontWeight: FontWeight.bold),
  //                               )),
  //                           numeric: false,
  //                           tooltip: "Qty",
  //                         ),
  //                         DataColumn(
  //                             label: Container(
  //                                 width: 40,
  //                                 child: Text(
  //                                   "Status",
  //                                   style: TextStyle(
  //                                       fontFamily: "Arial",
  //                                       fontWeight: FontWeight.bold),
  //                                 )),
  //                             numeric: false,
  //                             tooltip: "Status"),
  //                         DataColumn(
  //                             label: Container(width: 5, child: Text(""))),
  //                       ],
  //                       rows: searchdata
  //                           .map(
  //                             (idata) => DataRow(
  //                                 onSelectChanged: (b) {
  //                                   print(idata.materialName);
  //                                 },
  //                                 cells: [
  //                                   DataCell(
  //                                       Container(
  //                                         width: 80,
  //                                         child: Text(idata.materialName),
  //                                       ),
  //                                       onTap: () {}),
  //                                   //Container(width: 75, child: Text(idata.materialName)),
  //                                   // ),
  //                                   DataCell(
  //                                     Container(
  //                                       width: 38,
  //                                       child: Text(idata.qty),
  //                                       alignment: Alignment.centerRight,
  //                                     ),
  //                                   ),
  //                                   DataCell(
  //                                     Container(
  //                                       width: 35,
  //                                       alignment: Alignment.center,
  //                                       child: Align(
  //                                         alignment: Alignment.centerRight,
  //                                         child: Icon(
  //                                             idata.status == "1"
  //                                                 ? Icons.check_circle
  //                                                 : Icons.cancel_sharp,
  //                                             color: idata.status == "1"
  //                                                 ? Colors.green[400]
  //                                                 : Colors.red),
  //                                       ),
  //                                     ),
  //                                   ),
  //                                   DataCell(
  //                                     Container(
  //                                       width: 5,
  //                                       child: IconButton(
  //                                         onPressed: () {
  //                                           _detail(idata.materialCode,
  //                                               idata.materialName);
  //                                           print('onpress');
  //                                         },
  //                                         icon: Icon(
  //                                           Icons.arrow_forward_ios,
  //                                           size: 15,
  //                                         ),
  //                                         color: Colors.black,
  //                                       ),
  //                                     ),
  //                                     onTap: () {
  //                                       _detail(idata.materialCode,
  //                                           idata.materialName);
  //                                     },
  //                                     //IconButton(),
  //                                   ),
  //                                 ]),
  //                           )
  //                           .toList(),
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //             ),
