import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsreceipt.dart';
import 'package:panasonic/services/receiptservices.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Receipt Schedule List',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // initialRoute: '/receiptlist',
      // routes: {
      //   '/': (context) => ReceiptSchedule(),
      //   '/receiptlist': (context) => ReceiptScheduleList(),
      // },
      //home: const ReceiptScheduleList(title: 'Panasonic'),
    );
  }
}

class ReceiptScheduleList extends StatefulWidget {
  ReceiptScheduleList(
      {Key key,
      this.pReqNo,
      this.pMatCode,
      this.pMatName,
      this.userid,
      this.warehouse})
      : super(key: key);

  //final String title;
  final String pReqNo;
  final String pMatCode;
  final String pMatName;
  final String userid;
  final String warehouse;

  @override
  _ReceiptScheduleList createState() => _ReceiptScheduleList();
}

class _ReceiptScheduleList extends State<ReceiptScheduleList> {
  final ReceiptService api = ReceiptService();
  List<ReceiptScheduleDetail> recDList;

  List<ReceiptScheduleDetail> searchdata = [];
  TextEditingController icontroller = TextEditingController();
  final iReqNo = TextEditingController();
  final iMatCode = TextEditingController();
  final iMatName = TextEditingController();

  String _searchResult = '';
  @override
  void initState() {
    super.initState();
    iReqNo.text = widget.pReqNo;
    iMatCode.text = widget.pMatCode;
    iMatName.text = widget.pMatName;
    print(widget.warehouse);

    asynclistGetData(widget.pReqNo, widget.pMatCode);
  }

  void asynclistGetData(String _reqNo, String _matCode) async {
    recDList = await api.getReceiptDetail(_reqNo, _matCode);
    searchdata = recDList;
  }

  @override
  Widget build(BuildContext context) {
    // final pModels = ModalRoute.of(context).settings.arguments;
    // ReceiptScheduleDetail model = pModels;
    // widget.pReqNo = model.reqNo;
    // widget.pMatCode = model.materialCode;
    // widget.pMatName = model.materialName;
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text("Receipt List"),
        ),
        body: FutureBuilder(
          future: api.getReceiptDetail(widget.pReqNo, widget.pMatCode),
          builder: (context, snapshot) {
            final items = snapshot.data;
            List<ReceiptScheduleDetail> rdlist = items;
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }

            if (snapshot.hasData) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: <Widget>[
                  SizedBox(height: 10.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Request No",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      enableInteractiveSelection: false,
                      controller: TextEditingController(text: widget.pReqNo),
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                        // labelText: 'Request No',
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Material Code",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      enabled: false,
                      controller: TextEditingController(text: widget.pMatCode),
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Material Name",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      enabled: false,
                      controller: TextEditingController(text: widget.pMatName),
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                    height: 20,
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: icontroller,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        hintText: "",
                        labelText: 'Search',
                        prefixIcon: IconButton(
                          onPressed: () {},
                          icon: Icon(_searchResult == ''
                              ? Icons.search
                              : Icons.search),

                          //Icons.search,
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              icontroller.clear();
                              _searchResult = '';
                              searchdata = rdlist;
                            });
                          },
                          icon: Icon(_searchResult == ''
                              ? Icons.cancel
                              : Icons.cancel_outlined),
                          color: _searchResult == ''
                              ? Colors.white12.withOpacity(1)
                              : Colors.grey[600],
                        ),
                      ),
                      //focusNode: _focus,
                      onChanged: (value) {
                        setState(() {
                          if (value != null) {
                            _searchResult = value;
                            searchdata = rdlist
                                .where((idata) =>
                                    idata.barcodeNo.toUpperCase().contains(
                                        _searchResult.toUpperCase()) ||
                                    idata.lotNo
                                        .toUpperCase()
                                        .contains(_searchResult.toUpperCase()))
                                // idata.qty
                                //     .toUpperCase()
                                //     .contains(_searchResult.toUpperCase()))
                                .toList();
                          } else {
                            searchdata = rdlist;
                          }
                        });
                      },
                    ),
                  ),
                  SizedBox(height: 5),
                  Expanded(child: _dataTableWidget()),
                  // dataBody(
                  //(searchdata.length == 0) ? rdlist : searchdata)),
                  //),
                ],
              );
            }

            return const Center();
          },
        ),
      ),
    );
  }

  // SingleChildScrollView dataBody(List<ReceiptScheduleDetail> listData) {
  //   return SingleChildScrollView(
  //     scrollDirection: Axis.vertical,
  //     child: SingleChildScrollView(
  //       scrollDirection: Axis.horizontal,
  //       child: DataTable(
  //         decoration: BoxDecoration(
  //             border: Border.all(
  //           width: 1,
  //           color: Colors.grey[300],
  //         )),
  //         headingRowColor:
  //             MaterialStateColor.resolveWith((states) => Colors.grey[200]),
  //         sortColumnIndex: 0,
  //         showCheckboxColumn: false,
  //         columns: [
  //           DataColumn(
  //               label: Container(
  //                 width: 120,
  //                 child: Text(
  //                   "Barcode No",
  //                   style: TextStyle(
  //                       fontFamily: "Arial",
  //                       fontWeight: FontWeight.bold,
  //                       color: Colors.black),
  //                 ),
  //               ),
  //               numeric: false,
  //               tooltip: "Barcode No"),
  //           DataColumn(
  //             label: Container(
  //                 width: 50,
  //                 child: Text(
  //                   "Lot No",
  //                   style: TextStyle(
  //                       fontFamily: "Arial", fontWeight: FontWeight.bold),
  //                 )),
  //             numeric: false,
  //             tooltip: "Lot No",
  //           ),
  //           DataColumn(
  //             label: Container(
  //                 width: 40,
  //                 child: Text(
  //                   "Qty",
  //                   style: TextStyle(
  //                       fontFamily: "Arial", fontWeight: FontWeight.bold),
  //                 )),
  //             numeric: false,
  //             tooltip: "Qty",
  //           ),
  //         ],
  //         rows: listData
  //             .map(
  //               (idata) => DataRow(cells: [
  //                 DataCell(
  //                   Container(width: 75, child: Text(idata.barcodeNo)),
  //                 ),
  //                 DataCell(
  //                   Container(
  //                     width: 40,
  //                     child: Text(idata.lotNo),
  //                   ),
  //                 ),
  //                 DataCell(
  //                   Container(
  //                     width: 40,
  //                     child: Text(idata.qty),
  //                   ),
  //                 ),
  //               ]),
  //             )
  //             .toList(),
  //       ),
  //     ),
  //   );
  //}

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 1, Alignment.center),
      _getTitleItemWidget('Barcode No',
          MediaQuery.of(context).size.width * 0.45, Alignment.center),
      _getTitleItemWidget(
          'Lot No', MediaQuery.of(context).size.width * 0.3, Alignment.center),
      _getTitleItemWidget(
          'Qty', MediaQuery.of(context).size.width * 0.25, Alignment.center),
    ];
  }

  Widget _getTitleItemWidget(String label, double width, Alignment align) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      alignment: align,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      //child: Text(searchdata[index].barcodeNo),
      width: 1,
      //height: 45,
      //padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      //alignment: Alignment.centerLeft,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].barcodeNo),
          width: MediaQuery.of(context).size.width / 2.5,
          //height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(searchdata[index].lotNo),
          width: MediaQuery.of(context).size.width / 3,
          //height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(value.format(double.parse(searchdata[index].qty))),
          ),
          width: MediaQuery.of(context).size.width / 4,
          // height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
