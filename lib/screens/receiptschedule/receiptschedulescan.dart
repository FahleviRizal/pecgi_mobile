import 'package:flutter/material.dart';
import 'package:panasonic/models/clsreceipt.dart';
import 'package:panasonic/services/receiptservices.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
//import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Receipt Schedule',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class ReceiptScheduleScan extends StatefulWidget {
  ReceiptScheduleScan(
      {Key key,
      this.pReqNo,
      // this.pBarcodeNo,
      // this.pMatCode,
      // this.pMatName,
      // this.pLotNo,
      // this.pQty,
      // this.pExpiredDate,
      // this.pSuppName,
      this.pUser,
      this.warehouse})
      : super(key: key);

  final String pReqNo;
  // final String pBarcodeNo;
  // final String pMatCode;
  // final String pMatName;
  // final String pLotNo;
  // final String pQty;
  // final String pExpiredDate;
  // final String pSuppName;
  final String pUser;
  final String warehouse;

  @override
  _ReceiptScheduleScan createState() => _ReceiptScheduleScan();
}

class _ReceiptScheduleScan extends State<ReceiptScheduleScan> {
  final ReceiptService api = ReceiptService();
  List<ReceiptScheduleHeader> recHList;

  TextEditingController iBarcode = TextEditingController();
  TextEditingController iMatCode = TextEditingController();
  TextEditingController iMatName = TextEditingController();
  TextEditingController iLotNo = TextEditingController();
  TextEditingController iExpDate = TextEditingController();
  TextEditingController iSupplier = TextEditingController();
  TextEditingController iQty = TextEditingController();
  //TextEditingController param = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  /*
  void asyncSubmitData2() async {
    try {
      if (iQty.text == "0" || iQty.text == "") {
        String message = "Qty must greater than zero";
        var dialog = CustomAlertDialog(
          type: "2",
          title: "",
          message: message,
          okBtnText: 'Close',
        );
        showDialog(context: context, builder: (BuildContext context) => dialog);

        return;
      }

      String message;
      message = await api.submit(
          widget.pReqNo,
          widget.pBarcodeNo,
          widget.pMatCode,
          widget.pLotNo,
          iQty.text,
          widget.pExpiredDate,
          widget.pSuppName,
          widget.pUser);
      var dialog = CustomAlertDialog(
        type: (message == "success") ? "3" : "2",
        title: "",
        message: (message == "success") ? "Data saved successfully!" : message,
        okBtnText: 'Close',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    } catch (e) {
      print('error caught: $e');
    }
  }*/

  List<ReceiptScheduleDetail> listData;
  void asyncSubmitData(String pBarcodeNo, String pMatCode, String pLotNo,
      String pExpDate, String pSuppName, String qty) async {
    try {
      List<ReceiptScheduleDetail> listcheck = [];
      listcheck = await api.checkValidate(widget.pReqNo, pBarcodeNo, pMatCode);

      if (listcheck[0].id.toString() == "200") {
        if (qty == "0" || qty == "") {
          //asyncAlert("Qty must greater than zero", "2");
          Message.box(
              type: "2",
              message: "Qty must greater than zero",
              top: 45,
              position: "0",
              context: context);
          return;
        }
        listData = await ReceiptService.submitScan(
            widget.pReqNo,
            pBarcodeNo,
            pMatCode,
            pLotNo,
            qty,
            pExpDate,
            pSuppName,
            widget.pUser,
            widget.warehouse);

        if (listData[0].id == "200") {
          // ignore: await_only_futures
          await Navigator.pop(context, "1");
          iQty.text = '';
        } else {
          //asyncAlert(listData[0].message.toString(), "2");
          Message.box(
              type: "2",
              message: listData[0].message.toString(),
              top: 45,
              position: "0",
              context: context);
        }
      } else {
        //asyncAlert(listData[0].message.toString(), "2");
        Message.box(
            type: "2",
            message: listData[0].message.toString(),
            top: 45,
            position: "0",
            context: context);
      }
    } catch (e) {
      // Message.box(
      //     type: "2",
      //     message: e.toString(),
      //     top: 45,
      //     position: "0",
      //     context: context);
      asyncAlert(e.toString(), "2");
    }

    setState(() {});
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  @override
  Widget build(BuildContext context) {
    final pModels = ModalRoute.of(context).settings.arguments;
    ReceiptScheduleDetail model = pModels;
    iBarcode.text = model.barcodeNo;
    iMatCode.text = model.materialCode;
    iMatName.text = model.materialName;
    iLotNo.text = model.lotNo;
    iQty.text = model.qty;

    // final DateTime now = DateTime.parse(model.expiredDate.toString());
    // final DateFormat formatter = DateFormat('dd MMM yyyy');
    // final String formatted = formatter.format(now);

    iExpDate.text = model.expiredDate;

    //widget.pExpiredDate = model.expiredDate;
    iSupplier.text = model.supplierName;
    //widget.pUser = model.userid;

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, "0"),
          ),
          title: const Text("Receipt Schedule Detail"),
        ),
        body: FutureBuilder(
            future: api.getReceiptDetail('1', '1'),
            builder: (context, snapshot) {
              //final items = snapshot.data;
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              }

              if (snapshot.hasData) {
                return //SingleChildScrollView(
                    //physics: ClampingScrollPhysics(),
                    //child: IntrinsicHeight(
                    Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    SizedBox(height: 10.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "1. Scan Barcode No",
                          style: TextStyle(
                              fontFamily: 'Arial', fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        enableInteractiveSelection: false,
                        controller: iBarcode,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Material Code",
                          style: TextStyle(
                              fontFamily: 'Arial', fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        enableInteractiveSelection: false,
                        controller: iMatCode,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Material Name",
                          style: TextStyle(
                              fontFamily: 'Arial', fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        enableInteractiveSelection: false,
                        controller: iMatName,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Lot No",
                          style: TextStyle(
                              fontFamily: 'Arial', fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        enableInteractiveSelection: false,
                        controller: iLotNo,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Qty",
                          style: TextStyle(
                              fontFamily: 'Arial', fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        scrollPadding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.right,
                        maxLength: 11,
                        enableInteractiveSelection: false,
                        controller: iQty,
                        validator: (value) {
                          int qty = int.tryParse(value);
                          if (qty == null || qty <= 0) {
                            //return 'Qty must greater than zero';
                            print('Qty must greater than zero');
                          }
                          print('Qty must greater than zero');
                          return qty.toString();
                        },
                        enabled: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Expired Date",
                          style: TextStyle(
                              fontFamily: 'Arial', fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        enableInteractiveSelection: false,
                        controller: iExpDate,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Supplier Name",
                          style: TextStyle(
                              fontFamily: 'Arial', fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: TextFormField(
                        enableInteractiveSelection: false,
                        controller: iSupplier,
                        enabled: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          filled: true,
                          fillColor: disabledColor,
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Expanded(
                      child: Container(),
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(vertical: 0),
                        child: Container(
                          padding: EdgeInsets.only(bottom: 10),
                          width: MediaQuery.of(context).size.width * 0.90,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: FloatingActionButton.extended(
                            backgroundColor: Colors.blue[800],
                            onPressed: () {
                              asyncSubmitData(
                                  iBarcode.text,
                                  iMatCode.text,
                                  iLotNo.text,
                                  iExpDate.text,
                                  iSupplier.text,
                                  iQty.text);
                            },
                            elevation: 0,
                            label: Text(
                              "SUBMIT",
                              style: TextStyle(fontSize: 18.0),
                            ),
                          ),
                        )),
                  ],
                );
              }
              return const Center();
            }),
      ),
    );
  }
}
