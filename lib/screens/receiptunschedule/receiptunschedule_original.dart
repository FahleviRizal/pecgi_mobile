import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:panasonic/models/clsreceipt.dart';
import 'package:panasonic/services/receiptservices.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Receipt Schedule',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class ReceiptUnschedule extends StatefulWidget {
  const ReceiptUnschedule({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;
  @override
  _ReceiptUnschedule createState() => _ReceiptUnschedule();
}

class _ReceiptUnschedule extends State<ReceiptUnschedule> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final ReceiptService api = ReceiptService();
  TextEditingController iBarcodeNo = TextEditingController();
  TextEditingController iMatCode = TextEditingController();
  TextEditingController iMatName = TextEditingController();
  TextEditingController iLotNo = TextEditingController();
  TextEditingController iQty = TextEditingController();
  TextEditingController iExpDate = TextEditingController();
  TextEditingController iSuppName = TextEditingController();
  FocusNode nodeBarcode = FocusNode();
  FocusNode nodeQty = FocusNode();
  FocusNode nodeButton = FocusNode();

  String _searchResult = '';

  @override
  void initState() {
    super.initState();
    //hide keyboard
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  // void asyncSubmitData() async {
  //   try {
  //     if (iQty.text == "0" || iQty.text == "") {
  //       String message = "Qty must greater than zero";
  //       var dialog = CustomAlertDialog(
  //         type: "2",
  //         title: "",
  //         message: message,
  //         okBtnText: 'Close',
  //       );
  //       showDialog(context: context, builder: (BuildContext context) => dialog);

  //       return;
  //     }

  //     String message;
  //     message = await api.submitUnschedule(iBarcodeNo.text, iMatCode.text,
  //         iLotNo.text, iQty.text, iExpDate.text, iSuppName.text, widget.userid);
  //     var dialog = CustomAlertDialog(
  //       type: (message == "success") ? "3" : "2",
  //       title: "",
  //       message: (message == "success") ? "Data saved successfully!" : message,
  //       okBtnText: 'Close',
  //     );
  //     showDialog(context: context, builder: (BuildContext context) => dialog);
  //   } catch (e) {
  //     print('error caught: $e');
  //   }
  // }

  List<ReceiptScheduleDetail> listData;
  void asyncSubmitData(String barcode, String qty) async {
    try {
      if (barcode == "") {
        Message.box(
            type: "2",
            message: "Please scan barcode",
            top: 0,
            position: "0",
            context: context);
        //asyncAlert("Please scan barcode", "2");

        return;
      }
      List<ReceiptScheduleDetail> listcheck = [];
      listcheck = await api.checkValidateUnschedule(
          iBarcodeNo.text, iMatCode.text, "0");

      if (listcheck[0].id.toString() == "200") {
        if (qty == "0" || qty == "") {
          Message.box(
              type: "2",
              message: "Qty must greater than zero",
              top: 0,
              position: "0",
              context: context);
          // asyncAlert("Qty must greater than zero", "2");
          return;
        }
        listData = await ReceiptService.submitUnschedule(
            barcode,
            iMatCode.text,
            iLotNo.text,
            qty,
            iExpDate.text,
            iSuppName.text,
            widget.userid,
            widget.warehouse,
            '0');

        if (listData[0].id == "200") {
          Message.box(
              type: "1",
              message: "Submit data successfully",
              top: 0,
              position: "0",
              context: context);
          //asyncAlert("Submit data successfully", "3");
          iBarcodeNo.text = '';
          clearData();
          FocusScope.of(context).requestFocus(nodeBarcode);
        } else {
          Message.box(
              type: "2",
              message: listcheck[0].message.toString(),
              top: 0,
              position: "0",
              context: context);
          //asyncAlert(listcheck[0].message.toString(), "4");
        }
      } else {
        Message.box(
            type: "2",
            message: listcheck[0].message.toString(),
            top: 0,
            position: "0",
            context: context);
        //asyncAlert(listcheck[0].message.toString(), "2");
      }
    } catch (e) {
      asyncAlert(e.toString(), "2");
    }

    setState(() {});
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void clearData() {
    //iBarcodeNo.text = "";
    iMatCode.text = "";
    iMatName.text = "";
    iLotNo.text = "";
    iQty.text = "0";
    iExpDate.text = "";
    iSuppName.text = "";
  }

  List<ReceiptScheduleDetail> listScan;
  void _scanbarcode(String barcodeNo) async {
    List<ReceiptScheduleDetail> listcheck = [];

    listcheck = await api.checkValidateUnschedule(barcodeNo, '-', '0');
    listScan = await api.getReceiptUnschedule(barcodeNo);

    if (listcheck[0].id.toString() == "200") {
      if (listScan.length > 0) {
        iMatCode.text = listScan[0].materialCode.toString();
        iMatName.text = listScan[0].materialName.toString();
        iLotNo.text = listScan[0].lotNo.toString();

        final DateTime now = DateTime.parse(listScan[0].expiredDate.toString());
        final DateFormat formatter = DateFormat('dd MMM yyyy');
        final String formatted = formatter.format(now);
        iExpDate.text = formatted;
        iQty.text = listScan[0].qty;
        //iExpDate.text = listScan[0].expiredDate;
        iSuppName.text = listScan[0].supplierName.toString();
        FocusScope.of(context).requestFocus(nodeButton);
      } else {
        Message.box(
            type: "2",
            message: "Data not found",
            top: 0,
            position: "0",
            context: context);
        //asyncAlert("Data not found", "4");
        clearData();
        FocusScope.of(context).requestFocus(nodeBarcode);
      }
    } else {
      Message.box(
          type: "2",
          message: listcheck[0].message.toString(),
          top: 0,
          position: "0",
          context: context);
      //asyncAlert(listcheck[0].message.toString(), "4");
      clearData();
      FocusScope.of(context).requestFocus(nodeBarcode);
      iBarcodeNo.clear();
    }
  }

  @override
  Widget build(BuildContext context) => GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => FocusScope.of(context).unfocus(),
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: kAppBarColor,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text("Receipt Unschedule"),
          ),
          body: FutureBuilder(
              future: api.getReceiptUnschedule('1'),
              builder: (context, snapshot) {
                //final items = snapshot.data;
                if (!snapshot.hasData) {
                  return Center(child: CircularProgressIndicator());
                }

                if (snapshot.hasData) {
                  return Container(
                      key: _formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        verticalDirection: VerticalDirection.down,
                        children: <Widget>[
                          SizedBox(height: 10.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "1. Scan Barcode No",
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: TextFormField(
                              autofocus: true,
                              showCursor: true,
                              focusNode: nodeBarcode,
                              enableInteractiveSelection: false,
                              textInputAction: TextInputAction.none,
                              controller: iBarcodeNo,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                labelText: '',
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      iBarcodeNo.clear();
                                      _searchResult = '';
                                      clearData();
                                      FocusScope.of(context)
                                          .requestFocus(nodeBarcode);
                                    });
                                  },
                                  icon: Icon(_searchResult == ''
                                      ? Icons.cancel
                                      : Icons.cancel_outlined),
                                  color: _searchResult == ''
                                      ? Colors.white12.withOpacity(1)
                                      : Colors.grey[600],
                                ),
                              ),
                              onChanged: (value) {
                                _searchResult = value;
                                setState(() {});
                              },
                              onEditingComplete: () {
                                _scanbarcode(iBarcodeNo.text);
                              },
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Material Code",
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: TextFormField(
                              enableInteractiveSelection: false,
                              controller: iMatCode,
                              enabled: false,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                filled: true,
                                fillColor: disabledColor,
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Material Name",
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: TextFormField(
                              enableInteractiveSelection: false,
                              controller: iMatName,
                              enabled: false,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                filled: true,
                                fillColor: disabledColor,
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Lot No",
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: TextFormField(
                              enableInteractiveSelection: false,
                              controller: iLotNo,
                              enabled: false,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                filled: true,
                                fillColor: disabledColor,
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Qty",
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: TextFormField(
                              focusNode: nodeQty,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.right,
                              maxLength: 11,
                              enableInteractiveSelection: false,
                              controller: iQty,
                              validator: (value) {
                                int qty = int.tryParse(value);
                                if (qty == null || qty <= 0) {
                                  //return 'Qty must greater than zero';
                                  print('Qty must greater than zero');
                                }
                                print('Qty must greater than zero');
                                return qty.toString();
                              },
                              enabled: true,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                filled: true,
                                fillColor: Colors.white,
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Expired Date",
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: TextFormField(
                              enableInteractiveSelection: false,
                              controller: iExpDate,
                              enabled: false,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                filled: true,
                                fillColor: disabledColor,
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Supplier Name",
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: TextFormField(
                              enableInteractiveSelection: false,
                              controller: iSuppName,
                              enabled: false,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                filled: true,
                                fillColor: disabledColor,
                              ),
                            ),
                          ),
                          SizedBox(height: 20.0),
                          Expanded(
                            child: Container(),
                          ),
                          Padding(
                              padding: EdgeInsets.symmetric(vertical: 0),
                              child: Container(
                                padding: EdgeInsets.only(bottom: 10),
                                width: MediaQuery.of(context).size.width * 0.90,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: FloatingActionButton.extended(
                                  backgroundColor: Colors.blue[800],
                                  onPressed: () {
                                    asyncSubmitData(iBarcodeNo.text, iQty.text);
                                  },
                                  elevation: 0,
                                  label: Text(
                                    "SUBMIT",
                                    style: TextStyle(fontSize: 18.0),
                                  ),
                                ),
                              )),
                          // Padding(
                          //     padding: EdgeInsets.symmetric(horizontal: 50),
                          //     child: ElevatedButton(
                          //       child: Container(
                          //         child: Container(
                          //             height: 40,
                          //             width: 60,
                          //             alignment: Alignment.center,
                          //             child: Text(' Submit ')),
                          //       ),
                          //       onPressed: () {
                          //         asyncSubmitData();
                          //         print('submit button');
                          //       },
                          //     )),
                        ],
                      ));
                }
                return const Center();
              }),
        ),
      ));
  @override
  void dispose() {
    nodeBarcode.dispose();
    super.dispose();
  }
}
