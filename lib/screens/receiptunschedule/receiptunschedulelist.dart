// ignore_for_file: await_only_futures

import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsreceipt.dart';
import 'package:panasonic/screens/receiptunschedule/receiptunschedule.dart';
import 'package:panasonic/services/receiptservices.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Receipt Unschedule',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class ReceiptUnscheduleList extends StatefulWidget {
  const ReceiptUnscheduleList({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _ReceiptUnscheduleList createState() => _ReceiptUnscheduleList();
}

class _ReceiptUnscheduleList extends State<ReceiptUnscheduleList> {
  final ReceiptService api = ReceiptService();
  List<ReceiptScheduleDetail> recList;
  List<ReceiptScheduleDetail> searchdata = [];
  TextEditingController iBarcodeNo = TextEditingController();
  TextEditingController iSearch = TextEditingController();

  String _searchResult = '';
  String _scanBarcode = '';
  FocusNode nodeReqno = FocusNode();
  FocusNode nodeBarcode = FocusNode();

  @override
  void initState() {
    super.initState();
    asynclistGetData();
  }

  void asynclistGetData() async {
    recList = await api.getReceiptUnscheduleListData(widget.userid);
    searchdata = recList;
    setState(() {});
  }

  Future<bool> progressAction() async {
    await new Future.delayed(const Duration(seconds: 2));
    return true;
  }

  void _detail(String _reqNo, String _barcode, String _matCode, String _matName,
      String _lotNo, String _qty, String _expDate, String _suppName) {
    setState(() {
      ReceiptScheduleDetail model = new ReceiptScheduleDetail();
      model.reqNo = _reqNo;
      model.barcodeNo = _barcode;
      model.materialCode = _matCode;
      model.materialName = _matName;
      model.lotNo = _lotNo;
      model.qty = _qty;
      model.expiredDate = _expDate;
      model.supplierName = _suppName;

      Navigator.of(context)
          .push(
            MaterialPageRoute(
              builder: (context) => ReceiptUnschedule(
                  userid: widget.userid, warehouse: widget.warehouse),
              settings: RouteSettings(arguments: model),
            ),
          )
          .then((value) => setState(() {
                asynclistGetData();
                iBarcodeNo.clear();
              }));
      setState(() {});
    });
  }

  //auto submit data after scan
  void _scanbarcodesave(String barcodeNo) async {
    //List<ReceiptScheduleDetail> listcheck = [];
    //List<ReceiptScheduleDetail> listScan;

    //listcheck = await api.checkValidateUnschedule(barcodeNo, '-', "0");
    //listScan = await api.getReceiptUnschedule(barcodeNo);

    List<ReceiptScheduleDetail> listData;
    try {
      if (barcodeNo == "") {
        Message.box(
            type: "2",
            message: "Please scan barcode",
            top: 0,
            position: "0",
            context: context);
        return;
      }

      //if (listcheck[0].id.toString() == "200") {
      // if (qty == "0" || qty == "") {
      //   Message.box(
      //       type: "2",
      //       message: "Qty must greater than zero",
      //       top: 0,
      //       position: "0",
      //       context: context);
      //   // asyncAlert("Qty must greater than zero", "2");
      //   return;
      // }

      String matCode = "-";
      String lotNo = "-";
      String qty = "0";
      String expDate = "";
      String suppName = "-";

      // if (listScan.length > 0) {
      //   matCode = listScan[0].materialCode;
      //   lotNo = listScan[0].lotNo;
      //   qty = listScan[0].qty;
      //   expDate = listScan[0].expiredDate;
      //   suppName = listScan[0].supplierName;
      // }

      listData = await ReceiptService.submitUnschedule(barcodeNo, matCode,
          lotNo, qty, expDate, suppName, widget.userid, widget.warehouse, "0");

      if (listData[0].id == "200") {
        Message.box(
            type: "1",
            message: "Submit data successfully",
            top: 0,
            position: "0",
            context: context);
        //iBarcodeNo.clear();
        asynclistGetData();
        await iBarcodeNo.clear();
        //FocusScope.of(context).requestFocus(nodeBarcode);
      } else {
        Message.box(
            type: "2",
            message: listData[0].message.toString(),
            top: 0,
            position: "0",
            context: context);
        //asyncAlert(listcheck[0].message.toString(), "4");
      }
      // } else {
      //   Message.box(
      //       type: "2",
      //       message: listcheck[0].message.toString(),
      //       top: 0,
      //       position: "0",
      //       context: context);
      //   //asyncAlert(listcheck[0].message.toString(), "2");
      // }
    } catch (e) {
      asyncAlert(e.toString(), "2");
    }

    setState(() {
      iBarcodeNo.clear();
    });
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  @override
  Widget build(BuildContext context) {
    recList ??= <ReceiptScheduleDetail>[];

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: kAppBarColor,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text("Receipt Unschedule"),
          ),
          body: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              SizedBox(height: 10.0),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "2. Scan Label Barcode",
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextFormField(
                    focusNode: nodeBarcode,
                    enableInteractiveSelection: false,
                    textInputAction: TextInputAction.none,
                    controller: iBarcodeNo,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      labelText: '',
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            iBarcodeNo.clear();
                            _scanBarcode = '';
                            searchdata.clear();
                            FocusScope.of(context).requestFocus(nodeBarcode);
                          });
                        },
                        icon: Icon(_scanBarcode == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _scanBarcode == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                      ),
                    ),
                    onChanged: (value) {
                      _scanBarcode = value;
                      setState(() {});
                    },
                    // onEditingComplete: () {
                    //   _scanbarcodesave(iBarcodeNo.text);
                    // },
                    onFieldSubmitted: (value) async {
                      await _scanbarcodesave(value);
                    }),
              ),
              Divider(
                color: Colors.grey,
                height: 20,
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextFormField(
                  controller: iSearch,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    labelText: 'Search',
                    prefixIcon: IconButton(
                      onPressed: () {},
                      icon: Icon(
                          _searchResult == '' ? Icons.search : Icons.search),

                      //Icons.search,
                    ),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          iSearch.clear();
                          _searchResult = '';
                          searchdata = recList;
                        });
                      },
                      icon: Icon(_searchResult == ''
                          ? Icons.cancel
                          : Icons.cancel_outlined),
                      color: _searchResult == ''
                          ? Colors.white12.withOpacity(1)
                          : Colors.grey[600],
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {
                      if (value != null) {
                        _searchResult = value;
                        searchdata = recList
                            .where((rec) =>
                                rec.reqNo
                                    .toLowerCase()
                                    .toUpperCase()
                                    .contains(_searchResult.toUpperCase()) ||
                                rec.barcodeNo
                                    .toLowerCase()
                                    .toUpperCase()
                                    .contains(_searchResult.toUpperCase()) ||
                                rec.lotNo
                                    .toLowerCase()
                                    .toUpperCase()
                                    .contains(_searchResult.toUpperCase()) ||
                                rec.materialName
                                    .toUpperCase()
                                    .contains(_searchResult.toUpperCase()) ||
                                rec.qty
                                    .toUpperCase()
                                    .contains(_searchResult.toUpperCase()))
                            .toList();
                      } else {
                        searchdata = recList;
                      }
                    });
                  },
                ),
              ),
              SizedBox(height: 5),
              Expanded(child: _dataTableWidget()),
            ],
          )),
    );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3,
        rightHandSideColumnWidth: (MediaQuery.of(context).size.width),
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      //_getTitleItemWidget('', 1),
      _getTitleItemWidget('Material', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget(
          'Barcode No', MediaQuery.of(context).size.width / 3.2),
      _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('  ', 40),
    ];
  }

  Widget _getTitleItemWidget(String label, double pwidth) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      height: 45,
      width: pwidth,
      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(searchdata[index].materialName),
      width: MediaQuery.of(context).size.width / 3,
      height: 45,
      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
      alignment: Alignment.centerLeft,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Expanded(
      child: Row(
        children: <Widget>[
          Container(
            child: Text(searchdata[index].barcodeNo),
            width: MediaQuery.of(context).size.width / 2.8,
            //height: 50,
            padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
            alignment: Alignment.centerLeft,
          ),
          Container(
            child: Text(searchdata[index].lotNo),
            width: MediaQuery.of(context).size.width / 3.4,
            //height: 50,
            padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
            alignment: Alignment.centerLeft,
          ),
          Container(
            child: Text(value.format(double.parse(searchdata[index].qty))),
            width: MediaQuery.of(context).size.width / 4.5,
            //height: 50,
            padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
            alignment: Alignment.center,
          ),
          Container(
            width: 40,
            height: 45,
            child: IconButton(
              onPressed: () {
                _detail(
                    searchdata[index].reqNo,
                    searchdata[index].barcodeNo,
                    searchdata[index].materialCode,
                    searchdata[index].materialName,
                    searchdata[index].lotNo,
                    searchdata[index].qty,
                    searchdata[index].expiredDate,
                    searchdata[index].supplierName);
              },
              icon: Icon(
                Icons.arrow_forward_ios,
                size: 12,
              ),
              color: Colors.black,
              alignment: Alignment.center,
            ),
          ),
        ],
      ),
    );
  }
}
