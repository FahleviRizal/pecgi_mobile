// import 'package:flutter/material.dart';
// import 'package:panasonic/models/clsstuffingcheck.dart';
// import 'package:panasonic/services/stuffingcheckservice.dart';
// import 'package:panasonic/utilities/alertdialog.dart';
// import 'package:panasonic/utilities/constants.dart';
// import 'package:panasonic/utilities/message.dart';

// class ScanDetail extends StatefulWidget {
//   ScanDetail({Key key, this.barcodeNo, this.sjno, this.userid})
//       : super(key: key);

//   final String barcodeNo;
//   final String sjno;
//   final String userid;
//   @override
//   _ScanDetailState createState() => _ScanDetailState();
// }

// class _ScanDetailState extends State<ScanDetail> {
//   final StuffingCheckService api = StuffingCheckService();
//   List<ClsStuffingCheck> listData = [];

//   TextEditingController iBarcodeNo = TextEditingController();
//   TextEditingController iPalletNo = TextEditingController();
//   TextEditingController iSJNo = TextEditingController();
//   TextEditingController iType = TextEditingController();
//   TextEditingController iLotNo = TextEditingController();
//   TextEditingController iQty = TextEditingController();
//   FocusNode nodeScanBarcode = FocusNode();
//   bool isProcessing = false;

//   @override
//   void initState() {
//     super.initState();

//     iBarcodeNo.text = widget.barcodeNo;
//     asynclistGetData(widget.sjno, widget.barcodeNo);
//   }

//   List<ClsStuffingCheck> listScan;
//   void asynclistGetData(String sjno, String barcodeno) async {
//     listScan = await api.getDataScan(sjno, barcodeno);

//     if (listScan.length == 0) {
//       Message.box(
//           type: '2',
//           message: 'Data not found',
//           top: 0,
//           position: "0",
//           context: context);

//       //asyncAlert("Data not found", "2");
//       iBarcodeNo.text = '';
//       iPalletNo.text = '';
//       iSJNo.text = '';
//       iType.text = '';
//       iLotNo.text = '';
//       iQty.text = '';
//       FocusScope.of(context).requestFocus(nodeScanBarcode);
//       return;
//     } else {
//       if (listScan[0].id == "400") {
//         //asyncAlert(listScan[0].description, "4");
//         Message.box(
//             type: '2',
//             message: listScan[0].description,
//             top: 0,
//             position: "0",
//             context: context);

//         iPalletNo.text = '';
//         iSJNo.text = '';
//         iType.text = '';
//         iLotNo.text = '';
//         iQty.text = '';
//         FocusScope.of(context).requestFocus(nodeScanBarcode);
//         return;
//       } else if (listScan[0].id == "200") {
//         iBarcodeNo.text = barcodeno;
//         iPalletNo.text = listScan[0].palletNo.toString();
//         iSJNo.text = listScan[0].suratJalanNo.toString();
//         iType.text = listScan[0].typeDesc.toString();
//         iLotNo.text = listScan[0].lotNo.toString();
//         iQty.text = listScan[0].qtyDetail;
//       }
//     }
//     setState(() {});
//   }

//   void asyncAlert(String msg, String type) async {
//     var dialog = CustomAlertDialog(
//       type: type,
//       title: '',
//       message: msg,
//       okBtnText: '',
//     );
//     showDialog(context: context, builder: (BuildContext context) => dialog);
//   }

//   void asyncSubmitData() async {
//     try {
//       setState(() {
//         isProcessing = true;
//       });

//       listData = await StuffingCheckService.submitScan(
//           iSJNo.text, iBarcodeNo.text, widget.userid);

//       if (iBarcodeNo.text == "") {
//         Message.box(
//             type: '2',
//             message: "Please scan barcode!",
//             top: 0,
//             position: "0",
//             context: context);

//         // String message = "Please scan barcode!";
//         // var dialog = CustomAlertDialog(
//         //   type: "4",
//         //   title: "",
//         //   message: message,
//         //   okBtnText: 'Close',
//         // );
//         // showDialog(context: context, builder: (BuildContext context) => dialog);
//         return;
//       } else if (listData.length == 0) {
//         Message.box(
//             type: '2',
//             message: "Data not found",
//             top: 0,
//             position: "0",
//             context: context);
//         // asyncAlert("Data not found", "2");
//         return;
//       } else {
//         if (listData[0].id == "200") {
//           Message.box(
//               type: '1',
//               message: "Submit data successfully",
//               top: 0,
//               position: "0",
//               context: context);
//           //asyncAlert("Submit data successfully", "3");
//           iBarcodeNo.text = '';
//           iPalletNo.text = '';
//           iSJNo.text = '';
//           iType.text = '';
//           iLotNo.text = '';
//           iQty.text = '';

//           FocusScope.of(context).requestFocus(nodeScanBarcode);
//         } else {
//           asyncAlert(listData[0].description, "2");
//         }
//       }
//     } catch (e) {
//       asyncAlert(e.toString(), "2");
//     }

//     setState(() {});
//   }

//   // void asyncSubmitData() async {
//   //   try {
//   //     if (iBarcodeNo.text == "") {
//   //       String message = "Please scan barcode!";
//   //       var dialog = CustomAlertDialog(
//   //         type: "4",
//   //         title: "",
//   //         message: message,
//   //         okBtnText: 'Close',
//   //       );
//   //       showDialog(context: context, builder: (BuildContext context) => dialog);

//   //       return;
//   //     }
//   //     if (iQty.text == "0" || iQty.text == "") {
//   //       String message = "Qty must greater than zero";
//   //       var dialog = CustomAlertDialog(
//   //         type: "2",
//   //         title: "",
//   //         message: message,
//   //         okBtnText: 'Close',
//   //       );
//   //       showDialog(context: context, builder: (BuildContext context) => dialog);

//   //       return;
//   //     }

//   //     String message;
//   //     message =
//   //         await api.submitScan(iSJNo.text, iBarcodeNo.text, widget.userid);
//   //     var dialog = CustomAlertDialog(
//   //       type: (message == "success") ? "3" : "2",
//   //       title: "",
//   //       message: (message == "success") ? "Data saved successfully!" : message,
//   //       okBtnText: 'Close',
//   //     );
//   //     showDialog(context: context, builder: (BuildContext context) => dialog);
//   //   } catch (e) {
//   //     print('error caught: $e');
//   //   }
//   // }

//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//         onWillPop: () async => false,
//         child: Scaffold(
//             resizeToAvoidBottomInset: false,
//             appBar: AppBar(
//               flexibleSpace: kAppBarColor,
//               leading: IconButton(
//                 icon: Icon(Icons.arrow_back),
//                 onPressed: () => Navigator.of(context).pop(),
//               ),
//               title: const Text("Scan Detail"),
//             ),
//             body: Column(
//                 mainAxisSize: MainAxisSize.min,
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 verticalDirection: VerticalDirection.down,
//                 children: <Widget>[
//                   SizedBox(height: 5.0),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 20),
//                     child: Align(
//                       alignment: Alignment.centerLeft,
//                       child: Text(
//                         "Barcode No",
//                         style: TextStyle(
//                             fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 5.0),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 10),
//                     child: TextFormField(
//                       controller: iBarcodeNo,
//                       decoration: InputDecoration(
//                         border: OutlineInputBorder(),
//                         contentPadding: new EdgeInsets.symmetric(
//                             vertical: 10.0, horizontal: 10.0),
//                         filled: true,
//                         fillColor: Colors.white,
//                       ),
//                       onEditingComplete: () {
//                         asynclistGetData(iSJNo.text, iBarcodeNo.text);
//                       },
//                     ),
//                   ),
//                   SizedBox(height: 5.0),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 20),
//                     child: Align(
//                       alignment: Alignment.centerLeft,
//                       child: Text(
//                         "Pallet No",
//                         style: TextStyle(
//                             fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 5.0),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 10),
//                     child: TextFormField(
//                       controller: iPalletNo,
//                       enabled: false,
//                       decoration: InputDecoration(
//                         border: OutlineInputBorder(),
//                         contentPadding: new EdgeInsets.symmetric(
//                             vertical: 10.0, horizontal: 10.0),
//                         filled: true,
//                         fillColor: disabledColor,
//                       ),
//                     ),
//                   ),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 20),
//                     child: Align(
//                       alignment: Alignment.centerLeft,
//                       child: Text(
//                         "Surat Jalan",
//                         style: TextStyle(
//                             fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 5.0),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 10),
//                     child: TextFormField(
//                       controller: iSJNo,
//                       enabled: false,
//                       decoration: InputDecoration(
//                         border: OutlineInputBorder(),
//                         contentPadding: new EdgeInsets.symmetric(
//                             vertical: 10.0, horizontal: 10.0),
//                         filled: true,
//                         fillColor: disabledColor,
//                       ),
//                     ),
//                   ),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 20),
//                     child: Align(
//                       alignment: Alignment.centerLeft,
//                       child: Text(
//                         "Type",
//                         style: TextStyle(
//                             fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 5.0),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 10),
//                     child: TextFormField(
//                       controller: iType,
//                       enabled: false,
//                       decoration: InputDecoration(
//                         border: OutlineInputBorder(),
//                         contentPadding: new EdgeInsets.symmetric(
//                             vertical: 10.0, horizontal: 10.0),
//                         filled: true,
//                         fillColor: disabledColor,
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 5.0),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 20),
//                     child: Align(
//                       alignment: Alignment.centerLeft,
//                       child: Text(
//                         "Lot No",
//                         style: TextStyle(
//                             fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 5.0),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 10),
//                     child: TextFormField(
//                       controller: iLotNo,
//                       enabled: false,
//                       decoration: InputDecoration(
//                         border: OutlineInputBorder(),
//                         contentPadding: new EdgeInsets.symmetric(
//                             vertical: 10.0, horizontal: 10.0),
//                         filled: true,
//                         fillColor: disabledColor,
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 5.0),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 20),
//                     child: Align(
//                       alignment: Alignment.centerLeft,
//                       child: Text(
//                         "Qty",
//                         style: TextStyle(
//                             fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 5.0),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 10),
//                     child: TextFormField(
//                       textAlign: TextAlign.right,
//                       controller: iQty,
//                       enabled: false,
//                       decoration: InputDecoration(
//                         border: OutlineInputBorder(),
//                         contentPadding: new EdgeInsets.symmetric(
//                             vertical: 10.0, horizontal: 10.0),
//                         filled: true,
//                         fillColor: disabledColor,
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 20.0),
//                   Expanded(child: Container()),
//                   Padding(
//                       padding: EdgeInsets.symmetric(vertical: 0),
//                       child: Visibility(
//                           child: Container(
//                         padding: EdgeInsets.only(bottom: 10),
//                         width: MediaQuery.of(context).size.width * 0.90,
//                         decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(10.0),
//                         ),
//                         child: FloatingActionButton.extended(
//                           backgroundColor: (isProcessing)
//                               ? disabledButtonColor
//                               : enabledButtonColor,
//                           // onPressed: () {
//                           //   asyncSubmitData();
//                           // },
//                           onPressed: (isProcessing == false)
//                               ? () async {
//                                   //if buttonenabled == true then pass a function otherwise pass "null"
//                                   asyncSubmitData();
//                                   //plug delayed for wait while processing data
//                                   await Future.delayed(
//                                       const Duration(seconds: 3), () {
//                                     setState(() {
//                                       isProcessing = false;
//                                     });
//                                   });
//                                 }
//                               : null,
//                           elevation: 0,
//                           label: Text(
//                             "SUBMIT",
//                             style: TextStyle(
//                                 fontSize: 18.0,
//                                 color: (isProcessing)
//                                     ? disabledColorButtonText
//                                     : enabledColorButtonText),
//                           ),
//                         ),
//                       ))),
//                 ])));
//   }
// }
