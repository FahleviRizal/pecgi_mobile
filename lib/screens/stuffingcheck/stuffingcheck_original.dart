// import 'package:flutter/material.dart';
// import 'package:flutter_typeahead/flutter_typeahead.dart';
// import 'package:horizontal_data_table/horizontal_data_table.dart';
// import 'package:panasonic/models/clsstuffingcheck.dart';
// import 'package:panasonic/screens/stuffingcheck/stuffingscandetail.dart';
// import 'package:panasonic/screens/stuffingcheck/scandetail.dart';
// import 'package:panasonic/services/stuffingcheckservice.dart';
// import 'package:panasonic/utilities/constants.dart';

// void main() {
//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({Key key}) : super(key: key);

//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Stuffing Check',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//         visualDensity: VisualDensity.adaptivePlatformDensity,
//       ),
//     );
//   }
// }

// class StuffingCheck extends StatefulWidget {
//   const StuffingCheck({Key key, this.userid}) : super(key: key);

//   final String userid;
//   @override
//   _StuffingCheckState createState() => _StuffingCheckState();
// }

// class _StuffingCheckState extends State<StuffingCheck> {
//   StuffingCheckService api = StuffingCheckService();
//   List<ClsStuffingCheck> recHList = [];
//   List<ClsStuffingCheck> searchdata = [
//     ClsStuffingCheck(typeDesc: "BR2450A", planQty: "100", scanQty: "50"),
//   ];

//   String _searchCombo = '';

//   TextEditingController iSJNo = TextEditingController();
//   TextEditingController iCustomerCode = TextEditingController();
//   TextEditingController iCustomerName = TextEditingController();
//   TextEditingController iPONo = TextEditingController();
//   TextEditingController iBarcode = TextEditingController();
//   TextEditingController iSearch = TextEditingController();

//   List<ClsStuffingCheck> ddlSJNo = [];
//   List<ClsStuffingCheck> ddlSJNoFilter = [];

//   String _searchBarcode = '';
//   //String _selectedSuratJalanNo = '';

//   FocusNode node = FocusNode();
//   FocusNode nodeBarcode = FocusNode();

//   void asyncGetData(String sjNo) async {
//     recHList = await api.getDataHeader(sjNo);
//     searchdata = recHList;
//     setState(() {});
//   }

//   void _detail(String _sjNo, String _custcode, String _custname, String _pono,
//       String _itemcode, String _itemname) {
//     setState(() {
//       Navigator.of(context).push(
//         MaterialPageRoute(
//           builder: (context) => StuffingScanDetail(
//             sjNo: _sjNo,
//             custcode: _custcode,
//             custname: _custname,
//             pono: _pono,
//             itemcode: _itemcode,
//             itemName: _itemname,
//           ),
//           settings: RouteSettings(),
//         ),
//       );
//     });
//   }

//   void _scanBarcode(String scan) {
//     setState(() {
//       Navigator.of(context)
//           .push(
//             MaterialPageRoute(
//               builder: (context) => ScanDetail(
//                 sjno: iSJNo.text,
//                 barcodeNo: scan,
//                 userid: widget.userid,
//               ),
//             ),
//           )
//           .then((value) => setState(() {
//                 asyncGetData(iSJNo.text);
//                 iBarcode.text = '';
//               }));
//     });
//   }

//   // void _scanBarcode() async {
//   //   String value = "1";
//   //   //value = await api.checkData(iSJNo.text, iBarcode.text);

//   //   if (value != "0") {
//   //     Navigator.of(context)
//   //         .push(
//   //           MaterialPageRoute(
//   //             builder: (context) => ScanDetail(
//   //               barcodeNo: iBarcode.text,
//   //               userid: widget.userid,
//   //             ),
//   //           ),
//   //         )
//   //         .then((value) => setState(() {
//   //               asyncGetData(iSJNo.text);
//   //             }));
//   //   } else {
//   //     var dialog = CustomAlertDialog(
//   //       type: "4",
//   //       title: "",
//   //       message: "Barcode No not found",
//   //       okBtnText: 'Close',
//   //     );
//   //     showDialog(context: context, builder: (BuildContext context) => dialog);
//   //   }
//   // }

//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//         onWillPop: () async => false,
//         child: Scaffold(
//             //resizeToAvoidBottomInset: false, //biar ga kena error pixel dibawah
//             appBar: AppBar(
//               automaticallyImplyLeading: false,
//               flexibleSpace: kAppBarColor,
//               leading: IconButton(
//                 icon: Icon(Icons.arrow_back),
//                 onPressed: () => Navigator.of(context).pop(),
//               ),
//               title: const Text("Stuffing Check"),
//             ),
//             body: Column(
//               mainAxisSize: MainAxisSize.min,
//               mainAxisAlignment: MainAxisAlignment.center,
//               verticalDirection: VerticalDirection.down,
//               children: <Widget>[
//                 SizedBox(height: 10.0),
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: 20),
//                   child: Align(
//                     alignment: Alignment.centerLeft,
//                     child: Text(
//                       "1. Scan / Select Surat Jalan No",
//                       style: TextStyle(
//                           fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                 ),
//                 SizedBox(height: 5),
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: 10),
//                   child: TypeAheadField(
//                     hideSuggestionsOnKeyboardHide: false,
//                     textFieldConfiguration: TextFieldConfiguration(
//                         textInputAction: TextInputAction.done,
//                         onSubmitted: (value) {
//                           iSJNo.text = value;
//                           _searchCombo = value;

//                           ddlSJNoFilter
//                               .where((result) => result.suratJalanNo
//                                   .toLowerCase()
//                                   .contains(value.toLowerCase()))
//                               .toList();

//                           if (ddlSJNoFilter.length > 0) {
//                             iCustomerCode.text = ddlSJNoFilter[0].customerCode;
//                             iCustomerName.text = ddlSJNoFilter[0].customerName;
//                             iPONo.text = ddlSJNoFilter[0].poNo;
//                           }
//                           asyncGetData(value);
//                           FocusScope.of(context).requestFocus(nodeBarcode);
//                         },
//                         focusNode: node,
//                         controller: iSJNo,
//                         style: TextStyle(fontFamily: 'Arial'),
//                         decoration: InputDecoration(
//                           suffixIcon: IconButton(
//                             onPressed: () {
//                               setState(() {
//                                 iSJNo.clear();
//                                 iCustomerCode.text = '';
//                                 iCustomerName.text = '';
//                                 iPONo.text = '';
//                                 searchdata.clear();
//                                 _searchCombo = '';
//                               });
//                             },
//                             icon: Icon(_searchCombo == ''
//                                 ? Icons.cancel
//                                 : Icons.cancel_outlined),
//                             color: _searchCombo == ''
//                                 ? Colors.white12.withOpacity(1)
//                                 : Colors.grey[600],
//                           ),
//                           border: OutlineInputBorder(),
//                           contentPadding: new EdgeInsets.symmetric(
//                               vertical: -10.0, horizontal: 10.0),
//                         )),
//                     suggestionsCallback: (pattern) async {
//                       ddlSJNo = await StuffingCheckService.getSJNo();
//                       _ddlFilterResult = pattern;
//                       _searchCombo = pattern;
//                       ddlSJNoFilter = ddlSJNo
//                           .where((result) => result.suratJalanNo
//                               .toLowerCase()
//                               .contains(_ddlFilterResult.toLowerCase()))
//                           .toList();
//                       return ddlSJNoFilter;
//                     },
//                     itemBuilder: (context, suggestion) {
//                       print(suggestion);
//                       return ListTile(
//                         title: Text(suggestion.suratJalanNo),
//                       );
//                     },
//                     onSuggestionSelected: (suggestion) {
//                       iSJNo.text = suggestion.suratJalanNo;
//                       iCustomerCode.text = suggestion.customerCode;
//                       iCustomerName.text = suggestion.customerName;
//                       iPONo.text = suggestion.poNo;
//                       _searchCombo = suggestion.suratJalanNo;
//                       asyncGetData(suggestion.suratJalanNo);
//                       FocusScope.of(context).requestFocus(nodeBarcode);
//                     },
//                   ),
//                 ),
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: 20),
//                   child: Align(
//                     alignment: Alignment.centerLeft,
//                     child: Text(
//                       "Customer Name",
//                       style: TextStyle(
//                           fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                 ),
//                 SizedBox(height: 5),
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: 10),
//                   child: TextField(
//                     controller: iCustomerName,
//                     enabled: false,
//                     decoration: InputDecoration(
//                         filled: true,
//                         fillColor: disabledColor,
//                         border: OutlineInputBorder(),
//                         contentPadding: new EdgeInsets.symmetric(
//                             vertical: 10.0, horizontal: 10)),
//                   ),
//                 ),
//                 SizedBox(height: 5),
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: 20),
//                   child: Align(
//                     alignment: Alignment.centerLeft,
//                     child: Text(
//                       "PO No",
//                       style: TextStyle(
//                           fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                 ),
//                 SizedBox(height: 5),
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: 10),
//                   child: TextField(
//                     controller: iPONo,
//                     enabled: false,
//                     decoration: InputDecoration(
//                         filled: true,
//                         fillColor: disabledColor,
//                         border: OutlineInputBorder(),
//                         contentPadding: new EdgeInsets.symmetric(
//                             vertical: 10.0, horizontal: 10)),
//                   ),
//                 ),
//                 SizedBox(height: 5),
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: 20),
//                   child: Align(
//                     alignment: Alignment.centerLeft,
//                     child: Text(
//                       "2. Scan Barcode No",
//                       style: TextStyle(
//                           fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                 ),
//                 SizedBox(height: 5),
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: 10),
//                   child: TextFormField(
//                     enableInteractiveSelection: false,
//                     textInputAction: TextInputAction.none,
//                     controller: iBarcode,
//                     focusNode: nodeBarcode,
//                     decoration: InputDecoration(
//                       border: OutlineInputBorder(),
//                       contentPadding: new EdgeInsets.symmetric(
//                           vertical: 10.0, horizontal: 10.0),
//                       labelText: '',
//                       suffixIcon: IconButton(
//                         onPressed: () {
//                           setState(() {
//                             iBarcode.clear();
//                             _searchBarcode = '';
//                             searchdata.clear();

//                             FocusScope.of(context).requestFocus(nodeBarcode);
//                           });
//                         },
//                         icon: Icon(_searchBarcode == ''
//                             ? Icons.cancel
//                             : Icons.cancel_outlined),
//                         color: _searchBarcode == ''
//                             ? Colors.white12.withOpacity(1)
//                             : Colors.grey[600],
//                       ),
//                     ),
//                     onChanged: (value) {
//                       _searchBarcode = value;

//                       setState(() {});
//                     },
//                     // onTap: () {
//                     //   _scanBarcode();
//                     // },
//                     onFieldSubmitted: (value) {
//                       _scanBarcode(value);
//                     },
//                   ),
//                 ),
//                 Divider(
//                   color: Colors.grey,
//                   height: 20,
//                 ),
//                 SizedBox(height: 5.0),
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: 10),
//                   child: TextFormField(
//                     controller: iSearch,
//                     decoration: InputDecoration(
//                       border: OutlineInputBorder(),
//                       contentPadding: new EdgeInsets.symmetric(
//                           vertical: 10.0, horizontal: 10.0),
//                       labelText: 'Search',
//                       prefixIcon: IconButton(
//                         onPressed: () {},
//                         icon: Icon(
//                             _searchResult == '' ? Icons.search : Icons.search),

//                         //Icons.search,
//                       ),
//                       suffixIcon: IconButton(
//                         onPressed: () {
//                           setState(() {
//                             iSearch.clear();
//                             _searchResult = '';
//                             searchdata = recHList;
//                           });
//                         },
//                         icon: Icon(_searchResult == ''
//                             ? Icons.cancel
//                             : Icons.cancel_outlined),
//                         color: _searchResult == ''
//                             ? Colors.white12.withOpacity(1)
//                             : Colors.grey[600],
//                       ),
//                     ),
//                     onChanged: (value) {
//                       setState(() {
//                         if (value != null) {
//                           _searchResult = value;
//                           searchdata = recHList
//                               .where((rec) =>
//                                   rec.typeDesc
//                                       .toUpperCase()
//                                       .contains(_searchResult.toUpperCase()) ||
//                                   rec.planQty
//                                       .toUpperCase()
//                                       .contains(_searchResult.toUpperCase()) ||
//                                   rec.scanQty
//                                       .toUpperCase()
//                                       .contains(_searchResult.toUpperCase()))
//                               .toList();
//                         } else {
//                           searchdata = recHList;
//                         }
//                       });
//                     },
//                   ),
//                 ),
//                 SizedBox(height: 5),
//                 Expanded(child: _dataTableWidget()),
//               ],
//             ))); //);
//   }

//   Widget _dataTableWidget() {
//     return Container(
//       child: HorizontalDataTable(
//         leftHandSideColumnWidth: 0,
//         rightHandSideColumnWidth: MediaQuery.of(context).size.width,
//         isFixedHeader: true,
//         headerWidgets: _getTitleWidget(),
//         leftSideItemBuilder: _generateFirstColumnRow,
//         rightSideItemBuilder: _generateRightHandSideColumnRow,
//         itemCount: searchdata.length,
//         rowSeparatorWidget: const Divider(
//           color: Colors.black54,
//           height: 1.0,
//           thickness: 0.0,
//         ),
//         leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
//         rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
//         horizontalScrollbarStyle: const ScrollbarStyle(
//           thumbColor: Colors.grey,
//           isAlwaysShown: true,
//           thickness: 4.0,
//           radius: Radius.circular(5.0),
//         ),
//         enablePullToRefresh: false,
//         refreshIndicator: const WaterDropHeader(),
//         refreshIndicatorHeight: 60,
//       ),
//     );
//   }

//   List<Widget> _getTitleWidget() {
//     return [
//       _getTitleItemWidget('', 1),
//       _getTitleItemWidget('No', MediaQuery.of(context).size.width / 6.5),
//       _getTitleItemWidget('Type', MediaQuery.of(context).size.width / 4.25),
//       _getTitleItemWidget('Plan Qty', MediaQuery.of(context).size.width / 3.5),
//       _getTitleItemWidget('Scan Qty', MediaQuery.of(context).size.width / 3.1),
//     ];
//   }

//   Widget _getTitleItemWidget(String label, double width) {
//     return Container(
//       decoration: BoxDecoration(
//           color: datatableColor,
//           border: Border(
//               top: BorderSide(
//             color: Colors.grey,
//             width: 1,
//           ))),
//       child: Text(label,
//           style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
//       width: width,
//       height: 45,
//       padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
//       alignment: Alignment.center,
//     );
//   }

//   Widget _generateFirstColumnRow(BuildContext context, int index) {
//     return Container(
//       width: 1,
//       height: 45,
//     );
//   }

//   Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
//     return Row(
//       children: <Widget>[
//         Container(
//           child: Text((index + 1).toString()),
//           width: MediaQuery.of(context).size.width / 6.5,
//           height: 45,
//           padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
//           alignment: Alignment.center,
//         ),
//         Container(
//           child: Text(searchdata[index].typeDesc),
//           width: MediaQuery.of(context).size.width / 4,
//           height: 45,
//           padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
//           alignment: Alignment.center,
//         ),
//         Container(
//           child: Text(searchdata[index].planQty),
//           width: MediaQuery.of(context).size.width / 4.25,
//           height: 45,
//           padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
//           alignment: Alignment.centerRight,
//         ),
//         Container(
//           child: Text(searchdata[index].scanQty),
//           width: MediaQuery.of(context).size.width / 3.5,
//           height: 45,
//           padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
//           alignment: Alignment.centerRight,
//         ),
//         Container(
//           width: 15,
//           height: 45,
//           child: IconButton(
//             onPressed: () {
//               _detail(
//                   iSJNo.text,
//                   iCustomerCode.text,
//                   iCustomerName.text,
//                   iPONo.text,
//                   searchdata[index].type,
//                   searchdata[index].typeDesc);
//             },
//             icon: Icon(
//               Icons.arrow_forward_ios,
//               size: 15,
//             ),
//             color: Colors.black,
//             alignment: Alignment.centerRight,
//           ),
//         ),
//       ],
//     );
//   }
// }
