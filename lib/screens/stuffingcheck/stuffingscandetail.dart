// import 'package:flutter/material.dart';
// import 'package:horizontal_data_table/horizontal_data_table.dart';
// import 'package:panasonic/models/clsstuffingcheck.dart';
// import 'package:panasonic/services/stuffingcheckservice.dart';
// import 'package:panasonic/utilities/constants.dart';

// class StuffingScanDetail extends StatefulWidget {
//   //const stuffingScanDetail({ Key? key }) : super(key: key);
//   const StuffingScanDetail(
//       {Key key,
//       this.userid,
//       this.sjNo,
//       this.custcode,
//       this.custname,
//       this.pono,
//       this.itemcode,
//       this.itemName})
//       : super(key: key);

//   final String userid;
//   final String sjNo;
//   final String custcode;
//   final String custname;
//   final String pono;
//   final String itemcode;
//   final String itemName;
//   @override
//   _StuffingScanDetailState createState() => _StuffingScanDetailState();
// }

// class _StuffingScanDetailState extends State<StuffingScanDetail> {
//   final StuffingCheckService api = StuffingCheckService();
//   List<ClsStuffingCheck> listH;

//   List<ClsStuffingCheck> searchdata = [
//     // ClsManpowerPreparationDetil(empID: "K08324", empName: "Teguh Mahesa"),
//     // ClsManpowerPreparationDetil(
//     //     empID: "K0832", empName: "Boby Kurniawan Kuncoro Subibyo Mangunkusumo"),
//   ];

//   TextEditingController iSJNo = TextEditingController();
//   TextEditingController iCustName = TextEditingController();
//   TextEditingController iPONo = TextEditingController();
//   TextEditingController iItemName = TextEditingController();
//   TextEditingController iSearch = TextEditingController();

//   String _searchResult = '';

//   @override
//   void initState() {
//     super.initState();

//     iSJNo.text = widget.sjNo;
//     iCustName.text = widget.custname;
//     iPONo.text = widget.pono;
//     iItemName.text = widget.itemName;

//     asynclistGetData(widget.sjNo, widget.pono, widget.itemcode);
//   }

//   void asynclistGetData(String sjNo, String pono, String item) async {
//     listH = await api.getDataDetail(sjNo, pono, item);
//     searchdata = listH;
//     setState(() {});
//   }

//   List<ClsStuffingCheck> listDetail;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       resizeToAvoidBottomInset: false,
//       appBar: AppBar(
//         flexibleSpace: kAppBarColor,
//         leading: IconButton(
//           icon: Icon(Icons.arrow_back),
//           onPressed: () => Navigator.of(context).pop(),
//         ),
//         title: const Text("Stuffing Scan Detail"),
//       ),
//       body: Column(
//           mainAxisSize: MainAxisSize.min,
//           mainAxisAlignment: MainAxisAlignment.center,
//           verticalDirection: VerticalDirection.down,
//           children: <Widget>[
//             SizedBox(height: 10.0),
//             Padding(
//               padding: EdgeInsets.symmetric(horizontal: 20),
//               child: Align(
//                 alignment: Alignment.centerLeft,
//                 child: Text(
//                   "Surat Jalan No",
//                   style: TextStyle(
//                       fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                 ),
//               ),
//             ),
//             SizedBox(
//               height: 5,
//             ),
//             Padding(
//               padding: EdgeInsets.symmetric(horizontal: 10),
//               child: TextField(
//                 controller: iSJNo,
//                 enabled: false,
//                 decoration: InputDecoration(
//                     filled: true,
//                     fillColor: disabledColor,
//                     border: OutlineInputBorder(),
//                     contentPadding: new EdgeInsets.symmetric(
//                         vertical: 10.0, horizontal: 10)),
//               ),
//             ),
//             SizedBox(
//               height: 5,
//             ),
//             Padding(
//               padding: EdgeInsets.symmetric(horizontal: 20),
//               child: Align(
//                 alignment: Alignment.centerLeft,
//                 child: Text(
//                   "Customer Name",
//                   style: TextStyle(
//                       fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                 ),
//               ),
//             ),
//             SizedBox(height: 5),
//             Padding(
//               padding: EdgeInsets.symmetric(horizontal: 10),
//               child: TextField(
//                 controller: iCustName,
//                 enabled: false,
//                 decoration: InputDecoration(
//                     filled: true,
//                     fillColor: disabledColor,
//                     border: OutlineInputBorder(),
//                     contentPadding: new EdgeInsets.symmetric(
//                         vertical: 10.0, horizontal: 10)),
//               ),
//             ),
//             SizedBox(height: 5),
//             Padding(
//               padding: EdgeInsets.symmetric(horizontal: 20),
//               child: Align(
//                 alignment: Alignment.centerLeft,
//                 child: Text(
//                   "PO No",
//                   style: TextStyle(
//                       fontFamily: 'Arial', fontWeight: FontWeight.bold),
//                 ),
//               ),
//             ),
//             SizedBox(height: 5),
//             Padding(
//               padding: EdgeInsets.symmetric(horizontal: 10),
//               child: TextField(
//                 controller: iPONo,
//                 enabled: false,
//                 decoration: InputDecoration(
//                     filled: true,
//                     fillColor: disabledColor,
//                     border: OutlineInputBorder(),
//                     contentPadding: new EdgeInsets.symmetric(
//                         vertical: 10.0, horizontal: 10)),
//               ),
//             ),
//             SizedBox(height: 5),
//             Divider(
//               color: Colors.grey,
//               height: 20,
//             ),
//             SizedBox(height: 5.0),
//             Padding(
//               padding: EdgeInsets.symmetric(horizontal: 10),
//               child: TextFormField(
//                 controller: iSearch,
//                 decoration: InputDecoration(
//                   border: OutlineInputBorder(),
//                   contentPadding: new EdgeInsets.symmetric(
//                       vertical: 10.0, horizontal: 10.0),
//                   labelText: 'Search',
//                   prefixIcon: IconButton(
//                     onPressed: () {},
//                     icon:
//                         Icon(_searchResult == '' ? Icons.search : Icons.search),

//                     //Icons.search,
//                   ),
//                   suffixIcon: IconButton(
//                     onPressed: () {
//                       setState(() {
//                         iSearch.clear();
//                         _searchResult = '';
//                         searchdata = listH;
//                       });
//                     },
//                     icon: Icon(_searchResult == ''
//                         ? Icons.cancel
//                         : Icons.cancel_outlined),
//                     color: _searchResult == ''
//                         ? Colors.white12.withOpacity(1)
//                         : Colors.grey[600],
//                   ),
//                 ),
//                 onChanged: (value) {
//                   setState(() {
//                     if (value != null) {
//                       _searchResult = value;
//                       searchdata = listH
//                           .where((rec) =>
//                               rec.palletNo
//                                   .toUpperCase()
//                                   .contains(_searchResult.toUpperCase()) ||
//                               rec.barcodeNo
//                                   .toUpperCase()
//                                   .contains(_searchResult.toUpperCase()) ||
//                               rec.lotNo
//                                   .toUpperCase()
//                                   .contains(_searchResult.toUpperCase()))
//                           .toList();
//                     } else {
//                       searchdata = listH;
//                     }
//                   });
//                 },
//               ),
//             ),
//             SizedBox(height: 5),
//             Expanded(child: _dataTableWidget()),
//           ]),
//     );
//   }

//   Widget _dataTableWidget() {
//     return Container(
//       child: HorizontalDataTable(
//         leftHandSideColumnWidth: 0,
//         rightHandSideColumnWidth: MediaQuery.of(context).size.width,
//         isFixedHeader: true,
//         headerWidgets: _getTitleWidget(),
//         leftSideItemBuilder: _generateFirstColumnRow,
//         rightSideItemBuilder: _generateRightHandSideColumnRow,
//         itemCount: searchdata.length,
//         rowSeparatorWidget: const Divider(
//           color: Colors.black54,
//           height: 1.0,
//           thickness: 0.0,
//         ),
//         leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
//         rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
//         horizontalScrollbarStyle: const ScrollbarStyle(
//           thumbColor: Colors.grey,
//           isAlwaysShown: true,
//           thickness: 4.0,
//           radius: Radius.circular(5.0),
//         ),
//         enablePullToRefresh: false,
//         refreshIndicator: const WaterDropHeader(),
//         refreshIndicatorHeight: 60,
//       ),
//     );
//   }

//   List<Widget> _getTitleWidget() {
//     return [
//       _getTitleItemWidget('', 1),
//       _getTitleItemWidget('Pallet No', MediaQuery.of(context).size.width / 4),
//       _getTitleItemWidget('Barcode No', MediaQuery.of(context).size.width / 4),
//       _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 4),
//       _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 4),
//     ];
//   }

//   Widget _getTitleItemWidget(String label, double width) {
//     return Container(
//       decoration: BoxDecoration(
//           color: datatableColor,
//           border: Border(
//               top: BorderSide(
//             color: Colors.grey,
//             width: 1,
//           ))),
//       child: Text(label,
//           style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
//       width: width,
//       height: 45,
//       padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
//       alignment: Alignment.center,
//     );
//   }

//   Widget _generateFirstColumnRow(BuildContext context, int index) {
//     return Container(
//       width: 1,
//       height: 45,
//     );
//   }

//   Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
//     return Row(
//       children: <Widget>[
//         Container(
//           child: Text(searchdata[index].palletNo),
//           width: MediaQuery.of(context).size.width / 4,
//           height: 45,
//           padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
//           alignment: Alignment.center,
//         ),
//         Container(
//           child: Text(searchdata[index].barcodeNo),
//           width: MediaQuery.of(context).size.width / 4,
//           height: 45,
//           padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
//           alignment: Alignment.center,
//         ),
//         Container(
//           child: Text(searchdata[index].lotNo),
//           width: MediaQuery.of(context).size.width / 4,
//           height: 45,
//           padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
//           alignment: Alignment.centerRight,
//         ),
//         Container(
//           child: Text(searchdata[index].qty),
//           width: MediaQuery.of(context).size.width / 4,
//           height: 45,
//           padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
//           alignment: Alignment.centerRight,
//         ),
//       ],
//     );
//   }
// }
