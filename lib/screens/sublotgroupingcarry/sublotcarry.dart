// ignore_for_file: await_only_futures

import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clssublotgroupingcarry.dart';
import 'package:panasonic/screens/sublotgroupingcarry/sublotcarrydetail.dart';
import 'package:panasonic/services/sublotgroupcarryservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
//import 'package:panasonic/utilities/alertdialog.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sublot Grouping (Carry) ',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class SublotGrouppingCarry extends StatefulWidget {
  const SublotGrouppingCarry({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _SublotGroupingCarry createState() => _SublotGroupingCarry();
}

class _SublotGroupingCarry extends State<SublotGrouppingCarry> {
  final SublotGroupingCarryService api = SublotGroupingCarryService();
  List<ClsSublotGroupingCarry> listH;

  List<ClsSublotGroupingCarry> searchdata = [
    // ClsSublotGroupingCarry(
    //     type: "BR2450A",
    //     sublotNo: "1A",
    //     lotNo: "2021101",
    //     qty: "300",
    //     unit: "PCS"),
  ];

  TextEditingController iCarryNo = TextEditingController();
  TextEditingController iCarryName = TextEditingController();
  TextEditingController iSublotNo = TextEditingController();
  TextEditingController iSearch = TextEditingController();
  FocusNode node = FocusNode();
  FocusNode nodeSublot = FocusNode();
  String _searchCarry = '';
  String _searchResult = '';
  String _searchSublot = '';
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
  }

  void _getCarryName(String code) async {
    List<ClsSublotGroupingCarry> list;
    list = await api.getCarryName(code);

    if (list.length > 0) {
      iCarryName.text = list[0].carryName;
    }
  }

  void asynclistGetData(String carryNo) async {
    List<ClsSublotCarryDetail> listcheck = [];
    listcheck = await api.checkData(iCarryNo.text, "-");

    if (listcheck[0].id.toString() != "200") {
      Message.box(
          type: "2",
          message: listcheck[0].message.toString(),
          top: 0,
          position: "0",
          context: context);
      return;
    }

    listH = await api.getData(carryNo, widget.warehouse);
    searchdata = listH;
    //.clear();
    setState(() {});
  }

  //List<ClsSublotCarryDetail> listScan;
  void _scanSublot(String carryno, String sublotno) async {
    // String value = "";
    // value = await api.checkData(iCarryNo.text, iSublotNo.text, "0");

    List<ClsSublotCarryDetail> listcheck = [];
    listcheck = await api.checkData(carryno, sublotno);

    if (listcheck[0].id.toString() == "200") {
      // Navigator.of(context)
      //     .push(
      //       MaterialPageRoute(
      //         builder: (context) => SublotGrouppingCarryDetail(
      //           userid: widget.userid,
      //           call: "0",
      //           carryNo: iCarryNo.text,
      //           subLotNo: iSublotNo.text,
      //           warehouse: widget.warehouse,
      //         ),
      //       ),
      //     )
      //     .then((value) => setState(() {
      //           asynclistGetData(iCarryNo.text);
      //           iSublotNo.clear();
      //         }));

      await asyncSubmitData(carryno, sublotno);
      asynclistGetData(iCarryNo.text);
      setState(() {});
    } else {
      Message.box(
          type: "2",
          message:
              listcheck[0].message.toString(), // "Carry/Sublot No not found",
          top: 0,
          position: "0",
          context: context);
      iSublotNo.clear();
      // var dialog = CustomAlertDialog(
      //   type: "4",
      //   title: "",
      //   message: "Carry/Sublot No not found",
      //   okBtnText: 'Close',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
    }
    iSublotNo.clear();
  }

  void _detail(String carry, String subLotBarcode) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => SublotGrouppingCarryDetail(
          userid: widget.userid,
          call: "1",
          carryNo: carry,
          subLotNo: subLotBarcode,
        ),
      ),
    );
  }

  // void _complete(String carry, String subLotBarcode) async {
  //   List<ClsSublotCarryDetail> listData = [];
  //   listData = await SublotGroupingCarryService.complete(
  //       carry, subLotBarcode, widget.userid, widget.warehouse);

  //   if (listData[0].id == "200") {
  //     Message.box(
  //         type: "1",
  //         message: "Complete data successfully",
  //         top: 0,
  //         position: "0",
  //         context: context);
  //   } else {
  //     Message.box(
  //         type: "2",
  //         message: listData[0].message.toString(),
  //         top: 0,
  //         position: "0",
  //         context: context);
  //     //asyncAlert(listcheck[0].message.toString(), "4");
  //   }
  // }

  List<ClsSublotCarryDetail> listData;
  void asyncSubmitData(String carryNo, String barcode) async {
    try {
      if (barcode == "") {
        Message.box(
            type: "2",
            message: "Please scan sublot",
            top: 0,
            position: "0",
            context: context);
        FocusScope.of(context).requestFocus(nodeSublot);
        return;
      }
      List<ClsSublotCarryDetail> listcheck = [];
      listcheck = await api.checkData(carryNo, barcode);

      if (listcheck[0].id.toString() == "200") {
        listData = await SublotGroupingCarryService.submit(
            iCarryNo.text,
            iSublotNo.text,
            // "type",
            // "l",
            // 0,
            // iType.text,
            // iLotNo.text,
            // iQty.text,
            widget.userid,
            widget.warehouse);

        if (listData[0].id == "200") {
          Message.box(
              type: "1",
              message: "Submit data successfully",
              top: 0,
              position: "0",
              context: context);
          iSublotNo.clear();
          FocusScope.of(context).requestFocus(nodeSublot);
        } else {
          Message.box(
              type: "2",
              message: listData[0].message.toString(),
              top: 0,
              position: "0",
              context: context);
        }
      } else {
        Message.box(
            type: "2",
            message: listcheck[0].message.toString(),
            top: 0,
            position: "0",
            context: context);
      }
    } catch (e) {
      asyncAlert(e.toString(), "2");
    }

    setState(() {});
  }

  void _complete(String carry) async {
    setState(() {
      isProcessing = true;
    });
    List<ClsSublotGroupingCarry> listData = [];

    if (searchdata.length == 0) {
      Message.box(
          type: "2",
          message: "Data is empty",
          top: 0,
          position: "0",
          context: context);
      return;
    }

    try {
      if (iCarryNo.text == "") {
        //asyncAlert("Please select Location / Carry No", "2");
        Message.box(
            type: "2",
            message: "Please scan Carry No",
            top: 0,
            position: "0",
            context: context);
        FocusScope.of(context).requestFocus(node);
        return;
      }

      if (searchdata.length > 0) {
        listData = await SublotGroupingCarryService.complete(
            carry, widget.userid, widget.warehouse);

        // listData = await SublotGroupingCarryService.complete(
        //     searchdata, carry, widget.warehouse, widget.userid);

        if (listData[0].id == "200") {
          Message.box(
              type: "1",
              message: listData[0].message,
              top: 0,
              position: "0",
              context: context);
        } else {
          Message.box(
              type: "2",
              message: listData[0].message,
              top: 0,
              position: "0",
              context: context);
        }
      }
    } catch (e) {
      asyncAlert(e.toString(), "2");
    }
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  @override
  Widget build(BuildContext context) {
    listH ??= <ClsSublotGroupingCarry>[];
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text("Sublot Grouping (Carry)"),
        ),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "1. Scan Carry No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                autofocus: true,
                showCursor: true,
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                controller: iCarryNo,
                focusNode: node,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iCarryNo.clear();
                        iSublotNo.clear();
                        iCarryName.text = '';
                        _searchCarry = '';
                        searchdata.clear();

                        FocusScope.of(context).requestFocus(node);
                      });
                    },
                    icon: Icon(_searchCarry == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _searchCarry == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _searchCarry = value;

                  setState(() {});
                },
                onEditingComplete: () {
                  _getCarryName(iCarryNo.text);
                  asynclistGetData(iCarryNo.text);
                  FocusScope.of(context).requestFocus(nodeSublot);
                },
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Carry Name",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iCarryName,
                enabled: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  filled: true,
                  fillColor: disabledColor,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "2. Scan Sublot No",
                  style: TextStyle(
                      fontFamily: 'Arial', fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                autofocus: true,
                showCursor: true,
                enableInteractiveSelection: false,
                textInputAction: TextInputAction.none,
                controller: iSublotNo,
                focusNode: nodeSublot,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  labelText: '',
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iSublotNo.clear();
                        _searchSublot = '';

                        FocusScope.of(context).requestFocus(nodeSublot);
                      });
                    },
                    icon: Icon(_searchSublot == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _searchSublot == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  _searchSublot = value;
                  setState(() {});
                },
                onEditingComplete: () {
                  _scanSublot(iCarryNo.text, iSublotNo.text);
                },
              ),
            ),
            Divider(
              color: Colors.black,
              height: 20,
            ),
            SizedBox(height: 5.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: iSearch,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 15.0),
                  labelText: 'Search',
                  prefixIcon: IconButton(
                    onPressed: () {},
                    icon:
                        Icon(_searchResult == '' ? Icons.search : Icons.search),
                    //Icons.search,
                  ),
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        iSearch.clear();
                        _searchResult = '';
                        searchdata = listH;
                      });
                    },
                    icon: Icon(_searchResult == ''
                        ? Icons.cancel
                        : Icons.cancel_outlined),
                    color: _searchResult == ''
                        ? Colors.white12.withOpacity(1)
                        : Colors.grey[600],
                  ),
                ),
                onChanged: (value) {
                  setState(() {
                    if (value != null) {
                      _searchResult = value;
                      searchdata = listH
                          .where((rec) =>
                              rec.type
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()) ||
                              rec.sublotNo
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()) ||
                              rec.lotNo
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()) ||
                              rec.unit
                                  .toUpperCase()
                                  .contains(_searchResult.toUpperCase()))
                          .toList();
                    } else {
                      searchdata = listH;
                    }
                  });
                },
              ),
            ),
            SizedBox(height: 10),
            Expanded(
              //height: MediaQuery.of(context).size.height * 0.55,
              child: _dataTableWidget(),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                // height: MediaQuery.of(context).size.height * 0.1,
                // alignment: Alignment.center,
                children: <Widget>[
                  // Padding(
                  //   padding: EdgeInsets.symmetric(vertical: 5),
                  //   child:
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: (isProcessing)
                          ? disabledButtonColor
                          : enabledButtonColor,
                      minimumSize:
                          Size(MediaQuery.of(context).size.width * 0.90, 55),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                    ),
                    // onPressed: () {
                    //   _complete(iCarryNo.text);
                    // },
                    onPressed: (isProcessing == false)
                        ? () async {
                            //if buttonenabled == true then pass a function otherwise pass "null"
                            _complete(iCarryNo.text);
                            //plug delayed for wait while processing data
                            await Future.delayed(const Duration(seconds: 3),
                                () {
                              setState(() {
                                isProcessing = false;
                              });
                            });
                          }
                        : null,
                    child: Text(
                      "COMPLETE",
                      style: TextStyle(
                        fontSize: 18,
                        color: (isProcessing)
                            ? disabledColorButtonText
                            : enabledColorButtonText,
                      ),
                    ),
                  ),
                  // ),
                ]),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
          ],
        ),
      ),
    );
  }

  //datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3.5, //100,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width, //470,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Type', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget(
          'Sublot No', MediaQuery.of(context).size.width / 3.5), //, 100),
      _getTitleItemWidget(
          'LotNo', MediaQuery.of(context).size.width / 4), // 120),
      _getTitleItemWidget(
          'Qty', MediaQuery.of(context).size.width / 4), // 100),
      _getTitleItemWidget(
          'Unit', MediaQuery.of(context).size.width / 5), // 100),
      _getTitleItemWidget(' ', 20),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(searchdata[index].type),
      width: MediaQuery.of(context).size.width / 3.5,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].sublotNo),
          width: MediaQuery.of(context).size.width / 3.5, //100,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(searchdata[index].lotNo),
          width: MediaQuery.of(context).size.width / 4, //120,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(value.format(double.parse(searchdata[index].qty))),
          width: MediaQuery.of(context).size.width / 5, //100,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(searchdata[index].unit),
          width: MediaQuery.of(context).size.width / 5, //110,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          width: 20,
          height: 45,
          child: IconButton(
            onPressed: () {
              _detail(iCarryNo.text, searchdata[index].barcodeNo);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            color: Colors.black,
            alignment: Alignment.center,
          ),
        ),
      ],
    );
  }
}

/*
Container(
                padding: const EdgeInsets.only(top: 10),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      columnSpacing: 30.0,
                      decoration: BoxDecoration(
                          border: Border.all(
                        width: 1,
                        color: Colors.grey[300],
                      )),
                      headingRowColor: MaterialStateColor.resolveWith(
                          (states) => Colors.grey[200]),
                      sortColumnIndex: 0,
                      showCheckboxColumn: false,
                      columns: [
                        DataColumn(
                          label: Container(
                            //width: 60,
                            padding: EdgeInsets.symmetric(horizontal: 0),
                            child: Text(
                              "Type",
                              style: TextStyle(
                                  fontFamily: "Arial",
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                          ),
                          numeric: false,
                        ),
                        DataColumn(
                          label: Container(
                            padding: EdgeInsets.symmetric(horizontal: 0),
                            child: Text(
                              "Sublot No",
                              style: TextStyle(
                                  fontFamily: "Arial",
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                          ),
                          numeric: false,
                        ),
                        DataColumn(
                          label: Container(
                            padding: EdgeInsets.symmetric(horizontal: 0),
                            child: Text(
                              "Lot No",
                              style: TextStyle(
                                  fontFamily: "Arial",
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                          ),
                          numeric: false,
                        ),
                        DataColumn(
                          label: Container(
                            padding: EdgeInsets.symmetric(horizontal: 0),
                            child: Text(
                              "Qty",
                              style: TextStyle(
                                  fontFamily: "Arial",
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                          ),
                          numeric: false,
                        ),
                        DataColumn(
                          label: Container(
                            padding: EdgeInsets.symmetric(horizontal: 0),
                            child: Text(
                              "Unit",
                              style: TextStyle(
                                  fontFamily: "Arial",
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                          ),
                          numeric: false,
                        ),
                        DataColumn(
                            label: Container(
                          padding: EdgeInsets.symmetric(horizontal: 0),
                          child: Text(""),
                        ))
                      ],
                      rows: searchdata
                          .map(
                            (idata) => DataRow(onSelectChanged: (b) {}, cells: [
                              DataCell(
                                Container(
                                  //width: 80,
                                  child: Text(idata.type),
                                ),
                              ),
                              DataCell(
                                Container(
                                  child: Text(idata.sublotNo),
                                ),
                              ),
                              DataCell(
                                Container(
                                  child: Text(idata.lotNo),
                                ),
                              ),
                              DataCell(
                                Container(
                                  child: Text(idata.qty),
                                  alignment: Alignment.centerRight,
                                ),
                              ),
                              DataCell(
                                Container(
                                  child: Text(idata.unit),
                                ),
                              ),
                              DataCell(
                                Container(
                                  width: 5,
                                  child: IconButton(
                                    onPressed: () {
                                      _detail(idata.sublotNo);
                                    },
                                    icon: Icon(
                                      Icons.arrow_forward_ios,
                                      size: 15,
                                    ),
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  _detail(idata.sublotNo);
                                },
                                //IconButton(),
                              ),
                            ]),
                          )
                          .toList(),
                    ),
                  ),
                ),
              ),
*/