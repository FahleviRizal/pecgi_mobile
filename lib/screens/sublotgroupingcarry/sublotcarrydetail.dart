import 'package:flutter/material.dart';
import 'package:panasonic/services/sublotgroupcarryservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/models/clssublotgroupingcarry.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sublot Detail ',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class SublotGrouppingCarryDetail extends StatefulWidget {
  const SublotGrouppingCarryDetail(
      {Key key,
      this.userid,
      this.warehouse,
      this.call,
      this.carryNo,
      this.subLotNo})
      : super(key: key);

  final String userid;
  final String call;
  final String carryNo;
  final String subLotNo;
  final String warehouse;

  @override
  _SublotGroupingCarryDetail createState() => _SublotGroupingCarryDetail();
}

class _SublotGroupingCarryDetail extends State<SublotGrouppingCarryDetail> {
  TextEditingController iSublotNo = TextEditingController();
  TextEditingController iType = TextEditingController();
  TextEditingController iLotNo = TextEditingController();
  TextEditingController iQty = TextEditingController();
  TextEditingController iUnit = TextEditingController();
  TextEditingController iExpDate = TextEditingController();
  TextEditingController iLastUser = TextEditingController();
  TextEditingController iLastUpdate = TextEditingController();

  final SublotGroupingCarryService api = SublotGroupingCarryService();
  final value = new NumberFormat("#,##0.00", "en_US");
  FocusNode nodeBarcode = FocusNode();
  FocusNode nodeButton = FocusNode();
  String _searchSublot = '';

  @override
  void initState() {
    super.initState();
    iSublotNo.text = widget.subLotNo;
    asynclistGetData(widget.carryNo, widget.subLotNo);
  }

  List<ClsSublotCarryDetail> listScan;
  void asynclistGetData(String carryNo, String sublotNo) async {
    listScan = await api.getDataDetail(carryNo, sublotNo);

    if (listScan.length > 0) {
      if (listScan[0].id == '200') {
        _searchSublot = sublotNo;
        iSublotNo.text = sublotNo;
        iType.text = listScan[0].type.toString();
        iLotNo.text = listScan[0].lotNo.toString();
        iQty.text = value.format(double.parse(listScan[0].qty));
        //iQty.text = listScan[0].qty;
        iUnit.text = listScan[0].unit;
        iExpDate.text = listScan[0].expiredDate;
        iLastUser.text = listScan[0].lastUser;
        iLastUpdate.text = listScan[0].lastUpdate;
        FocusScope.of(context).requestFocus(nodeButton);
        // if (listScan[0].expiredDate != "") {
        //   final DateTime now = DateTime.parse(listScan[0].expiredDate);
        //   final DateFormat formatter = DateFormat('dd MMM yyyy');
        //   final String formatted = formatter.format(now);
        //   iExpDate.text = formatted;
        // } else {
        //   iExpDate.text = "";
        // }

        // if (listScan[0].lastUpdate != "") {
        //   //iLastUpdate.text = listScan[0].lastUpdate;
        //   final DateTime now2 = DateTime.parse(listScan[0].lastUpdate);
        //   final DateFormat formatter2 = DateFormat('dd MMM yyyy HH:mm:ss');
        //   final String formatted2 = formatter2.format(now2);
        //   iLastUpdate.text = formatted2;
        // } else {
        //   iLastUpdate.text = "";
        // }
      } else {
        Message.box(
            type: "2",
            message: listScan[0].message.toString(),
            top: 0,
            position: "0",
            context: context);

        iType.text = '';
        iLotNo.text = '';
        iQty.text = '0';
        iUnit.text = '';
        iExpDate.text = '';
        iLastUser.text = '';
        iLastUpdate.text = '';
      }
    } else {
      // var dialog = CustomAlertDialog(
      //   type: "4",
      //   title: "",
      //   message: "Sublot is not found",
      //   okBtnText: 'Close',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
      Message.box(
          type: "2",
          message: "Sublot No not found",
          top: 0,
          position: "0",
          context: context);

      //iSublotNo.text = '';
      iType.text = '';
      iLotNo.text = '';
      iQty.text = '0';
      iUnit.text = '';
      iExpDate.text = '';
      iLastUser.text = '';
      iLastUpdate.text = '';
    }
  }

  List<ClsSublotCarryDetail> listData;
  void asyncSubmitData(String barcode) async {
    try {
      if (barcode == "") {
        Message.box(
            type: "2",
            message: "Please scan barcode",
            top: 0,
            position: "0",
            context: context);
        //asyncAlert("Please scan barcode", "2");
        FocusScope.of(context).requestFocus(nodeBarcode);
        return;
      }
      List<ClsSublotCarryDetail> listcheck = [];
      listcheck = await api.checkData(widget.carryNo, iSublotNo.text);

      if (listcheck[0].id.toString() == "200") {
        listData = await SublotGroupingCarryService.submit(
            widget.carryNo,
            iSublotNo.text,
            // iType.text,
            // iLotNo.text,
            // iQty.text,
            widget.userid,
            widget.warehouse);

        if (listData[0].id == "200") {
          Message.box(
              type: "1",
              message: "Submit data successfully",
              top: 0,
              position: "0",
              context: context);
          //asyncAlert("Submit data successfully", "3");
          clearData();
          iSublotNo.clear();
          FocusScope.of(context).requestFocus(nodeBarcode);
        } else {
          Message.box(
              type: "2",
              message: listData[0].message.toString(),
              top: 0,
              position: "0",
              context: context);
          clearData();
          //asyncAlert(listcheck[0].message.toString(), "4");
        }
      } else {
        Message.box(
            type: "2",
            message: listcheck[0].message.toString(),
            top: 0,
            position: "0",
            context: context);
        clearData();
        //asyncAlert(listcheck[0].message.toString(), "2");
      }
    } catch (e) {
      asyncAlert(e.toString(), "2");
    }

    setState(() {});
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void clearData() {
    iType.text = "";
    iLotNo.text = "";
    iQty.text = "0";
    iExpDate.text = "";
    iUnit.text = "";
    iLastUser.text = "";
    iLastUpdate.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              title: Text((widget.call == "0")
                  ? "Sublot Detail"
                  : "View Sublot Detail"),
            ),
            body: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: <Widget>[
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Scan Sublot No",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      //autofocus: true,
                      //showCursor: true,
                      focusNode: nodeBarcode,
                      enableInteractiveSelection: false,
                      textInputAction: TextInputAction.none,
                      controller: iSublotNo,
                      enabled: false, //(widget.call == "0") ? true : false,
                      decoration: InputDecoration(
                        filled: true, //(widget.call == "0") ? true : false,
                        fillColor: Colors.grey[200], // (widget.call == "0")
                        // ? Colors.white
                        // : Colors.grey[200],
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        labelText: '',
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              iSublotNo.clear();
                              _searchSublot = '';
                              clearData();
                              FocusScope.of(context).requestFocus(nodeBarcode);
                            });
                          },
                          icon: Icon(_searchSublot == ''
                              ? Icons.cancel
                              : Icons.cancel_outlined),
                          color: _searchSublot == ''
                              ? Colors.white12.withOpacity(1)
                              : Colors.grey[600],
                        ),
                      ),
                      onChanged: (value) {
                        _searchSublot = value;
                        setState(() {});
                      },
                      onFieldSubmitted: (value) {
                        asynclistGetData(widget.carryNo, iSublotNo.text);
                      },
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Type",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iType,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Lot No",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iLotNo,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Qty",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      textAlign: TextAlign.right,
                      controller: iQty,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Unit",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iUnit,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Expired Date",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iExpDate,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Register User",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iLastUser,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Last Update",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iLastUpdate,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Expanded(child: Container()),
                  //button is visible false
                  Padding(
                      padding: EdgeInsets.symmetric(vertical: 0),
                      child: Visibility(
                          visible:
                              false, // (widget.call == "0") ? true : false,
                          child: Container(
                            padding: EdgeInsets.only(bottom: 10),
                            width: MediaQuery.of(context).size.width * 0.90,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: FloatingActionButton.extended(
                              backgroundColor: Colors.blue[800],
                              onPressed: () {
                                asyncSubmitData(iSublotNo.text);
                              },
                              focusNode: nodeButton,
                              elevation: 0,
                              label: Text(
                                "SUBMIT",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                          ))),
                  // Padding(
                  //     padding: EdgeInsets.symmetric(horizontal: 50),
                  //     child: Visibility(
                  //         visible: (widget.call == "0") ? true : false,
                  //         child: ElevatedButton(
                  //           child: Container(
                  //             child: Container(
                  //                 height: 40,
                  //                 width: 60,
                  //                 alignment: Alignment.center,
                  //                 child: Text(' Submit ')),
                  //           ),
                  //           onPressed: () {
                  //             asyncSubmitData();
                  //             print('submit button');
                  //           },
                  //         ))),
                ])));
  }
}
