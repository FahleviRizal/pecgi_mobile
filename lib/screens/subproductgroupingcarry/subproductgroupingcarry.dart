import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsApiResponse.dart';
import 'package:panasonic/models/clssublotgroupingcarry.dart';
import 'package:panasonic/models/clssubproductgroupingcarry.dart';
import 'package:panasonic/services/sublotgroupcarryservice.dart';
import 'package:panasonic/services/subproductgroupingcarryservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

class SubProductGroupingCarry extends StatefulWidget {
  const SubProductGroupingCarry({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  State<SubProductGroupingCarry> createState() =>
      _SubProductGroupingCarryState();
}

class _SubProductGroupingCarryState extends State<SubProductGroupingCarry> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final SublotGroupingCarryService api = SublotGroupingCarryService();
  final SubProductGroupingCarryService apiSubProd =
      SubProductGroupingCarryService();
  List<ClsSubProductGroupingCarry> searchdata = [];
  List<ClsSubProductGroupingCarry> payloaddata = [];
  // List<String> listBarcode = [];

  TextEditingController carryNo = TextEditingController();
  TextEditingController carryName = TextEditingController();
  TextEditingController barcode = TextEditingController();
  TextEditingController search = TextEditingController();
  ScrollController _verticalScrollController = ScrollController();
  ScrollController _horizontalScrollController = ScrollController();
  FocusNode carryNode = FocusNode();
  FocusNode barcodeNode = FocusNode();
  FocusNode searchNode = FocusNode();
  String _carryNoSearch = '';
  String _barcodeSearch = '';
  String _search = '';
  double totalQty = 0;

  bool isProcessing = false;

  void getCarryName(String pCarryNo) async {
    List<ClsSublotGroupingCarry> list;
    list = await api.getCarryName(pCarryNo);
    if (list.length > 0) {
      carryName.text = list[0].carryName;
    } else {
      carryNo.clear();
      carryNo.text = carryName.text = '';

      Message.box(
          message: 'Invalid Carry No',
          type: '2',
          top: 0,
          position: '0',
          context: context);

      FocusScope.of(context).requestFocus(carryNode);
    }
  }

  // bool validateBarcode(String pBarcodeNo) {
  //   if(pBarcodeNo == "") {
  //     Message.box(
  //         type: '2',
  //         message: 'Please scan Barcode No',
  //         top: 0,
  //         position: '0',
  //         context: context);

  //     return false;
  //   }

  //   if (searchdata.where((element) => element.barcodeNo == pBarcodeNo).length >
  //       0) {
  //     Message.box(
  //         type: '2',
  //         message: 'Barcode No already exists',
  //         top: 0,
  //         position: '0',
  //         context: context);
  //     return false;
  //   } else {
  //     return true;
  //   }

  // }

  void getDataBarcode(String pBarcodeNo) async {
    if(pBarcodeNo == "") {
      Message.box(
          type: '2',
          message: 'Please scan Barcode No',
          top: 0,
          position: '0',
          context: context);

      return;
    }

    if (searchdata.where((element) => element.barcodeNo == pBarcodeNo).length >
        0) {
      Message.box(
          type: '2',
          message: 'Barcode No already exists',
          top: 0,
          position: '0',
          context: context);
      return;
    }

    try {
      // ClsSubProductGroupingCarry result;
      ClsSubProductGroupingCarry result =
          await apiSubProd.getListDataBarcode(pBarcodeNo);
      if (result.barcodeNo != null) {
        if (searchdata.length > 0) searchdata.removeAt(searchdata.length - 1);

        searchdata.add(result);
        payloaddata.add(result);

        totalQty += double.parse(result.qty);
        searchdata.add(ClsSubProductGroupingCarry(
            itemName: 'Total', qty: totalQty.toString()));

        barcode.clear();
        // setState(() {
        //   barcode.clear();
        //   barcode.text = '';
        // });
      } else {
        Message.box(
            message: 'Invalid Barcode No',
            type: '2',
            top: 0,
            position: '0',
            context: context);
        // barcode.clear();
        // barcode.text = _barcodeSearch = '';
        //FocusScope.of(context).requestFocus(barcodeNode);
        return;
      }
    } catch (e) {
      alertError(e.toString());
    }
  }

  void complete(String pCarryNo) async {
    setState(() {
      isProcessing = true;
    });
    
    if (pCarryNo == '') {
      Message.box(
          message: 'Please fill Carry No.',
          type: '2',
          top: 0,
          position: '0',
          context: context);
      return;
    }

    if (payloaddata.length == 0) {
      Message.box(
          message: 'Please scan barcode',
          type: '2',
          top: 0,
          position: '0',
          context: context);
      return;
    }

    for (var data in payloaddata) {
      data.carryNo = pCarryNo;
      data.userID = widget.userid;
    }

    try {
      ClsApiResponse response = await apiSubProd.complete(payloaddata);
      if (response.StatusCode == '200') {
        carryNo.clear();
        barcode.clear();
        carryName.clear();
        search.clear();
        searchdata.clear();
        payloaddata.clear();
        carryNo.text = barcode.text = carryName.text = search.text = '';
        totalQty = 0;
        setState(() {});
      }
      Message.box(
          message: response.Message,
          type: response.StatusCode == '200' ? '1' : '2',
          top: 0,
          position: '0',
          context: context);
    } catch (e) {
      alertError(e.toString());
    }
  }

  void alertError(String message) {
    var dialog = CustomAlertDialog(
      type: '2',
      title: '',
      message: message,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  @override
  Widget build(BuildContext context) {
    payloaddata ??= <ClsSubProductGroupingCarry>[];
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: kAppBarColor,
          title: Text('Sub Product Group Carry'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )),
      body:
          // ListView(
          //   scrollDirection: Axis.vertical,
          //   children: [
          Padding(
        padding: const EdgeInsets.all(10.0),
        child: Form(
          key: formkey,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              verticalDirection: VerticalDirection.down,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('1. Scan Carry No',
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold)),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                    autofocus: true,/*  */
                    enableInteractiveSelection: false,
                    controller: carryNo,
                    focusNode: carryNode,
                    textInputAction: TextInputAction.none,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: '',
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      suffixIcon: IconButton(
                        icon: Icon(_carryNoSearch == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _carryNoSearch == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                        onPressed: () {
                          setState(() {
                            carryNo.clear();
                            carryName.clear();
                            barcode.clear();
                            search.clear();
                          _carryNoSearch = _barcodeSearch = '';
                            FocusScope.of(context).requestFocus(carryNode);
                          });
                        },
                      ),
                    ),
                    onChanged: (val) {
                      _carryNoSearch = val;
                      setState(() {});
                    },
                    onEditingComplete: () {
                      getCarryName(carryNo.text);
                      FocusScope.of(context).requestFocus(barcodeNode);
                    }),
                SizedBox(
                  height: 10,
                ),
                Text('Carry Name',
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold)),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: carryName,
                  enabled: false,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: '',
                      contentPadding: EdgeInsets.all(10),
                      fillColor: disabledColor,
                      filled: true),
                ),
                SizedBox(
                  height: 10,
                ),
                Text('2. Scan Barcode',
                    style: TextStyle(
                        fontFamily: 'Arial', fontWeight: FontWeight.bold)),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  enableInteractiveSelection: false,
                  textInputAction: TextInputAction.none,
                  controller: barcode,
                  focusNode: barcodeNode,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      contentPadding: EdgeInsets.all(10),
                      suffixIcon: IconButton(
                        icon: Icon(_barcodeSearch == ''
                            ? Icons.cancel
                            : Icons.cancel_outlined),
                        color: _carryNoSearch == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                        onPressed: () {
                          setState(() {
                            barcode.clear();
                            search.clear();
                          _barcodeSearch = '';
                            FocusScope.of(context).requestFocus(barcodeNode);
                          });
                        },
                      )),
                  onChanged: (val) {
                    _barcodeSearch = val;
                    setState(() {});
                  },
                onFieldSubmitted: (val) async {
                  await getDataBarcode(barcode.text);
                  FocusScope.of(context).requestFocus(barcodeNode);
                  // setState(() {
                  //   barcode.clear();
                  //   _barcodeSearch = '';
                  // });
                  }
                ),
                Divider(color: Colors.black, height: 10),
                SizedBox(height: 5),
                TextFormField(
                  enableInteractiveSelection: false,
                  textInputAction: TextInputAction.none,
                  controller: search,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search),
                    suffixIcon: IconButton(
                        icon: Icon(
                            _search == '' ? Icons.cancel : Icons.cancel_outlined),
                        color: _search == ''
                            ? Colors.white12.withOpacity(1)
                            : Colors.grey[600],
                        onPressed: () {
                          setState(() {
                            search.clear();
                            search.text = _search = '';
                            FocusScope.of(context).requestFocus(searchNode);
                            searchdata = payloaddata;
                          });
                        }),
                    contentPadding: EdgeInsets.all(10),
                    border: OutlineInputBorder(),
                  ),
                  onChanged: (val) {
                    _search = val;
                    setState(() {
                      if (val == null) {
                        searchdata = payloaddata;
                      } else {
                        searchdata = payloaddata
                            .where((data) =>
                                data.itemName
                                    .toUpperCase()
                                    .contains(_search.toUpperCase()) ||
                                data.lotNo
                                    .toUpperCase()
                                    .contains(_search.toUpperCase()))
                            .toList();
                      }
                    });
                  },
                ),
                SizedBox(height: 10),
                Expanded(
                  // height: M  ediaQuery.of(context).size.height * 0.45,
                  child: _dataTableWidget(),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        style: TextButton.styleFrom(
                          backgroundColor: isProcessing ? disabledButtonColor : enabledButtonColor,
                          minimumSize:
                              Size(MediaQuery.of(context).size.width * 0.90, 55),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50.0),
                          ),
                        ),
                        onPressed: (!isProcessing) ? () async {
                            complete(carryNo.text);
                            await Future.delayed(Duration(seconds: 3), () {
                              setState(() {
                                isProcessing = false;
                              });
                            });
                        } : null,
                        child: Center(
                          child: Text(
                            "COMPLETE",
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      // ),
                    ]),
                SizedBox(height: MediaQuery.of(context).size.height * 0.01),
              ]),
        ),
      ),
      // ])
    );
  }

//datatable
  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3, //100,
        rightHandSideColumnWidth:
            MediaQuery.of(context).size.width * 5 / 6, //470,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Item Name', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 4),
      _getTitleItemWidget('Sub Lot', MediaQuery.of(context).size.width / 6),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 5),
      _getTitleItemWidget('Unit', MediaQuery.of(context).size.width / 5)
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    if (index == searchdata.length - 1) {
      return _getTitleItemWidget(
          'Total', MediaQuery.of(context).size.width / 3);
    } else {
      return Container(
        child: Text(searchdata[index].itemName),
        width: MediaQuery.of(context).size.width / 3,
        height: 45,
        padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
        alignment: Alignment.center,
      );
    }
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    if (index == searchdata.length - 1) {
      return Row(
        children: [
          _getTitleItemWidget('', MediaQuery.of(context).size.width / 4),
          _getTitleItemWidget('', MediaQuery.of(context).size.width / 6),
          _getTitleItemWidget(value.format(double.parse(searchdata[index].qty)),
              MediaQuery.of(context).size.width / 5),
          _getTitleItemWidget('', MediaQuery.of(context).size.width / 5)
        ],
      );
    } else {
      return Row(
        children: <Widget>[
          Container(
            child: Text(searchdata[index].lotNo),
            width: MediaQuery.of(context).size.width / 4, //120,
            height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
            alignment: Alignment.center,
          ),
          Container(
            child: Text(searchdata[index].subLotNo),
            width: MediaQuery.of(context).size.width / 6, //120,
            height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
            alignment: Alignment.center,
          ),
          Container(
            child: Text(value.format(double.parse(searchdata[index].qty))),
            width: MediaQuery.of(context).size.width / 5, //100,
            height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
            alignment: Alignment.centerRight,
          ),
          Container(
            child: Text(searchdata[index].unitDesc),
            width: MediaQuery.of(context).size.width / 5, //100,
            height: 45,
            padding: EdgeInsets.fromLTRB(5, 0, 30, 0),
            alignment: Alignment.centerRight,
          ),
        ],
      );
    }
  }
}
