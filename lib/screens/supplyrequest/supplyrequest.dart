import 'package:flutter/material.dart';
import 'package:panasonic/models/clsSupplyRequest.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/screens/supplyrequest/supplyrequestdetail.dart';
import 'package:panasonic/screens/supplyrequest/supplyrequestliststock.dart';
import 'package:panasonic/services/supplyrequestservice.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/screens/supplyrequest/supplyrequestlist.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:intl/intl.dart';
import 'package:panasonic/utilities/message.dart';

class SupplyRequest extends StatefulWidget {
  const SupplyRequest({Key key, this.userid, this.warehouse}) : super(key: key);

  final String userid;
  final String warehouse;
  @override
  _SupplyRequestState createState() => _SupplyRequestState();
}

class _SupplyRequestState extends State<SupplyRequest> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsSupplyRequest> listGetData = [];
  List<ClsSupplyRequest> listGetDataFiltered = [];
  List<ClsSupplyRequest> listSupplyNo;
  List<ClsSupplyRequest> listSupplyNoFiltered;
  List<ClsSupplyRequest> listLine;
  List<ClsSupplyRequest> listLineNoFiltered;

  final TextEditingController txtSupplyNo = new TextEditingController();
  final TextEditingController txtInstructionNo = new TextEditingController();
  final TextEditingController txtLine = new TextEditingController();
  final TextEditingController txtScanBarcode = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();
  final TextEditingController iLineCode = new TextEditingController();

  String _searchSupplyNo = '';
  String _scanBarcode = '';
  String _searchDataResult = '';
  String _searchLine = '';

  //String _selectedSupplyNo = '';

  FocusNode nodeSupplyNo = FocusNode();
  FocusNode nodeBarcode = FocusNode();
  FocusNode nodeLine = new FocusNode();

  @override
  void initState() {
    //asynclistGetData("-", "-");
    super.initState();
  }

  void asynclistGetData(String supplyNo, String insNo) async {
    listGetData = await SupplyRequestService.getDataListHeader(supplyNo, insNo);
    listGetDataFiltered = listGetData;
    setState(() {});
  }

  void supplyList(String materialCode, String materialDesc) {
    setState(() {
      ClsSupplyRequest listData = new ClsSupplyRequest();
      listData.supplyNo = txtSupplyNo.text;
      listData.materialCode = materialCode;
      listData.materialDesc = materialDesc;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SupplyRequestList(),
          settings: RouteSettings(
            arguments: listData,
          ),
        ),
      );
    });
  }

  void supplyListLot(
      String supplyNo, String materialCode, String materialDesc) {
    setState(() {
      ClsSupplyRequest listData = new ClsSupplyRequest();
      listData.supplyNo = txtSupplyNo.text;
      listData.materialCode = materialCode;
      listData.materialDesc = materialDesc;
      listData.warehouselogin = widget.warehouse;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SupplyRequestListStock(),
          settings: RouteSettings(
            arguments: listData,
          ),
        ),
      );
    });
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void supplyDetail(String iSupplyNo, String iBarcode) async {
    List<ClsSupplyRequest> listGetData2 = [];
    listGetData2 = await SupplyRequestService.getDataScan(
        iSupplyNo, iBarcode, widget.warehouse);
    if (listGetData2[0].id == "200") {
      ClsSupplyRequest listData = new ClsSupplyRequest();
      listData.supplyNo = txtSupplyNo.text;
      listData.labelBarcode = txtScanBarcode.text;

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SupplyRequestDetail(
            userid: widget.userid,
            warehouse: widget.warehouse,
          ),
          settings: RouteSettings(
            arguments: listData,
          ),
        ),
      ).then((value) => setState(() {
            asynclistGetData(txtSupplyNo.text, txtInstructionNo.text);
            txtScanBarcode.text = '';
          }));
      setState(() {});
    } else {
      Message.box(
          type: "2",
          message: listGetData2[0].description,
          top: 0,
          position: "0",
          context: context);
      //asyncAlert(listGetData2[0].description, "2");
      txtScanBarcode.clear();
      _scanBarcode = '';
      FocusScope.of(context).requestFocus(nodeBarcode);
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text("Supply Request Scan"),
            ),
            body: Column(children: <Widget>[
              Flexible(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                          child: Form(
                            key: formkey,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "1. Select/Scan Machine Process",
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TypeAheadField(
                                    hideSuggestionsOnKeyboardHide: false,
                                    textFieldConfiguration:
                                        TextFieldConfiguration(
                                            textInputAction:
                                                TextInputAction.done,
                                            onSubmitted: (value) {
                                              _searchLine = value;
                                              iLineCode.text = value;
                                              listLineNoFiltered
                                                  .where((result) => result
                                                      .lineCode
                                                      .toLowerCase()
                                                      .contains(
                                                          value.toLowerCase()))
                                                  .toList();

                                              final newval =
                                                  listLineNoFiltered.firstWhere(
                                                      (item) =>
                                                          item.lineCode ==
                                                          value,
                                                      orElse: () => null);

                                              setState(() {
                                                txtLine.text = newval.lineName;
                                              });
                                            },
                                            focusNode: nodeLine,
                                            controller: this.txtLine,
                                            style:
                                                TextStyle(fontFamily: 'Arial'),
                                            decoration: InputDecoration(
                                              suffixIcon: IconButton(
                                                onPressed: () {
                                                  setState(() {
                                                    iLineCode.clear();
                                                    txtLine.text = '';
                                                    txtSupplyNo.text = '';
                                                    listGetDataFiltered.clear();
                                                    _searchLine = '';
                                                    _searchSupplyNo = '';
                                                    txtInstructionNo.text = '';
                                                  });
                                                },
                                                icon: Icon(_searchLine == ''
                                                    ? Icons.cancel
                                                    : Icons.cancel_outlined),
                                                color: _searchLine == ''
                                                    ? Colors.white12
                                                        .withOpacity(1)
                                                    : Colors.grey[600],
                                              ),
                                              border: OutlineInputBorder(),
                                              contentPadding:
                                                  new EdgeInsets.symmetric(
                                                      vertical: -10.0,
                                                      horizontal: 10.0),
                                            )),
                                    suggestionsCallback: (pattern) async {
                                      listLine =
                                          await SupplyRequestService.getLine(
                                              widget.userid);

                                      _searchLine = pattern;

                                      listLineNoFiltered = listLine
                                          .where((user) =>
                                              user.lineCode
                                                  .toLowerCase()
                                                  .contains(_searchLine
                                                      .toLowerCase()) ||
                                              user.lineName
                                                  .toLowerCase()
                                                  .contains(_searchLine
                                                      .toLowerCase()))
                                          .toList();
                                      return listLineNoFiltered;
                                    },
                                    itemBuilder: (context, suggestion) {
                                      return ListTile(
                                        title: Text(suggestion.lineCode),
                                        subtitle: Text(suggestion.lineName),
                                      );
                                    },
                                    onSuggestionSelected: (suggestion) async {
                                      this.iLineCode.text = suggestion.lineCode;
                                      this.txtLine.text = suggestion.lineName;
                                      _searchLine = suggestion.lineCode;

                                      FocusScope.of(context)
                                          .requestFocus(nodeSupplyNo);
                                    },
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "2. Select / Scan Supply No",
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TypeAheadField(
                                    hideSuggestionsOnKeyboardHide: false,
                                    textFieldConfiguration:
                                        TextFieldConfiguration(
                                            textInputAction:
                                                TextInputAction.done,
                                            onSubmitted: (value) {
                                              txtSupplyNo.text = value;
                                              _searchSupplyNo = value;
                                              setState(() {
                                                listSupplyNoFiltered
                                                    .where((result) => result
                                                        .supplyNo
                                                        .toLowerCase()
                                                        .contains(value
                                                            .toLowerCase()))
                                                    .toList();

                                                if (listSupplyNoFiltered
                                                        .length >
                                                    0) {
                                                  txtInstructionNo.text =
                                                      listSupplyNoFiltered[0]
                                                          .instNo;
                                                  // txtLine.text =
                                                  //     listSupplyNoFiltered[0]
                                                  //         .lineName;
                                                }
                                              });
                                              asynclistGetData(
                                                  value,
                                                  listSupplyNoFiltered[0]
                                                      .instNo);
                                              FocusScope.of(context)
                                                  .requestFocus(nodeBarcode);
                                            },
                                            focusNode: nodeSupplyNo,
                                            controller: this.txtSupplyNo,
                                            style:
                                                TextStyle(fontFamily: 'Arial'),
                                            decoration: InputDecoration(
                                              suffixIcon: IconButton(
                                                onPressed: () {
                                                  setState(() {
                                                    txtSupplyNo.clear();
                                                    txtInstructionNo.text = '';
                                                    //txtLine.text = '';
                                                    listGetDataFiltered.clear();
                                                    _searchSupplyNo = '';
                                                  });
                                                },
                                                icon: Icon(_searchSupplyNo == ''
                                                    ? Icons.cancel
                                                    : Icons.cancel_outlined),
                                                color: _searchSupplyNo == ''
                                                    ? Colors.white12
                                                        .withOpacity(1)
                                                    : Colors.grey[600],
                                              ),
                                              border: OutlineInputBorder(),
                                              contentPadding:
                                                  new EdgeInsets.symmetric(
                                                      vertical: -10.0,
                                                      horizontal: 10.0),
                                            )),
                                    suggestionsCallback: (pattern) async {
                                      listSupplyNo = await SupplyRequestService
                                          .getSupplyNo(iLineCode.text);
                                      _searchSupplyNo = pattern;
                                      listSupplyNoFiltered = listSupplyNo
                                          .where((user) => user.supplyNo
                                              .toLowerCase()
                                              .contains(_searchSupplyNo
                                                  .toLowerCase()))
                                          .toList();
                                      return listSupplyNoFiltered;
                                    },
                                    itemBuilder: (context, suggestion) {
                                      return ListTile(
                                        title: Text(suggestion.typeDesc),
                                        subtitle: Text(suggestion.supplyNo),
                                      );
                                    },
                                    onSuggestionSelected: (suggestion) async {
                                      this.txtSupplyNo.text =
                                          suggestion.supplyNo;
                                      //_selectedSupplyNo = suggestion.supplyNo;
                                      this.txtInstructionNo.text =
                                          suggestion.instNo;
                                      // this.txtLine.text = suggestion.lineName;
                                      _searchSupplyNo = suggestion.supplyNo;
                                      asynclistGetData(suggestion.supplyNo,
                                          suggestion.instNo);
                                      FocusScope.of(context)
                                          .requestFocus(nodeBarcode);
                                    },
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Instruction No",
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtInstructionNo,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      filled: true,
                                      fillColor: disabledColor,
                                    ),
                                  ),
                                ),

                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),

                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Scan Label Barcode",
                                      style: TextStyle(
                                          fontFamily: 'Arial',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    focusNode: nodeBarcode,
                                    enableInteractiveSelection: false,
                                    textInputAction: TextInputAction.none,
                                    controller: txtScanBarcode,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      labelText: '',
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            txtScanBarcode.clear();
                                            _scanBarcode = '';
                                            //listGetDataFiltered.clear();
                                            FocusScope.of(context)
                                                .requestFocus(nodeBarcode);
                                          });
                                        },
                                        icon: Icon(_scanBarcode == ''
                                            ? Icons.cancel
                                            : Icons.cancel_outlined),
                                        color: _scanBarcode == ''
                                            ? Colors.white12.withOpacity(1)
                                            : Colors.grey[600],
                                      ),
                                    ),
                                    onChanged: (value) {
                                      _scanBarcode = value;
                                      setState(() {});
                                    },
                                    onFieldSubmitted: (value) {
                                      supplyDetail(txtSupplyNo.text,
                                          txtScanBarcode.text);
                                    },
                                  ),
                                ),
                                Divider(color: Colors.grey, height: 20),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                      controller: txtSearch,
                                      decoration: InputDecoration(
                                        labelText: 'Search',
                                        prefixIcon: IconButton(
                                          onPressed: () {},
                                          icon: Icon(_searchDataResult == ''
                                              ? Icons.search
                                              : Icons.search),

                                          //Icons.search,
                                        ),
                                        suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              txtSearch.clear();
                                              _searchDataResult = '';
                                              listGetDataFiltered = listGetData;
                                            });
                                          },
                                          icon: Icon(_searchDataResult == ''
                                              ? Icons.cancel
                                              : Icons.cancel_outlined),
                                          color: _searchDataResult == ''
                                              ? Colors.white12.withOpacity(1)
                                              : Colors.grey[600],
                                        ),
                                        border: OutlineInputBorder(),
                                        contentPadding:
                                            new EdgeInsets.symmetric(
                                                vertical: 10.0,
                                                horizontal: 10.0),
                                      ),
                                      onChanged: (value) {
                                        setState(() {
                                          _searchDataResult = value;
                                          listGetDataFiltered = listGetData
                                              .where((user) =>
                                                  user.location
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()) ||
                                                  user.lot
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()))
                                              .toList();
                                        });
                                      }),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Expanded(
                                  child: Container(child: _dataTableWidget()),
                                ),
                                // SizedBox(
                                //   height: 300,
                                //   child: SingleChildScrollView(
                                //     scrollDirection: Axis.horizontal,
                                //     child: SingleChildScrollView(
                                //       scrollDirection: Axis.vertical,
                                //       child: DataTable(
                                //           columns: const <DataColumn>[
                                //             DataColumn(
                                //               label: Text(
                                //                 'Material',
                                //                 style: TextStyle(
                                //                     fontFamily: 'Arial',
                                //                     fontWeight: FontWeight.bold),
                                //               ),
                                //             ),
                                //             DataColumn(
                                //               label: Text(
                                //                 'Plan Qty',
                                //                 style: TextStyle(
                                //                     fontFamily: 'Arial',
                                //                     fontWeight: FontWeight.bold),
                                //               ),
                                //             ),
                                //             DataColumn(
                                //               label: Text(
                                //                 'Result',
                                //                 style: TextStyle(
                                //                     fontFamily: 'Arial',
                                //                     fontWeight: FontWeight.bold),
                                //               ),
                                //             ),
                                //             DataColumn(
                                //               label: Text(
                                //                 'Location',
                                //                 style: TextStyle(
                                //                     fontFamily: 'Arial',
                                //                     fontWeight: FontWeight.bold),
                                //               ),
                                //             ),
                                //             DataColumn(
                                //               label: Text(
                                //                 '',
                                //               ),
                                //             ),
                                //           ],
                                //           rows: List.generate(
                                //             listGetDataFiltered.length,
                                //             (index) => DataRow(
                                //               cells: <DataCell>[
                                //                 DataCell(Text(
                                //                     listGetDataFiltered[index]
                                //                         .MaterialDesc)),
                                //                 DataCell(Container(
                                //                   alignment: Alignment.centerRight,
                                //                   child: Text(
                                //                     listGetDataFiltered[index]
                                //                         .PlanQty,
                                //                   ),
                                //                 )),
                                //                 DataCell(Container(
                                //                   alignment: Alignment.centerRight,
                                //                   child: Text(
                                //                       listGetDataFiltered[index]
                                //                           .ResultQty,
                                //                       textAlign: TextAlign.right),
                                //                 )),
                                //                 DataCell(Text(
                                //                     listGetDataFiltered[index]
                                //                         .Location)),
                                //                 DataCell(
                                //                   Container(
                                //                     width: 5,
                                //                     child: IconButton(
                                //                       onPressed: () {
                                //                         SupplyList(
                                //                             listGetDataFiltered[
                                //                                     index]
                                //                                 .MaterialCode);
                                //                         print('onpress');
                                //                       },
                                //                       icon: Icon(
                                //                         Icons.arrow_forward_ios,
                                //                         size: 15,
                                //                       ),
                                //                       color: Colors.black,
                                //                     ),
                                //                   ),
                                //                   onTap: () {
                                //                     SupplyList(
                                //                         listGetDataFiltered[index]
                                //                             .MaterialCode);
                                //                   },
                                //                   //IconButton(),
                                //                 ),
                                //               ],
                                //             ),
                                //           )),
                                //       // ]
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ])));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0, //MediaQuery.of(context).size.width / 3.5,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 0),
      _getTitleItemWidget('Material', MediaQuery.of(context).size.width / 3.5),
      _getTitleItemWidget('Plan Qty', MediaQuery.of(context).size.width / 4.5),
      _getTitleItemWidget(
          'Result Qty', MediaQuery.of(context).size.width / 4.5),
      _getTitleItemWidget('Location', MediaQuery.of(context).size.width / 4.5),
      _getTitleItemWidget('', 20),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].materialDesc),
      width: 0,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      alignment: Alignment.centerLeft,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(listGetDataFiltered[index].materialDesc),
          ),
          width: MediaQuery.of(context).size.width / 4,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
                value.format(double.parse(listGetDataFiltered[index].planQty))),
          ),
          width: MediaQuery.of(context).size.width / 4.5,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(value
                .format(double.parse(listGetDataFiltered[index].resultQty))),
          ),
          width: MediaQuery.of(context).size.width / 4.5,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          //child: Text(listGetDataFiltered[index].location),
          width: MediaQuery.of(context).size.width / 4.5,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
          child: TextButton(
            child: Text('detail'),
            onPressed: () {
              supplyListLot(
                  txtSupplyNo.text,
                  listGetDataFiltered[index].materialCode,
                  listGetDataFiltered[index].materialDesc);
            },
          ),
        ),
        Container(
          width: 20,
          child: IconButton(
            onPressed: () {
              supplyList(listGetDataFiltered[index].materialCode,
                  listGetDataFiltered[index].materialDesc);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              size: 12,
            ),
            color: Colors.black,
            padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
            alignment: Alignment.centerRight,
          ),
        ),
      ],
    );
  }
}
