import 'package:flutter/material.dart';
import 'package:panasonic/models/clsSupplyRequest.dart';
import 'package:panasonic/services/supplyrequestservice.dart';
import 'package:panasonic/utilities/constants.dart';
// import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter/services.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/message.dart';

class SupplyRequestDetail extends StatefulWidget {
  const SupplyRequestDetail({Key key, this.userid, this.warehouse})
      : super(key: key);

  final String userid;
  final String warehouse;

  @override
  _SupplyRequestDetailState createState() => _SupplyRequestDetailState();
}

class _SupplyRequestDetailState extends State<SupplyRequestDetail> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  bool init = false;

  List<ClsSupplyRequest> listGetData = [];
  final TextEditingController txtScanBarcodeNo = new TextEditingController();
  final TextEditingController txtMaterialCode = new TextEditingController();
  final TextEditingController txtMaterialName = new TextEditingController();
  final TextEditingController txtLotNo = new TextEditingController();
  final TextEditingController txtQty = new TextEditingController();
  final TextEditingController txtUnit = new TextEditingController();
  final TextEditingController txtSupplierName = new TextEditingController();
  final TextEditingController txtExpDate = new TextEditingController();
  final TextEditingController txtLocation = new TextEditingController();

  String supplyNo = '';
  String message;
  String type;
  FocusNode nodeQty = FocusNode();
  FocusNode nodeScanBarcode = FocusNode();

  String _scanBarcode = '';
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void clearText() {
    setState(() {
      txtMaterialCode.text = "";
      txtMaterialName.text = "";
      txtLotNo.text = "";
      txtQty.text = "";
      txtUnit.text = "";
      txtExpDate.text = "";
      txtSupplierName.text = "";
      txtLocation.text = "";
    });
  }

  void asynclistGetData() async {
    init = true;
    listGetData = await SupplyRequestService.getDataScan(
        supplyNo, txtScanBarcodeNo.text, widget.warehouse);
    if (listGetData[0].id == "200") {
      setState(() {
        txtMaterialCode.text = listGetData[0].materialCode;
        txtMaterialName.text = listGetData[0].materialDesc;
        txtLotNo.text = listGetData[0].lot;
        txtQty.text = listGetData[0].planQty;
        txtUnit.text = listGetData[0].unitName;
        // final DateTime now = DateTime.parse(listGetData[0].expDate);
        // final DateFormat formatter = DateFormat('dd MMM yyyy');
        // final String formatted = formatter.format(now);

        txtExpDate.text = listGetData[0].expDate;
        txtSupplierName.text = listGetData[0].supplierName;
        txtLocation.text = listGetData[0].location;
        FocusScope.of(context).requestFocus(nodeQty);
      });
    } else {
      Message.box(
          type: "2",
          message: listGetData[0].description,
          top: 0,
          position: "0",
          context: context);
      //asyncAlert(listGetData[0].description, "2");

      clearText();
      return;
    }
  }

  void asyncSubmit(
      String barcode, String item, String lotno, String qty) async {
    listGetData = await SupplyRequestService.submit(
        supplyNo, barcode, item, lotno, qty, widget.userid, widget.warehouse);

    setState(() {
      isProcessing = true;
    });

    if (listGetData.length > 0) {
      if (listGetData[0].id == "200") {
        Message.box(
            type: "1",
            message: listGetData[0].description,
            top: 0,
            position: "0",
            context: context);
        //asyncAlert(listGetData[0].description, "3");
        clearText();
        txtScanBarcodeNo.clear();
        FocusScope.of(context).requestFocus(nodeScanBarcode);
      } else if (listGetData[0].id == "400") {
        Message.box(
            type: "2",
            message: listGetData[0].description,
            top: 0,
            position: "0",
            context: context);
        txtScanBarcodeNo.clear();
        FocusScope.of(context).requestFocus(nodeScanBarcode);
        //asyncAlert(listGetData[0].description, "2");
      } else {
        Message.box(
            type: "2",
            message: listGetData[0].description,
            top: 0,
            position: "0",
            context: context);
        txtScanBarcodeNo.clear();
        FocusScope.of(context).requestFocus(nodeScanBarcode);
        //asyncAlert(listGetData[0].description, "2");
      }
    }
  }

  void clearData() {
    txtMaterialCode.text = '';
    txtMaterialName.text = '';
    txtExpDate.text = '';
    txtLocation.text = '';
    txtLotNo.text = '';
    txtQty.text = '';
    txtSupplierName.text = '';
    txtUnit.text = '';
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final pModels = ModalRoute.of(context).settings.arguments;
    ClsSupplyRequest model = pModels;

    if (init == false) {
      supplyNo = model.supplyNo;
      txtScanBarcodeNo.text = model.labelBarcode;
      asynclistGetData();
    }
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text("Supply Detail"),
            ),
            body: Column(children: <Widget>[
              Flexible(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                          child: Form(
                            key: formkey,
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Scan Barcode No",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),

                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    autofocus: true,
                                    showCursor: true,
                                    focusNode: nodeScanBarcode,
                                    enableInteractiveSelection: false,
                                    textInputAction: TextInputAction.none,
                                    controller: txtScanBarcodeNo,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      labelText: '',
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            txtScanBarcodeNo.clear();
                                            _scanBarcode = '';
                                            clearData();
                                            FocusScope.of(context)
                                                .requestFocus(nodeScanBarcode);
                                          });
                                        },
                                        icon: Icon(_scanBarcode == ''
                                            ? Icons.cancel
                                            : Icons.cancel_outlined),
                                        color: _scanBarcode == ''
                                            ? Colors.white12.withOpacity(1)
                                            : Colors.grey[600],
                                      ),
                                    ),
                                    onChanged: (value) {
                                      _scanBarcode = value;
                                      setState(() {});
                                    },
                                    onFieldSubmitted: (value) {
                                      asynclistGetData();
                                    },
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Material Code",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtMaterialCode,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      fillColor: disabledColor,
                                      filled: true,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Material Name",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtMaterialName,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      fillColor: disabledColor,
                                      filled: true,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Lot No",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtLotNo,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      fillColor: disabledColor,
                                      filled: true,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Qty",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtQty,
                                    enabled: true,
                                    focusNode: nodeQty,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp(r"[0-9.]")),
                                      TextInputFormatter.withFunction(
                                          (oldValue, newValue) {
                                        try {
                                          final text = newValue.text;
                                          if (text.isNotEmpty)
                                            double.parse(text);
                                          return newValue;
                                        } catch (e) {}
                                        return oldValue;
                                      }),
                                    ],

                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true, signed: false),
                                    // inputFormatters: [
                                    //   FilteringTextInputFormatter.digitsOnly
                                    // ],
                                    // keyboardType: TextInputType.number,
                                    textAlign: TextAlign.right,
                                    decoration: InputDecoration(
                                      // fillColor: disabledColor,
                                      // filled: true,
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Unit",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtUnit,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      fillColor: disabledColor,
                                      filled: true,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Expired Date",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtExpDate,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      fillColor: disabledColor,
                                      filled: true,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                // Align(
                                //   alignment: Alignment.centerLeft,
                                //   child: Padding(
                                //     padding: EdgeInsets.fromLTRB(
                                //         15.0, 0.0, 0.0, 0.0),
                                //     child: Text(
                                //       "Supplier Name",
                                //       style: TextStyle(
                                //         fontFamily: 'Arial',
                                //         fontWeight: FontWeight.bold,
                                //       ),
                                //     ),
                                //   ),
                                // ),
                                // SizedBox(
                                //     height: MediaQuery.of(context).size.height *
                                //         0.0),
                                // Padding(
                                //   padding: EdgeInsets.symmetric(horizontal: 10),
                                //   child: TextFormField(
                                //     controller: txtSupplierName,
                                //     enabled: false,
                                //     decoration: InputDecoration(
                                //       border: OutlineInputBorder(),
                                //       contentPadding: new EdgeInsets.symmetric(
                                //           vertical: 10.0, horizontal: 10.0),
                                //       fillColor: disabledColor,
                                //       filled: true,
                                //     ),
                                //   ),
                                // ),
                                // SizedBox(
                                //     height: MediaQuery.of(context).size.height *
                                //         0.01),
                                // Align(
                                //   alignment: Alignment.centerLeft,
                                //   child: Padding(
                                //     padding: EdgeInsets.fromLTRB(
                                //         15.0, 0.0, 0.0, 0.0),
                                //     child: Text(
                                //       "Location",
                                //       style: TextStyle(
                                //         fontFamily: 'Arial',
                                //         fontWeight: FontWeight.bold,
                                //       ),
                                //     ),
                                //   ),
                                // ),
                                // SizedBox(
                                //     height: MediaQuery.of(context).size.height *
                                //         0.0),
                                // Padding(
                                //   padding: EdgeInsets.symmetric(horizontal: 10),
                                //   child: TextFormField(
                                //     controller: txtLocation,
                                //     enabled: false,
                                //     decoration: InputDecoration(
                                //       border: OutlineInputBorder(),
                                //       contentPadding: new EdgeInsets.symmetric(
                                //           vertical: 10.0, horizontal: 10.0),
                                //       fillColor: disabledColor,
                                //       filled: true,
                                //     ),
                                //   ),
                                // ),
                                Expanded(
                                  child: SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height),
                                ),
                                Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      TextButton(
                                        style: TextButton.styleFrom(
                                          backgroundColor: (isProcessing)
                                              ? disabledButtonColor
                                              : enabledButtonColor,
                                          minimumSize: Size(
                                              MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.90,
                                              55),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50.0),
                                          ),
                                        ),
                                        onPressed: (isProcessing == false)
                                            ? () async {
                                                //if buttonenabled == true then pass a function otherwise pass "null"
                                                asyncSubmit(
                                                    txtScanBarcodeNo.text,
                                                    txtMaterialCode.text,
                                                    txtLotNo.text,
                                                    txtQty.text);
                                                //plug delayed for wait while processing data
                                                await Future.delayed(
                                                    const Duration(seconds: 3),
                                                    () {
                                                  setState(() {
                                                    isProcessing = false;
                                                  });
                                                });
                                              }
                                            : null,
                                        child: Text(
                                          "SUBMIT",
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: (isProcessing)
                                                  ? disabledColorButtonText
                                                  : enabledColorButtonText),
                                        ),
                                      ),
                                      // ),
                                    ]),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                // Container(
                                //   alignment: Alignment.centerRight,
                                //   child: Padding(
                                //     padding: EdgeInsets.symmetric(
                                //         horizontal: 15, vertical: 15),
                                //     child: TextButton(
                                //       style: TextButton.styleFrom(
                                //         backgroundColor: Colors.blue,
                                //         minimumSize: Size(150, 40),
                                //       ),
                                //       onPressed: () {
                                //         asyncSubmit();
                                //       },
                                //       child: Text(
                                //         "Submit",
                                //         style: TextStyle(
                                //           fontSize: 16,
                                //           color: Colors.white,
                                //         ),
                                //       ),
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ])));
  }
}

// _onBasicAlertPressed(context, title, desc) {
//   Alert(
//     context: context,
//     title: title,
//     desc: desc,
//     buttons: [
//       DialogButton(
//         child: Text(
//           "OK",
//           style: TextStyle(color: Colors.white, fontSize: 20),
//         ),
//         onPressed: () => Navigator.pop(context),
//         radius: BorderRadius.circular(0.0),
//       ),
//     ],
//   ).show();
// }
