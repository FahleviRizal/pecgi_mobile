import 'package:flutter/material.dart';
import 'package:panasonic/models/clsSupplyRequest.dart';
import 'package:panasonic/services/supplyrequestservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:intl/intl.dart';

class SupplyRequestList extends StatefulWidget {
  @override
  _SupplyRequestListState createState() => _SupplyRequestListState();
}

class _SupplyRequestListState extends State<SupplyRequestList> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsSupplyRequest> listGetData = [];
  List<ClsSupplyRequest> listGetDataFiltered = [];
  List<ClsSupplyRequest> listScanBarcode = [];
  List<ClsSupplyRequest> listSupplyNo;
  List<ClsSupplyRequest> listSupplyNoFiltered;

  final TextEditingController txtSupplyNo = new TextEditingController();
  final TextEditingController txtInstructionNo = new TextEditingController();
  final TextEditingController txtItem = new TextEditingController();
  final TextEditingController txtItemName = new TextEditingController();
  final TextEditingController txtUnit = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();

  String _searchDataResult = '';
  bool init = false;

  @override
  void initState() {
    // asynclistGetData();
    super.initState();
  }

  void asynclistGetData() async {
    init = true;
    listGetData = await SupplyRequestService.getDataListDetail(
        txtSupplyNo.text, txtItem.text);
    listGetDataFiltered = listGetData;
    if (listGetDataFiltered.length > 0) {
      txtInstructionNo.text = listGetDataFiltered[0].instNo;
      txtUnit.text = listGetDataFiltered[0].unitName;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final pModels = ModalRoute.of(context).settings.arguments;
    ClsSupplyRequest model = pModels;
    txtSupplyNo.text = model.supplyNo;
    txtItem.text = model.materialCode;
    txtItemName.text = model.materialDesc;
    if (init == false) {
      asynclistGetData();
    }
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text("Supply List"),
            ),
            body: Column(children: <Widget>[
              Flexible(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                          child: Form(
                            key: formkey,
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Supply No",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtSupplyNo,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      fillColor: disabledColor,
                                      filled: true,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Instruction No",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtInstructionNo,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      fillColor: disabledColor,
                                      filled: true,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Item",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtItemName,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      fillColor: disabledColor,
                                      filled: true,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Unit",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtUnit,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      fillColor: disabledColor,
                                      filled: true,
                                    ),
                                  ),
                                ),
                                Divider(color: Colors.grey, height: 20),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                      controller: txtSearch,
                                      decoration: InputDecoration(
                                        labelText: 'Search',
                                        prefixIcon: IconButton(
                                          onPressed: () {},
                                          icon: Icon(_searchDataResult == ''
                                              ? Icons.search
                                              : Icons.search),

                                          //Icons.search,
                                        ),
                                        suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              txtSearch.clear();
                                              _searchDataResult = '';
                                              listGetDataFiltered = listGetData;
                                            });
                                          },
                                          icon: Icon(_searchDataResult == ''
                                              ? Icons.cancel
                                              : Icons.cancel_outlined),
                                          color: _searchDataResult == ''
                                              ? Colors.white12.withOpacity(1)
                                              : Colors.grey[600],
                                        ),
                                        border: OutlineInputBorder(),
                                        contentPadding:
                                            new EdgeInsets.symmetric(
                                                vertical: 10.0,
                                                horizontal: 10.0),
                                      ),
                                      onChanged: (value) {
                                        setState(() {
                                          _searchDataResult = value;
                                          listGetDataFiltered = listGetData
                                              .where((user) =>
                                                  user.barcode
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()) ||
                                                  user.lot
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()) ||
                                                  user.qty
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()))
                                              .toList();
                                        });
                                      }),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Expanded(
                                  child: Container(child: _dataTableWidget()),
                                ),
                                // SizedBox(
                                //   height: 300,
                                //   child: SingleChildScrollView(
                                //     scrollDirection: Axis.horizontal,
                                //     child: SingleChildScrollView(
                                //       scrollDirection: Axis.vertical,
                                //       child: DataTable(
                                //           columns: <DataColumn>[
                                //             DataColumn(
                                //               label: Container(
                                //                 width: 130.00,
                                //                 child: Text(
                                //                   'Barcode',
                                //                   style: TextStyle(
                                //                       fontFamily: 'Arial',
                                //                       fontWeight: FontWeight.bold),
                                //                 ),
                                //               ),
                                //             ),
                                //             DataColumn(
                                //               label: Text(
                                //                 'Lot',
                                //                 style: TextStyle(
                                //                     fontFamily: 'Arial',
                                //                     fontWeight: FontWeight.bold),
                                //               ),
                                //             ),
                                //             DataColumn(
                                //               label: Text(
                                //                 'Location',
                                //                 style: TextStyle(
                                //                     fontFamily: 'Arial',
                                //                     fontWeight: FontWeight.bold),
                                //               ),
                                //             ),
                                //             DataColumn(
                                //               label: Container(
                                //                 width: 50.00,
                                //                 child: Text(
                                //                   'Qty',
                                //                   style: TextStyle(
                                //                       fontFamily: 'Arial',
                                //                       fontWeight: FontWeight.bold),
                                //                 ),
                                //               ),
                                //             ),
                                //           ],
                                //           rows: List.generate(
                                //             listGetDataFiltered.length,
                                //             (index) => DataRow(
                                //               cells: <DataCell>[
                                //                 DataCell(Text(
                                //                     listGetDataFiltered[index]
                                //                         .Barcode)),
                                //                 DataCell(Text(
                                //                     listGetDataFiltered[index]
                                //                         .Lot)),
                                //                 DataCell(Text(
                                //                     listGetDataFiltered[index]
                                //                         .Location)),
                                //                 DataCell(
                                //                   Container(
                                //                     alignment:
                                //                         Alignment.centerRight,
                                //                     child: Text(
                                //                       listGetDataFiltered[index]
                                //                           .Qty,
                                //                       textAlign: TextAlign.right,
                                //                     ),
                                //                   ),
                                //                 ),
                                //               ],
                                //             ),
                                //           )),
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ])));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 0),
      _getTitleItemWidget('Barcode', MediaQuery.of(context).size.width / 2.5),
      _getTitleItemWidget('Lot', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 3.5),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].barcode),
      width: 0,
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      alignment: Alignment.centerLeft,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(listGetDataFiltered[index].barcode),
          width: MediaQuery.of(context).size.width / 2.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(listGetDataFiltered[index].lot),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
                value.format(double.parse(listGetDataFiltered[index].qty))),
          ),
          width: MediaQuery.of(context).size.width / 4,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
