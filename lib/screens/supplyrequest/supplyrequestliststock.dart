import 'package:flutter/material.dart';
import 'package:panasonic/models/clsSupplyRequest.dart';
import 'package:panasonic/services/supplyrequestservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:intl/intl.dart';

class SupplyRequestListStock extends StatefulWidget {
  @override
  _SupplyRequestListStockState createState() => _SupplyRequestListStockState();
}

class _SupplyRequestListStockState extends State<SupplyRequestListStock> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsSupplyRequest> listGetData = [];
  List<ClsSupplyRequest> listGetDataFiltered = [];

  final TextEditingController txtSupplyNo = new TextEditingController();
  final TextEditingController txtItem = new TextEditingController();
  final TextEditingController txtItemName = new TextEditingController();
  final TextEditingController txtSearch = new TextEditingController();
  String warehouseLogin = '';

  String _searchDataResult = '';
  bool init = false;

  @override
  void initState() {
    // asynclistGetData();
    super.initState();
  }

  void asynclistGetData(supply, item, warehouseLogin) async {
    init = true;
    listGetData =
        await SupplyRequestService.getlistStock(supply, item, warehouseLogin);
    listGetDataFiltered = listGetData;
    if (listGetDataFiltered.length > 0) {
      txtItem.text = listGetDataFiltered[0].materialCode;
      txtItemName.text = listGetDataFiltered[0].materialDesc;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final pModels = ModalRoute.of(context).settings.arguments;
    ClsSupplyRequest model = pModels;
    txtSupplyNo.text = model.supplyNo;
    txtItem.text = model.materialCode;
    txtItemName.text = model.materialDesc;
    warehouseLogin = model.warehouselogin;
    if (init == false) {
      asynclistGetData(
          model.supplyNo, model.materialCode, model.warehouselogin);
    }
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              flexibleSpace: kAppBarColor,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text("Detail Lot"),
            ),
            body: Column(children: <Widget>[
              Flexible(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                          child: Form(
                            key: formkey,
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text(
                                      "Material",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                    controller: txtItemName,
                                    enabled: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      fillColor: disabledColor,
                                      filled: true,
                                    ),
                                  ),
                                ),
                                Divider(color: Colors.grey, height: 20),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: TextFormField(
                                      controller: txtSearch,
                                      decoration: InputDecoration(
                                        labelText: 'Search',
                                        prefixIcon: IconButton(
                                          onPressed: () {},
                                          icon: Icon(_searchDataResult == ''
                                              ? Icons.search
                                              : Icons.search),

                                          //Icons.search,
                                        ),
                                        suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              txtSearch.clear();
                                              _searchDataResult = '';
                                              listGetDataFiltered = listGetData;
                                            });
                                          },
                                          icon: Icon(_searchDataResult == ''
                                              ? Icons.cancel
                                              : Icons.cancel_outlined),
                                          color: _searchDataResult == ''
                                              ? Colors.white12.withOpacity(1)
                                              : Colors.grey[600],
                                        ),
                                        border: OutlineInputBorder(),
                                        contentPadding:
                                            new EdgeInsets.symmetric(
                                                vertical: 10.0,
                                                horizontal: 10.0),
                                      ),
                                      onChanged: (value) {
                                        setState(() {
                                          _searchDataResult = value;
                                          listGetDataFiltered = listGetData
                                              .where((user) =>
                                                  user.barcode
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()) ||
                                                  user.lot
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()) ||
                                                  user.qty
                                                      .toLowerCase()
                                                      .contains(
                                                          _searchDataResult
                                                              .toLowerCase()))
                                              .toList();
                                        });
                                      }),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Expanded(
                                  child: Container(child: _dataTableWidget()),
                                ),
                              ],
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ])));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: MediaQuery.of(context).size.width / 3,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: listGetDataFiltered.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Warehouse', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Location', MediaQuery.of(context).size.width / 3),
      _getTitleItemWidget('Lot No', MediaQuery.of(context).size.width / 2.5),
      _getTitleItemWidget('Qty', MediaQuery.of(context).size.width / 3.5),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(listGetDataFiltered[index].warehouse),
      width: MediaQuery.of(context).size.width / 3,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  final value = new NumberFormat("#,##0.00", "en_US");
  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(listGetDataFiltered[index].location),
          width: MediaQuery.of(context).size.width / 3,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Text(listGetDataFiltered[index].lot),
          width: MediaQuery.of(context).size.width / 2.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.center,
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
                value.format(double.parse(listGetDataFiltered[index].qty))),
          ),
          width: MediaQuery.of(context).size.width / 4,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
