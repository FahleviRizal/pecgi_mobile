// import 'dart:ui';
// ignore_for_file: unused_field, unnecessary_cast

import 'dart:async';
import 'dart:typed_data';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:panasonic/models/clsSyncLamp.dart';
import 'package:panasonic/models/clsTowerLampInformation.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/message.dart';
import 'package:usb_serial/transaction.dart';
import 'package:usb_serial/usb_serial.dart';

class TowerLampInformation extends StatefulWidget {
  const TowerLampInformation({Key key, this.userid}) : super(key: key);

  final String userid;

  @override
  _TowerLampInformationState createState() => _TowerLampInformationState();
}

class _TowerLampInformationState extends State<TowerLampInformation> {
  BuildContext dialogContext;
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  List<ClsTowerLampInformation> listGetData = [];
  String message;
  FocusNode nodeScanCarryNo = FocusNode();
  var showdialog;
  final TextEditingController txtBattery = new TextEditingController();
  final TextEditingController txtCarryDesc = new TextEditingController();
  final TextEditingController txtCarryNo = new TextEditingController();
  final TextEditingController txtChannelNo = new TextEditingController();
  final TextEditingController txtFactory = new TextEditingController();
  final TextEditingController txtLastUpdate = new TextEditingController();
  final TextEditingController txtLocation = new TextEditingController();
  final TextEditingController txtMode = new TextEditingController();
  final TextEditingController txtTowerLampDesc = new TextEditingController();
  final TextEditingController txtTowerLampID = new TextEditingController();
  final TextEditingController txtWarehouse = new TextEditingController();
  String type;
  String _scanCarry = '';

  String __cmdReturn = "";
  String __datereceive = "";
  String __datesend = "";
  UsbDevice __device;
  String __ipaddress = "";
  String __lampID = "";
  UsbPort __port;
  List<Widget> __ports = [];
  String __remarks = "";
  List<Widget> __serialData = [];
  String __status = "Idle";
  bool __statuscall = false;
  bool __statuspercentage = false;
  Timer __timer;
  Transaction<String> __transaction;
  String __valuepercentage = "";
  String __valuevolt = "";
  StreamSubscription<String> _subscription;
  bool _visibleprogress = true;

  @override
  void dispose() {
    __timer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    // UsbSerial.usbEventStream.listen((UsbEvent event) {
    //   __getPorts();
    //   __loadConnect();
    // });

    super.initState();
  }

  void asynclistGetData() async {
    listGetData = await ClsTowerLampInformation.getData(txtCarryNo.text);

    if (listGetData.length == 0) {
      Message.box(
          type: "2",
          message: "Data not found",
          top: 45,
          position: "0",
          context: context);
      // message = "Data not found";
      // type = '2';
      // asyncAlert(message, type);
      FocusScope.of(context).requestFocus(nodeScanCarryNo);
      return;
    } else {
      final controller = TextEditingController();
      showDialog(
        context: context,
        builder: (_) => MyDialog(controller),
      );
      txtCarryDesc.text = listGetData[0].carryName;
      txtFactory.text = listGetData[0].factoryName;
      txtWarehouse.text = listGetData[0].warehouseName;
      txtLocation.text = listGetData[0].locationName;
      txtTowerLampID.text = listGetData[0].towerLampCode;
      txtTowerLampDesc.text = listGetData[0].towerLampDesc;
      txtChannelNo.text = listGetData[0].channelNo;
      txtBattery.text = listGetData[0].battery;
      txtChannelNo.text = listGetData[0].channelNo;
      txtMode.text = listGetData[0].mode;
      txtLastUpdate.text = listGetData[0].lastUpdate;

      // if (listGetData[0].statusLamp == "Stand By") {
      //   __statuscall = true;
      //   int _timer = 8000;
      //   List<ClsSyncLamp> _listgetdatatimer = [];
      //   _listgetdatatimer = await ClsSyncLamp.timer();
      //   if (_listgetdatatimer.length > 0) {
      //     __timer = Timer.periodic(Duration(milliseconds: _timer),
      //         (Timer t5) => __setVoltPercentage("Percentage"));
      //   }
      // } else {
    Future.delayed(const Duration(milliseconds: 300), () {
         Navigator.of(context).pop();
          return;
    });
      // }
    }
    setState(() {});
  }

  Future<void> loadProgress() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return Center(
            child: CircularProgressIndicator(),
          );
        });

    // showDialog(
    //     context: context,
    //     builder: (BuildContext context) {
    //       return Center(
    //         child: CircularProgressIndicator(),
    //       );
    //     });

    // ignore: missing_return
    // FutureBuilder(builder: (context, snapshot) {
    //   if (_visibleprogress == true) {
    //     showDialog(
    //         context: context,
    //         builder: (BuildContext context) {
    //           return CircularProgressIndicator();
    //         });
    //   }
    //   if (_visibleprogress == false) {
    //     Navigator.pop(context);
    //   }
    // });

    // builder: (context, snapshot) {
    //   if (visibleprogress == true) {
    //     showDialog(
    //         context: context,
    //         builder: (BuildContext context) {
    //           return CircularProgressIndicator();
    //         });
    //   }
    //   if (snapshot.connectionState == ConnectionState.done) {
    //     Navigator.pop(context);
    //     }
    //   }
    // },
    // )
    // showDialog(
    //     context: context,
    //     builder: (BuildContext context) {
    //       return Center(
    //         child: CircularProgressIndicator(),
    //       );
    //     });
  }

  // void returna() async {
  //   // Navigator.of(dialogContext);
  //   if (_visibleprogress == true) {
  //     setState(() {
  //       _visibleprogress = false;
  //     });
  //   } else {
  //     setState(() {
  //       _visibleprogress = true;
  //     });
  //   }
  //   // return;
  // }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  // void __getPorts() async {
  //   __ports = [];
  //   bool __flagConnect = true;

  //   List<UsbDevice> _devices = await UsbSerial.listDevices();
  //   if (!_devices.contains(__device)) {
  //     __connectTo(null);
  //   }
  //   for (var _device in _devices) {
  //     __ports.add(ListTile(
  //         leading: const Icon(Icons.usb),
  //         title: Text('Product : ' + _device.productName),
  //         subtitle: Text('Dev ID : ' + _device.deviceId.toString()),
  //         trailing: ElevatedButton.icon(
  //           label: Text(__device == _device ? "Disconnect" : "Connect"),
  //           icon: Icon(
  //             __device == _device ? Icons.sensors_off : Icons.sensors,
  //             color: __device == _device ? Colors.white : Colors.yellow,
  //             size: 20.0,
  //           ),
  //           style: ElevatedButton.styleFrom(
  //             primary: Colors.indigo,
  //             onPrimary: __device == _device ? Colors.white : Colors.yellow,
  //             textStyle: TextStyle(
  //               color: Colors.blue,
  //               fontSize: 12,
  //             ),
  //             shape: RoundedRectangleBorder(
  //                 borderRadius: BorderRadius.circular(32.0)),
  //             minimumSize: Size(130, 40),
  //           ),
  //           onPressed: () {
  //             __flagConnect = !__flagConnect;

  //             __connectTo(__device == _device ? null : _device).then((res) {
  //               __getPorts();
  //             });
  //           },
  //         )));

  //     __flagConnect = !__flagConnect;

  //     __connectTo(__device == _device ? null : _device).then((res) {
  //       // _getPorts();
  //     });
  //     // }

  //   }

  //   setState(() {
  //     // print(_ports);
  //   });
  // }

  // Future<bool> __connectTo(device) async {
  //   __serialData.clear();

  //   if (_subscription != null) {
  //     _subscription.cancel();
  //     _subscription = null;
  //   }

  //   if (__transaction != null) {
  //     __transaction.dispose();
  //     __transaction = null;
  //   }

  //   if (__port != null) {
  //     __port.close();
  //     __port = null;
  //   }

  //   if (device == null) {
  //     __device = null;
  //     setState(() {});
  //     return true;
  //   }

  //   __port = await device.create();
  //   if (await (__port.open()) != true) {
  //     setState(() {});
  //     return false;
  //   }
  //   __device = device;

  //   await __port.setDTR(true);
  //   await __port.setRTS(true);
  //   await __port.setPortParameters(
  //       9600, UsbPort.DATABITS_8, UsbPort.STOPBITS_1, UsbPort.PARITY_NONE);

  //   __transaction = Transaction.stringTerminated(
  //       __port.inputStream as Stream<Uint8List>, Uint8List.fromList([13, 10]));

  //   _subscription = __transaction.stream.listen((String line) {
  //     setState(() async {
  //       __cmdReturn = line;
  //       String dataFeedback = __lampID + ":VB";

  //       if ((__cmdReturn.contains(dataFeedback) == true &&
  //               __cmdReturn.contains("%") == true) &&
  //           __statuspercentage == false) {
  //         __statuspercentage = true;

  //         __valuepercentage = __cmdReturn
  //             .substring(9)
  //             .substring(0, __cmdReturn.substring(9).length - 1);

  //         __timer.cancel();

  //         if (__datesend != "") {
  //           __datesend = "";
  //           DateTime now = DateTime.now();
  //           __datereceive = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

  //           // ignore: unused_local_variable
  //           List<ClsSyncLamp> _listinsdatalamp = [];
  //           _listinsdatalamp = await ClsSyncLamp.insTowerlampHistory(
  //               txtCarryNo.text,
  //               __lampID,
  //               __ipaddress,
  //               "1",
  //               __cmdReturn,
  //               __datereceive,
  //               widget.userid);
  //         }

  //         int _timer = 8000;
  //         List<ClsSyncLamp> _listgetdatatimer = [];
  //         _listgetdatatimer = await ClsSyncLamp.timer();
  //         if (_listgetdatatimer.length > 0) {
  //           // __timer = Timer.periodic(Duration(milliseconds: _timer),
  //           //     (Timer t5) => __setVoltPercentage("Percentage"));
  //           __timer = Timer.periodic(Duration(milliseconds: _timer),
  //               (Timer t6) => __setVoltPercentage("Volt"));
  //         }

  //         // __timer = Timer.periodic(Duration(milliseconds: 1500),
  //         //     (Timer t6) => __setVoltPercentage("Volt"));
  //       }
  //       if ((__cmdReturn.contains(dataFeedback) == true &&
  //               __cmdReturn.contains("%") == false) &&
  //           __statuspercentage == true) {
  //         __valuevolt = __cmdReturn.substring(9);

  //         __timer.cancel();

  //         __statuspercentage = false;
  //         __statuscall = false;

  //         if (__datesend != "") {
  //           __datesend = "";
  //           DateTime now = DateTime.now();
  //           __datereceive = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

  //           // ignore: unused_local_variable
  //           List<ClsSyncLamp> _listinsdatalamp = [];
  //           _listinsdatalamp = await ClsSyncLamp.insTowerlampHistory(
  //               txtCarryNo.text,
  //               __lampID,
  //               __ipaddress,
  //               "1",
  //               __cmdReturn,
  //               __datereceive,
  //               widget.userid);

  //           List<ClsSyncLamp> _listupdmscarry = [];
  //           _listupdmscarry = await ClsSyncLamp.updMSCarry('2', txtCarryNo.text,
  //               '', __valuepercentage, __valuevolt, '', '', widget.userid);

  //           setState(() {
  //             txtBattery.text = __valuepercentage + " %";
  //             txtLastUpdate.text = _listupdmscarry[0].lastUpdate;
  //             Navigator.of(context).pop();
  //           });
  //         }
  //       }

  //       if (__serialData.length > 20) {
  //         __serialData.removeAt(0);
  //       }
  //     });
  //   });

  //   setState(() {
  //     __status = "Connected";
  //   });
  //   return true;
  // }

  // void __loadConnect() async {
  //   List<UsbDevice> devices = await UsbSerial.listDevices();
  //   bool __flagConnect = true;
  //   for (var device in devices) {
  //     __flagConnect = !__flagConnect;
  //     print('onPressed - connect to device');
  //     print(__flagConnect);
  //     __connectTo(__device == device ? null : device).then((res) {});
  //   }
  //   __getPorts();
  // }

  // Future<void> __setVoltPercentage(_flag) async {
  //   String data = "";
  //   if (__port == null) {
  //     UsbSerial.usbEventStream.listen((UsbEvent event) {
  //       __getPorts();
  //     });
  //     __getPorts();
  //     return;
  //   }

  //   if (__statuscall == true) {
  //     final info = NetworkInfo();
  //     __ipaddress = await info.getWifiIP();
  //     __lampID = txtTowerLampID.text;

  //     if (_flag == "Percentage") {
  //       data = __lampID + ":CHKBTTP";
  //       __remarks = __lampID + ":CHKBTTP";
  //     } else if (_flag == "Volt") {
  //       data = __lampID + ":CHKBTT";
  //       __remarks = __lampID + ":CHKBTT";
  //     }

  //     data = data + "\r\n";

  //     if (data.length >= 12 && data.contains(':')) {
  //       __cmdReturn = "";
  //     } else {
  //       __cmdReturn = "";
  //     }

  //     await __port.write(Uint8List.fromList(data.codeUnits));

  //     if (__datesend == "") {
  //       __datereceive = "";
  //       DateTime now = DateTime.now();
  //       __datesend = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

  //       // ignore: unused_local_variable
  //       List<ClsSyncLamp> _listinsdatalamp = [];
  //       _listinsdatalamp = await ClsSyncLamp.insTowerlampHistory(
  //           txtCarryNo.text,
  //           __lampID,
  //           __ipaddress,
  //           "0",
  //           __remarks,
  //           __datesend,
  //           widget.userid);
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          flexibleSpace: kAppBarColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Tower Lamp Information"),
        ),
        body: Column(children: <Widget>[
          Flexible(
            flex: 1,
            child: Row(
              children: <Widget>[
                // FutureBuilder(builder: (context, snapshot) {
                //   if (_visibleprogress == true) {
                //     showDialog(
                //         context: context,
                //         builder: (BuildContext context) {
                //           return CircularProgressIndicator();
                //         });
                //   }
                //   if (_visibleprogress == false) {
                //     Navigator.pop(context);
                //   }
                // }),
                // Visibility(
                //     maintainSize: true,
                //     maintainAnimation: true,
                //     maintainState: true,
                //     visible: visibleprogress,
                //     child: Container(
                //         margin: EdgeInsets.only(top: 50, bottom: 30),
                //         child: CircularProgressIndicator())),
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Form(
                        key: formkey,
                        child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  "Scan Label Carry",
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: TextFormField(
                                autofocus: true,
                                showCursor: true,
                                enableInteractiveSelection: false,
                                textInputAction: TextInputAction.none,
                                controller: txtCarryNo,
                                focusNode: nodeScanCarryNo,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  labelText: '',
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        txtCarryNo.clear();
                                        _scanCarry = '';
                                        txtCarryDesc.text = '';
                                        txtFactory.text = '';
                                        txtWarehouse.text = '';
                                        txtLocation.text = '';
                                        txtTowerLampID.text = '';
                                        txtTowerLampDesc.text = '';
                                        txtChannelNo.text = '';
                                        txtBattery.text = '';
                                        txtMode.text = '';
                                        txtLastUpdate.text = '';

                                        FocusScope.of(context)
                                            .requestFocus(nodeScanCarryNo);
                                      });
                                    },
                                    icon: Icon(_scanCarry == ''
                                        ? Icons.cancel
                                        : Icons.cancel_outlined),
                                    color: _scanCarry == ''
                                        ? Colors.white12.withOpacity(1)
                                        : Colors.grey[600],
                                  ),
                                ),
                                onFieldSubmitted: (value) {
                                  asynclistGetData();
                                },
                                onChanged: (value) {
                                  setState(() {
                                    _scanCarry = value;
                                    txtCarryDesc.text = '';
                                    txtFactory.text = '';
                                    txtWarehouse.text = '';
                                    txtLocation.text = '';
                                    txtTowerLampID.text = '';
                                    txtTowerLampDesc.text = '';
                                    txtChannelNo.text = '';
                                    txtBattery.text = '';
                                    txtMode.text = '';
                                    txtLastUpdate.text = '';
                                  });
                                },
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Container(
                              color: Colors.white,
                              child: (Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Divider(color: Colors.black),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0),
                                  child: Container(
                                    margin: EdgeInsets.all(0.0),
                                    child: Text(
                                      "Carry",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 15.0, 0.0),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      margin: EdgeInsets.all(0.0),
                                      child: Text(
                                        txtCarryDesc.text,
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                        ),
                                      )),
                                ),
                              ),
                            ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Container(
                              color: Colors.white,
                              child: (Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Divider(color: Colors.black),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0),
                                  child: Container(
                                    margin: EdgeInsets.all(0.0),
                                    child: Text(
                                      "Factory",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 15.0, 0.0),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      margin: EdgeInsets.all(0.0),
                                      child: Text(
                                        txtFactory.text,
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                        ),
                                      )),
                                ),
                              ),
                            ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Container(
                              color: Colors.white,
                              child: (Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Divider(color: Colors.black),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0),
                                  child: Container(
                                    margin: EdgeInsets.all(0.0),
                                    child: Text(
                                      "Warehouse",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 15.0, 0.0),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      margin: EdgeInsets.all(0.0),
                                      child: Text(
                                        txtWarehouse.text,
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                        ),
                                      )),
                                ),
                              ),
                            ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Container(
                              color: Colors.white,
                              child: (Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Divider(color: Colors.black),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0),
                                  child: Container(
                                    margin: EdgeInsets.all(0.0),
                                    child: Text(
                                      "Location",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 15.0, 0.0),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      margin: EdgeInsets.all(0.0),
                                      child: Text(
                                        txtLocation.text,
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                        ),
                                      )),
                                ),
                              ),
                            ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Container(
                              color: Colors.white,
                              child: (Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Divider(color: Colors.black),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0),
                                  child: Container(
                                    margin: EdgeInsets.all(0.0),
                                    child: Text(
                                      "Tower Lamp ID",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 15.0, 0.0),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      margin: EdgeInsets.all(0.0),
                                      child: Text(
                                        txtTowerLampID.text,
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                        ),
                                      )),
                                ),
                              ),
                            ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Container(
                              color: Colors.white,
                              child: (Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Divider(color: Colors.black),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0),
                                  child: Container(
                                    margin: EdgeInsets.all(0.0),
                                    child: Text(
                                      "Description",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 15.0, 0.0),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      margin: EdgeInsets.all(0.0),
                                      child: Text(
                                        txtTowerLampDesc.text,
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                        ),
                                      )),
                                ),
                              ),
                            ]),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.01),
                            // Container(
                            //   color: Colors.white,
                            //   child: (Row(
                            //     children: <Widget>[
                            //       Expanded(
                            //         child: Column(
                            //           children: <Widget>[
                            //             Padding(
                            //               padding: EdgeInsets.symmetric(
                            //                   horizontal: 15),
                            //               child: Divider(color: Colors.black),
                            //             )
                            //           ],
                            //         ),
                            //       )
                            //     ],
                            //   )),
                            // ),
                            // SizedBox(
                            //     height:
                            //         MediaQuery.of(context).size.height * 0.01),
                            // Row(children: [
                            //   Container(
                            //     width: MediaQuery.of(context).size.width * 0.5,
                            //     child: Padding(
                            //       padding: const EdgeInsets.fromLTRB(
                            //           15.0, 0.0, 0.0, 0.0),
                            //       child: Container(
                            //         margin: EdgeInsets.all(0.0),
                            //         child: Text(
                            //           "Channel No",
                            //           style: TextStyle(
                            //             fontFamily: 'Arial',
                            //             fontWeight: FontWeight.bold,
                            //           ),
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            //   Container(
                            //     width: MediaQuery.of(context).size.width * 0.5,
                            //     child: Padding(
                            //       padding: const EdgeInsets.fromLTRB(
                            //           0.0, 0.0, 15.0, 0.0),
                            //       child: Container(
                            //           width: MediaQuery.of(context).size.width *
                            //               0.5,
                            //           margin: EdgeInsets.all(0.0),
                            //           child: Text(
                            //             txtChannelNo.text,
                            //             style: TextStyle(
                            //               fontFamily: 'Arial',
                            //             ),
                            //           )),
                            //     ),
                            //   ),
                            // ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Container(
                              color: Colors.white,
                              child: (Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Divider(color: Colors.black),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0),
                                  child: Container(
                                    margin: EdgeInsets.all(0.0),
                                    child: Text(
                                      "Battery (%)",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 15.0, 0.0),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      margin: EdgeInsets.all(0.0),
                                      child: Text(
                                        txtBattery.text,
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                        ),
                                      )),
                                ),
                              ),
                            ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Container(
                              color: Colors.white,
                              child: (Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Divider(color: Colors.black),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0),
                                  child: Container(
                                    margin: EdgeInsets.all(0.0),
                                    child: Text(
                                      "Mode",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 15.0, 0.0),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      margin: EdgeInsets.all(0.0),
                                      child: Text(
                                        txtMode.text,
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                        ),
                                      )),
                                ),
                              ),
                            ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Container(
                              color: Colors.white,
                              child: (Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Divider(color: Colors.black),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0),
                                  child: Container(
                                    margin: EdgeInsets.all(0.0),
                                    child: Text(
                                      "Last Update",
                                      style: TextStyle(
                                        fontFamily: 'Arial',
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 15.0, 0.0),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      margin: EdgeInsets.all(0.0),
                                      child: Text(
                                        txtLastUpdate.text,
                                        style: TextStyle(
                                          fontFamily: 'Arial',
                                        ),
                                      )),
                                ),
                              ),
                            ]),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            Container(
                              color: Colors.white,
                              child: (Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Divider(color: Colors.black),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                            ),
                            Expanded(
                              child: SizedBox(
                                  height: MediaQuery.of(context).size.height),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01),
                            // Container(
                            //   alignment: Alignment.centerRight,
                            //   child: Padding(
                            //     padding: EdgeInsets.symmetric(
                            //         horizontal: 15, vertical: 15),
                            //     child: TextButton(
                            //       style: TextButton.styleFrom(
                            //         backgroundColor: Colors.blue,
                            //         minimumSize: Size(150, 40),
                            //       ),
                            //       onPressed: () {
                            //         asyncSubmit();
                            //       },
                            //       child: Text(
                            //         "Submit",
                            //         style: TextStyle(
                            //           fontSize: 16,
                            //           color: Colors.white,
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ]));
  }
}

class MyDialog extends StatelessWidget {
  MyDialog(this.controller);

  final TextEditingController controller;

  void showAlert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 5), () {
      return;
    });
    // Future.delayed(Duration.zero, () => showAlert(context));
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}
