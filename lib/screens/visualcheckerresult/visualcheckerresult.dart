import 'package:flutter/material.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:panasonic/models/clsvisualcheckerresult.dart';
import 'package:panasonic/services/visualcheckerresultservice.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/utilities/message.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Visual Checker Result',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class VisualCheckerResult extends StatefulWidget {
  const VisualCheckerResult({Key key, this.userid}) : super(key: key);

  final String userid;

  @override
  _VisualCheckerResult createState() => _VisualCheckerResult();
}

class _VisualCheckerResult extends State<VisualCheckerResult> {
  VisualCheckerResultService api = VisualCheckerResultService();
  TextEditingController iCarryNo = TextEditingController();
  TextEditingController iAssessmentAging = TextEditingController();
  TextEditingController iAssessmentAgingDay = TextEditingController();
  TextEditingController iAssessmentAgingHour = TextEditingController();

  TextEditingController iAssessmentCooling = TextEditingController();
  TextEditingController iAssessmentCoolingDay = TextEditingController();
  TextEditingController iAssessmentCoolingHour = TextEditingController();

  TextEditingController iAssessmentDate = TextEditingController();
  TextEditingController iRemarks = TextEditingController();
  TextEditingController iScanSublot = TextEditingController();
  TextEditingController iSearch = TextEditingController();

  String _ddlFilterResult = '';
  String _searchResult = '';
  String _searchcombo = '';
  String _scanSublot = '';

  List<ClsVisualCheckerResult> searchdata = [
    // ClsVisualCheckerResult(sublotNo: "Sublot 1", resultQty: "10", ngQty: "0"),
    // ClsVisualCheckerResult(sublotNo: "Sublot 2", resultQty: "10", ngQty: "5"),
    // ClsVisualCheckerResult(sublotNo: "Sublot 3", resultQty: "20", ngQty: "0"),
    // ClsVisualCheckerResult(sublotNo: "Sublot 4", resultQty: "60", ngQty: "10"),
    // ClsVisualCheckerResult(sublotNo: "Sublot 5", resultQty: "1", ngQty: "0"),
  ];
  List<ClsVisualCheckerResult> listH = [];

  List<ClsVisualCheckerResult> ddlCarryNo = [];
  List<ClsVisualCheckerResult> ddlCarryNoFilter = [];

  FocusNode node = FocusNode();
  FocusNode nodeSublot = FocusNode();

  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
    // iAssessmentAging.text = "Aging";
    // iAssessmentAgingDay.text = "0 Days";
    // iAssessmentAgingHour.text = "07:00 Hours";

    // iAssessmentCooling.text = "Cooling";
    // iAssessmentCoolingDay.text = "1 Days";
    // iAssessmentCoolingHour.text = "05:00 Hours";
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void asyncGetData(String carryNo) async {
    List<ClsVisualCheckerResult> list = [];

    list = await api.getData(carryNo);
    setState(() {
      if (list.length > 0) {
        iAssessmentAging.text = list[0].assessmentAging.toString();
        iAssessmentAgingDay.text = list[0].assessmentAgingDays.toString();
        iAssessmentAgingHour.text = list[0].assessmentAgingHours.toString();

        iAssessmentCooling.text = list[0].assessmentCooling.toString();
        iAssessmentCoolingDay.text = list[0].assessmentCoolingDays.toString();
        iAssessmentCoolingHour.text = list[0].assessmentCoolingHours.toString();

        iAssessmentDate.text = list[0].assessmentDate.toString();
        iRemarks.text = list[0].remarks.toString();
      } else {
        iAssessmentAging.text = "";
        iAssessmentAgingDay.text = "";
        iAssessmentAgingHour.text = "";
        iAssessmentCooling.text = "";
        iAssessmentCoolingDay.text = "";
        iAssessmentCoolingHour.text = "";

        iAssessmentDate.text = "";
        iRemarks.text = "";

        asyncAlert("Carry No is not found", "4");
        iCarryNo.text = "";
      }
    });
  }

  void asyncGetList(String carryNo, String sublot) async {
    listH = await api.getList(carryNo, sublot);
    searchdata = listH;
  }

  void asyncSubmit(String carryNo, String sublot) async {
    try {
      setState(() {
        isProcessing = true;
      });

      if (carryNo == "") {
        Message.box(
            type: "2",
            message: "Pleasae scan carry",
            top: 0,
            position: "0",
            context: context);
        return;
      }

      if (sublot == "") {
        Message.box(
            type: "2",
            message: "Pleasae scan sublot",
            top: 0,
            position: "0",
            context: context);
        return;
      }

      String message = "";

      message = await api.submit(carryNo, sublot, widget.userid);
      Message.box(
          type: (message == "success") ? "1" : "2",
          message: message,
          top: 0,
          position: "0",
          context: context);
      return;
      // var dialog = CustomAlertDialog(
      //   type: (message == "success") ? "3" : "2",
      //   title: "",
      //   message: (message == "success") ? "Data saved successfully!" : message,
      //   okBtnText: 'Close',
      // );
      // showDialog(context: context, builder: (BuildContext context) => dialog);
    } catch (e) {
      var dialog = CustomAlertDialog(
        type: "2",
        title: "",
        message: e,
        okBtnText: 'Close',
      );
      showDialog(context: context, builder: (BuildContext context) => dialog);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          //resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: kAppBarColor,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text("Visual Checker Result"),
          ),
          body: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: IntrinsicHeight(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: [
                  SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "1. Select/Scan Carry",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TypeAheadField(
                      hideSuggestionsOnKeyboardHide: false,
                      textFieldConfiguration: TextFieldConfiguration(
                          textInputAction: TextInputAction.done,
                          onSubmitted: (value) {
                            iCarryNo.text = value;
                            _searchcombo = '';
                            setState(() {
                              searchdata
                                  .where((result) => result.carryNo
                                      .toLowerCase()
                                      .contains(value.toLowerCase()))
                                  .toList();

                              if (searchdata.length > 0) {
                                asyncGetData(value);
                                FocusScope.of(context).requestFocus(nodeSublot);
                              }
                            });
                          },
                          focusNode: node,
                          controller: iCarryNo,
                          style: TextStyle(fontFamily: 'Arial'),
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              onPressed: () {
                                setState(() {
                                  iCarryNo.clear();
                                  iAssessmentAging.text = '';
                                  iAssessmentAgingDay.text = '';
                                  iAssessmentAgingHour.text = '';
                                  iAssessmentCooling.text = '';
                                  iAssessmentCoolingDay.text = '';
                                  iAssessmentCoolingHour.text = '';
                                  iAssessmentDate.text = '';
                                  iRemarks.text = '';

                                  searchdata.clear();
                                  _searchcombo = '';
                                });
                              },
                              icon: Icon(_searchcombo == ''
                                  ? Icons.cancel
                                  : Icons.cancel_outlined),
                              color: _searchcombo == ''
                                  ? Colors.white12.withOpacity(1)
                                  : Colors.grey[600],
                            ),
                            border: OutlineInputBorder(),
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: -10.0, horizontal: 10.0),
                          )),
                      suggestionsCallback: (pattern) async {
                        ddlCarryNo = await api.getCarryNo();
                        _ddlFilterResult = pattern;
                        _searchcombo = pattern;
                        ddlCarryNoFilter = ddlCarryNo
                            .where((result) => result.carryNo
                                .toLowerCase()
                                .contains(_ddlFilterResult.toLowerCase()))
                            .toList();
                        return ddlCarryNoFilter;
                      },
                      itemBuilder: (context, suggestion) {
                        print(suggestion);
                        return ListTile(
                          title: Text(suggestion.carryNo),
                          subtitle: Text('${suggestion.carryName}'),
                        );
                      },
                      onSuggestionSelected: (suggestion) {
                        _searchcombo = suggestion.carryNo;
                        iCarryNo.text = suggestion.carryNo;
                        asyncGetData(suggestion.carryNo);
                        FocusScope.of(context).requestFocus(nodeSublot);
                      },
                    ),
                  ),
                  SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Assessment",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              iAssessmentAging.text,
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.normal),
                            )),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              iAssessmentAgingDay.text,
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.normal),
                            )),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              iAssessmentAgingHour.text,
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.normal),
                            )),
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              iAssessmentCooling.text,
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.normal),
                            )),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              iAssessmentCoolingDay.text,
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.normal),
                            )),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              iAssessmentCoolingHour.text,
                              style: TextStyle(
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.normal),
                            )),
                      ),
                    ],
                  ),
                  Divider(
                    height: 10,
                    color: Colors.grey,
                    indent: 10,
                    endIndent: 10,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Assessment Date",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iAssessmentDate,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Remarks",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: iRemarks,
                      enabled: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        filled: true,
                        fillColor: disabledColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "2. Scan Sub LOT",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      scrollPadding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).viewInsets.bottom),
                      controller: iScanSublot,
                      enableInteractiveSelection: false,
                      textInputAction: TextInputAction.none,
                      focusNode: nodeSublot,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        labelText: '',
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              iScanSublot.clear();
                              searchdata.clear();
                              _scanSublot = '';
                            });
                          },
                          icon: Icon(_scanSublot == ''
                              ? Icons.cancel
                              : Icons.cancel_outlined),
                          color: _scanSublot == ''
                              ? Colors.white12.withOpacity(1)
                              : Colors.grey[600],
                        ),
                      ),
                      onChanged: (value) {
                        _scanSublot = value;
                        setState(() {});
                      },
                      onEditingComplete: () {
                        asyncGetList(iCarryNo.text, iScanSublot.text);
                      },
                    ),
                  ),
                  //SizedBox(height: 10),
                  Divider(
                    height: 10,
                    color: Colors.grey,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      scrollPadding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).viewInsets.bottom),
                      controller: iSearch,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        labelText: 'Search',
                        prefixIcon: IconButton(
                          onPressed: () {},
                          icon: Icon(_searchResult == ''
                              ? Icons.search
                              : Icons.search),
                          //Icons.search,
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              iSearch.clear();
                              _searchResult = '';
                              searchdata = listH;
                            });
                          },
                          icon: Icon(_searchResult == ''
                              ? Icons.cancel
                              : Icons.cancel_outlined),
                          color: _searchResult == ''
                              ? Colors.white12.withOpacity(1)
                              : Colors.grey[600],
                        ),
                      ),
                      onChanged: (value) {
                        setState(() {
                          if (value != null) {
                            _searchResult = value;
                            searchdata = listH
                                .where((rec) =>
                                    rec.sublotNo.toUpperCase().contains(
                                        _searchResult.toUpperCase()) ||
                                    rec.resultQty.toUpperCase().contains(
                                        _searchResult.toUpperCase()) ||
                                    rec.ngQty
                                        .toUpperCase()
                                        .contains(_searchResult.toUpperCase()))
                                .toList();
                          } else {
                            searchdata = listH;
                          }
                        });
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(height: 225, child: _dataTableWidget()),
                  Divider(
                    height: 10,
                    color: Colors.grey,
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(vertical: 0),
                      child: Container(
                        padding: EdgeInsets.only(top: 0),
                        width: MediaQuery.of(context).size.width * 0.90,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: FloatingActionButton.extended(
                          backgroundColor: (isProcessing)
                              ? disabledButtonColor
                              : enabledButtonColor,
                          onPressed: (isProcessing == false)
                              ? () async {
                                  //if buttonenabled == true then pass a function otherwise pass "null"
                                  asyncSubmit(iCarryNo.text, iScanSublot.text);
                                  //plug delayed for wait while processing data
                                  await Future.delayed(
                                      const Duration(seconds: 3), () {
                                    setState(() {
                                      isProcessing = false;
                                    });
                                  });
                                }
                              : null,
                          elevation: 0,
                          label: Text(
                            "SUBMIT",
                            style: TextStyle(
                                fontSize: 18.0,
                                color: (isProcessing)
                                    ? disabledColorButtonText
                                    : enabledColorButtonText),
                          ),
                        ),
                      )),
                  SizedBox(
                    height: 10,
                    child: Expanded(
                      child: Container(),
                    ),
                  )
                ],
              ))),
        ));
  }

  Widget _dataTableWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 0,
        rightHandSideColumnWidth: MediaQuery.of(context).size.width,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: searchdata.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        horizontalScrollbarStyle: const ScrollbarStyle(
          thumbColor: Colors.grey,
          isAlwaysShown: true,
          thickness: 4.0,
          radius: Radius.circular(5.0),
        ),
        enablePullToRefresh: false,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
      ),
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('', 1),
      _getTitleItemWidget('Sublot No', MediaQuery.of(context).size.width / 2.5),
      _getTitleItemWidget(
          'Result Qty', MediaQuery.of(context).size.width / 3.45),
      _getTitleItemWidget('NG Qty', MediaQuery.of(context).size.width / 3.25),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      decoration: BoxDecoration(
          color: datatableColor,
          border: Border(
              top: BorderSide(
            color: Colors.grey,
            width: 1,
          ))),
      child: Text(label,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      width: width,
      height: 45,
      padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      width: 1,
      height: 45,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Text(searchdata[index].sublotNo),
          width: MediaQuery.of(context).size.width / 2.5,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(searchdata[index].resultQty),
          width: MediaQuery.of(context).size.width / 3.45,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
        Container(
          child: Text(searchdata[index].ngQty),
          width: MediaQuery.of(context).size.width / 3.6,
          height: 45,
          padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
          alignment: Alignment.centerRight,
        ),
      ],
    );
  }
}
