import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:panasonic/models/clswarehouse.dart';
import 'package:panasonic/utilities/alertdialog.dart';
import 'package:panasonic/utilities/constants.dart';
import 'package:panasonic/utilities/message.dart';
import 'home.dart';
import 'login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Select Warehouse',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

class WarehouseSelect extends StatefulWidget {
  const WarehouseSelect({Key key, this.version, this.userid, this.list})
      : super(key: key);

  final String version;
  final String userid;
  final List<ClsWarehouse> list;

  @override
  _WarehouseSelect createState() => _WarehouseSelect();
}

class _WarehouseSelect extends State<WarehouseSelect> {
  TextEditingController iWarehouseCode = TextEditingController();
  TextEditingController iWarehouseName = TextEditingController();
  List<ClsWarehouse> ddlWarehouse = [];
  List<ClsWarehouse> ddlWarehouseFilter = [];
  String _ddlFilterResult = '';
  String _searchResult = '';

  FocusNode nodeWH = FocusNode();

  @override
  void initState() {
    //listofCombo();
    super.initState();
  }

  void listofCombo() async {
    ddlWarehouse = await ClsWarehouse.getComboWarehouse(widget.userid)
        .catchError((error, stackTrace) {
      // ignore: invalid_return_type_for_catch_error
      return Future.error(Message.box(
          type: "2",
          message: error.toString(),
          position: "0",
          top: 45,
          context: context));
    });
    setState(() {});
  }

  void selectedItem(BuildContext context, item) {
    switch (item) {
      case 0:
        // Navigator.of(context)
        //     .push(MaterialPageRoute(builder: (context) => SettingPage()));
        break;
      case 1:
        print("Logged out");
        var dialog = CustomAlertDialog(
            type: "1",
            title: "Logout",
            message: "Are you sure, do you want to logout?",
            onOkPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) => LoginScreen(
                            lastuserid: widget.userid,
                          )),
                  (route) => false);
            },
            okBtnText: 'Yes',
            noBtnText: 'No');
        showDialog(context: context, builder: (BuildContext context) => dialog);

        break;
    }
  }

  void asyncAlert(String msg, String type) async {
    var dialog = CustomAlertDialog(
      type: type,
      title: '',
      message: msg,
      okBtnText: '',
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: kAppBarColor,
              title: Text(
                "Welcome " + widget.userid,
                style: TextStyle(
                    fontFamily: 'Arial',
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              actions: <Widget>[
                PopupMenuButton<int>(
                  icon: Icon(Icons.more_vert, color: Colors.white),
                  itemBuilder: (context) => [
                    PopupMenuItem<int>(
                        value: 1,
                        child: Row(
                          children: [
                            Icon(
                              Icons.logout,
                              color: Colors.lightBlue,
                            ),
                            const SizedBox(
                              width: 7,
                            ),
                            Text("Logout")
                          ],
                        )),
                    PopupMenuItem<int>(
                      value: 2,
                      child: Row(
                        children: [
                          Icon(
                            Icons.info,
                            color: Colors.lightBlue,
                          ),
                          const SizedBox(
                            width: 7,
                          ),
                          Text('Version ' + widget.version)
                        ],
                      ),
                    ),
                  ],
                  onSelected: (item) => selectedItem(context, item),
                ),
              ],
              bottom: PreferredSize(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                  child: Text(
                    "WAREHOUSE PRIVILEGE",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24.0,
                        fontFamily: 'Arial',
                        fontWeight: FontWeight.w900),
                  ),
                ),
                preferredSize: Size(0.0, 40.0),
              ),
            ),
            body: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 5,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Select Warehouse to Main Menu",
                        style: TextStyle(
                            fontFamily: 'Arial', fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TypeAheadField(
                      hideSuggestionsOnKeyboardHide: false,
                      textFieldConfiguration: TextFieldConfiguration(
                          onSubmitted: (value) {
                            iWarehouseCode.text = value;
                            _searchResult = value;
                          },
                          textInputAction: TextInputAction.done,
                          focusNode: nodeWH,
                          onEditingComplete: () {
                            //asyncAlert("on editing complete", "2");
                          },
                          controller: this.iWarehouseName,
                          //enableSuggestions: true,
                          style: TextStyle(fontFamily: 'Arial'),
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              onPressed: () {
                                setState(() {
                                  iWarehouseCode.clear();
                                  _searchResult = '';
                                });
                              },
                              icon: Icon(_searchResult == ''
                                  ? Icons.cancel
                                  : Icons.cancel_outlined),
                              color: _searchResult == ''
                                  ? Colors.white12.withOpacity(1)
                                  : Colors.grey[600],
                            ),
                            border: OutlineInputBorder(),
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: -10.0, horizontal: 10.0),
                          )),
                      suggestionsCallback: (pattern) async {
                        ddlWarehouse =
                            await ClsWarehouse.getComboWarehouse(widget.userid);
                        _ddlFilterResult = pattern;
                        ddlWarehouseFilter = ddlWarehouse
                            .where((result) =>
                                result.warehouseCode
                                    .toLowerCase()
                                    .contains(_ddlFilterResult.toLowerCase()) ||
                                result.warehouseName
                                    .toLowerCase()
                                    .contains(_ddlFilterResult.toLowerCase()))
                            .toList();
                        return ddlWarehouseFilter;
                      },
                      itemBuilder: (context, suggestion) {
                        return ListTile(
                          title: Text(suggestion.warehouseCode),
                          subtitle: Text('${suggestion.warehouseName}'),
                        );
                      },
                      onSuggestionSelected: (suggestion) async {
                        // asyncSelectLocationCarry();
                        this.iWarehouseCode.text = suggestion.warehouseCode;
                        this.iWarehouseName.text = suggestion.warehouseName;
                        _searchResult = suggestion.warehouseCode;

                        //FocusScope.of(context).requestFocus(nodeWH);
                      },
                    ),
                  ),

                  // Padding(
                  //     padding: EdgeInsets.symmetric(horizontal: 15),
                  //     child: SearchField(
                  //       suggestionState: Suggestion.expand,
                  //       suggestionAction: SuggestionAction.unfocus,
                  //       suggestions: ddlWarehouse
                  //           .map((e) => SearchFieldListItem(
                  //               e.warehouseCode + " - " + e.warehouseName))
                  //           .toList(),
                  //       textInputAction: TextInputAction.done,
                  //       hasOverlay: false,
                  //       emptyWidget: SizedBox.shrink(),
                  //       controller: _searchController,
                  //       hint: 'Fill a Warehouse',
                  //       itemHeight: 40,
                  //       searchInputDecoration: InputDecoration(
                  //         focusedBorder: OutlineInputBorder(
                  //           borderSide: BorderSide(
                  //             color: Colors.black.withOpacity(0.8),
                  //           ),
                  //         ),
                  //         border: OutlineInputBorder(
                  //           borderSide: BorderSide(color: Colors.red),
                  //         ),
                  //         contentPadding: new EdgeInsets.symmetric(
                  //             vertical: -10.0, horizontal: 10.0),
                  //       ),
                  //       maxSuggestionsInViewPort: 6,
                  //       onTap: (SearchFieldListItem x) {
                  //         setState(() {
                  //           asyncAlert(x.searchKey, "2");
                  //           final val = x.searchKey;
                  //           final val1 = val.split(' - ');
                  //           _searchController.text = val1[1].trim();
                  //           iWarehouseCode.text = val1[0].trim();
                  //         });
                  //       },
                  //       onEditingComplete: (SearchFieldListItem x) {
                  //         setState(() {
                  //           asyncAlert(x.searchKey, "2");
                  //           final val = x.searchKey;
                  //           final val1 = val.split(' - ');
                  //           _searchController.text = val1[1].trim();
                  //           iWarehouseCode.text = val1[0].trim();
                  //         });
                  //       },
                  //     )),
                  const SizedBox(height: 10),
                  Padding(
                      padding: EdgeInsets.symmetric(vertical: 0),
                      child: Container(
                        padding: EdgeInsets.only(top: 0),
                        width: MediaQuery.of(context).size.width * 0.90,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: FloatingActionButton.extended(
                          backgroundColor: Colors.blue[800],
                          onPressed: () {
                            if (iWarehouseCode.text == null ||
                                iWarehouseCode.text == "") {
                              Message.box(
                                  //title: "info",
                                  type: "2",
                                  message: "Please select warehouse",
                                  position: "0",
                                  top: 45,
                                  context: context);
                              //asyncAlert("Please select warehouse", "2");
                              // MessageBox(
                              //     type: "2",
                              //     textmessage: "Please select warehouse");
                              return;
                            }
                            Navigator.pop(context);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => MyHomePage(
                                  userid: widget.userid,
                                  whcode: iWarehouseCode.text,
                                  whname: iWarehouseName.text,
                                  version: widget.version,
                                ),
                                // settings: RouteSettings(
                                //   arguments: myUsernameController.text,
                                // ),
                              ),
                            );
                          },
                          elevation: 0,
                          label: Text(
                            "MAIN MENU",
                            style: TextStyle(fontSize: 18.0),
                          ),
                        ),
                      )),
                  SizedBox(height: 20),
                ])));
  }
}
