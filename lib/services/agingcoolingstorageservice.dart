import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsagingcoolingstorage.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class AgingCoolingStorageService {
  // Get Location Address
  Future<List<ClsAgingCoolingStorage>> getLocationAddress(
      String pCode, String pType) async {
    String apiURL =
        URLAPI + '/agingcoolingstorageaddress?code=' + pCode + '&type=' + pType;

    var response = await http.get(Uri.parse(apiURL));

    if (response.statusCode == 200) {
      List<ClsAgingCoolingStorage> listData;
      Iterable jsonObject = jsonDecode(response.body);
      if (pType == "0") {
        listData = jsonObject
            .map((e) => ClsAgingCoolingStorage.getLocationAddress1(e))
            .toList();
      } else {
        listData = jsonObject
            .map((e) => ClsAgingCoolingStorage.getLocationAddress2(e))
            .toList();
      }

      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List Header
  Future<List<ClsAgingCoolingStorage>> getData(String carryNo) async {
    final response = await http.get(
        Uri.parse(URLAPI + '/agingcoolingstorageheader?carryno=' + carryNo));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsAgingCoolingStorage> listData = body
          .map((dynamic item) => ClsAgingCoolingStorage.listHeader(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List Detail
  Future<List<ClsAgingCoolingStorage>> getDetail(
      String carryNo, String itemCode, String lotNo) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/agingcoolingstoragedetail?carryno=' +
        carryNo +
        '&item=' +
        itemCode +
        '&lotno=' +
        lotNo));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsAgingCoolingStorage> listData = body
          .map((dynamic item) => ClsAgingCoolingStorage.listDetail(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  Future<List<ClsAgingCoolingStorage>> submit(
      String carryNo, String locDestination, String userId) async {
    final response = await http.post(Uri.parse(URLAPI +
        '/agingcoolingstoragesubmit?carryno=' +
        carryNo +
        '&loc=' +
        locDestination +
        '&userid=' +
        userId));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsAgingCoolingStorage> listData = body
          .map((dynamic item) => ClsAgingCoolingStorage.listSubmit(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //ListDeepSleep
  Future<List<ClsAgingCoolingStorage>> deepsleep(
      String carryNo, String location) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/agingcoolingstoragedeepsleep?carryno=' +
        carryNo +
        '&locationto=' +
        location));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsAgingCoolingStorage> listData = body
          .map((dynamic item) => ClsAgingCoolingStorage.listDeepSleep(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  // Future<String> submit(
  //     String carryNo, String locDestination, String userId) async {
  //   var body;
  //   final response = await http.post(
  //       Uri.parse(URLAPI +
  //           '/agingcoolingstoragesubmit?carryno=' +
  //           carryNo +
  //           '&loc=' +
  //           locDestination +
  //           '&userid=' +
  //           userId),
  //       //body: json.encode({"finalResponse": jsonList}["finalResponse"]),
  //       headers: {'Content-type': 'application/json'});

  //   if (response.statusCode == 200) {
  //     body = jsonDecode(response.body);
  //     return body;
  //   } else {
  //     body = "Failed to submit data";
  //     return body;
  //   }
  // }
}
