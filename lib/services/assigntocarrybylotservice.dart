import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:panasonic/models/clsassigntocarrybylot.dart';
import 'dart:convert';
import 'package:panasonic/utilities/constantsApi.dart';

class AssignToCarryByLotService {
  static Future getCarryDesc(String carryno) async {
    try {
      String apiURL = URLAPI + '/assigntocarrybylot_getdesc?carryno=' + carryno;

      var response = await http.get(Uri.parse(apiURL));

      if (response.statusCode == 200) {
        Iterable jsonObject = jsonDecode(response.body);
        List<ClsAssignToCarryByLot> listData = jsonObject
            .map((e) => ClsAssignToCarryByLot.getCarryDesc(e))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  //List Detail
  Future<List<ClsAssignToCarryByLot>> getDataDetail(
      String item, String lotno, String barcode) async {
    try {
      final response = await http.get(Uri.parse(URLAPI +
          '/assigntocarrybylot_detail?item=' +
          item +
          '&lotno=' +
          lotno +
          '&barcodeno=' +
          barcode));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ClsAssignToCarryByLot> listData = body
            .map((dynamic item) => ClsAssignToCarryByLot.getDetailData(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  Future<List<ClsAssignToCarryByLot>> scanDataBarcode(String fromcarry,
      String tocarry, String lotscan, String warehouselogin) async {
    try {
      final response = await http.get(Uri.parse(URLAPI +
          '/assigntocarrybylot_listscan?fromcarry=' +
          fromcarry +
          '&tocarry=' +
          tocarry +
          '&lotscan=' +
          lotscan +
          '&warehouse=' +
          warehouselogin));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ClsAssignToCarryByLot> list = body
            .map((dynamic item) => ClsAssignToCarryByLot.scanBarcode(item))
            .toList();
        return list;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  static Future submit(String fromcarry, String tocarry, String lotscan,
      String userid, String warehouselogin) async {
    final response = await http.post(Uri.parse(URLAPI +
        '/assigntocarrybylot_submit?fromcarry=' +
        fromcarry +
        '&tocarry=' +
        tocarry +
        '&lotscan=' +
        lotscan +
        '&userid=' +
        userid +
        '&warehouse=' +
        warehouselogin));
    Iterable jsonObject = jsonDecode(response.body);
    List<ClsAssignToCarryByLot> complete =
        jsonObject.map((e) => ClsAssignToCarryByLot.submit(e)).toList();
    return complete;
  }
}
