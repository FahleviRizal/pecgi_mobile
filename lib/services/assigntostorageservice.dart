import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsassigntostorage.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class AssignToStorageService {
  static Future getLocationCarry(String whcode) async {
    // String apiURL = URLAPI + '/assignstoragegetlocation?whcode=' + whcode;

    // var apiResult = await http.get(Uri.parse(apiURL));
    // Iterable jsonObject = jsonDecode(apiResult.body);
    // List<ClsAssignToStorage> clsAssignToStorageData =
    //     jsonObject.map((e) => ClsAssignToStorage.ddlLocationCarry(e)).toList();
    // return clsAssignToStorageData;

    String apiURL = URLAPI + '/assignstorage_getlocation?whcode=' + whcode;

    var response = await http.get(Uri.parse(apiURL));

    if (response.statusCode == 200) {
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsAssignToStorage> listData = jsonObject
          .map((e) => ClsAssignToStorage.ddlLocationCarry(e))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  static Future getLocationCarryDesc(String location) async {
    // String apiURL = URLAPI + '/assignstoragegetlocation?whcode=' + whcode;

    // var apiResult = await http.get(Uri.parse(apiURL));
    // Iterable jsonObject = jsonDecode(apiResult.body);
    // List<ClsAssignToStorage> clsAssignToStorageData =
    //     jsonObject.map((e) => ClsAssignToStorage.ddlLocationCarry(e)).toList();
    // return clsAssignToStorageData;
    try {
      String apiURL =
          URLAPI + '/assignstorage_getlocationbyid?location=' + location;

      var response = await http.get(Uri.parse(apiURL));

      if (response.statusCode == 200) {
        Iterable jsonObject = jsonDecode(response.body);
        List<ClsAssignToStorage> listData = jsonObject
            .map((e) => ClsAssignToStorage.getLocationCarryDesc(e))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  // static Future getData(String LocationCarryCode) async {
  //   String apiURL =
  //       URLAPI + 'assigntostorage/getdata?LocationCarry=' + LocationCarryCode;

  //   var apiResult = await http.get(Uri.parse(apiURL));
  //   Iterable jsonObject = jsonDecode(apiResult.body);
  //   List<clsAssignToStorage> clsAssignToStorageData =
  //       jsonObject.map((e) => clsAssignToStorage.GetData(e)).toList();
  //   return clsAssignToStorageData;
  // }

  Future<List<ClsAssignToStorage>> scanDataBarcode(
      String fromwarehouse, String locationCarry, String barcode) async {
    // String apiURL = URLAPI +
    //     'assigntostorage/scandatabarcode?locationcarry=' +
    //     locationCarry +
    //     '&barcode=' +
    //     barcode;

    // var apiResult = await http.post(Uri.parse(apiURL));
    // Iterable jsonObject = jsonDecode(apiResult.body);
    // List<ClsAssignToStorage> clsAssignToStorageData =
    //     jsonObject.map((e) => ClsAssignToStorage.scanBarcode(e)).toList();
    // return clsAssignToStorageData;
    try {
      final response = await http.get(Uri.parse(URLAPI +
          '/assignstorage_scandata?fromwarehouse=' +
          fromwarehouse +
          '&locationcarry=' +
          locationCarry +
          '&barcode=' +
          barcode));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ClsAssignToStorage> list = body
            .map((dynamic item) => ClsAssignToStorage.scanBarcode(item))
            .toList();
        return list;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  // static Future submitData(String locationcarry) async {
  //   String apiURL =
  //       URLAPI + 'assigntostorage/submit?locationcarry=' + locationcarry;

  //   var apiResult = await http.post(Uri.parse(apiURL));
  //   Iterable jsonObject = jsonDecode(apiResult.body);
  //   List<ClsAssignToStorage> clsAssignToStorageData =
  //       jsonObject.map((e) => ClsAssignToStorage.submit(e)).toList();
  //   return clsAssignToStorageData;
  // }

  static Future submitData(
      List<ClsAssignToStorage> model, String userId) async {
    try {
      //var body;
      List jsonList = [];
      model.map((item) => jsonList.add(item.toJson())).toList();

      final response = await http.post(
          Uri.parse(URLAPI + '/assignstorage_submit?userid=' + userId),
          body: json.encode({"finalResponse": jsonList}["finalResponse"]),
          headers: {'Content-type': 'application/json'});

      // if (response.statusCode == 200) {
      //   body = jsonDecode(response.body);
      //   return body;
      // } else {
      //   body = "Failed to save data";
      //   return body;
      // }

      Iterable jsonObject = jsonDecode(response.body);
      List<ClsAssignToStorage> submit =
          jsonObject.map((e) => ClsAssignToStorage.submit(e)).toList();
      return submit;
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }
}
