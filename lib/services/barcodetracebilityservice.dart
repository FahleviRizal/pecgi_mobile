import 'dart:convert';
import 'dart:io';

import 'package:panasonic/models/clsbarcodetracebility.dart';
import 'package:http/http.dart' as http;
import 'package:panasonic/utilities/constantsApi.dart';

class BarcodeTracebilityService {
  Future<String> getCurrentStock(String barcodeNo) async {
    final response = await http.get(Uri.parse(
        URLAPI + '/barcodetracebility/getcurrentstock?Barcode=' + barcodeNo));
    if (response.statusCode == 200) {
      dynamic body = jsonDecode(response.body);    
      String currentStock = body["CurrentStock"] as String;  
      return currentStock;
    } else {
      throw "Failed to load data";
    }
  }

  Future<List<ClsBarcodeTracebility>> getDataDetail(String barcodeNo) async {
    try {
      final response = await http.get(Uri.parse(
          URLAPI + '/barcodetracebility/getdetail?Barcode=' + barcodeNo));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ClsBarcodeTracebility> listData = body
            .map((dynamic item) => ClsBarcodeTracebility.fromJson(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }
}
