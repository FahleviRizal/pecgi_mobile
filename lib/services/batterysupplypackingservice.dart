import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsbatterysupplypacking.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class BatterySupplyPackingService {
  static Future getSupplyNo() async {
    String apiURL = URLAPI + '/batterysupplypacking_getsupplyno?';

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsBatterySupplyPacking> clsSupplyRequestData =
        jsonObject.map((e) => ClsBatterySupplyPacking.ddlSupplyNo(e)).toList();
    return clsSupplyRequestData;
  }

  static Future getDataListHeader(String supplyNo, String carryNo) async {
    String apiURL = URLAPI +
        '/batterysupplypacking_getlist?supplyno=' +
        supplyNo +
        '&carryno=' +
        carryNo;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsBatterySupplyPacking> clsSupplyRequestData =
        jsonObject.map((e) => ClsBatterySupplyPacking.getListDataH(e)).toList();
    return clsSupplyRequestData;
  }

  static Future submit(String supplyNo, String carryNo, String userID) async {
    String apiURL = URLAPI +
        '/batterysupplypacking_submit?supplyno=' +
        supplyNo +
        '&carryno=' +
        carryNo +
        '&userid=' +
        userID;
    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsBatterySupplyPacking> clsSupplyRequestData =
        jsonObject.map((e) => ClsBatterySupplyPacking.submitData(e)).toList();
    return clsSupplyRequestData;
  }
}
