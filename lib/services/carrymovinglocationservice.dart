import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clscarrymovinglocation.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class CarryMovingLocationService {
  // Get Location Address
  Future<List<ClsCarryMovingLocation>> getLocationAddress(
      String pCode, String pType) async {
    String apiURL = URLAPI +
        '/carrymovinglocationgetaddress?code=' +
        pCode +
        '&type=' +
        pType;

    var response = await http.get(Uri.parse(apiURL));

    if (response.statusCode == 200) {
      List<ClsCarryMovingLocation> listData;
      Iterable jsonObject = jsonDecode(response.body);
      if (pType == "0") {
        listData = jsonObject
            .map((e) => ClsCarryMovingLocation.getLocationAddress1(e))
            .toList();
      } else {
        listData = jsonObject
            .map((e) => ClsCarryMovingLocation.getLocationAddress2(e))
            .toList();
      }

      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List Header
  Future<List<ClsCarryMovingLocation>> getData(String carryNo) async {
    final response = await http.get(
        Uri.parse(URLAPI + '/carrymovinglocationheader?carryno=' + carryNo));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsCarryMovingLocation> listData = body
          .map((dynamic item) => ClsCarryMovingLocation.listHeader(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List Detail
  Future<List<ClsCarryMovingLocation>> getDetail(
      String carryNo, String itemCode, String lotNo) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/carrymovinglocationdetail?carryno=' +
        carryNo +
        '&item=' +
        itemCode +
        '&lotno=' +
        lotNo));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsCarryMovingLocation> listData = body
          .map((dynamic item) => ClsCarryMovingLocation.listDetail(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  Future<List<ClsCarryMovingLocation>> submit(
      String carryNo, String locDestination, String userId) async {
    final response = await http.post(Uri.parse(URLAPI +
        '/carrymovinglocationsubmit?carryno=' +
        carryNo +
        '&loc=' +
        locDestination +
        '&userid=' +
        userId));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsCarryMovingLocation> listData = body
          .map((dynamic item) => ClsCarryMovingLocation.listSubmit(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  // Future<String> submit(
  //     String carryNo, String locDestination, String userId) async {
  //   var body;
  //   final response = await http.post(
  //       Uri.parse(URLAPI +
  //           '/carrymovinglocationsubmit?carryno=' +
  //           carryNo +
  //           '&loc=' +
  //           locDestination +
  //           '&userid=' +
  //           userId),
  //       //body: json.encode({"finalResponse": jsonList}["finalResponse"]),
  //       headers: {'Content-type': 'application/json'});

  //   if (response.statusCode == 200) {
  //     body = jsonDecode(response.body);
  //     return body;
  //   } else {
  //     body = "Failed to submit data";
  //     return body;
  //   }
  // }
}
