import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsconsumpunschedule.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class ConsumpUnscheduleService {
  static Future getDataScan(String labelBarcode, String warehouse) async {
    String apiURL = URLAPI +
        '/consumpunschedule_scandata?labelbarcode=' +
        labelBarcode +
        '&warehouse=' +
        warehouse;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsConsumpUnschedule> idata =
        jsonObject.map((e) => ClsConsumpUnschedule.scanData(e)).toList();
    return idata;
  }

  static Future submit(
      String labelBarcode,
      String item,
      String lotno,
      String qtybefore,
      String qtyactual,
      String userid,
      String warehouse) async {
    String apiURL = URLAPI +
        '/consumpunschedule_submit?labelbarcode=' +
        labelBarcode +
        '&item=' +
        item +
        '&lotno=' +
        lotno +
        '&qtybefore=' +
        qtybefore +
        '&qtyactual=' +
        qtyactual +
        '&userid=' +
        userid +
        '&warehouse=' +
        warehouse;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsConsumpUnschedule> clsMaterialConsumptionData =
        jsonObject.map((e) => ClsConsumpUnschedule.submitData(e)).toList();
    return clsMaterialConsumptionData;
  }
}
