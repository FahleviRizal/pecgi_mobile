import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsmanpower.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class ManpowerPreparationService {
  static Future getLine(String userid) async {
    String apiURL = URLAPI + '/cbouserline?userid=' + userid;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsManpowerPreparation> clsMaterialRequestData =
        jsonObject.map((e) => ClsManpowerPreparation.comboLine(e)).toList();
    return clsMaterialRequestData;
  }

  // Get Dropdown InsNo
  static Future getInstructionNo(String line, String pUser) async {
    String apiURL =
        URLAPI + '/manpower_getinsno?line=' + line + '&userid=' + pUser;

    var response = await http.get(Uri.parse(apiURL));

    if (response.statusCode == 200) {
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsManpowerPreparation> listData = jsonObject
          .map((e) => ClsManpowerPreparation.ddlInstructionNo(e))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List Manpower preparation
  Future<List<ClsManpowerPreparation>> getData(
      String insNo, String lineCd) async {
    final response = await http.get(Uri.parse(
        URLAPI + '/manpower_getlist?insNo=' + insNo + '&line=' + lineCd));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsManpowerPreparation> listData = body
          .map((dynamic item) => ClsManpowerPreparation.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List manpower Detail
  Future<List<ClsManpowerPreparationDetil>> getDataDetail(
      String insNo, String skill) async {
    final response = await http.get(Uri.parse(
        URLAPI + '/manpower_detail?insno=' + insNo + '&skill=' + skill));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsManpowerPreparationDetil> listData = body
          .map((dynamic item) => ClsManpowerPreparationDetil.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  // Future<String> scanEmployee(
  //     String insNo, String lineCode, String empId, String userId) async {
  //   var body;
  //   final response = await http.post(Uri.parse(URLAPI +
  //       '/manpower_scan?insNo=' +
  //       insNo +
  //       '&line=' +
  //       lineCode +
  //       '&empid=' +
  //       empId +
  //       '&userid=' +
  //       userId));

  //   if (response.statusCode == 200) {
  //     body = jsonDecode(response.body);
  //     return body;
  //   } else {
  //     body = "Failed to scan data";
  //     return body;
  //   }
  // }

  static Future scanSubmitEmployee(
      String insNo, String lineCode, String empId, String userId) async {
    String apiURL = URLAPI +
        '/manpower_scan?insNo=' +
        insNo +
        '&line=' +
        lineCode +
        '&empid=' +
        empId +
        '&userid=' +
        userId;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsManpowerPreparation> scnSubmit = jsonObject
        .map((e) => ClsManpowerPreparation.scanSubmitData(e))
        .toList();
    return scnSubmit;
  }
}
