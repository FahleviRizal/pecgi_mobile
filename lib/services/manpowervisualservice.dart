import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsmanpowervisual.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class ManpowerPreparationVisualService {
  static Future getLine(String userid) async {
    String apiURL = URLAPI + '/cbouserline?userid=' + userid;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsManpowerPreparationVisual> clsMaterialRequestData = jsonObject
        .map((e) => ClsManpowerPreparationVisual.comboLine(e))
        .toList();
    return clsMaterialRequestData;
  }

  // Get Dropdown InsNo
  static Future getInstructionNo(
      String line, String lotcard, String pUser) async {
    String apiURL = URLAPI +
        '/manpowervisual_getinsno?line=' +
        line +
        '&lotcard=' +
        ((lotcard == "") ? "-" : lotcard) +
        '&userid=' +
        pUser;

    var response = await http.get(Uri.parse(apiURL));

    if (response.statusCode == 200) {
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsManpowerPreparationVisual> listData = jsonObject
          .map((e) => ClsManpowerPreparationVisual.ddlInstructionNo(e))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List Manpower preparation
  Future<List<ClsManpowerPreparationVisual>> getData(
      String insNo, String lineCd, String lot) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/manpowervisual_getlist?insNo=' +
        insNo +
        '&line=' +
        lineCd +
        '&lot=' +
        lot));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsManpowerPreparationVisual> listData = body
          .map((dynamic item) => ClsManpowerPreparationVisual.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List manpower Detail
  Future<List<ClsManpowerPreparationVisualDetil>> getDataDetail(
      String insNo, String skill) async {
    final response = await http.get(Uri.parse(
        URLAPI + '/manpowervisual_detail?insno=' + insNo + '&skill=' + skill));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsManpowerPreparationVisualDetil> listData = body
          .map((dynamic item) =>
              ClsManpowerPreparationVisualDetil.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  // Future<String> scanEmployee(
  //     String insNo, String lineCode, String empId, String userId) async {
  //   var body;
  //   final response = await http.post(Uri.parse(URLAPI +
  //       '/manpower_scan?insNo=' +
  //       insNo +
  //       '&line=' +
  //       lineCode +
  //       '&empid=' +
  //       empId +
  //       '&userid=' +
  //       userId));

  //   if (response.statusCode == 200) {
  //     body = jsonDecode(response.body);
  //     return body;
  //   } else {
  //     body = "Failed to scan data";
  //     return body;
  //   }
  // }

  static Future scanLotCard(String lineCode, String lotCard) async {
    String apiURL = URLAPI +
        '/manpowervisual_scanlot?&line=' +
        lineCode +
        '&lot=' +
        lotCard;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsManpowerPreparationVisual> scnLot = jsonObject
        .map((e) => ClsManpowerPreparationVisual.scanLotCard(e))
        .toList();
    return scnLot;
  }

  static Future scanSubmitEmployee(String insNo, String lineCode, String empId,
      String lotCard, String userId) async {
    String apiURL = URLAPI +
        '/manpowervisual_scan?insNo=' +
        insNo +
        '&line=' +
        lineCode +
        '&empid=' +
        empId +
        '&lotcard=' +
        lotCard +
        '&userid=' +
        userId;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsManpowerPreparationVisual> scnSubmit = jsonObject
        .map((e) => ClsManpowerPreparationVisual.scanSubmitData(e))
        .toList();
    return scnSubmit;
  }
}
