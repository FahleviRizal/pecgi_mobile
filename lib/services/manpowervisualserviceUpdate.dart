import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsmanpowervisualUpdate.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class ManpowerPreparationVisualService {
  static Future getLine(String userid) async {
    String apiURL = URLAPI + '/Updatecbouserline?userid=' + userid;
    //String apiURL = URLAPI + '/Updatecbouserline?userid=AdminTos';
    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsManpowerPreparationVisualUpdate> clsMaterialRequestData = jsonObject
        .map((e) => ClsManpowerPreparationVisualUpdate.comboLine(e))
        .toList();
    return clsMaterialRequestData;
  }

  static Future getLinevis(String userid) async {
    String apiURL = URLAPI + '/Updatecbouserlinevis?userid=' + userid;
    //String apiURL = URLAPI + '/Updatecbouserlinevis?userid=AdminTos';

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsManpowerPreparationVisualUpdate> clsMaterialRequestData = jsonObject
        .map((e) => ClsManpowerPreparationVisualUpdate.comboLine(e))
        .toList();
    return clsMaterialRequestData;
  }

  static Future getLotCardvis(String LineCode, String lotCard) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/Updatecbolotcardvis?linecode=' +
        LineCode +
        '&lotCard=' +
        lotCard));
    if (response.statusCode == 200) {
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsManpowerPreparationVisualUpdate> clsMaterialRequestData =
          jsonObject
              .map((e) => ClsManpowerPreparationVisualUpdate.comboLotCard(e))
              .toList();
      return clsMaterialRequestData;
    } else {
      throw "Failed to load data, Please Select Line First";
    }
  }

  // Get Dropdown InsNo
  static Future getInstructionNo(
      String line, String lotcard, String pUser) async {
    String apiURL = URLAPI +
        '/manpowervisual_getinsno?line=' +
        line +
        '&lotcard=' +
        ((lotcard == "") ? "-" : lotcard) +
        '&userid=' +
        pUser;

    var response = await http.get(Uri.parse(apiURL));

    if (response.statusCode == 200) {
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsManpowerPreparationVisualUpdate> listData = jsonObject
          .map((e) => ClsManpowerPreparationVisualUpdate.ddlInstructionNo(e))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  static Future getEmpID(String SkillCode) async {
    String apiURL = URLAPI + '/Updatecboemployeeidvis?skilcode=' + SkillCode;
    if (SkillCode == '') {
      throw "skill code is empty";
    }
    final response = await http.get(Uri.parse(apiURL));
    if (response.statusCode == 200) {
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsManpowerPreparationVisualUpdate> clsMaterialRequestData =
          jsonObject
              .map((e) => ClsManpowerPreparationVisualUpdate.comboEmpID(e))
              .toList();
      return clsMaterialRequestData;
    } else {
      throw "Failed to load data, Please Select Line First";
    }
  }

  static Future getMachineID(String LineCode) async {
    String apiURL = URLAPI + '/Updatecbomachinevis?linecode=' + LineCode;
    final response = await http.get(Uri.parse(apiURL));
    if (response.statusCode == 200) {
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsManpowerPreparationVisualUpdate> clsMaterialRequestData =
          jsonObject
              .map((e) => ClsManpowerPreparationVisualUpdate.comboMachineID(e))
              .toList();
      return clsMaterialRequestData;
    } else {
      throw "Failed to load data, Please Select Line First";
    }
  }

  //List Table Total Scan Manpower preparation
  Future<List<ClsManpowerPreparationVisualUpdate>> getData(
      String insNo, String lineCd, String lot) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/manpowervisual_getlist?insNo=' +
        insNo +
        '&line=' +
        lineCd +
        '&lot=' +
        lot));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsManpowerPreparationVisualUpdate> listData = body
          .map((dynamic item) =>
              ClsManpowerPreparationVisualUpdate.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List manpower Detail
  Future<List<ClsManpowerPreparationVisualUpdateDetil>> getDataDetail(
      String insNo, String skill) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/Updatemanpowervisual_detail?insno=' +
        insNo +
        '&skill=' +
        skill));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsManpowerPreparationVisualUpdateDetil> listData = body
          .map((dynamic item) =>
              ClsManpowerPreparationVisualUpdateDetil.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List manpower Detail
  Future<List<ClsManpowerPreparationVisualUpdate>> getDataDetailUpdate(
      String insNo, String skill) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/Updatemanpower_detail_update?insno=' +
        insNo +
        '&skill=' +
        skill));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsManpowerPreparationVisualUpdate> listData = body
          .map((dynamic item) =>
              ClsManpowerPreparationVisualUpdate.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  Future<List<ClsManpowerPreparationVisualUpdate>> getBarcodeData(
      String line, String lot) async {
    final response = await http.get(Uri.parse(
        URLAPI + '/Updatecbolotcardvis?linecode=' + line + '&lotcard=' + lot));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsManpowerPreparationVisualUpdate> listData = body
          .map((dynamic item) =>
              ClsManpowerPreparationVisualUpdate.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  static Future scanLotCard(String lineCode, String lotCard) async {
    String apiURL = URLAPI +
        '/manpowervisual_scanlot?&line=' +
        lineCode +
        '&lot=' +
        lotCard;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsManpowerPreparationVisualUpdate> scnLot = jsonObject
        .map((e) => ClsManpowerPreparationVisualUpdate.scanLotCard(e))
        .toList();
    return scnLot;
  }

  static Future scanSubmitEmployee(String insNo, String lineCode, String empId,
      String lotCard, String userId, String MachineNo) async {
    String apiURL = URLAPI +
        '/manpowervisual_scan?insNo=' +
        insNo +
        '&line=' +
        lineCode +
        '&empid=' +
        empId +
        '&lotcard=' +
        lotCard +
        '&userid=' +
        userId +
        '&machineno=' +
        MachineNo;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsManpowerPreparationVisualUpdate> scnSubmit = jsonObject
        .map((e) => ClsManpowerPreparationVisualUpdate.scanSubmitData(e))
        .toList();
    return scnSubmit;
  }
}
