import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/ClsMaterialConsumption.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class MaterialConsumptionService {
  static Future getInstructionNo(String warehouse, String line) async {
    String apiURL = URLAPI +
        '/materialconsumption_getinstructionno?warehouse=' +
        warehouse +
        '&line=' +
        line;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialConsumption> clsMaterialConsumptionData = jsonObject
        .map((e) => ClsMaterialConsumption.combotInstructionNo(e))
        .toList();

    return clsMaterialConsumptionData;
  }

  static Future getLine(String userid) async {
    String apiURL = URLAPI + '/cbouserline?userid=' + userid;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialConsumption> clsMaterialRequestData =
        jsonObject.map((e) => ClsMaterialConsumption.comboLine(e)).toList();
    return clsMaterialRequestData;
  }

  static Future getData(String instructionNo) async {
    String apiURL =
        URLAPI + '/materialconsumption_getdata?instructionno=' + instructionNo;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialConsumption> clsMaterialConsumptionData =
        jsonObject.map((e) => ClsMaterialConsumption.getDataList(e)).toList();
    return clsMaterialConsumptionData;
  }

  static Future getDataDetail(String instructionNo, String labelBarcode) async {
    String apiURL = URLAPI +
        '/materialconsumption_scandata?instructionno=' +
        instructionNo +
        '&labelbarcode=' +
        labelBarcode;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialConsumption> clsMaterialConsumptionData =
        jsonObject.map((e) => ClsMaterialConsumption.scanData(e)).toList();
    return clsMaterialConsumptionData;
  }

  static Future submit(String instructionNo, String labelBarcode, String item,
      String lotno, String qty, String userid) async {
    String apiURL = URLAPI +
        '/materialconsumption_submit?instructionno=' +
        instructionNo +
        '&labelbarcode=' +
        labelBarcode +
        '&item=' +
        item +
        '&lotno=' +
        lotno +
        '&qty=' +
        qty +
        '&userid=' +
        userid;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialConsumption> clsMaterialConsumptionData =
        jsonObject.map((e) => ClsMaterialConsumption.submitData(e)).toList();
    return clsMaterialConsumptionData;
  }
}
