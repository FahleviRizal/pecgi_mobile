import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsmaterialpreparation.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class MaterialPreparationService {
  static Future getLine(String userid) async {
    String apiURL = URLAPI + '/cbouserline?userid=' + userid;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialpreparation> clsMaterialRequestData =
        jsonObject.map((e) => ClsMaterialpreparation.getLine(e)).toList();
    return clsMaterialRequestData;
  }

  static Future getInstructionNo(String line, String userid) async {
    String apiURL = URLAPI +
        'materialpreparation_getinstructionno?line=' +
        line +
        '&userid=' +
        userid;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialpreparation> clsMaterialRequestData = jsonObject
        .map((e) => ClsMaterialpreparation.getInstructionNo(e))
        .toList();
    return clsMaterialRequestData;
  }

  static Future getdata(String instructionno) async {
    String apiURL = URLAPI +
        'materialpreparation_getdataproductioninformation?instructionno=' +
        instructionno;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialpreparation> clsMaterialRequestData =
        jsonObject.map((e) => ClsMaterialpreparation.getData(e)).toList();
    return clsMaterialRequestData;
  }

  static Future getDataDetail(String instructionNo, String materialCode) async {
    String apiURL = URLAPI +
        'materialpreparation_getdatalist?instructionno=' +
        instructionNo +
        '&materialcode=' +
        materialCode;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialpreparation> clsMaterialRequestData =
        jsonObject.map((e) => ClsMaterialpreparation.getDataDetail(e)).toList();
    return clsMaterialRequestData;
  }

  static Future submit(
      String instructionNo, String labelBarcode, String userid) async {
    String apiURL = URLAPI +
        'materialpreparation_submit?instructionno=' +
        instructionNo +
        '&labelbarcode=' +
        labelBarcode +
        '&userid=' +
        userid;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsMaterialpreparation> clsMaterialRequestData =
        jsonObject.map((e) => ClsMaterialpreparation.submitdata(e)).toList();
    return clsMaterialRequestData;
  }
}
