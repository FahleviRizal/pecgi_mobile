import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:panasonic/models/clsmixingsublotgroupcarry.dart';
import 'dart:convert';
import 'package:panasonic/utilities/constantsApi.dart';

class MixingSublotGroupingCarryService {
  //get carry name
  Future<List<ClsMixingSublotGroupCarry>> getCarryName(String carryNo) async {
    try {
      final response = await http.get(Uri.parse(
          URLAPI + '/mixsublotgroupcarry_getname?carryno=' + carryNo));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ClsMixingSublotGroupCarry> listData = body
            .map((dynamic item) => ClsMixingSublotGroupCarry.fromJson(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  //List Header ?
  Future<List<ClsMixingSublotGroupCarry>> getData(
      String carryNo, String warehouse) async {
    try {
      final response = await http.get(Uri.parse(URLAPI +
          '/mixsublotgroupcarry_getlist?carryno=' +
          carryNo +
          '&warehouse=' +
          warehouse));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ClsMixingSublotGroupCarry> listData = body
            .map((dynamic item) => ClsMixingSublotGroupCarry.fromJson(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  //Submit ?
  static Future submit(
      String carryNo, String sublotNo, String userId, String warehouse) async {
    try {
      final response = await http.post(Uri.parse(URLAPI +
          '/mixsublotgroupcarry_submit?carryno=' +
          carryNo +
          '&sublotno=' +
          sublotNo +
          '&userid=' +
          userId +
          '&warehouse=' +
          warehouse));
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsMixingSublotGroupCarry> submit =
          jsonObject.map((e) => ClsMixingSublotGroupCarry.submit(e)).toList();
      return submit;
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }
}
