import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsphysicalinventory.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class PhysicalInventoryService {
  //Get Period
  Future<String> getPeriod() async {
    String apiURL = URLAPI + '/physicalinventorybarcode_getperiod';

    var response = await http.get(Uri.parse(apiURL));

    if (response.statusCode == 200 || response.statusCode == 400) {
      String body = jsonDecode(response.body);
      return body;
      // return ClsPhysicalInventoryByBarcode.getPeriod(
      //   json.decode(response.body),
      // );
    } else {
      throw Exception('Failed to load data!');
    }
  }

  //get data
  Future<List<ClsPhysicalInventoryByBarcode>> getData(
      String period, String barcodeNo, String warehouse) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/physicalinventorybarcode_getdata?period=' +
        period +
        '&barcodeno=' +
        barcodeNo +
        '&warehouse=' +
        warehouse));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsPhysicalInventoryByBarcode> listData = body
          .map((dynamic item) => ClsPhysicalInventoryByBarcode.getData(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //get data
  Future<List<ClsPhysicalInventoryByBarcode>> actualLocation(
      String locationCode) async {
    final response = await http.get(Uri.parse(
        URLAPI + '/physicalinventorybarcode_getloc?code=' + locationCode));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsPhysicalInventoryByBarcode> listData = body
          .map((dynamic item) =>
              ClsPhysicalInventoryByBarcode.getLocationNameActual(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  static Future submit(
      String period,
      String barcodeNo,
      String itemCode,
      String lotNo,
      String actualQty,
      String actualLoc,
      String userId,
      String warehouse) async {
    //var body;
    final response = await http.post(Uri.parse(URLAPI +
        '/physicalinventorybarcode_submit?period=' +
        period +
        '&barcodeno=' +
        barcodeNo +
        '&item=' +
        itemCode +
        '&lotno=' +
        lotNo +
        '&actualqty=' +
        actualQty +
        '&actualloc=' +
        actualLoc +
        '&userid=' +
        userId +
        '&warehouse=' +
        warehouse));
    Iterable jsonObject = jsonDecode(response.body);
    List<ClsPhysicalInventoryByBarcode> submit =
        jsonObject.map((e) => ClsPhysicalInventoryByBarcode.submit(e)).toList();
    return submit;
    // if (response.statusCode == 200) {
    //   body = jsonDecode(response.body);
    //   return body;
    // } else {
    //   body = "Failed to scan data";
    //   return body;
    // }
  }

  //get combo/dropdownlist
  Future<List<ClsPhysicalInventoryByCarry>> getCarryNo() async {
    final response =
        await http.get(Uri.parse(URLAPI + '/physicalinventorycarry_getcombo?'));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsPhysicalInventoryByCarry> listData = body
          .map((dynamic item) => ClsPhysicalInventoryByCarry.ddlCarryNo(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //get data
  Future<List<ClsPhysicalInventoryByCarry>> getListHeader(
      String carryNo) async {
    final response = await http.get(Uri.parse(
        URLAPI + '/physicalinventorycarry_header?carryno=' + carryNo));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsPhysicalInventoryByCarry> listData = body
          .map((dynamic item) => ClsPhysicalInventoryByCarry.listHeader(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //get data detail
  Future<List<ClsPhysicalInventoryByCarry>> getListDetail(
      String carryNo, String type, String lotNo, String date) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/physicalinventorycarry_detail?carryno=' +
        carryNo +
        '&type=' +
        type +
        '&lotno=' +
        lotNo +
        '&date=' +
        date));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsPhysicalInventoryByCarry> listData = body
          .map((dynamic item) => ClsPhysicalInventoryByCarry.listDetail(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //submit by carry
  static Future submitCarry(
      // String carryNo,
      // String type,
      // String lotNo,
      // String date,
      List<ClsPhysicalInventoryByCarry> model,
      String userId) async {
    List jsonList = [];
    model.map((item) => jsonList.add(item.toJson())).toList();

    final response = await http.post(
        Uri.parse(URLAPI +
            '/physicalinventorycarry_submit?userid=' //carryno=' +
            // carryNo +
            // '&type=' +
            // type +
            // '&lotno=' +
            // lotNo +
            // '&date=' +
            // date +
            // 'userid=' +
            +
            userId),
        body: json.encode({"finalResponse": jsonList}["finalResponse"]),
        headers: {'Content-type': 'application/json'});

    Iterable jsonObject = jsonDecode(response.body);
    List<ClsPhysicalInventoryByCarry> submit =
        jsonObject.map((e) => ClsPhysicalInventoryByCarry.submit(e)).toList();
    return submit;
    // if (response.statusCode == 200) {
    //   body = jsonDecode(response.body);
    //   return body;
    // } else {
    //   body = "Failed to scan data";
    //   return body;
    // }
  }
}
