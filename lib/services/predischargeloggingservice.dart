import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clspredischargelogging.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class PredischargeLoggingService {
  // Get Dropdown InsNo
  static Future getInstructionNo(String line, String pUser) async {
    String apiURL = URLAPI +
        '/predischargelogging_getinsno?line=' +
        line +
        '&userid=' +
        pUser;

    var response = await http.get(Uri.parse(apiURL));

    if (response.statusCode == 200) {
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsPredischargeLogging> listData = jsonObject
          .map((e) => ClsPredischargeLogging.ddlInstructionNo(e))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  static Future getListHeader(String insno) async {
    String apiURL = URLAPI + 'predischargelogging_list?instructionno=' + insno;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsPredischargeLogging> clsSupplyRequestData =
        jsonObject.map((e) => ClsPredischargeLogging.getList(e)).toList();
    return clsSupplyRequestData;
  }

  static Future submit(String insno, String barcodeno, String userID,
      String warehouselogin) async {
    String apiURL = URLAPI +
        '/predischargelogging_submit?instructionno=' +
        insno +
        '&barcodeno=' +
        barcodeno +
        '&userid=' +
        userID +
        '&warehouselogin=' +
        warehouselogin;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsPredischargeLogging> clsSupplyRequestData =
        jsonObject.map((e) => ClsPredischargeLogging.submitData(e)).toList();
    return clsSupplyRequestData;
  }
}
