import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsprocesstimelogging.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class ProcessTimeLoggingService {
  // Get Dropdown Location
  static Future getLocation(String pUser) async {
    String apiURL = URLAPI + '/processtimeloggetloc?userid=' + pUser;

    var response = await http.get(Uri.parse(apiURL));

    if (response.statusCode == 200) {
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsProcessTimeLogging> listData =
          jsonObject.map((e) => ClsProcessTimeLogging.ddlLocation(e)).toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  Future<List<ClsProcessTimeLogging>> autoFillData(
      String location, String barcode) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/processtimelogstatus?loc=' +
        location +
        '&barcode=' +
        barcode));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsProcessTimeLogging> listData = body
          .map((dynamic item) => ClsProcessTimeLogging.autoComplete(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List
  Future<List<ClsProcessTimeLogging>> getData(
      String location, String barcode) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/processtimeloglist?loc=' +
        location +
        '&barcode=' +
        barcode));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsProcessTimeLogging> listData = body
          .map((dynamic item) => ClsProcessTimeLogging.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List Detail
  Future<List<ClsProcessTimeLogging>> getDataDetail(String carryNo) async {
    try {
      final response = await http
          .get(Uri.parse(URLAPI + '/processtimelogdetail?carryno=' + carryNo));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ClsProcessTimeLogging> listData = body
            .map((dynamic item) => ClsProcessTimeLogging.getlistProd(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  //submit data
  static Future submit(String location, String carryNo, String barcode,
      String type, String userId) async {
    String apiURL = URLAPI +
        '/processtimelogsubmit?loc=' +
        location +
        '&carryno=' +
        carryNo +
        '&barcode=' +
        barcode +
        '&type=' +
        type +
        '&userid=' +
        userId;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsProcessTimeLogging> scnSubmit =
        jsonObject.map((e) => ClsProcessTimeLogging.submitData(e)).toList();
    return scnSubmit;
  }

  //unused
  static Future completeCarry(
      String carryNo, String userId, String warehouse) async {
    final response = await http.post(Uri.parse(URLAPI +
        '/processtimelogcomplete?carryno=' +
        carryNo +
        '&userid=' +
        userId));
    Iterable jsonObject = jsonDecode(response.body);
    List<ClsProcessTimeLogging> complete =
        jsonObject.map((e) => ClsProcessTimeLogging.complete(e)).toList();
    return complete;
  }

  Future<String> checkLocation(String location) async {
    final response = await http.get(
        Uri.parse(URLAPI + '/processtimelogcheckloc?location=' + location));
    var body;
    if (response.statusCode == 200) {
      body = jsonDecode(response.body);
      return body;
    } else {
      body = "Failed to check data";
      return body;
    }
  }

  //save data
  // Future<String> submit(
  //     String location, String barcode, String type, String userId) async {
  //   var body;
  //   final response = await http.post(Uri.parse(URLAPI +
  //       '/processtimelogsubmit?loc=' +
  //       location +
  //       '&barcode=' +
  //       barcode +
  //       '&type=' +
  //       type +
  //       '&userid=' +
  //       userId));

  //   if (response.statusCode == 200) {
  //     body = jsonDecode(response.body);
  //     return body;
  //   } else {
  //     body = "Failed to scan data";
  //     return body;
  //   }
  // }
}
