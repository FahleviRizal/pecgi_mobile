import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsprodresultpacking.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class ProdResultPackingService {
  Future<List<ClsProdResultPacking>> scanDataBarcode(
      String barcode, String warehouse) async {
    try {
      final response = await http.get(Uri.parse(URLAPI +
          '/prodresultpacking_scandata?barcode=' +
          barcode +
          '&warehouse=' +
          warehouse));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ClsProdResultPacking> list = body
            .map((dynamic item) => ClsProdResultPacking.scanBarcode(item))
            .toList();
        return list;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  static Future submitData(
      List<ClsProdResultPacking> model, String userId) async {
    try {
      //var body;
      List jsonList = [];
      model.map((item) => jsonList.add(item.toJson())).toList();

      final response = await http.post(
          Uri.parse(URLAPI + '/prodresultpacking_submit?userid=' + userId),
          body: json.encode({"finalResponse": jsonList}["finalResponse"]),
          headers: {'Content-type': 'application/json'});

      Iterable jsonObject = jsonDecode(response.body);
      List<ClsProdResultPacking> submit =
          jsonObject.map((e) => ClsProdResultPacking.submit(e)).toList();
      return submit;
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }
}
