import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsreceipt.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class ReceiptService {
  // Get Dropdown RequestNo
  static Future getRequestNo() async {
    try {
      String apiURL = URLAPI + '/receipt_getrequestno';

      var response = await http.get(Uri.parse(apiURL));

      if (response.statusCode == 200) {
        Iterable jsonObject = jsonDecode(response.body);
        List<ReceiptScheduleHeader> listData = jsonObject
            .map((e) => ReceiptScheduleHeader.ddlRequestNo(e))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  //List Receipt
  Future<List<ReceiptScheduleHeader>> getReceipt(
      String reqNo, String reqDate) async {
    try {
      final response = await http
          .get(Uri.parse(URLAPI + '/receiptschedule_getlist?reqNo=' + reqNo));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ReceiptScheduleHeader> listData = body
            .map((dynamic item) => ReceiptScheduleHeader.listH(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  //List Receipt Detail
  Future<List<ReceiptScheduleDetail>> getReceiptDetail(
      String reqNo, String matCode) async {
    try {
      final response = await http.get(Uri.parse(URLAPI +
          '/receiptschedule_detail?reqNo=' +
          reqNo +
          '&matCode=' +
          matCode));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ReceiptScheduleDetail> listData = body
            .map((dynamic item) => ReceiptScheduleDetail.listData(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  //List Receipt Scan
  Future<List<ReceiptScheduleDetail>> getReceiptScan(
      String reqNo, String barcodeno) async {
    try {
      final response = await http.get(Uri.parse(URLAPI +
          '/receiptschedule_scan?requestno=' +
          reqNo +
          '&barcodeno=' +
          barcodeno));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ReceiptScheduleDetail> listData = body
            .map((dynamic item) => ReceiptScheduleDetail.listData(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  Future<List<ReceiptScheduleDetail>> checkValidate(
      String reqNo, String barcodeno, String matcode) async {
    try {
      final response = await http.get(Uri.parse(URLAPI +
          '/receiptvalidation?requestno=' +
          reqNo +
          '&barcodeno=' +
          barcodeno +
          '&matcode=' +
          matcode));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ReceiptScheduleDetail> listData = body
            .map((dynamic item) => ReceiptScheduleDetail.validation(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  static Future submitScan(
      String reqNo,
      String barcodeno,
      String matCode,
      String lotNo,
      String qty,
      String expDate,
      String suppName,
      String userId,
      String warehouse) async {
    try {
      //var body;
      final response = await http.post(Uri.parse(URLAPI +
          '/receiptschedule_submit?reqNo=' +
          reqNo +
          '&barcodeNo=' +
          barcodeno +
          '&matCode=' +
          matCode +
          '&lotNo=' +
          lotNo +
          '&qty=' +
          qty +
          '&expDate=' +
          expDate +
          '&suppName=' +
          suppName +
          '&userid=' +
          userId +
          '&warehouse=' +
          warehouse));
      Iterable jsonObject = jsonDecode(response.body);
      List<ReceiptScheduleDetail> submit =
          jsonObject.map((e) => ReceiptScheduleDetail.submit(e)).toList();
      return submit;
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  // Future<String> submit(
  //     String reqNo,
  //     String barcodeno,
  //     String matCode,
  //     String lotNo,
  //     String qty,
  //     String expDate,
  //     String suppName,
  //     String userId) async {
  //   //String status;
  //   var body;
  //   final response = await http.post(Uri.parse(URLAPI +
  //       '/receiptschedulesubmit?reqNo=' +
  //       reqNo +
  //       '&barcodeNo=' +
  //       barcodeno +
  //       '&matCode=' +
  //       matCode +
  //       '&lotNo=' +
  //       lotNo +
  //       '&qty=' +
  //       qty +
  //       '&expDate=' +
  //       expDate +
  //       '&suppName=' +
  //       suppName +
  //       '&userid=' +
  //       userId));

  //   if (response.statusCode == 200) {
  //     body = jsonDecode(response.body);
  //     return body;
  //   } else {
  //     body = "Failed to send data";
  //     return body;
  //   }
  // }

  Future<List<ReceiptScheduleDetail>> getReceiptUnschedule(
      String barcodeno) async {
    try {
      final response = await http
          .get(Uri.parse(URLAPI + '/receiptunschedule?barcodeno=' + barcodeno));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ReceiptScheduleDetail> listData = body
            .map((dynamic item) => ReceiptScheduleDetail.listData(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  Future<List<ReceiptScheduleDetail>> getReceiptUnscheduleListData(
      String userid) async {
    try {
      final response = await http
          .get(Uri.parse(URLAPI + '/receiptunschedule_list?userid=' + userid));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ReceiptScheduleDetail> listData = body
            .map((dynamic item) => ReceiptScheduleDetail.listData(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  Future<List<ReceiptScheduleDetail>> checkValidateUnschedule(
      String barcodeno, String matcode, String callfrom) async {
    try {
      final response = await http.get(Uri.parse(URLAPI +
          '/receiptunschedule_validation?barcodeno=' +
          barcodeno +
          '&matcode=' +
          matcode +
          '&callfrom=' +
          callfrom));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ReceiptScheduleDetail> listData = body
            .map((dynamic item) => ReceiptScheduleDetail.validation(item))
            .toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  static Future submitUnschedule(
      String barcodeno,
      String matCode,
      String lotNo,
      String qty,
      String expDate,
      String suppName,
      String userId,
      String warehouse,
      String callfrom) async {
    try {
      //var body;
      final response = await http.post(Uri.parse(URLAPI +
          '/receiptunschedule_submit?barcodeNo=' +
          barcodeno +
          '&matCode=' +
          matCode +
          '&lotNo=' +
          lotNo +
          '&qty=' +
          qty +
          '&expDate=' +
          expDate +
          '&suppName=' +
          suppName +
          '&userid=' +
          userId +
          '&warehouse=' +
          warehouse +
          '&callfrom=' +
          callfrom));
      Iterable jsonObject = jsonDecode(response.body);
      List<ReceiptScheduleDetail> submit =
          jsonObject.map((e) => ReceiptScheduleDetail.submit(e)).toList();
      return submit;
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }
  // static List<ReceiptScheduleHeader> parseResponse(String responseBody) {
  //   final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  //   parsed
  //       .map<ReceiptScheduleHeader>(
  //           (json) => ReceiptScheduleHeader.fromJson(json))
  //       .toList();
  //   return parsed;
  // }
}
