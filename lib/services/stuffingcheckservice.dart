import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsstuffingcheck.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class StuffingCheckService {
  // Get Dropdown InvoiceNO
  static Future getInvoiceNo() async {
    String apiURL = URLAPI + '/stuffingcheck_getinvoice';

    var response = await http.get(Uri.parse(apiURL));

    if (response.statusCode == 200) {
      Iterable jsonObject = jsonDecode(response.body);
      List<ClsStuffingCheck> listData =
          jsonObject.map((e) => ClsStuffingCheck.ddlInvoiceNo(e)).toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  Future<String> checkData(String sjno) async {
    final response = await http
        .get(Uri.parse(URLAPI + '/stuffingcheck_existsdata?sjno=' + sjno));

    if (response.statusCode == 200) {
      String body = jsonDecode(response.body);
      return body;
    } else {
      throw "Failed to load data";
    }
  }

  //List Stuffing Check Header
  Future<List<ClsStuffingCheck>> getDataHeader(String invoiceno) async {
    final response = await http.get(
        Uri.parse(URLAPI + '/stuffingcheck_getlist?invoiceno=' + invoiceno));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsStuffingCheck> listData = body
          .map((dynamic item) => ClsStuffingCheck.listHeader(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List Stuffing Check Detail
  Future<List<ClsStuffingCheck>> getDataDetail(
      String invoiceno, String item) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/stuffingcheck_getdetail?invoiceno=' +
        invoiceno +
        '&item=' +
        item));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsStuffingCheck> listDataD = body
          .map((dynamic item) => ClsStuffingCheck.listDetail(item))
          .toList();
      return listDataD;
    } else {
      throw "Failed to load data";
    }
  }

  // Future<List<ClsStuffingCheck>> getDataScan(
  //     String suratjalanno, String barcodeno) async {
  //   final response = await http.get(Uri.parse(URLAPI +
  //       '/stuffingcheck_scan?sjno=' +
  //       suratjalanno +
  //       '&barcodeno=' +
  //       barcodeno));

  //   if (response.statusCode == 200) {
  //     List<dynamic> body = jsonDecode(response.body);
  //     List<ClsStuffingCheck> listData = body
  //         .map((dynamic item) => ClsStuffingCheck.getDataScan(item))
  //         .toList();
  //     return listData;
  //   } else {
  //     throw "Failed to load data";
  //   }
  // }

  static Future submitScan(
      String invoiceno, String barcodeno, String userid) async {
    //var body;
    final response = await http.post(Uri.parse(URLAPI +
        '/stuffingcheck_submit?invoiceno=' +
        invoiceno +
        '&barcodeno=' +
        barcodeno +
        '&userid=' +
        userid));
    Iterable jsonObject = jsonDecode(response.body);
    List<ClsStuffingCheck> submit =
        jsonObject.map((e) => ClsStuffingCheck.submit(e)).toList();
    return submit;
  }
}
