import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clssublotgroupingcarry.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class SublotGroupingCarryService {
  //get carry name
  Future<List<ClsSublotGroupingCarry>> getCarryName(String carryNo) async {
    final response = await http
        .get(Uri.parse(URLAPI + '/sublotcarry_getname?carryno=' + carryNo));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsSublotGroupingCarry> listData = body
          .map((dynamic item) => ClsSublotGroupingCarry.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List Header
  Future<List<ClsSublotGroupingCarry>> getData(
      String carryNo, String warehouse) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/sublotcarrylist?carryno=' +
        carryNo +
        '&warehouse=' +
        warehouse));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsSublotGroupingCarry> listData = body
          .map((dynamic item) => ClsSublotGroupingCarry.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  // Future<String> checkData(String carryNo, String sublotNo, String call) async {
  //   final response = await http.get(Uri.parse(URLAPI +
  //       '/sublotcarrycheck?carryno=' +
  //       carryNo +
  //       '&sublotno=' +
  //       sublotNo +
  //       '&call=' +
  //       call));

  //   if (response.statusCode == 200) {
  //     String body = jsonDecode(response.body);
  //     return body;
  //   } else {
  //     throw "Failed to load data";
  //   }
  // }

  Future<List<ClsSublotCarryDetail>> checkData(
      String carryNo, String sublotNo) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/sublotcarrycheck?carryno=' +
        carryNo +
        '&sublotno=' +
        sublotNo));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsSublotCarryDetail> listData = body
          .map((dynamic item) => ClsSublotCarryDetail.checkData(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  //List Detail
  Future<List<ClsSublotCarryDetail>> getDataDetail(
      String carryNo, String sublotNo) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/sublotcarrydetail?carryno=' +
        carryNo +
        '&sublotno=' +
        sublotNo));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsSublotCarryDetail> listData = body
          .map((dynamic item) => ClsSublotCarryDetail.fromJson(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  static Future submit(
      String carryNo, String sublotNo, String userId, String warehouse) async {
    final response = await http.post(Uri.parse(URLAPI +
        '/sublotcarrysubmit?carryno=' +
        carryNo +
        '&sublotno=' +
        sublotNo +
        // '&type=' +
        // type +
        // '&lotno=' +
        // lotNo +
        // '&qty=' +
        // qty +
        '&userid=' +
        userId +
        '&warehouse=' +
        warehouse));
    Iterable jsonObject = jsonDecode(response.body);
    List<ClsSublotCarryDetail> submit =
        jsonObject.map((e) => ClsSublotCarryDetail.submit(e)).toList();
    return submit;
    // if (response.statusCode == 200) {
    //   body = jsonDecode(response.body);
    //   return body;
    // } else {
    //   body = "Failed to submit data";
    //   return body;
    // }
  }

  //if dont want looping use this
  static Future complete(
      String carryNo, String userId, String warehouse) async {
    final response = await http.post(Uri.parse(URLAPI +
        '/sublotcarry_complete?carryno=' +
        carryNo +
        '&userid=' +
        userId +
        '&warehouse=' +
        warehouse));
    Iterable jsonObject = jsonDecode(response.body);
    List<ClsSublotGroupingCarry> complete =
        jsonObject.map((e) => ClsSublotGroupingCarry.complete(e)).toList();
    return complete;
  }

  // static Future complete(List<ClsSublotGroupingCarry> model, String carryno,
  //     String warehouse, String userId) async {
  //   List jsonList = [];
  //   model.map((item) => jsonList.add(item.toJson())).toList();

  //   final response = await http.post(
  //       Uri.parse(URLAPI +
  //           '/sublotcarry_complete?carryno=' +
  //           carryno +
  //           '&warehouse=' +
  //           warehouse +
  //           '&userid=' +
  //           userId),
  //       body: json.encode({"finalResponse": jsonList}["finalResponse"]),
  //       headers: {'Content-type': 'application/json'});
  //   Iterable jsonObject = jsonDecode(response.body);
  //   List<ClsSublotGroupingCarry> submit =
  //       jsonObject.map((e) => ClsSublotGroupingCarry.complete(e)).toList();
  //   return submit;
  // }
}
