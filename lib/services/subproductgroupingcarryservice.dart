import 'dart:convert';

import 'package:panasonic/models/clsApiResponse.dart';
import 'package:panasonic/models/clssubproductgroupingcarry.dart';
import 'package:http/http.dart' as http;
import 'package:panasonic/utilities/constantsApi.dart';

class SubProductGroupingCarryService {
  Future<ClsSubProductGroupingCarry> getListDataBarcode(
      String barcodeNo) async {
    try {
      final response = await http.get(Uri.parse(URLAPI +
          "/subproductgroupingcarry/getdatabarcode?BarcodeNo=" +
          barcodeNo));
      if (response.statusCode.toString() == "200") {
        dynamic body = jsonDecode(response.body);
        ClsSubProductGroupingCarry data =
            ClsSubProductGroupingCarry.fromJson(body);
        return data;
      } else {
        dynamic body = jsonDecode(response.body);
        throw body["ExceptionMessage"].toString();
      }
    } catch (e) {
      throw e.toString();
    }
  }

  Future<ClsApiResponse> complete(
      List<ClsSubProductGroupingCarry> model) async {
    List data = [];
    for (var item in model) {
      data.add(item.toJson());
    }

    final response = await http.post(
        Uri.parse(URLAPI + '/subproductgroupingcarry/complete'),
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json'
        },
        body: json.encode(data));
    if (response.statusCode.toString() == '200') {
      dynamic body = jsonDecode(response.body);
      ClsApiResponse res = ClsApiResponse.fromJson(body);
      return res;
    } else {
      throw 'Data save failed';
    }
  }
}
