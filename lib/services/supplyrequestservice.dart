import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsSupplyRequest.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class SupplyRequestService {
  static Future getLine(String userid) async {
    String apiURL = URLAPI + '/cbouserline?userid=' + userid;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSupplyRequest> clsMaterialRequestData =
        jsonObject.map((e) => ClsSupplyRequest.ddlLine(e)).toList();
    return clsMaterialRequestData;
  }

  static Future getSupplyNo(String line) async {
    String apiURL = URLAPI + '/supplyrequest_getsupplyno?line=' + line;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSupplyRequest> clsSupplyRequestData =
        jsonObject.map((e) => ClsSupplyRequest.ddlSupplyNo(e)).toList();
    return clsSupplyRequestData;
  }

  static Future getDataListHeader(String supplyNo, String insNo) async {
    String apiURL = URLAPI +
        'supplyrequest_getlist?supplyno=' +
        supplyNo +
        '&insno=' +
        insNo;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSupplyRequest> clsSupplyRequestData =
        jsonObject.map((e) => ClsSupplyRequest.getListDataH(e)).toList();
    return clsSupplyRequestData;
  }

  static Future getDataScan(
      String supplyNo, String labelBarcode, String warehouse) async {
    String apiURL = URLAPI +
        '/supplyrequest_getscan?supplyno=' +
        supplyNo +
        '&labelbarcode=' +
        labelBarcode +
        '&warehouse=' +
        warehouse;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSupplyRequest> clsSupplyRequestData =
        jsonObject.map((e) => ClsSupplyRequest.getScan(e)).toList();
    return clsSupplyRequestData;
  }

  static Future getDataListDetail(String supplyNo, String materialCode) async {
    String apiURL = URLAPI +
        '/supplyrequest_getdetail?supplyno=' +
        supplyNo +
        '&materialcode=' +
        materialCode;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSupplyRequest> clsSupplyRequestData =
        jsonObject.map((e) => ClsSupplyRequest.getDetail(e)).toList();
    return clsSupplyRequestData;
  }

  static Future getlistStock(
      String supplyNo, String materialCode, String warehouse) async {
    String apiURL = URLAPI +
        '/supplyrequest_liststock?supplyno=' +
        supplyNo +
        '&materialcode=' +
        materialCode +
        '&warehouselogin=' +
        warehouse;

    var apiResult = await http.get(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSupplyRequest> clsSupplyRequestData =
        jsonObject.map((e) => ClsSupplyRequest.getlistStock(e)).toList();
    return clsSupplyRequestData;
  }

  static Future submit(
      String supplyNo,
      String labelBarcode,
      String itemCode,
      String lotNo,
      String planQty,
      String userID,
      String warehouselogin) async {
    String apiURL = URLAPI +
        '/supplyrequest_submit?supplyno=' +
        supplyNo +
        '&labelbarcode=' +
        labelBarcode +
        '&itemcode=' +
        itemCode +
        '&lotno=' +
        lotNo +
        '&planqty=' +
        planQty +
        '&userid=' +
        userID +
        '&warehouse=' +
        warehouselogin;

    var apiResult = await http.post(Uri.parse(apiURL));
    Iterable jsonObject = jsonDecode(apiResult.body);
    List<ClsSupplyRequest> clsSupplyRequestData =
        jsonObject.map((e) => ClsSupplyRequest.submitData(e)).toList();
    return clsSupplyRequestData;
  }
}
