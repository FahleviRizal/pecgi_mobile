import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:panasonic/models/clsmenu.dart';
import 'dart:convert';
import 'package:panasonic/models/user.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class APIService {
  Future<UserResponseModel> login(User requestModel) async {
    try {
      String url = URLAPI +
          '/auth?UserID=' +
          requestModel.userid +
          '&Password=' +
          requestModel.password;
      final response =
          await http.get(Uri.parse(url)); //, body: requestModel.toJson());
      if (response.statusCode == 200 || response.statusCode == 400) {
        return UserResponseModel.fromJson(
          json.decode(response.body),
        );
      } else {
        throw Exception('Failed to load data!');
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }

  Future<List<ClsMenu>> getMenu(String userid) async {
    try {
      final response =
          await http.get(Uri.parse(URLAPI + '/usermenu?userid=' + userid));

      if (response.statusCode == 200) {
        List<dynamic> body = jsonDecode(response.body);
        List<ClsMenu> listData =
            body.map((dynamic item) => ClsMenu.getMenu(item)).toList();
        return listData;
      } else {
        throw "Failed to load data";
      }
    } on SocketException catch (_) {
      //throw ("No Connection network")
      throw ('No connection! please check your network');
    }
  }
}
