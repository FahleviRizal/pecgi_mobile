import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:panasonic/models/clsvisualcheckerresult.dart';
import 'package:panasonic/utilities/constantsApi.dart';

class VisualCheckerResultService {
  //get combo/dropdownlist
  Future<List<ClsVisualCheckerResult>> getCarryNo() async {
    final response =
        await http.get(Uri.parse(URLAPI + '/visualchecker_getcarryno?'));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsVisualCheckerResult> listData = body
          .map((dynamic item) => ClsVisualCheckerResult.ddlCarryNo(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  Future<List<ClsVisualCheckerResult>> getData(String carryNo) async {
    final response = await http
        .get(Uri.parse(URLAPI + '/visualchecker_getdata?carryno=' + carryNo));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsVisualCheckerResult> listData = body
          .map((dynamic item) => ClsVisualCheckerResult.getData(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  Future<List<ClsVisualCheckerResult>> getList(
      String carryNo, String sublot) async {
    final response = await http.get(Uri.parse(URLAPI +
        '/visualchecker_getlist?carryno=' +
        carryNo +
        '&sublot=' +
        sublot));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<ClsVisualCheckerResult> listData = body
          .map((dynamic item) => ClsVisualCheckerResult.listHeader(item))
          .toList();
      return listData;
    } else {
      throw "Failed to load data";
    }
  }

  Future<String> submit(String carryNo, String sublot, String userId) async {
    var body;
    final response = await http.post(
        Uri.parse(URLAPI +
            '/visualchecker_submit?carryno=' +
            carryNo +
            '&sublot=' +
            sublot +
            '&userid=' +
            userId),
        headers: {'Content-type': 'application/json'});

    if (response.statusCode == 200) {
      body = jsonDecode(response.body);
      return body;
    } else {
      body = "Failed to submit data";
      return body;
    }
  }
}
