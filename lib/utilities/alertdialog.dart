import 'package:flutter/material.dart';

class CustomAlertDialog extends StatelessWidget {
  final String type;
  final Color bgColor;
  final String title;
  final String message;
  final String okBtnText;
  final String noBtnText;
  final Function onOkPressed;
  final Function onNoPressed;
  final double circularBorderRadius;

  CustomAlertDialog({
    this.type, //1=confirm message , 2=error message , 3=success message ,4=info
    this.title,
    this.message,
    this.circularBorderRadius = 15.0,
    this.bgColor = Colors.white,
    this.okBtnText,
    this.noBtnText,
    this.onOkPressed,
    this.onNoPressed,
  })  : assert(bgColor != null),
        assert(circularBorderRadius != null);

  @override
  Widget build(BuildContext context) {
    //confirm message
    if (type == "1") {
      return AlertDialog(
        title: Container(
            alignment: Alignment.center,
            child: Icon(
              Icons.exit_to_app,
              size: 55,
              color: Colors.blue,
            )),
        content: message != null
            ? Text(
                message,
                textAlign: TextAlign.center,
              )
            : null,
        backgroundColor: bgColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(circularBorderRadius)),
        actions: <Widget>[
          noBtnText != null
              ? ElevatedButton(
                  child: Text(noBtnText),
                  onPressed: () {
                    Navigator.of(context).pop();
                    if (onNoPressed != null) {
                      onNoPressed();
                    }
                  },
                )
              : null,
          okBtnText != null
              ? ElevatedButton(
                  child: Text(okBtnText),
                  onPressed: () {
                    if (onOkPressed != null) {
                      onOkPressed();
                    }
                  },
                )
              : null,
        ],
      );
      //error message
    } else if (type == '2') {
      return AlertDialog(
        title: Container(
            alignment: Alignment.center,
            child: Icon(
              Icons.cancel_outlined,
              size: 55,
              color: Colors.red,
            )),
        content: message != null
            ? Text(
                message,
                textAlign: TextAlign.center,
              )
            : null,
        backgroundColor: bgColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(circularBorderRadius)),
        actions: <Widget>[
          // Divider(
          //   color: Colors.black,
          //   height: 5,
          // ),
          okBtnText != null
              ? Container(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                )
              : null,
        ],
      );
    } else if (type == '3') {
      return AlertDialog(
        title: Container(
            alignment: Alignment.center,
            child: Icon(
              Icons.check_circle_outline_rounded,
              size: 55,
              color: Colors.green,
            )),
        content: message != null
            ? Text(
                message,
                textAlign: TextAlign.center,
              )
            : null,
        backgroundColor: bgColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(circularBorderRadius)),
        actions: <Widget>[
          // Divider(
          //   color: Colors.black,
          //   height: 5,
          // ),
          okBtnText != null
              ? Container(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                )
              : null,
        ],
      );
      //info message
    } else if (type == '4') {
      return AlertDialog(
        title: Container(
            alignment: Alignment.center,
            child: Icon(
              Icons.info_outline_rounded,
              size: 55,
              color: Colors.lightBlue,
            )),
        content: message != null
            ? Text(
                message,
                textAlign: TextAlign.center,
              )
            : null,
        backgroundColor: bgColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(circularBorderRadius)),
        actions: <Widget>[
          // Divider(
          //   color: Colors.black,
          //   height: 5,
          // ),
          okBtnText != null
              ? Container(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                )
              : null,
        ],
      );
    } else {
      return AlertDialog();
    }
  }
}
