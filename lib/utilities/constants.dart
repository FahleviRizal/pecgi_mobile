import 'package:flutter/material.dart';

final kHintTextStyle = TextStyle(
  color: Colors.indigo[800],
  fontFamily: 'OpenSans',
);

final kLabelStyle = TextStyle(
  color: Colors.indigo[800],
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);

final kBoxDecorationStyleText = BoxDecoration(
  color: Colors.indigo[800],
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);

Color kChangeColor = Colors.lightBlue;
final kAppBarColor = Container(
  decoration: BoxDecoration(
      gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: <Color>[
        Colors.indigo[800],
        Colors.indigo[900].withOpacity(0.2),
        Colors.indigo[800]
      ])),
);

Color disabledColor = Colors.grey[200]; //Color(0xFFbdc6cf);
Color datatableColor = Colors.grey[300];

Color disabledButtonColor = Colors.grey[400]; //Color(0xFFbdc6cf);
Color enabledButtonColor = Colors.blue[800];
Color disabledColorButtonText = Colors.grey[700];
Color enabledColorButtonText = Colors.white;
