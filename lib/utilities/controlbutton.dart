import 'package:flutter/material.dart';
import 'package:panasonic/screens/agingcoolingstorage/agingcoolingstorage.dart';
import 'package:panasonic/screens/assigntocarrybylot/assigntocarrybylot.dart';
import 'package:panasonic/screens/assigntostorage/assigntostorage.dart';
import 'package:panasonic/screens/barcodetracebility/barcodetracebility.dart';
import 'package:panasonic/screens/batterysupplypacking/batterysupplypacking.dart';
import 'package:panasonic/screens/carryinformationdetail/carryinformationdetail.dart';
import 'package:panasonic/screens/carrymovinglocation/carrymovinglocation.dart';
import 'package:panasonic/screens/carryresetstatus/carryresetstatus.dart';
import 'package:panasonic/screens/consumpunschedule/consumpunschedule.dart';
import 'package:panasonic/screens/deliveryscan/deliveryscan.dart';
import 'package:panasonic/screens/manpowervisual/manpowervisual.dart';
import 'package:panasonic/screens/manpowervisualupdate/manpowervisualupdate.dart';
import 'package:panasonic/screens/materialconsumption/materialconsumption.dart';
import 'package:panasonic/screens/materialpreparation/materialpreparation.dart';
import 'package:panasonic/screens/mixingsublotgroupcarry/mixingsublotcarry.dart';
//import 'package:panasonic/screens/materialrequest/materialrequest.dart';
import 'package:panasonic/screens/physicalinventory/physicalinventorybybarcode.dart';
import 'package:panasonic/screens/physicalinventory/physicalinventorybycarry.dart';
import 'package:panasonic/screens/predischargelogging/predischargelogging.dart';
import 'package:panasonic/screens/prodresultpacking/prodresultpacking.dart';
import 'package:panasonic/screens/receiptschedule/receiptschedule.dart';
import 'package:panasonic/screens/receiptunschedule/receiptunschedulelist.dart';
import 'package:panasonic/screens/manpower/manpower.dart';
import 'package:panasonic/screens/stuffingcheck/stuffingcheck.dart';
import 'package:panasonic/screens/sublotgroupingcarry/sublotcarry.dart';
import 'package:panasonic/screens/processtimelog/processtimelogging.dart';
import 'package:panasonic/screens/subproductgroupingcarry/subproductgroupingcarry.dart';
import 'package:panasonic/screens/supplyrequest/supplyrequest.dart';
import 'package:panasonic/screens/towerlampinformation/towerlampinformation.dart';
import 'package:panasonic/screens/visualcheckerresult/visualcheckerresult.dart';

class ControlButton extends StatefulWidget {
  const ControlButton(
      {Key key,
      @required this.size,
      this.icon,
      this.title,
      this.idxMenu,
      this.gesture,
      this.userid,
      this.warehouse,
      this.version})
      : super(key: key);

  final Size size;
  final Image icon;
  final String title;
  final int idxMenu;
  final String userid;
  final String warehouse;
  final String version;
  final GestureDetector gesture;

  @override
  _ControlButtonState createState() => _ControlButtonState();
}

class _ControlButtonState extends State<ControlButton> {
  bool isSelected = false;
  int index;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              if (widget.idxMenu == 0) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ReceiptSchedule(
                        userid: widget.userid, warehouse: widget.warehouse)));
              } else if (widget.idxMenu == 1) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ReceiptUnscheduleList(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 2) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => AssignToStorage(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 3) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => SupplyRequest(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 4) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => MaterialConsumption(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 5) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => MaterialPreparation(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 6) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ManPowerPreparation(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 7) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProcessTimeLogging(
                          userid: widget.userid,
                        )));
                // } else if (widget.idxMenu == 8) {
                //   Navigator.of(context).push(MaterialPageRoute(
                //       builder: (context) => MaterialRequest(
                //             userid: widget.userid,
                //           )));
              } else if (widget.idxMenu == 9) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => SublotGrouppingCarry(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 10) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => AgingCoolingStorage(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 11) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => DeliveryScan(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 12) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => StuffingCheck(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 13) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PhysicalInventoryByBarcode(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 14) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PhysicalInventoryByCarry(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 15) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => CarryMovingLocation(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 16) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => CarryInformationDetail(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 17) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => CarryResetStatus(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 18) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => TowerLampInformation(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 19) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => VisualCheckerResult(
                          userid: widget.userid,
                        )));
              } else if (widget.idxMenu == 20) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ManPowerPreparationVisual(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 21) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PredischargeLogging(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 22) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => AssignToCarryByLot(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 23) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ConsumpUnschedule(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 24) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => SubProductGroupingCarry(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 25) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => MixingSublotGrouppingCarry(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 26) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProdResultPacking(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                          version: widget.version,
                        )));
              } else if (widget.idxMenu == 27) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => BarcodeTracebility(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 28) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => BatterySupplyPacking(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              } else if (widget.idxMenu == 29) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ManPowerPreparationVisualUpdate(
                          userid: widget.userid,
                          warehouse: widget.warehouse,
                        )));
              }
              //isSelected = !isSelected;
            });
          },
          child: Container(
              height: widget.size.height * 0.105,
              width: widget.size.width * 0.21,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.shade200,
                    blurRadius: 30,
                    offset: Offset(5, 5),
                  ),
                ],
              ),
              child: widget.icon
              //widget.icon,
              //color: Colors.lightBlue,
              // isSelected ? Colors.white : kDarkGreyColor.withOpacity(0.6),
              //size: 45,
              //),
              ),
        ),
        SizedBox(height: widget.size.height * 0.005),
        Text(
          widget.title,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Color(0xFF727C9B),
              fontWeight: FontWeight.w600,
              fontSize: 12),
        ),
      ],
    );
  }
}
