import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Message extends Flushbar {
  // No need to store the values
  Message({
    //@required String title,
    @required String message,
    @required String type,
    @required int top,
    @required String position,
    Duration duration = const Duration(seconds: 3),
    bool shouldIconPulse = false,
    // Other properties here and their default values. If you specify no default value, they will be null.
  }) : super(
          icon: (type == "1")
              ? Icon(Icons.check_circle_outline_rounded,
                  size: 28, color: Colors.white)
              : Icon(Icons.error, size: 28, color: Colors.white),
          //titleText: Text(title),
          messageText: Text(message,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),

          backgroundColor: (type == "1")
              ? Colors.green.withOpacity(0.8)
              : Colors.blue.withOpacity(0.8),
          duration: duration,
          shouldIconPulse: shouldIconPulse,
          flushbarPosition: (position == "0")
              ? FlushbarPosition.TOP
              : FlushbarPosition.BOTTOM,
          padding: EdgeInsets.all(15),
          borderRadius: BorderRadius.circular(16),
          barBlur: 20,
          margin: EdgeInsets.fromLTRB(5, kToolbarHeight + top, 5, 0),
          dismissDirection: FlushbarDismissDirection.HORIZONTAL,
          // Etc.
        );
  // So what we are doing here is creating a Flushbar with some default properties and naming it MyInfoBar.
  // Now whenever you instantiate MyInfoBar, you get the default notification and you can of course adjust the properties.

  // Static methods are cleaner in this context as in your example,
  // you are instantiating this class but only using it to instantiate
  // a new Flushbar thus eliminating the purpose of the first object you created.
  static Future box(
      {
      //@required String title,
      @required String type,
      @required String message,
      @required int top,
      @required String position,
      @required BuildContext context}) {
    return Message(
      //title: title,
      type: type,
      message: message,
      top: top,
      position: position,
    ).show(
        context); // Other properties get the default values as you have not specified any here.
  }

  // Similarly you can define other methods.
}
